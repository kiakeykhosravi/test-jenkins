// eslint-disable-next-line no-undef
module.exports = {
    compress: false,
    async headers() {
        return [
            {
                source: '/(.*).(jpeg|jpg|png|svg|ico)',
                headers: [
                    {
                        key: 'Cache-Control',
                        value: 'public, max-age=86400, immutable',
                    },
                ],
            },
        ];
    },

    async rewrites() {
        return [
            {
                source: '/instructors/:id/:slug*',
                destination: '/instructors/:id',
            },
            {
                source: '/organizations/:slug/blog',
                destination: '/organizations/:slug/blog/page/1',
            },
            {
                source: '/organizations/:slug/blog/page',
                destination: '/organizations/:slug/blog/page/1',
            },
            {
                source: '/organizations/:slug/blog/:id(\\d+)/:title*',
                destination: '/organizations/:slug/blog/:id',
            },
            {
                source: '/webinars/:id/:slug*',
                destination: '/webinars/:id',
            },
            {
                source: '/blog',
                destination: '/blog/page/1',
            },
            {
                source: '/blog/page',
                destination: '/blog/page/1',
            },
            {
                source: '/blog/page/:page',
                destination: '/blog/page/:page',
            },
            {
                source: '/blog/:id(\\d+)/:slug*',
                destination: '/blog/:id',
            },
            {
                source: '/blog/tag/:tag',
                destination: '/blog/tag/:tag/page/1',
            },
            {
                source: '/blog/category/:cat',
                destination: '/blog/category/:cat/page/1',
            },
            {
                source: '/profile',
                destination: '/profile/webinars',
            },
            {
                source: '/:slug/admin/:slug2*',
                destination: '/:slug/admin',
            },
        ];
    },

    async redirects() {
        return [
            {
                permanent: true,
                source: '/instructors/[id]',
                destination: '/instructors/id',
            },
            {
                permanent: true,
                source: '/blog/[id]',
                destination: '/blog/id',
            },
            {
                permanent: true,
                source: '/:slug(.{1,3}|(?!blog).{4}|.{5,})/search',
                destination: '/:slug',
            },
            {
                permanent: false,
                source: '/profile/webinars',
                destination: '/profile',
            },
            {
                permanent: true,
                source: '/:slug/webinars/:slug2*',
                destination: '/webinars/:slug2*',
            },
            {
                permanent: true,
                source: '/organizations/asanseminar/blog/:any*',
                destination: '/blog/:any*',
            },
            // --------- ORGANIZATIONS REDIRECTS -------------
            {
                permanent: true,
                source: '/asep/admin/:slug*',
                destination: '/asanseminar/admin/:slug*',
            },
            {
                permanent: true,
                source: '/sina/admin/:slug*',
                destination: '/sina-noavar/admin/:slug*',
            },
            {
                permanent: true,
                source: '/irandds/admin/:slug*',
                destination: '/ida-dent/admin/:slug*',
            },
            {
                permanent: true,
                source: '/MFUrmia/admin/:slug*',
                destination: '/parnian-afkar/admin/:slug*',
            },
            {
                permanent: true,
                source: '/IBSA/admin/:slug*',
                destination: '/paizan-tejarat-mehr/admin/:slug*',
            },
            {
                permanent: true,
                source: '/Zonkan/admin/:slug*',
                destination: '/zounkan/admin/:slug*',
            },
            {
                permanent: true,
                source: '/Markazidoe/admin/:slug*',
                destination: '/mohitzist-markazi/admin/:slug*',
            },
            {
                permanent: true,
                source: '/MJ/admin/:slug*',
                destination: '/mehr-aien/admin/:slug*',
            },
            {
                permanent: true,
                source: '/DOEofKerman/admin/:slug*',
                destination: '/mohitzist-kerman/admin/:slug*',
            },
            {
                permanent: true,
                source: '/DOEofQaz/admin/:slug*',
                destination: '/mohitzist-ghazvin/admin/:slug*',
            },
            {
                permanent: true,
                source: '/irimc/admin/:slug*',
                destination: '/irimc-ag/admin/:slug*',
            },
            {
                permanent: true,
                source: '/DOEofKhozestan/admin/:slug*',
                destination: '/mohitzist-khouzestan/admin/:slug*',
            },
            {
                permanent: true,
                source: '/AG2/admin/:slug*',
                destination: '/avaye-golchin2/admin/:slug*',
            },
            {
                permanent: true,
                source: '/ae/admin/:slug*',
                destination: '/avaye-elm/admin/:slug*',
            },
            {
                permanent: true,
                source: '/pe/admin/:slug*',
                destination: '/poyaye-elm/admin/:slug*',
            },
            {
                permanent: true,
                source: '/ag1/admin/:slug*',
                destination: '/avaye-golchin /admin/:slug*',
            },
            {
                permanent: true,
                source: '/ge/admin/:slug*',
                destination: '/golchin-elm/admin/:slug*',
            },
            {
                permanent: true,
                source: '/far/admin/:slug*',
                destination: '/farhang/admin/:slug*',
            },
            {
                permanent: true,
                source: '/wastp/admin/:slug*',
                destination: '/techpark-ag/admin/:slug*',
            },
            {
                permanent: true,
                source: '/eh/admin/:slug*',
                destination: '/elm-afarinan/admin/:slug*',
            },
            {
                permanent: true,
                source: '/wap/admin/:slug*',
                destination: '/ostandari-ag/admin/:slug*',
            },
            {
                permanent: true,
                source: '/hooshe_bartar/admin/:slug*',
                destination: '/hooshe-bartar/admin/:slug*',
            },
            {
                permanent: true,
                source: '/DOEofَArdabil/admin/:slug*',
                destination: '/mohitzist-ardabil/admin/:slug*',
            },
            {
                permanent: true,
                source: '/oshnavie/admin/:slug*',
                destination: '/farmandari-oshnavieh/admin/:slug*',
            },
            {
                permanent: true,
                source: '/makoo/admin/:slug*',
                destination: '/farmandari-makoo/admin/:slug*',
            },
            {
                permanent: true,
                source: '/azgmpo/admin/:slug*',
                destination: '/mporg-ag/admin/:slug*',
            },
            {
                permanent: true,
                source: '/AshayerAG/admin/:slug*',
                destination: '/ashayer-ag/admin/:slug*',
            },
            {
                permanent: true,
                source: '/Sh_salmas/admin/:slug*',
                destination: '/shahrdari-salmas/admin/:slug*',
            },
            {
                permanent: true,
                source: '/SR/admin/:slug*',
                destination: '/rah-abrisham/admin/:slug*',
            },
            {
                permanent: true,
                source: '/A.K.T/admin/:slug*',
                destination: '/fanbazar-ag/admin/:slug*',
            },
            {
                permanent: true,
                source: '/DOEofyazd/admin/:slug*',
                destination: '/mohitzist-yazd/admin/:slug*',
            },
            {
                permanent: true,
                source: '/Bazneshaste/admin/:slug*',
                destination: '/cspf-bushehr/admin/:slug*',
            },
            {
                permanent: true,
                source: '/dms/admin/:slug*',
                destination: '/maaref-sadra/admin/:slug*',
            },
            {
                permanent: true,
                source: '/RCCR/admin/:slug*',
                destination: '/rccr-babolsar/admin/:slug*',
            },
            {
                permanent: true,
                source: '/Fh/admin/:slug*',
                destination: '/farhangestan-hendijan/admin/:slug*',
            },
            {
                permanent: true,
                source: '/IM_qom/admin/:slug*',
                destination: '/sim-ghom/admin/:slug*',
            },
            {
                permanent: true,
                source: '/Fava/admin/:slug*',
                destination: '/fava-shahrdari-urmia/admin/:slug*',
            },
            {
                permanent: true,
                source: '/STR/admin/:slug*',
                destination: '/taavon-rostayi-ghom/admin/:slug*',
            },
            {
                permanent: true,
                source: '/Masirfartak/admin/:slug*',
                destination: '/fartak/admin/:slug*',
            },
            {
                permanent: true,
                source: '/PQD/admin/:slug*',
                destination: '/pishgam-tarah/admin/:slug*',
            },
            {
                permanent: true,
                source: '/TGAD/admin/:slug*',
                destination: '/ayda-danesh/admin/:slug*',
            },
            {
                permanent: true,
                source: '/Doe/admin/:slug*',
                destination: '/mohitzist-mazandaran/admin/:slug*',
            },
            {
                permanent: true,
                source: '/AG.DOE/admin/:slug*',
                destination: '/mohitzist-ag/admin/:slug*',
            },
            {
                permanent: true,
                source: '/Armknkr/admin/:slug*',
                destination: '/aramesh-konkour/admin/:slug*',
            },
            {
                permanent: true,
                source: '/Pnukarafrini/admin/:slug*',
                destination: '/pnu-karafarini/admin/:slug*',
            },
            {
                permanent: true,
                source: '/ASA/admin/:slug*',
                destination: '/azar-asa/admin/:slug*',
            },
            {
                permanent: true,
                source: '/wikimoshaver/admin/:slug*',
                destination: '/wiki-moshaver/admin/:slug*',
            },
            {
                permanent: true,
                source: '/WAPMCO/admin/:slug*',
                destination: '/hmyr-ag/admin/:slug*',
            },
            {
                permanent: true,
                source: '/prclass/admin/:slug*',
                destination: '/class-khososi/admin/:slug*',
            },
            {
                permanent: true,
                source: '/Tekab-ag/admin/:slug*',
                destination: '/farmandari-tekab/admin/:slug*',
            },
            {
                permanent: true,
                source: '/Wa.ict/admin/:slug*',
                destination: '/waict/admin/:slug*',
            },
            {
                permanent: true,
                source: '/payamenor-boshehr/admin/:slug*',
                destination: '/pnu-bushehr/admin/:slug*',
            },
            {
                permanent: true,
                source: '/taknedapardaz/admin/:slug*',
                destination: '/takneda-pardaz/admin/:slug*',
            },
            {
                permanent: true,
                source: '/ksaa/admin/:slug*',
                destination: '/rasm/admin/:slug*',
            },
            {
                permanent: true,
                source: '/hosein-sadeghi/admin/:slug*',
                destination: '/hossein-sadeghi/admin/:slug*',
            },
            {
                permanent: true,
                source: '/taghizade/admin/:slug*',
                destination: '/taghizadeh/admin/:slug*',
            },
            {
                permanent: true,
                source: '/saraelm/admin/:slug*',
                destination: '/saraye-elm/admin/:slug*',
            },
            {
                permanent: true,
                source: '/khrshidetaban/admin/:slug*',
                destination: '/khorshid-taban/admin/:slug*',
            },
            {
                permanent: true,
                source: '/sdsc/admin/:slug*',
                destination: '/shahabian/admin/:slug*',
            },
            {
                permanent: true,
                source: '/N_Zarrin/admin/:slug*',
                destination: '/neshan-zarin/admin/:slug*',
            },
            {
                permanent: true,
                source: '/R.S.School/admin/:slug*',
                destination: '/shohadaye-ravansar/admin/:slug*',
            },
            {
                permanent: true,
                source: '/ch.h/admin/:slug*',
                destination: '/cheshmeh-hayat/admin/:slug*',
            },
            {
                permanent: true,
                source: '/fanzarin/admin/:slug*',
                destination: '/fanavari-zarin/admin/:slug*',
            },
            {
                permanent: true,
                source: '/Asr.sh/admin/:slug*',
                destination: '/asrshayestegan/admin/:slug*',
            },
            {
                permanent: true,
                source: '/RHA/admin/:slug*',
                destination: '/rayan-hesab/admin/:slug*',
            },
            {
                permanent: true,
                source: '/ra/admin/:slug*',
                destination: '/roze-abi/admin/:slug*',
            },
            {
                permanent: true,
                source: '/ch.z/admin/:slug*',
                destination: '/charm-zarin/admin/:slug*',
            },
            {
                permanent: true,
                source: '/g.n/admin/:slug*',
                destination: '/gol-narvan/admin/:slug*',
            },
            {
                permanent: true,
                source: '/j.b/admin/:slug*',
                destination: '/jahanbani/admin/:slug*',
            },
            {
                permanent: true,
                source: '/honam/admin/:slug*',
                destination: '/hoonam/admin/:slug*',
            },
            {
                permanent: true,
                source: '/Y.P/admin/:slug*',
                destination: '/yeganeh-poosh/admin/:slug*',
            },
            {
                permanent: true,
                source: '/m.kh/admin/:slug*',
                destination: '/mah-khanoom/admin/:slug*',
            },
            {
                permanent: true,
                source: '/n.d/admin/:slug*',
                destination: '/noavaran-davari/admin/:slug*',
            },
            {
                permanent: true,
                source: '/n.m/admin/:slug*',
                destination: '/noghosh-mandegar/admin/:slug*',
            },
            {
                permanent: true,
                source: '/b.z/admin/:slug*',
                destination: '/barg-zarin/admin/:slug*',
            },
            {
                permanent: true,
                source: '/o.n/admin/:slug*',
                destination: '/ofogh-no/admin/:slug*',
            },
            {
                permanent: true,
                source: '/gr.a/admin/:slug*',
                destination: '/golshan-raz-shabestar/admin/:slug*',
            },
            {
                permanent: true,
                source: '/M.G/admin/:slug*',
                destination: '/marefat-gostar/admin/:slug*',
            },
            {
                permanent: true,
                source: '/p.f.a/admin/:slug*',
                destination: '/poormosa/admin/:slug*',
            },
            {
                permanent: true,
                source: '/m.t/admin/:slug*',
                destination: '/maharat-va-tosee/admin/:slug*',
            },
            {
                permanent: true,
                source: '/s.e/admin/:slug*',
                destination: '/soheil/admin/:slug*',
            },
            {
                permanent: true,
                source: '/a.a/admin/:slug*',
                destination: '/aris/admin/:slug*',
            },
            {
                permanent: true,
                source: '/m.agin/admin/:slug*',
                destination: '/mehr-agin/admin/:slug*',
            },
            {
                permanent: true,
                source: '/a.k/admin/:slug*',
                destination: '/amirkabir/admin/:slug*',
            },
            {
                permanent: true,
                source: '/s.k/admin/:slug*',
                destination: '/saghiye-kosar/admin/:slug*',
            },
            {
                permanent: true,
                source: '/ahmadzadeh.a/admin/:slug*',
                destination: '/dr-ahmadzadeh/admin/:slug*',
            },
            {
                permanent: true,
                source: '/fatemeh/admin/:slug*',
                destination: '/khodayari/admin/:slug*',
            },
            {
                permanent: true,
                source: '/sfang/admin/:slug*',
                destination: '/sfanj/admin/:slug*',
            },
            {
                permanent: true,
                source: '/kh.m/admin/:slug*',
                destination: '/khaneh-memari/admin/:slug*',
            },
            {
                permanent: true,
                source: '/tar.pood/admin/:slug*',
                destination: '/taropood/admin/:slug*',
            },
            {
                permanent: true,
                source: '/i.p/admin/:slug*',
                destination: '/imen-pooya/admin/:slug*',
            },
            {
                permanent: true,
                source: '/o.at/admin/:slug*',
                destination: '/olum-atiyeh/admin/:slug*',
            },
            {
                permanent: true,
                source: '/h.f.z/admin/:slug*',
                destination: '/hazrat-fatemeh-zahra/admin/:slug*',
            },
            {
                permanent: true,
                source: '/m.sana/admin/:slug*',
                destination: '/mehr-sana/admin/:slug*',
            },
            {
                permanent: true,
                source: '/gol.ch/admin/:slug*',
                destination: '/gol-chehreh/admin/:slug*',
            },
            {
                permanent: true,
                source: '/zahra.v/admin/:slug*',
                destination: '/vahab/admin/:slug*',
            },
            {
                permanent: true,
                source: '/s.aroos/admin/:slug*',
                destination: '/sagheh-aroos/admin/:slug*',
            },
            {
                permanent: true,
                source: '/penny/admin/:slug*',
                destination: '/nasohian/admin/:slug*',
            },
            {
                permanent: true,
                source: '/kh.d/admin/:slug*',
                destination: '/khaneh-dookht/admin/:slug*',
            },
            {
                permanent: true,
                source: '/mehr.afag/admin/:slug*',
                destination: '/mehr-afagh/admin/:slug*',
            },
            {
                permanent: true,
                source: '/omid.m/admin/:slug*',
                destination: '/omid-madar/admin/:slug*',
            },
            {
                permanent: true,
                source: '/paris/admin/:slug*',
                destination: '/parisan/admin/:slug*',
            },
            {
                permanent: true,
                source: '/nam.a/admin/:slug*',
                destination: '/namavaran/admin/:slug*',
            },
            {
                permanent: true,
                source: '/gol.sara/admin/:slug*',
                destination: '/gol-sara/admin/:slug*',
            },
            {
                permanent: true,
                source: '/rama.e/admin/:slug*',
                destination: '/rama-electronic/admin/:slug*',
            },
            {
                permanent: true,
                source: '/fani-herfei-azarbayejan/admin/:slug*',
                destination: '/tvto-ag/admin/:slug*',
            },
            {
                permanent: true,
                source: '/asep',
                destination: '/organizations/asanseminar',
            },
            {
                permanent: true,
                source: '/organizations/brand-page',
                destination: '/organizations/brandpage',
            },
            {
                permanent: true,
                source: '/sina',
                destination: '/organizations/sina-noavar',
            },
            {
                permanent: true,
                source: '/irandds',
                destination: '/organizations/ida-dent',
            },
            {
                permanent: true,
                source: '/MFUrmia',
                destination: '/organizations/parnian-afkar',
            },
            {
                permanent: true,
                source: '/IBSA',
                destination: '/organizations/paizan-tejarat-mehr',
            },
            {
                permanent: true,
                source: '/Zonkan',
                destination: '/organizations/zounkan',
            },
            {
                permanent: true,
                source: '/Markazidoe',
                destination: '/organizations/mohitzist-markazi',
            },
            {
                permanent: true,
                source: '/MJ',
                destination: '/organizations/mehr-aien',
            },
            {
                permanent: true,
                source: '/DOEofKerman',
                destination: '/organizations/mohitzist-kerman',
            },
            {
                permanent: true,
                source: '/DOEofQaz',
                destination: '/organizations/mohitzist-ghazvin',
            },
            {
                permanent: true,
                source: '/irimc',
                destination: '/organizations/irimc-ag',
            },
            {
                permanent: true,
                source: '/DOEofKhozestan',
                destination: '/organizations/mohitzist-khouzestan',
            },
            {
                permanent: true,
                source: '/AG2',
                destination: '/organizations/avaye-golchin2',
            },
            {
                permanent: true,
                source: '/ae',
                destination: '/organizations/avaye-elm',
            },
            {
                permanent: true,
                source: '/pe',
                destination: '/organizations/poyaye-elm',
            },
            {
                permanent: true,
                source: '/ag1',
                destination: '/organizations/avaye-golchin ',
            },
            {
                permanent: true,
                source: '/ge',
                destination: '/organizations/golchin-elm',
            },
            {
                permanent: true,
                source: '/far',
                destination: '/organizations/farhang',
            },
            {
                permanent: true,
                source: '/wastp',
                destination: '/organizations/techpark-ag',
            },
            {
                permanent: true,
                source: '/dehkhoda',
                destination: '/organizations/dehkhoda',
            },
            {
                permanent: true,
                source: '/rooyesh',
                destination: '/organizations/rooyesh',
            },
            {
                permanent: true,
                source: '/sana',
                destination: '/organizations/sana',
            },
            {
                permanent: true,
                source: '/kherad2',
                destination: '/organizations/kherad2',
            },
            {
                permanent: true,
                source: '/eh',
                destination: '/organizations/elm-afarinan',
            },
            {
                permanent: true,
                source: '/shahriar',
                destination: '/organizations/shahriar',
            },
            {
                permanent: true,
                source: '/wap',
                destination: '/organizations/ostandari-ag',
            },
            {
                permanent: true,
                source: '/hooshe_bartar',
                destination: '/organizations/hooshe-bartar',
            },
            {
                permanent: true,
                source: '/DOEofَArdabil',
                destination: '/organizations/mohitzist-ardabil',
            },
            {
                permanent: true,
                source: '/oshnavie',
                destination: '/organizations/farmandari-oshnavieh',
            },
            {
                permanent: true,
                source: '/tebex',
                destination: '/organizations/tebex',
            },
            {
                permanent: true,
                source: '/makoo',
                destination: '/organizations/farmandari-makoo',
            },
            {
                permanent: true,
                source: '/azgmpo',
                destination: '/organizations/mporg-ag',
            },
            {
                permanent: true,
                source: '/AshayerAG',
                destination: '/organizations/ashayer-ag',
            },
            {
                permanent: true,
                source: '/Golman',
                destination: '/organizations/golman',
            },
            {
                permanent: true,
                source: '/farmandari-salmas',
                destination: '/organizations/farmandari-salmas',
            },
            {
                permanent: true,
                source: '/Sh_salmas',
                destination: '/organizations/shahrdari-salmas',
            },
            {
                permanent: true,
                source: '/SR',
                destination: '/organizations/rah-abrisham',
            },
            {
                permanent: true,
                source: '/A.K.T',
                destination: '/organizations/fanbazar-ag',
            },
            {
                permanent: true,
                source: '/javanmard',
                destination: '/organizations/javanmard',
            },
            {
                permanent: true,
                source: '/DOEofyazd',
                destination: '/organizations/mohitzist-yazd',
            },
            {
                permanent: true,
                source: '/Bazneshaste',
                destination: '/organizations/cspf-bushehr',
            },
            {
                permanent: true,
                source: '/inamad',
                destination: '/organizations/inamad',
            },
            {
                permanent: true,
                source: '/dms',
                destination: '/organizations/maaref-sadra',
            },
            {
                permanent: true,
                source: '/RCCR',
                destination: '/organizations/rccr-babolsar',
            },
            {
                permanent: true,
                source: '/Fh',
                destination: '/organizations/farhangestan-hendijan',
            },
            {
                permanent: true,
                source: '/IM_qom',
                destination: '/organizations/sim-ghom',
            },
            {
                permanent: true,
                source: '/Fava',
                destination: '/organizations/fava-shahrdari-urmia',
            },
            {
                permanent: true,
                source: '/STR',
                destination: '/organizations/taavon-rostayi-ghom',
            },
            {
                permanent: true,
                source: '/Masirfartak',
                destination: '/organizations/fartak',
            },
            {
                permanent: true,
                source: '/PQD',
                destination: '/organizations/pishgam-tarah',
            },
            {
                permanent: true,
                source: '/CHISTA',
                destination: '/organizations/chista',
            },
            {
                permanent: true,
                source: '/TGAD',
                destination: '/organizations/ayda-danesh',
            },
            {
                permanent: true,
                source: '/LMSAS',
                destination: '/organizations/lmsas',
            },
            {
                permanent: true,
                source: '/Doe',
                destination: '/organizations/mohitzist-mazandaran',
            },
            {
                permanent: true,
                source: '/AG.DOE',
                destination: '/organizations/mohitzist-ag',
            },
            {
                permanent: true,
                source: '/IRSA',
                destination: '/organizations/irsa',
            },
            {
                permanent: true,
                source: '/Armknkr',
                destination: '/organizations/aramesh-konkour',
            },
            {
                permanent: true,
                source: '/Pnukarafrini',
                destination: '/organizations/pnu-karafarini',
            },
            {
                permanent: true,
                source: '/ASA',
                destination: '/organizations/azar-asa',
            },
            {
                permanent: true,
                source: '/wikimoshaver',
                destination: '/organizations/wiki-moshaver',
            },
            {
                permanent: true,
                source: '/WAPMCO',
                destination: '/organizations/hmyr-ag',
            },
            {
                permanent: true,
                source: '/prclass',
                destination: '/organizations/class-khososi',
            },
            {
                permanent: true,
                source: '/ISMOH',
                destination: '/organizations/ismoh',
            },
            {
                permanent: true,
                source: '/Tekab-ag',
                destination: '/organizations/farmandari-tekab',
            },
            {
                permanent: true,
                source: '/Wa.ict',
                destination: '/organizations/waict',
            },
            {
                permanent: true,
                source: '/mohitzist-fars',
                destination: '/organizations/mohitzist-fars',
            },
            {
                permanent: true,
                source: '/payamenor-boshehr',
                destination: '/organizations/pnu-bushehr',
            },
            {
                permanent: true,
                source: '/taknedapardaz',
                destination: '/organizations/takneda-pardaz',
            },
            {
                permanent: true,
                source: '/mohitzist-kordestan',
                destination: '/organizations/mohitzist-kordestan',
            },
            {
                permanent: true,
                source: '/ksaa',
                destination: '/organizations/rasm',
            },
            {
                permanent: true,
                source: '/hosein-sadeghi',
                destination: '/organizations/hossein-sadeghi',
            },
            {
                permanent: true,
                source: '/termeh',
                destination: '/organizations/termeh',
            },
            {
                permanent: true,
                source: '/taghizade',
                destination: '/organizations/taghizadeh',
            },
            {
                permanent: true,
                source: '/shamimbaft',
                destination: '/organizations/shamimbaft',
            },
            {
                permanent: true,
                source: '/khalagh',
                destination: '/organizations/khalagh',
            },
            {
                permanent: true,
                source: '/tajzarin',
                destination: '/organizations/tajzarin',
            },
            {
                permanent: true,
                source: '/saraelm',
                destination: '/organizations/saraye-elm',
            },
            {
                permanent: true,
                source: '/khrshidetaban',
                destination: '/organizations/khorshid-taban',
            },
            {
                permanent: true,
                source: '/etemad',
                destination: '/organizations/etemad',
            },
            {
                permanent: true,
                source: '/faraz',
                destination: '/organizations/faraz',
            },
            {
                permanent: true,
                source: '/atlas',
                destination: '/organizations/atlas',
            },
            {
                permanent: true,
                source: '/sdsc',
                destination: '/organizations/shahabian',
            },
            {
                permanent: true,
                source: '/N_Zarrin',
                destination: '/organizations/neshan-zarin',
            },
            {
                permanent: true,
                source: '/ZagrosELC',
                destination: '/organizations/zagroselc',
            },
            {
                permanent: true,
                source: '/R.S.School',
                destination: '/organizations/shohadaye-ravansar',
            },
            {
                permanent: true,
                source: '/Omid',
                destination: '/organizations/omid',
            },
            {
                permanent: true,
                source: '/ch.h',
                destination: '/organizations/cheshmeh-hayat',
            },
            {
                permanent: true,
                source: '/fanzarin',
                destination: '/organizations/fanavari-zarin',
            },
            {
                permanent: true,
                source: '/Asr.sh',
                destination: '/organizations/asrshayestegan',
            },
            {
                permanent: true,
                source: '/aria',
                destination: '/organizations/aria',
            },
            {
                permanent: true,
                source: '/moda',
                destination: '/organizations/moda',
            },
            {
                permanent: true,
                source: '/RHA',
                destination: '/organizations/rayan-hesab',
            },
            {
                permanent: true,
                source: '/ra',
                destination: '/organizations/roze-abi',
            },
            {
                permanent: true,
                source: '/parsian',
                destination: '/organizations/parsian',
            },
            {
                permanent: true,
                source: '/ch.z',
                destination: '/organizations/charm-zarin',
            },
            {
                permanent: true,
                source: '/g.n',
                destination: '/organizations/gol-narvan',
            },
            {
                permanent: true,
                source: '/j.b',
                destination: '/organizations/jahanbani',
            },
            {
                permanent: true,
                source: '/honam',
                destination: '/organizations/hoonam',
            },
            {
                permanent: true,
                source: '/boresh',
                destination: '/organizations/boresh',
            },
            {
                permanent: true,
                source: '/Y.P',
                destination: '/organizations/yeganeh-poosh',
            },
            {
                permanent: true,
                source: '/m.kh',
                destination: '/organizations/mah-khanoom',
            },
            {
                permanent: true,
                source: '/behnam',
                destination: '/organizations/behnam',
            },
            {
                permanent: true,
                source: '/parmira',
                destination: '/organizations/parmira',
            },
            {
                permanent: true,
                source: '/n.d',
                destination: '/organizations/noavaran-davari',
            },
            {
                permanent: true,
                source: '/n.m',
                destination: '/organizations/noghosh-mandegar',
            },
            {
                permanent: true,
                source: '/sorena',
                destination: '/organizations/sorena',
            },
            {
                permanent: true,
                source: '/b.z',
                destination: '/organizations/barg-zarin',
            },
            {
                permanent: true,
                source: '/o.n',
                destination: '/organizations/ofogh-no',
            },
            {
                permanent: true,
                source: '/viana',
                destination: '/organizations/viana',
            },
            {
                permanent: true,
                source: '/gr.a',
                destination: '/organizations/golshan-raz-shabestar',
            },
            {
                permanent: true,
                source: '/nikan',
                destination: '/organizations/nikan',
            },
            {
                permanent: true,
                source: '/tara',
                destination: '/organizations/tara',
            },
            {
                permanent: true,
                source: '/kiana',
                destination: '/organizations/kiana',
            },
            {
                permanent: true,
                source: '/fatemieh',
                destination: '/organizations/fatemieh',
            },
            {
                permanent: true,
                source: '/pasargad',
                destination: '/organizations/pasargad',
            },
            {
                permanent: true,
                source: '/M.G',
                destination: '/organizations/marefat-gostar',
            },
            {
                permanent: true,
                source: '/p.f.a',
                destination: '/organizations/poormosa',
            },
            {
                permanent: true,
                source: '/m.t',
                destination: '/organizations/maharat-va-tosee',
            },
            {
                permanent: true,
                source: '/pegah',
                destination: '/organizations/pegah',
            },
            {
                permanent: true,
                source: '/s.e',
                destination: '/organizations/soheil',
            },
            {
                permanent: true,
                source: '/a.a',
                destination: '/organizations/aris',
            },
            {
                permanent: true,
                source: '/m.agin',
                destination: '/organizations/mehr-agin',
            },
            {
                permanent: true,
                source: '/a.k',
                destination: '/organizations/amirkabir',
            },
            {
                permanent: true,
                source: '/houzhan',
                destination: '/organizations/houzhan',
            },
            {
                permanent: true,
                source: '/s.k',
                destination: '/organizations/saghiye-kosar',
            },
            {
                permanent: true,
                source: '/maramat',
                destination: '/organizations/maramat',
            },
            {
                permanent: true,
                source: '/ahmadzadeh.a',
                destination: '/organizations/dr-ahmadzadeh',
            },
            {
                permanent: true,
                source: '/fatemeh',
                destination: '/organizations/khodayari',
            },
            {
                permanent: true,
                source: '/sfang',
                destination: '/organizations/sfanj',
            },
            {
                permanent: true,
                source: '/kh.m',
                destination: '/organizations/khaneh-memari',
            },
            {
                permanent: true,
                source: '/poopak',
                destination: '/organizations/poopak',
            },
            {
                permanent: true,
                source: '/erisa',
                destination: '/organizations/erisa',
            },
            {
                permanent: true,
                source: '/tar.pood',
                destination: '/organizations/taropood',
            },
            {
                permanent: true,
                source: '/i.p',
                destination: '/organizations/imen-pooya',
            },
            {
                permanent: true,
                source: '/o.at',
                destination: '/organizations/olum-atiyeh',
            },
            {
                permanent: true,
                source: '/h.f.z',
                destination: '/organizations/hazrat-fatemeh-zahra',
            },
            {
                permanent: true,
                source: '/m.sana',
                destination: '/organizations/mehr-sana',
            },
            {
                permanent: true,
                source: '/anis',
                destination: '/organizations/anis',
            },
            {
                permanent: true,
                source: '/kasra',
                destination: '/organizations/kasra',
            },
            {
                permanent: true,
                source: '/golabetoon',
                destination: '/organizations/golabetoon',
            },
            {
                permanent: true,
                source: '/gol.ch',
                destination: '/organizations/gol-chehreh',
            },
            {
                permanent: true,
                source: '/fouzhan',
                destination: '/organizations/fouzhan',
            },
            {
                permanent: true,
                source: '/delsa',
                destination: '/organizations/delsa',
            },
            {
                permanent: true,
                source: '/zahra.v',
                destination: '/organizations/vahab',
            },
            {
                permanent: true,
                source: '/s.aroos',
                destination: '/organizations/sagheh-aroos',
            },
            {
                permanent: true,
                source: '/penny',
                destination: '/organizations/nasohian',
            },
            {
                permanent: true,
                source: '/valfajr',
                destination: '/organizations/valfajr',
            },
            {
                permanent: true,
                source: '/mahtaban',
                destination: '/organizations/mahtaban',
            },
            {
                permanent: true,
                source: '/rozh',
                destination: '/organizations/rozh',
            },
            {
                permanent: true,
                source: '/kh.d',
                destination: '/organizations/khaneh-dookht',
            },
            {
                permanent: true,
                source: '/javadi',
                destination: '/organizations/javadi',
            },
            {
                permanent: true,
                source: '/segal',
                destination: '/organizations/segal',
            },
            {
                permanent: true,
                source: '/tamela',
                destination: '/organizations/tamela',
            },
            {
                permanent: true,
                source: '/mehr.afag',
                destination: '/organizations/mehr-afagh',
            },
            {
                permanent: true,
                source: '/rosana',
                destination: '/organizations/rosana',
            },
            {
                permanent: true,
                source: '/shadab',
                destination: '/organizations/shadab',
            },
            {
                permanent: true,
                source: '/omid.m',
                destination: '/organizations/omid-madar',
            },
            {
                permanent: true,
                source: '/paris',
                destination: '/organizations/parisan',
            },
            {
                permanent: true,
                source: '/olampiad',
                destination: '/organizations/olampiad',
            },
            {
                permanent: true,
                source: '/nam.a',
                destination: '/organizations/namavaran',
            },
            {
                permanent: true,
                source: '/gol.sara',
                destination: '/organizations/gol-sara',
            },
            {
                permanent: true,
                source: '/tavana',
                destination: '/organizations/tavana',
            },
            {
                permanent: true,
                source: '/rama.e',
                destination: '/organizations/rama-electronic',
            },
            {
                permanent: true,
                source: '/oxin',
                destination: '/organizations/oxin',
            },
            {
                permanent: true,
                source: '/tala',
                destination: '/organizations/tala',
            },
            {
                permanent: true,
                source: '/negah',
                destination: '/organizations/negah',
            },
            {
                permanent: true,
                source: '/apameh',
                destination: '/organizations/apameh',
            },
            {
                permanent: true,
                source: '/hamandishan',
                destination: '/organizations/hamandishan',
            },
            {
                permanent: true,
                source: '/rayaneh-karan-javan',
                destination: '/organizations/rayaneh-karan-javan',
            },
            {
                permanent: true,
                source: '/khaneh-omran',
                destination: '/organizations/khaneh-omran',
            },
            {
                permanent: true,
                source: '/rose',
                destination: '/organizations/rose',
            },
            {
                permanent: true,
                source: '/shabnam-banoo',
                destination: '/organizations/shabnam-banoo',
            },
            {
                permanent: true,
                source: '/hanay',
                destination: '/organizations/hanay',
            },
            {
                permanent: true,
                source: '/fartak',
                destination: '/organizations/fartak',
            },
            {
                permanent: true,
                source: '/sevil',
                destination: '/organizations/sevil',
            },
            {
                permanent: true,
                source: '/delphi',
                destination: '/organizations/delphi',
            },
            {
                permanent: true,
                source: '/taaloman',
                destination: '/organizations/taaloman',
            },
            {
                permanent: true,
                source: '/fani-herfei-azarbayejan',
                destination: '/organizations/tvto-ag',
            },
            {
                permanent: true,
                source: '/he-veri',
                destination: '/organizations/he-veri',
            },
            {
                permanent: true,
                source: '/fara-honar',
                destination: '/organizations/fara-honar',
            },
            {
                permanent: true,
                source: '/mahde-honar',
                destination: '/organizations/mahde-honar',
            },
            {
                permanent: true,
                source: '/pari-mah',
                destination: '/organizations/pari-mah',
            },
            {
                permanent: true,
                source: '/mehr',
                destination: '/organizations/mehr',
            },
            {
                permanent: true,
                source: '/nikoo',
                destination: '/organizations/nikoo',
            },
            {
                permanent: true,
                source: '/rahiyan',
                destination: '/organizations/rahiyan',
            },
            {
                permanent: true,
                source: '/sepideh',
                destination: '/organizations/sepideh',
            },
            {
                permanent: true,
                source: '/pishro',
                destination: '/organizations/pishro',
            },
            {
                permanent: true,
                source: '/narimani',
                destination: '/organizations/narimani',
            },
            {
                permanent: true,
                source: '/danesh-ara',
                destination: '/organizations/danesh-ara',
            },
            // --------- BLOG REDIRECTS -------------
            {
                permanent: true,
                source:
                    '/blog/tag/%D8%A8%D8%B1%D9%86%D8%AF%20%D9%BE%DB%8C%D8%AC%20%D9%BE%D8%B1%DB%8C%D9%85%DB%8C%D9%88%D9%85',
                destination: '/organizations/brandpage/blog/100283464',
            },
            {
                permanent: true,
                source:
                    '/blog/category/%D8%A7%D8%AE%D8%A8%D8%A7%D8%B1/%D8%A7%D8%AE%D8%A8%D8%A7%D8%B1-%D8%AA%D8%AD%D8%B5%DB%8C%D9%84%DB%8C',
                destination: '/blog/category/%D8%A7%D8%AE%D8%A8%D8%A7%D8%B1-%D8%AA%D8%AD%D8%B5%DB%8C%D9%84%DB%8C',
            },
            {
                permanent: true,
                source:
                    '/blog/%D8%A2%D8%AF%D8%A7%D8%A8-%D9%88-%D8%B1%D8%B3%D9%88%D9%85-%D8%B4%D8%B1%DA%A9%D8%AA-%D8%AF%D8%B1-%DA%A9%D9%84%D8%A7%D8%B3-%D9%87%D8%A7%DB%8C-%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86',
                destination: '/blog/1230359522',
            },
            {
                permanent: true,
                source:
                    '/blog/%D8%A2%DB%8C%D8%A7-%DA%A9%D8%A7%D8%B1-%D8%A8%D8%A7-%D8%B3%DB%8C%D8%B3%D8%AA%D9%85-%D8%A2%D9%85%D9%88%D8%B2%D8%B4-%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86-%D9%88-%D9%86%D8%B1%D9%85-%D8%A7%D9%81%D8%B2%D8%A7%D8%B1-%D8%A2%D9%85%D9%88%D8%B2%D8%B4-%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86-%D8%AE%D8%B7%D8%B1%D9%86%D8%A7%DA%A9-%D8%A7%D8%B3%D8%AA',
                destination: '/blog/421022856',
            },
            {
                permanent: true,
                source:
                    '/blog/%D8%A7%D9%86%DA%AF%DB%8C%D8%B2%D9%87-%D8%B4%D9%85%D8%A7-%D8%A7%D8%B2-%D8%A7%DB%8C%D8%AC%D8%A7%D8%AF-%D8%A2%D9%85%D9%88%D8%B2%D8%B4%DA%AF%D8%A7%D9%87-%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86-%DA%86%DB%8C%D8%B3%D8%AA',
                destination: '/blog/616395218',
            },
            {
                permanent: true,
                source:
                    '/blog/%D8%A7%D9%88%D9%84%DB%8C%D9%86-%D8%A2%D9%85%D9%88%D8%B2%D8%B4%DA%AF%D8%A7%D9%87-%D8%A8%D8%A7%D9%81%D8%AA',
                destination: '/blog/868377116',
            },
            {
                permanent: true,
                source:
                    '/blog/%D8%A7%D9%88%D9%84%DB%8C%D9%86-%D8%A2%D9%85%D9%88%D8%B2%D8%B4%DA%AF%D8%A7%D9%87-%D8%AA%D8%AE%D8%B5%D8%B5%DB%8C-%D8%A8%D8%A7%D9%81%D8%AA--%D8%A7%D9%87%D9%88%D8%A7%D8%B2',
                destination: '/blog/779151551',
            },
            {
                permanent: true,
                source:
                    '/blog/%D8%A8%D8%A7%DB%8C%D8%AF%D9%87%D8%A7%DB%8C-%D9%85%D9%87%D9%85-%D8%AF%D8%B1-%D8%B1%D8%A7%D9%87-%D8%A7%D9%86%D8%AF%D8%A7%D8%B2%DB%8C-%D8%A2%D9%85%D9%88%D8%B2%D8%B4%DA%AF%D8%A7%D9%87-%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86',
                destination: '/blog/107915524',
            },
            {
                permanent: true,
                source:
                    '/blog/%D8%A8%D9%87%D8%AA%D8%B1%DB%8C%D9%86-%D8%B3%DB%8C%D8%B3%D8%AA%D9%85-%D8%A2%D9%85%D9%88%D8%B2%D8%B4%DB%8C-%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86',
                destination: '/blog/1425734700',
            },
            {
                permanent: true,
                source:
                    '/blog/%D9%BE%D8%B4%D8%AA%DB%8C%D8%A8%D8%A7%D9%86%DB%8C-%D8%A2%D9%85%D9%88%D8%B2%D8%B4-%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86-%D8%B4%DA%A9%D8%B3%D8%AA%D9%86-%D8%B4%D8%A7%D8%AE-%D8%BA%D9%88%D9%84-%D8%AF%D8%B1-%D9%85%D8%AF%DB%8C%D8%B1%DB%8C%D8%AA-%D8%B3%DB%8C%D8%B3%D8%AA%D9%85-%D8%A2%D9%85%D9%88%D8%B2%D8%B4-%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86',
                destination: '/blog/585877109',
            },
            {
                permanent: true,
                source:
                    '/blog/%D9%BE%D8%B4%D8%AA%DB%8C%D8%A8%D8%A7%D9%86%DB%8C-%D8%B3%DB%8C%D8%B3%D8%AA%D9%85-%D8%A2%D9%85%D9%88%D8%B2%D8%B4-%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86',
                destination: '/blog/390518059',
            },
            {
                permanent: true,
                source:
                    '/blog/%D8%AA%D9%81%D8%A7%D9%88%D8%AA-%D8%A2%D9%85%D9%88%D8%B2%D8%B4-%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86-%D9%88-%D9%85%D8%AC%D8%A7%D8%B2%DB%8C',
                destination: '/blog/2090549159',
            },
            {
                permanent: true,
                source:
                    '/blog/%DA%86%D8%B7%D9%88%D8%B1-%D8%A7%D8%B2-%D9%88%D8%A8%DB%8C%D9%86%D8%A7%D8%B1-%D9%88-%D8%AA%D8%AF%D8%B1%DB%8C%D8%B3-%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86-%D8%AF%D8%B1%D8%A2%D9%85%D8%AF-%D8%B2%D8%A7%DB%8C%DB%8C-%DA%A9%D9%86%DB%8C%D9%85',
                destination: '/blog/2060027130',
            },
            {
                permanent: true,
                source:
                    '/blog/%D8%AE%D8%B7%D8%A7-%D9%BE%D8%B0%DB%8C%D8%B1%DB%8C-%D9%86%D9%88%D8%B9%DB%8C-%D8%B3%D8%B1%D8%B7%D8%A7%D9%86-%D8%AF%D8%B1-%D8%B3%DB%8C%D8%B3%D8%AA%D9%85-%D8%A2%D9%85%D9%88%D8%B2%D8%B4-%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86',
                destination: '/blog/1582069641',
            },
            {
                permanent: true,
                source:
                    '/blog/%D8%AE%D8%B7%D8%B1-%D8%B3%DB%8C%D8%B3%D8%AA%D9%85-%D8%B1%D8%A7%DB%8C%DA%AF%D8%A7%D9%86-%D8%A2%D9%85%D9%88%D8%B2%D8%B4-%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86',
                destination: '/blog/1903562013',
            },
            {
                permanent: true,
                source:
                    '/blog/%D8%AE%D8%B7%D8%B1%D8%A7%D8%AA-%D8%A2%D9%85%D9%88%D8%B2%D8%B4-%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86',
                destination: '/blog/1395082575',
            },
            {
                permanent: true,
                source: '/blog/%D9%84%D9%88%D8%B1%D9%85-%D8%A7%DB%8C%D9%BE%D8%B3%D9%88%D9%85',
                destination: '/blog/1199722501',
            },
            {
                permanent: true,
                source:
                    '/blog/%D9%86%D8%B1%D9%85-%D8%A7%D9%81%D8%B2%D8%A7%D8%B1-%D8%A2%D9%85%D9%88%D8%B2%D8%B4-%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86-%DA%A9%D8%A7%D8%B1-%D9%86%D9%85%DB%8C-%DA%A9%D9%86%D8%AF-%D8%B3%DB%8C%D8%B3%D8%AA%D9%85-%D8%A2%D9%85%D9%88%D8%B2%D8%B4-%D8%A2%D9%86%D9%84%D8%A7%DB%8C%D9%86-%DA%A9%D8%A7%D8%B1-%D9%85%DB%8C-%DA%A9%D9%86%D8%AF',
                destination: '/blog/136323297',
            },
            {
                permanent: true,
                source:
                    '/blog/%D9%86%D8%B3%D8%AE%D9%87-%D8%AC%D8%AF%DB%8C%D8%AF-%D8%A2%D8%B3%D8%A7%D9%86-%D8%B3%D9%85%DB%8C%D9%86%D8%A7%D8%B1',
                destination: '/blog/898997753',
            },
            {
                permanent: true,
                source:
                    '/blog/%D9%87%D9%85%D9%87-%DA%86%DB%8C%D8%B2-%D8%AF%D8%B1%D8%A8%D8%A7%D8%B1%D9%87-%D8%A8%DB%8C%DA%AF-%D8%A8%D9%84%D9%88%D8%A8%D8%A7%D8%AA%D9%86',
                destination: '/blog/234052174',
            },
        ];
    },
};
