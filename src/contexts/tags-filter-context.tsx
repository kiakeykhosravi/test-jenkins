import React from 'react';
import { ITag } from 'models/tag';

const defaultValue: { tags: ITag[]; changeTags: (newValue: ITag[]) => void } = {
    tags: [],
    // eslint-disable-next-line @typescript-eslint/no-empty-function
    changeTags: (newValue: ITag[]) => {},
};

export const TagsFilterContext = React.createContext(defaultValue);
