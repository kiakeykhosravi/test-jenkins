import React from 'react';
import { IUser } from '../models/user';

export interface IAuthContextType {
    user: IUser;
    changeIsLoggedIn: (newValue: IUser | null) => void;
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
export const AuthContext = React.createContext<IAuthContextType>({ user: null, changeIsLoggedIn: (newValue) => {} });
