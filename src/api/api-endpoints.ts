class endpoints {
    public static frontendBase =
        process.env.NEXT_PUBLIC_FRONTEND_API_ADDRESS[process.env.NEXT_PUBLIC_FRONTEND_API_ADDRESS.length - 1] === '/'
            ? process.env.NEXT_PUBLIC_FRONTEND_API_ADDRESS + 'api'
            : process.env.NEXT_PUBLIC_FRONTEND_API_ADDRESS + '/api';

    public static backendBase = process.env.NEXT_PRIVATE_BACKEND_API_ADDRESS;

    public static frontend = {
        // Authentication apis
        login: endpoints.frontendBase + '/auth/login',
        login_nopass: endpoints.frontendBase + '/auth/login_nopass',
        login_with_token: endpoints.frontendBase + '/fronts_auth/login',
        verify: endpoints.frontendBase + '/auth/verify',
        logout: endpoints.frontendBase + '/auth/logout',

        // Guest apis
        guest_enter: endpoints.frontendBase + '/webinars/guests/{secret}/enter',

        // Homepage apis
        related_organizations: endpoints.frontendBase + '/organizations/related/for_user',

        // header apis
        header_time: endpoints.frontendBase + '/currentservertime/now',

        // Profile apis
        profile_user: endpoints.frontendBase + '/profile/user',
        profile_candidates: endpoints.frontendBase + '/profile/candidates',
        orders: endpoints.frontendBase + '/profile/orders',
        meetings: endpoints.frontendBase + '/profile/meetings',
        transactions: endpoints.frontendBase + '/profile/financial/transactions',
        credit: endpoints.frontendBase + '/profile/financial/credit',
        credit_add: endpoints.frontendBase + '/profile/financial/credit/add',
        profile_password_update: endpoints.frontendBase + '/profile/user/password',
        admin_authentication: endpoints.frontendBase + '/fronts_auth/secret',
        marketing_webinars: endpoints.frontendBase + '/profile/marketing/webinars/search',
        user_marketing_webinars_stats: endpoints.frontendBase + '/profile/marketing/webinars/statistics',
        user_marketing_webinars_sales_users_list:
            endpoints.frontendBase + '/profile/marketing/webinars/{webinar}/users',
        user_marketing_brandpage_stats: endpoints.frontendBase + '/profile/marketing/brandpage/statistics',
        user_marketing_brandpage_sales_users_list: endpoints.frontendBase + '/profile/marketing/brandpage/candidates',
        profile_marketing_brandpage_is_marketer: endpoints.frontendBase + '/profile/marketing/brandpage/is_marketer',

        // Courses apis
        courses_tags: endpoints.frontendBase + '/webinars/tags',

        // Search apis
        search_header: endpoints.frontendBase + '/search/header',
        search_webinars: endpoints.frontendBase + '/search/webinars',
        search_instructors: endpoints.frontendBase + '/search/instructors',
        search_organizations: endpoints.frontendBase + '/search/organizations',
        search_organization_webinars: endpoints.frontendBase + '/search/organizations/{organization}/webinars',
        search_organization_blogs: endpoints.frontendBase + '/search/organizations/{organization}/posts',

        // Webinars apis
        order: endpoints.frontendBase + '/webinars/{webinar}/order',
        webinar_rate: endpoints.frontendBase + '/webinars/{webinar}/submit_rating',
        webinar: endpoints.frontendBase + '/webinars/',
        webinar_meetings: endpoints.frontendBase + '/webinars/{webinar}/meetings',

        // Blog apis
        comment_add: endpoints.frontendBase + '/blogs/{post}/comments/add',
        like: endpoints.frontendBase + '/blogs/{post}/like',
        unlike: endpoints.frontendBase + '/blogs/{post}/unlike',
        blog_categories: endpoints.frontendBase + '/blogs/categories/organizations/{organization_slug}',
        latest_news: endpoints.frontendBase + '/blogs/latest_news',

        // Meetings apis
        meeting_enter: endpoints.frontendBase + '/meetings/{meeting}/enter',
        meeting_replay: endpoints.frontendBase + '/meetings/{meeting}/recording',

        // Asanquiz apis
        asanquiz_enter: endpoints.frontendBase + '/webinars/{webinar}/asanquizzes/{asanquiz}/enter',
        asanquiz_create: endpoints.frontendBase + '/webinars/{webinar}/asanquizzes/create',

        // Discount apis
        discounts_check_webinar: endpoints.frontendBase + '/discounts/{discount_code}/check/',

        // Candidate apis
        candidate_complete: endpoints.frontendBase + '/candidate/{id}/complete',
        register_candidate: endpoints.frontendBase + '/candidate/register',
        upload_organization_image: endpoints.frontendBase + '/upload/organization_image',
        send_code: endpoints.frontendBase + '/candidate/sendcode',
        verify_code: endpoints.frontendBase + '/candidate/verifycode',
        retry_brandpage: endpoints.frontendBase + '/candidate/{secret}/brandpage_pay_retry',

        // Ticket apis
        upload_resume: endpoints.frontendBase + '/upload/resume',
        ticket_joinus: endpoints.frontendBase + '/tickets/joinus',

        // Specialties apis
        specialties: endpoints.frontendBase + '/specialties',

        // Public marketers
        public_marketer: endpoints.frontendBase + '/public_marketer/{public_marketer}',
    };

    public static backend = {
        // Guest apis
        guest_info: endpoints.backendBase + '/webinars/guests/{secret}',

        // Homepage apis
        home_top_banners: endpoints.backendBase + '/sliders/main',
        home_organizations_top: endpoints.backendBase + '/organizations/homepage-recommended',
        home_user_stories: endpoints.backendBase + '/feedbacks/all',
        home_courses: endpoints.backendBase + '/webinars/homepage-recommended',
        home_metadata: endpoints.backendBase + '/metadata/landing_counters',
        related_organizations: endpoints.backendBase + '/organizations/related/for_user',

        // Instructor apis
        // TODO: Bring {id} param in here
        instructor: endpoints.backendBase + '/instructors',

        // Organization apis
        organization: endpoints.backendBase + '/organizations/{organization_slug}',

        // Organization Product apis
        organization_product: endpoints.backendBase + '/organization_products/{organization_product_id}',

        // Profile apis
        profile_user: endpoints.backendBase + '/profile/user',
        profile_candidates: endpoints.backendBase + '/profile/candidates',
        orders: endpoints.backendBase + '/profile/orders',
        meetings: endpoints.backendBase + '/profile/meetings',
        transactions: endpoints.backendBase + '/profile/financial/transactions',
        credit: endpoints.backendBase + '/profile/financial/credit',
        admin_authentication: endpoints.backendBase + '/fronts_auth/secret',
        marketing_webinars: endpoints.backendBase + '/profile/marketing/webinars/search',
        user_marketing_webinars_stats: endpoints.backendBase + '/profile/marketing/webinars/statistics',
        user_marketing_webinars_sales_users_list: endpoints.backendBase + '/profile/marketing/webinars/{webinar}/users',
        user_marketing_brandpage_stats: endpoints.backendBase + '/profile/marketing/brandpage/statistics',
        user_marketing_brandpage_sales_users_list: endpoints.backendBase + '/profile/marketing/brandpage/candidates',
        profile_marketing_brandpage_is_marketer: endpoints.frontendBase + '/profile/marketing/brandpage/is_marketer',

        // Courses apis
        courses_tags: endpoints.backendBase + '/webinars/tags',

        // Search apis
        search_instructors: endpoints.backendBase + '/search/instructors',
        search_organizations: endpoints.backendBase + '/search/organizations',
        search_organization_webinars: endpoints.backendBase + '/search/organizations/{organization}/webinars',
        search_organization_blogs: endpoints.backendBase + '/search/organizations/{organization}/posts',
        search_posts: endpoints.backendBase + '/search/posts',

        // Webinars apis
        // TODO: Bring {id} param in here
        webinar: endpoints.backendBase + '/webinars/',

        // Blog apis
        blogs_schema_main: endpoints.backendBase + '/blogs/schema/main',
        blogs_latest: endpoints.backendBase + '/blogs/latest',
        blogs_most_liked: endpoints.backendBase + '/blogs/most_liked',
        // TODO: Bring {id} param in here
        blogs: endpoints.backendBase + '/blogs/',
        blog_categories: endpoints.backendBase + '/blogs/categories/organizations/{organization_slug}',

        // Provinces apis
        provinces: endpoints.backendBase + '/location/provinces_with_cities',

        // Candidate apis
        candidate_show: endpoints.backendBase + '/candidate/{id}/show',

        // Site maps
        sitemap_index: endpoints.backendBase + '/sitemap/index',
        sitemap_blog_tags: endpoints.backendBase + '/sitemap/blogs/tags',
        sitemap_blog_categories: endpoints.backendBase + '/sitemap/blogs/categories',
        sitemap_blog_posts: endpoints.backendBase + '/sitemap/blogs/posts',
        sitemap_webinars: endpoints.backendBase + '/sitemap/webinars',
        sitemap_organizations_list: endpoints.backendBase + '/sitemap/organizations/list',
        sitemap_organizations_blogs: endpoints.backendBase + '/sitemap/organizations/blogs',
        sitemap_organizations_products: endpoints.backendBase + '/sitemap/organizations/products',
        sitemap_pages: endpoints.backendBase + '/sitemap/pages',
    };
}

export default endpoints;
