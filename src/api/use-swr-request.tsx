import api from './axios-config';
import useSWR, { responseInterface, ConfigInterface } from 'swr';
import { AxiosError, AxiosRequestConfig } from 'axios';
import { IApiResponseType } from './types';

export default function useSwrRequest<Type>(
    method: 'get' | 'post' | 'put' | 'delete',
    url: string,
    params?: Record<string, any>,
    fetcherConfig?: AxiosRequestConfig,
    swrConfig?: ConfigInterface,
): IApiResponseType<Type> {
    const defaultSWRConfig = {
        errorRetryCount: 0,
    };
    url = encodeURI(url);

    const swrConfigMorphed = Object.assign(defaultSWRConfig, swrConfig);

    const fetcher = function () {
        let promise;
        switch (method) {
            case 'get':
            case 'delete':
                promise = api[method](url, fetcherConfig);
                break;
            case 'post':
            case 'put':
                promise = api[method](url, params, fetcherConfig);
                break;
        }
        return promise.then((response) => response.data);
    };

    const { data, error }: responseInterface<IApiResponseType<Type>, AxiosError> = useSWR(
        url,
        fetcher,
        swrConfigMorphed,
    );

    if (error) {
        let result;
        if (error.response) {
            result = error.response.data;
        } else {
            result = {
                data: null,
                errors: error,
                message: error.message === 'Network Error' ? 'خطای اتصال به اینترنت' : error.message,
            };
        }
        if (typeof result !== 'object') {
            result = { data: result };
        }
        result.status = 'error';
        result.errorMeta = error;

        return result;
    } else if (!data) {
        return {
            data: null,
            errors: null,
            message: '',
            status: 'loading',
        };
    } else {
        return { ...data, status: 'data' };
    }
}
