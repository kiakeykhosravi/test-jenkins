import { AxiosError } from 'axios';

export interface IApiResponseType<dataType> {
    message: string;
    data: dataType;
    errors: Record<string, any>;
    status: 'data' | 'error' | 'loading';
    errorMeta?: AxiosError;
}
