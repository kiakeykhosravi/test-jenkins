import api from './axios-config';
import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import { IApiResponseType } from './types';

export default async function apiRequest<Type = any>(
    method: 'get' | 'post' | 'put' | 'delete',
    url: string,
    params?: Record<string, any>,
    onSuccess?: any,
    onFail?: any,
    config?: AxiosRequestConfig,
): Promise<IApiResponseType<Type>> {
    let promise;
    url = encodeURI(url);

    switch (method) {
        case 'get':
        case 'delete':
            promise = api[method](url, config);
            break;
        case 'post':
        case 'put':
            promise = api[method](url, params, config);
            break;
    }

    return await promise
        .then(
            onSuccess ||
                ((response: AxiosResponse) => {
                    return {
                        ...response.data,
                        status: 'data',
                    };
                }),
        )
        .catch(
            onFail ||
                ((error: AxiosError) => {
                    let result;
                    if (error.response) {
                        result = {
                            ...error.response.data,
                        };
                    } else {
                        result = {
                            data: null,
                            errors: error,
                            message:
                                error.message === 'Network Error'
                                    ? 'خطای اتصال به اینترنت'
                                    : error.message || 'خطایی در پردازش درخواست رخ داده است',
                        };
                    }
                    result.status = 'error';
                    result.errorMeta = error;
                    return result;
                }),
        );
}
