import axios, { AxiosInstance } from 'axios';
import endpoints from './api-endpoints';
import cookieCutter from 'cookie-cutter';
import { getAuthCookie } from 'utils/client-cookie-utils';

const api: AxiosInstance = axios.create({
    headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
    },
});

api.interceptors.request.use((config) => {
    if (cookieCutter && cookieCutter.get) {
        const token = getAuthCookie();

        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }
    }

    return config;
});

export default api;
