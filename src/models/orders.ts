import { IWebinarCompact } from './webinar-compact';

export enum OrderStatusEnum {
    IN_PROGRESS = 'in_progress',
    PAID = 'paid',
    REFUNDED = 'refunded',
    FAILED = 'failed',
}

export interface IOrder {
    id: number;
    user_id: number;
    webinar_id: number;
    price_final: number;
    rating: number;
    status: string;
    status_fa: string;
    webinar: IWebinarCompact;
}
