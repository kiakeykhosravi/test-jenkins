import { IMedia } from './media';
import { IOrganizationSlider } from './organization-slider';

export enum OrganizationType {
    SCHOOL = 'school',
    EDUCATIONAL_INSTITUTION = 'educational_institution',
    OTHER = 'other',
    BRANDPAGE_PREMIUM = 'brandpage_premium',
    BRANDPAGE_PLUS = 'brandpage_plus',
    BRANDPAGE_GOLDEN = 'brandpage_golden',
    BRANDPAGE_PLATINUM = 'brandpage_platinum',
}

// TODO: The question marks are because of some fake data used in some parts which are still pending api
export interface IOrganization {
    id: number;
    owner_id?: number;
    title: string;
    name: string;
    slug?: string;
    description?: string;
    slogan?: string;
    link: string;
    short_link: string;
    mobile_number: null | string[];
    phone_number: null | string[];
    address: null | string;
    instagram: null | string;
    telegram: null | string;
    twitter: null | string;
    aparat: null | string;
    youtube: null | string;
    whatsapp: null | string;
    whatsapp_business?: null | string;
    website: null | string;
    linkedin: null | string;
    email?: null | string;
    active: boolean;
    logo_id: number;
    settings?: any;
    is_school?: boolean;
    type: OrganizationType;
    type_fa: string;
    logo?: IMedia;
    media?: IMedia[];
    schema?: Record<string, any>;
    sliders?: IOrganizationSlider[];
}
