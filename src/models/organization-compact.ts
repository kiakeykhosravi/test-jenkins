import { IMedia } from './media';
import { OrganizationType } from './organization';

export interface IOrganizationCompact {
    id: number;
    title: string;
    name: string;
    slug: string;
    active: boolean;
    logo_id: number;
    type: OrganizationType;
    logo?: IMedia;
}
