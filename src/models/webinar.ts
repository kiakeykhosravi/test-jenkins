import { IMedia } from './media';
import { IOrganizationCompact } from './organization-compact';
import { IUserSafe } from './user-safe';
import { IMeeting } from './meeting';

export enum WebinarStatusEnum {
    OPEN = 'open',
    CLOSED = 'closed',
    FINISHED = 'finished',
}

export interface IWebinar {
    id: number;
    organization_id: number;
    manager_id: number;
    instructor_id: number;
    category_id: number;
    title: string;
    seo_description?: string;
    description: string;
    code: string;
    max_user: number;
    meetings_count: number;
    price: number;
    off: number;
    price_with_off: number;
    marketer_percent: number | null;
    status: string;
    active: boolean | number;
    allow_order: boolean | number;
    weight: number;
    closed: boolean | number;
    rating: number;
    created_at_jalali: string;
    field_webinar_code_required: boolean | number;
    field_discount_code: boolean | number;
    field_marketer_code: boolean | number;
    field_marketer_code_required: boolean | number;
    schema?: Record<string, any>;
    media: IMedia[];
    organization: IOrganizationCompact;
    instructor?: IUserSafe;
    instructors?: IUserSafe[];
    meetings?: IMeeting[];
}
