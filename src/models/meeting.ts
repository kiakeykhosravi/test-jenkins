import { IWebinarCompact } from './webinar-compact';

export enum MeetingStatusEnum {
    PLANNED = 'planned',
    RUNNING = 'running',
    CANCELED = 'canceled',
    FINISHED = 'finished',
}

export enum MeetingStatusFarsiEnum {
    PLANNED = 'در انتظار برگزاری',
    RUNNING = 'در حال برگزاری',
    CANCELED = 'لغو شده',
    FINISHED = 'پایان یافته',
}

export interface IMeeting {
    id: number;
    webinar_id: number;
    title: string;
    datetime: string;
    datetime_fadate: string;
    datetime_fatime: string;
    status: string;
    status_fa: string;
    is_quiz: boolean | number;
    is_meeting: true; // Always true
    active: boolean | number;
    links?: {
        title: string[];
        link: string[];
    };
    bbb_ready_for_play: boolean;
    bbb_published: boolean;
    organization_session_duration: string;
    access_level?: 'moderator' | 'attendee';
    can_enter: boolean;
    can_replay: boolean;
    webinar?: IWebinarCompact;
}
