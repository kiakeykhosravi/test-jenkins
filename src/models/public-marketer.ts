export interface IPublicMarketer {
    marketer_id: string | number;
    mobile: string;
    fullname: string;
    email?: string;
    whatsapp?: string;
    instagram?: string;
}
