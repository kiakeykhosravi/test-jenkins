export interface ITag {
    id: number;
    title: string;
    link?: string;
}
