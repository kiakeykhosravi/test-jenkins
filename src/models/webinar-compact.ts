import { IMedia } from './media';
import { IOrganizationCompact } from './organization-compact';
import { IUserSafe } from './user-safe';

export interface IWebinarCompact {
    id: number;
    organization_id: number;
    manager_id: number;
    instructor_id: number;
    category_id: number;
    title: string;
    price: number;
    off: number;
    price_with_off: number;
    marketer_percent: number | null;
    marketing_amount: number | null;
    active: boolean | number;
    weight: number;
    created_at_jalali: string;
    media: IMedia[];
    organization: IOrganizationCompact;
    instructor?: IUserSafe;
    instructors?: IUserSafe[];
}
