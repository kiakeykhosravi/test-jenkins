import { IMedia } from './media';

export enum CandidateTypeEnum {
    SCHOOL = 'school',
    EDUCATIONAL_INSTITUTION = 'educational_institution',
    OTHER = 'other',
    BRANDPAGE_PREMIUM = 'brandpage_premium',
    BRANDPAGE_PLUS = 'brandpage_plus',
    BRANDPAGE_GOLDEN = 'brandpage_golden',
    BRANDPAGE_PLATINUM = 'brandpage_platinum',
    SUBSCRIPTION = 'subscription',
}

export enum CandidateTypeFarsiEnum {
    SCHOOL = 'مدرسه',
    EDUCATIONAL_INSTITUTION = 'آموزشگاه',
    OTHER = 'سازمان',
    BRANDPAGE_PREMIUM = 'برند پیج پریمیوم',
    BRANDPAGE_PLUS = 'برند پیج پلاس',
    BRANDPAGE_GOLDEN = 'برند پیج طلایی',
    BRANDPAGE_PLATINUM = 'برند پیج پلاتینیوم',
    SUBSCRIPTION = 'مشاوره',
}

export enum CandidateTypeEnToFaEnum {
    'school' = 'مدرسه',
    'educational_institution' = 'آموزشگاه',
    'other' = 'سازمان',
    'brandpage_premium' = 'برند پیج پریمیوم',
    'brandpage_plus' = 'برند پیج پلاس',
    'brandpage_golden' = 'برند پیج طلایی',
    'brandpage_platinum' = 'برند پیج پلاتینیوم',
    'subscription' = 'مشاوره',
}

export interface ICandidate {
    id: number;
    name: string;
    owner_name: string;
    slogan?: null;
    type: CandidateTypeEnum;
    type_fa: CandidateTypeFarsiEnum;
    email?: string;
    address?: string;
    postal_code?: string;
    registration_number?: string;
    logo_id?: number;
    phone_number: string[];
    mobile_number: string[];
    whatsapp_business?: null;
    instagram?: string;
    telegram?: string;
    twitter?: string;
    aparat?: string;
    youtube?: string;
    whatsapp?: string;
    website?: string;
    logo?: IMedia;
    linkedin?: string;
    specialty?: string;
    specialty_id?: null;
}
