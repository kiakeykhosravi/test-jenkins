import { IUserSafe } from './user-safe';
import { ITag } from './tag';
import { IMedia } from './media';

export interface IBlogCompact {
    id: number;
    title: string;
    description: string;
    author: IUserSafe;
    likes_count?: number;
    comments_count?: number;
    tags: ITag[];
    media: IMedia[];
    created_at: string;
    created_at_fadate: string;
    updated_at: string;
    updated_at_fadate: string;
}
