export interface IBlogCategory {
    title: string;
    status: string;
    sub_categories?: IBlogCategory[];
    parent_category?: IBlogCategory | null;
    schema?: Record<string, any>;
}
