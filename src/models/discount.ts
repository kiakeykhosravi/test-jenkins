import { IWebinarCompact } from './webinar-compact';

export interface IDiscount {
    id: number;
    webinar_id: number;
    organization_id: number;
    title: string;
    code: string;
    active: boolean | number;
    webinar: IWebinarCompact;
}
