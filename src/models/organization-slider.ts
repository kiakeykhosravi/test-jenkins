import { IOrganizationSliderProduct } from './organization-slider-product';

export interface IOrganizationSlider {
    id: number;
    title: string;
    organization_id: number;
    products?: IOrganizationSliderProduct[];
    active: boolean | number;
}
