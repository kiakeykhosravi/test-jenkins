import { IMedia } from './media';

export interface ISlide {
    id: number;
    title: string;
    url: string;
    media_id: number;
    location: string;
    active: number;
    media: IMedia;
}
