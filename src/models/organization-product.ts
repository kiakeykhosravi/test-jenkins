import { IMedia } from './media';

export interface IOrganizationProduct {
    id: number;
    title: string;
    short_description?: string;
    description: string;
    external_url?: string;
    is_external: boolean | number;
    custom_product_id?: string;
    active: boolean | number;
    media?: IMedia[];
}
