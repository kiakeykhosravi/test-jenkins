// TODO: The question marks are because of some fake data used in some parts which are still pending api
import { RoleEnum, RoleFaEnum } from './role';

export interface IUser {
    id: number;
    fullname: string;
    email: string;
    mobile: string;
    label?: string;
    slug?: string;
    description: string;
    whatsapp?: string;
    instagram?: string;
    national_code?: string;
    city_id?: string;
    marketer_code?: string;
    role: RoleEnum;
    role_fa: RoleFaEnum;
    created_at?: string;
}
