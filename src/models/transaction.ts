export interface ITransaction {
    id: number;
    via_fa: string;
    type_fa: string;
    amount: number;
    success: boolean;
    description: string;
    created_at: string;
    created_at_fadate: string;
    created_at_fatime: string;
}
