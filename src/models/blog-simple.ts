import { IMedia } from './media';

export interface IBlogSimple {
    id: number;
    title: string;
    likes_count?: number;
    comments_count?: number;
    media: IMedia[];
    created_at: string;
    created_at_fadate: string;
}
