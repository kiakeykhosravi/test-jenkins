import { IUserSafe } from './user-safe';

export interface IComment {
    id: number;
    user_id: number;
    user: IUserSafe;
    text: string;
    created_at: string;
    created_at_fadate: string;
    created_at_fatime: string;
}
