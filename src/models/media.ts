export interface IMedia {
    id: number;
    storage: string;
    title: string;
    url: string;
    thumbnail_url: string;
    secret: string;
    mime_type: string;
    size: number;
}
