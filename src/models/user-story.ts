import { IMedia } from './media';

export interface IUserStory {
    id: number;
    author_name: string;
    organization_name: string;
    message: string;
    media_id: number;
    active: true;
    media: IMedia;
}
