export enum RoleEnum {
    ADMIN = 'admin',
    MANAGER = 'manager',
    SUPPORT = 'support',
    MARKETER = 'marketer',
    INSTRUCTOR = 'instructor',
    STUDENT = 'student',
    FINANCIAL = 'financial',
    ORGANIZATION_MANAGER = 'organization_manager',
    ORGANIZATION_SUPPORT = 'organization_support',
    ORGANIZATION_LEGAL_USER = 'organization_legal_user',
}

export enum RoleFaEnum {
    'admin' = 'ادمین',
    'manager' = 'مدیر',
    'support' = 'پشتیبان',
    'marketer' = 'سفیر',
    'instructor' = 'استاد',
    'student' = 'کاربر',
    'financial' = 'حسابدار',
    'organization_manager' = 'ادمین سازمان',
    'organization_support' = 'پشتیبان سازمان',
    'organization_legal_user' = 'شخص حقوقی سازمان',
}
