export interface IUserSafe {
    id: number;
    fullname: string;
    description?: string;
}
