import { ITag } from './tag';

export interface IPost {
    link: string;
    title: string;
    date: string;
    banner: string;
    description: string;
    tags: ITag[];
    author: string;
    comments: number;
    likes: number;
}
