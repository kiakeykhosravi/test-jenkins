export interface ITeamMember {
    img: string;
    name: string;
    job: string;
    description: string;
}
