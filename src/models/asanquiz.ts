export enum AsanquizStateEnum {
    HIDDEN = 'hidden',
    PLANNED = 'planned',
    ONGOING = 'ongoing',
    PENDING_SUBMIT = 'pending_submit',
    SUBMITTING = 'submitting',
    INITIAL_SUBMIT = 'initial_submit',
    FINALIZED = 'finalized',
}

export interface IAsanquiz {
    id: number;
    title: string;
    start_date: Date | string;
    end_date: Date | string;
    start_date_fadate: string;
    start_date_fatime: string;
    end_date_fadate: string;
    end_date_fatime: string;
    duration: number;
    state: AsanquizStateEnum;
    state_fa: string;
    can_enter: boolean;
    action_button_text: string;
    webinar_id?: number;
    organization_id?: number;
    is_asanquiz: true;
}
