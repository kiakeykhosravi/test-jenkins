export interface ISpecialty {
    id: number;
    title: string;
    active: boolean;
}
