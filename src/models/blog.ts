import { IUserSafe } from './user-safe';
import { IOrganizationCompact } from './organization-compact';
import { IBlogCategory } from './blog-category';
import { ITag } from './tag';
import { IMedia } from './media';
import { IComment } from './comment';

export interface IBlog {
    id: number;
    link: string;
    title: string;
    description?: string;
    body: string;
    author: IUserSafe;
    is_liked?: boolean;
    likes_count?: number;
    study_time: number;
    comments_count: number;
    comments?: IComment[];
    organization?: IOrganizationCompact;
    category: IBlogCategory;
    tags: ITag[];
    media: IMedia[];
    schema?: Record<string, any>;
    created_at: string;
    created_at_fadate: string;
    created_at_fatime: string;
    created_at_diff_for_human: string;
}
