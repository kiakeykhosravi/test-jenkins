import { ICity } from './city';

export interface IProvince {
    id: number;
    name: string;
    cities: ICity[];
}
