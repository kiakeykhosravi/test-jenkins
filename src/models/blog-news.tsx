import { IMedia } from './media';

export interface IBlogNews {
    id: number;
    title: string;
    link: string;
    created_at: string;
    created_at_fadate: string;
    media: IMedia;
}
