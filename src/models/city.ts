import { IProvince } from './province';

export interface ICity {
    id: number;
    name: string;
    province_id: number;
    province: IProvince;
}
