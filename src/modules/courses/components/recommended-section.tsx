import React from 'react';
import ListSection from 'modules/shared/components/list-section';
import styles from '../styles/recommended-section.module.scss';
import { IWebinarCompact } from 'models/webinar-compact';

export default function RecommendedSection(props: { data: IWebinarCompact[] }) {
    return (
        <>
            <h3 className={styles.title}>دوره های جدید</h3>
            <ListSection data={props.data} />
        </>
    );
}
