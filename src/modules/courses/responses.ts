import { ITag } from 'models/tag';

export interface ICoursesTagsResponses {
    tags: ITag[];
}
