import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { ITag } from 'models/tag';
import { ICoursesTagsResponses } from '../responses';

export default async function apiCoursesTags(
    isBackend = false,
    isAsanseminar = false,
    limit?: number,
): Promise<{ data?: ITag[]; status: 'error' | 'data' }> {
    let url = isBackend ? endpoints.backend.courses_tags : endpoints.frontend.courses_tags;
    url += isAsanseminar ? '?asanseminar=1' : '?asanseminar=0';
    url += limit ? `&limit=${limit}` : '';
    const response = await apiRequest<ICoursesTagsResponses>('get', url);

    if (response.status === 'data') {
        return {
            data: response.data.tags,
            status: 'data',
        };
    }
    return {
        status: 'error',
    };
}
