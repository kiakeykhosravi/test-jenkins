import endpoints from 'api/api-endpoints';
import apiSitemap from './api-sitemap';

export default async function apiSitemapOrganizationsBlogs() {
    return await apiSitemap(endpoints.backend.sitemap_organizations_blogs);
}
