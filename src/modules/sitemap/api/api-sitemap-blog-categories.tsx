import endpoints from 'api/api-endpoints';
import apiSitemap from './api-sitemap';

export default async function apiSitemapBlogCategories() {
    return await apiSitemap(endpoints.backend.sitemap_blog_categories);
}
