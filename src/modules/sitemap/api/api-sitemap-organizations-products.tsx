import endpoints from 'api/api-endpoints';
import apiSitemap from './api-sitemap';

export default async function apiSitemapOrganizationsProducts() {
    return await apiSitemap(endpoints.backend.sitemap_organizations_products);
}
