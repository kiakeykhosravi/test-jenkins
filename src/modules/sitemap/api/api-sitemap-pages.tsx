import endpoints from 'api/api-endpoints';
import apiSitemap from './api-sitemap';

export default async function apiSitemapPages() {
    return await apiSitemap(endpoints.backend.sitemap_pages);
}
