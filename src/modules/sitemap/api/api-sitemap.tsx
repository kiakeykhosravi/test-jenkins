import api from 'api/axios-config';
import { AxiosResponse } from 'axios';

export default async function apiSitemap(endpoint: string) {
    return await api['get'](endpoint)
        .then((response: AxiosResponse) => {
            if (response.status >= 200 && response.status <= 204) {
                return response.data;
            }
            return '<?xml version="1.0" encoding="UTF-8"?>';
        })
        .catch((error) => {
            return '<?xml version="1.0" encoding="UTF-8"?>';
        });
}
