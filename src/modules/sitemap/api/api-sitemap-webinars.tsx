import endpoints from 'api/api-endpoints';
import apiSitemap from './api-sitemap';

export default async function apiSitemapWebinars() {
    return await apiSitemap(endpoints.backend.sitemap_webinars);
}
