import endpoints from 'api/api-endpoints';
import apiSitemap from './api-sitemap';

export default async function apiSitemapBlogTags() {
    return await apiSitemap(endpoints.backend.sitemap_blog_tags);
}
