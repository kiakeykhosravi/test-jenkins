import endpoints from 'api/api-endpoints';
import apiSitemap from './api-sitemap';

export default async function apiSitemapOrganizationsList() {
    return await apiSitemap(endpoints.backend.sitemap_organizations_list);
}
