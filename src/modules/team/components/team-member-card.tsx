import React from 'react';
import styles from 'modules/team/styles/team-member-card.module.scss';
import { Col, Image } from 'react-bootstrap';
import toPersianNum from 'utils/to-persian-num';

export default function TeamMemberCard(props: { data?: any }) {
    return (
        <Col sm={6} md={4} lg={3} className={styles['col']}>
            <div className={styles['card']}>
                <div className={styles['card-img-container']}>
                    <Image src={props.data.img} className={styles['img']} alt={props.data.name} />
                </div>
                <div className={styles['body']}>
                    <h2 className={styles['name']}>{toPersianNum(props.data.name)}</h2>
                    <div className={styles['job']}>{toPersianNum(props.data.job)}</div>
                    <div className={styles['description']}>{toPersianNum(props.data.description)}</div>
                </div>
            </div>
        </Col>
    );
}
