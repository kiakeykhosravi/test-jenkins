import React from 'react';
import Section from 'modules/shared/components/section';
import classNames from 'classnames';
import styles from '../styles/team-member-section.module.scss';
import { Container, Row } from 'react-bootstrap';
import TeamMemberCard from './team-member-card';

export default function TeamMemberSection(props: { data?: any; classes?: string }) {
    const renderTeamMemberCards = () => {
        if (props.data.length) {
            return props.data.map((item: any, index: number) => {
                return <TeamMemberCard data={item} key={index} />;
            });
        }
        return null;
    };
    return (
        <Section classes={classNames(props.classes, styles['bg-light'])} itemsData={props.data}>
            <Container className="overflow-hidden">
                <Row className={styles['row']}>{renderTeamMemberCards()}</Row>
            </Container>
        </Section>
    );
}
