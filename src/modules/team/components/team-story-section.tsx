import React from 'react';
import styles from '../styles/team-story-section.module.scss';
import { Col, Container, Image, Nav, Row, Tab } from 'react-bootstrap';
import toPersianNum from 'utils/to-persian-num';
import DOMPurify from 'isomorphic-dompurify';

export default function TeamStorySection(props: { data: any }) {
    const datesImages = [
        '/images/team/team-story-1.png',
        '/images/team/team-story-2.png',
        '/images/team/team-story-3.png',
    ];

    const tabs = () => {
        if (props.data) {
            return props.data.map((item: any, index: number) => {
                return (
                    <Nav.Item key={index}>
                        <Nav.Link eventKey={`tab-${index + 1}`} className={styles['tab']}>
                            {toPersianNum(item.tab)}
                        </Nav.Link>
                    </Nav.Item>
                );
            });
        }
        return null;
    };
    const tabsContent = () => {
        if (props.data) {
            return props.data.map((item: any, index: number) => {
                return (
                    <Tab.Pane eventKey={`tab-${index + 1}`} key={index} className={styles.content}>
                        <Col sm={12} lg={6} className={styles['content-title-container']}>
                            <h2 className={styles['content-title']}>{item.title}</h2>
                            <div
                                dangerouslySetInnerHTML={{
                                    __html: DOMPurify.sanitize(item.content),
                                }}
                            />
                        </Col>
                        <Col sm={12} lg={6} className={styles['content-image-container']}>
                            <Image src={datesImages[index]} className={styles['img']} alt={'تاریخچه آسان سمینار'} />
                        </Col>
                    </Tab.Pane>
                );
            });
        }
        return null;
    };
    return (
        <Container className={'team-story-section'}>
            <Tab.Container defaultActiveKey="tab-1">
                <Row>
                    <div className={styles['connect-line']} />
                    <Nav variant="pills" className={styles['tabs']}>
                        {tabs()}
                    </Nav>
                </Row>
                <Row className={styles['tabs-content']}>
                    <Tab.Content>{tabsContent()}</Tab.Content>
                </Row>
            </Tab.Container>
        </Container>
    );
}
