import React from 'react';
import { ITag } from 'models/tag';
import { IWebinar } from 'models/webinar';
import { IOrganizationCompact } from 'models/organization-compact';
import { IUserSafe } from 'models/user-safe';
import styles from '../styles/header-dropdown.module.scss';
import classNames from 'classnames';
import TagsList from 'modules/shared/components/tags-list';
import Link from 'next/link';
import toPersianNum from 'utils/to-persian-num';

const cn = classNames;

export default function HeaderDropdown(props: {
    tags: ITag[] | [];
    webinars: IWebinar[] | [];
    organizations: IOrganizationCompact[] | [];
    instructors: IUserSafe[] | [];
    isLoading?: boolean;
}) {
    const { webinars, organizations, instructors, tags } = { ...props };

    const hasItems = tags.length || webinars.length || organizations.length || instructors.length;

    return (
        <div className={styles.main}>
            <div className={styles['webinars-section']}>{renderWebinars(webinars)}</div>
            {renderLoadingOrNotfound(hasItems, props.isLoading)}
            {hasItems ? (
                <div className={styles['bottom-section']}>
                    {renderTagsRow(tags)}
                    {renderOrganizationsRow(organizations)}
                    {renderInstructorsRow(instructors)}
                </div>
            ) : null}
        </div>
    );
}

function renderLoadingOrNotfound(hasItems: number, isLoading?: boolean) {
    if (isLoading) {
        return <div className={styles['loading']} />;
    }
    if (!hasItems) {
        return <div className={styles['notfound']}>هیچ نتیجه ای یافت نشد</div>;
    }
    return null;
}

function renderTagsRow(tags: ITag[]) {
    return tags.length ? (
        <div className={cn(styles.row, styles['tags-row'])}>
            <span className={styles['row-title']}>تگ ها</span>
            <TagsList
                data={tags}
                containerClasses={styles['tags-list']}
                textClasses={styles['tag-item']}
                areLinks={true}
            />
        </div>
    ) : null;
}

function renderOrganizationsRow(organizations: IOrganizationCompact[]) {
    const renderOrganizations = () => {
        return organizations.map((organization) => {
            return (
                <Link href={`/organizations/${organization.slug}`} key={organization.id}>
                    <span className={cn(styles.item, styles['organization-item'])}>
                        <img
                            className={styles['organization-item-logo']}
                            src={organization.logo && organization.logo.thumbnail_url}
                            alt={organization.title}
                        />
                        <span>{organization.title}</span>
                    </span>
                </Link>
            );
        });
    };

    return organizations.length ? (
        <div className={cn(styles.row, styles['organizations-row'])}>
            <span className={styles['row-title']}>سازمان ها</span>
            <span>{renderOrganizations()}</span>
        </div>
    ) : null;
}

function renderInstructorsRow(instructors: IUserSafe[]) {
    const renderInstructors = () => {
        return instructors.map((instructor) => {
            return (
                // <Link href={`/instructors/${instructor.id}/${instructor.fullname}`} key={instructor.id}>
                <span key={instructor.id} className={cn(styles.item, styles['instructor-item'])}>
                    {instructor.fullname}
                </span>
                // </Link>
            );
        });
    };

    return instructors.length ? (
        <div className={cn(styles.row, styles['instructors-row'])}>
            <span className={styles['row-title']}>اساتید</span>
            <span>{renderInstructors()}</span>
        </div>
    ) : null;
}

function renderWebinars(webinars: IWebinar[]) {
    const limitedWebinars = webinars.slice(0, Math.min(5, webinars.length));

    return limitedWebinars.map((webinar) => {
        return (
            <div className={styles.row} key={webinar.id}>
                <Link href={`/webinars/${webinar.id}/${webinar.title}`} key={webinar.id}>
                    <div className={styles.webinar}>
                        <div className={styles['webinar-logo-container']}>
                            <img
                                className={styles['webinar-logo']}
                                src={webinar.media && webinar.media.length && webinar.media[0].thumbnail_url}
                            />
                        </div>
                        <span className={styles['webinar-title']}>{webinar.title}</span>
                        <span className={styles['webinar-instructor']}>
                            استاد: {webinar.instructor && webinar.instructor.fullname}
                        </span>
                        <span className={styles['webinar-date']}>{toPersianNum(webinar.created_at_jalali)}</span>
                    </div>
                </Link>
            </div>
        );
    });
}
