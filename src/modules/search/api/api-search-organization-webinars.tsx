import endpoints from 'api/api-endpoints';
import { ISearchWebinarsResponse } from '../responses';
import { IApiResponseType } from 'api/types';
import apiRequest from 'api/api-request';

export default async function apiSearchOrganizationWebinars(
    isBackend = false,
    data?: {
        phrase?: string;
        page?: number;
        organization?: string;
        organizationID?: number;
        organizationSlug?: string;
        date?: string;
        price?: string;
        tags?: string;
        sortDate?: string;
    },
): Promise<IApiResponseType<ISearchWebinarsResponse>> {
    data = data || {};
    const url = generateUrl(
        isBackend,
        data.phrase,
        data.page,
        data.organization,
        data.organizationID,
        data.organizationSlug,
        data.date,
        data.price,
        data.tags,
        data.sortDate,
    );

    return await apiRequest<ISearchWebinarsResponse>('get', url);
}

function generateUrl(
    isBackend: boolean,
    phrase?: string,
    page?: number,
    organization?: string,
    organizationID?: number,
    organizationSlug?: string,
    date?: string,
    price?: string,
    tags?: string,
    sortDate?: string,
) {
    const queryParamList: string[] = [];
    organizationID !== undefined ? queryParamList.push(`organization=${organizationID}`) : '';
    organizationSlug !== undefined ? queryParamList.push(`organization-slug=${organizationSlug}`) : '';
    date ? queryParamList.push(`date=${date}`) : '';
    price ? queryParamList.push(`price=${price}`) : '';
    tags ? queryParamList.push(`tags=${tags}`) : '';
    sortDate ? queryParamList.push(`sort-date=${sortDate}`) : '';
    phrase !== undefined && phrase.length ? queryParamList.push(`q=${phrase}`) : '';
    page > 1 ? queryParamList.push(`page=${page}`) : '';

    let queryParams = queryParamList.length ? '?' : '';

    for (const param of queryParamList) {
        queryParams += param + '&';
    }

    return (
        (isBackend
            ? endpoints.backend.search_organization_webinars
            : endpoints.frontend.search_organization_webinars
        ).replace('{organization}', organizationSlug) + queryParams
    );
}
