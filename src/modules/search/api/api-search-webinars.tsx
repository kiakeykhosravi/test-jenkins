import endpoints from 'api/api-endpoints';
import { ISearchWebinarsResponse } from '../responses';
import { IApiResponseType } from 'api/types';
import apiRequest from 'api/api-request';

export default async function apiSearchWebinars(data?: {
    phrase?: string;
    page?: number;
    organization?: string;
    organizationSlug?: string;
    date?: string;
    price?: string;
    tags?: string;
    sortDate?: string;
}): Promise<IApiResponseType<ISearchWebinarsResponse>> {
    data = data || {};
    const url = generateUrl(
        data.phrase,
        data.page,
        data.organization,
        data.organizationSlug,
        data.date,
        data.price,
        data.tags,
        data.sortDate,
    );

    return await apiRequest<ISearchWebinarsResponse>('get', url);
}

function generateUrl(
    phrase?: string,
    page?: number,
    organization?: string,
    organizationSlug?: string,
    date?: string,
    price?: string,
    tags?: string,
    sortDate?: string,
) {
    const queryParamList: string[] = [];
    organization !== undefined ? queryParamList.push(`organization=${organization}`) : '';
    organizationSlug !== undefined ? queryParamList.push(`organization-slug=${organizationSlug}`) : '';
    date ? queryParamList.push(`date=${date}`) : '';
    price ? queryParamList.push(`price=${price}`) : '';
    tags ? queryParamList.push(`tags=${tags}`) : '';
    sortDate ? queryParamList.push(`sort-date=${sortDate}`) : '';
    phrase !== undefined && phrase.length ? queryParamList.push(`q=${phrase}`) : '';
    page > 1 ? queryParamList.push(`page=${page}`) : '';

    let queryParams = queryParamList.length ? '?' : '';

    for (const param of queryParamList) {
        queryParams += param + '&';
    }

    return endpoints.frontend.search_webinars + queryParams;
}
