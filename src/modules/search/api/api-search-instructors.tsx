import endpoints from 'api/api-endpoints';
import { ISearchInstructorsResponse } from '../responses';
import { IApiResponseType } from 'api/types';
import apiRequest from 'api/api-request';

export default async function apiSearchInstructors(
    isBackend = false,
    phrase?: string,
    page?: number,
): Promise<IApiResponseType<ISearchInstructorsResponse>> {
    const url = generateUrl(isBackend, phrase, page);

    return await apiRequest<ISearchInstructorsResponse>('get', url);
}

function generateUrl(isBackend: boolean, phrase?: string, page?: number) {
    const phraseParam = phrase !== undefined && phrase !== null ? `q=${phrase}` : '';
    const pageParam = page > 1 ? `page=${page}` : '';

    const queryParams = phraseParam || pageParam ? `?${phraseParam}&${pageParam}` : '';

    return (isBackend ? endpoints.backend.search_instructors : endpoints.frontend.search_instructors) + queryParams;
}
