import endpoints from 'api/api-endpoints';
import { ISearchHeaderResponse } from '../responses';
import { IApiResponseType } from 'api/types';
import apiRequest from 'api/api-request';

export default function apiSearchHeader(phrase?: string): Promise<IApiResponseType<ISearchHeaderResponse>> {
    const url = endpoints.frontend.search_header + (phrase !== undefined && phrase !== null ? `?q=${phrase}` : '');

    return apiRequest<ISearchHeaderResponse>('get', url);
}
