import { IWebinarCompact } from 'models/webinar-compact';
import { IUserSafe } from 'models/user-safe';
import { ITag } from 'models/tag';
import { IOrganizationCompact } from 'models/organization-compact';

export interface ISearchHeaderResponse {
    organizations: IOrganizationCompact[] | [];
    webinars: IWebinarCompact[] | [];
    instructors: IUserSafe[] | [];
    tags: ITag[] | [];
}

export interface ISearchOrganizationsResponse {
    organizations: IOrganizationCompact[] | [];
    lastpage: number;
}

export interface ISearchWebinarsResponse {
    webinars: IWebinarCompact[] | [];
    lastpage: number;
}

export interface ISearchInstructorsResponse {
    instructors: IUserSafe[] | [];
    lastpage: number;
}
