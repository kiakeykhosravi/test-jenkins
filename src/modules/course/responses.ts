import { IWebinar } from 'models/webinar';
import { EventType, MixedEventType } from './types';
import { IDiscount } from 'models/discount';
import { ITag } from 'models/tag';

export interface IDiscountCheckResponse {
    discount: IDiscount;
    discount_amount: number;
}

export interface IWebinarResponse {
    webinar: IWebinar;
    is_instructor: boolean;
    can_create_asanquiz: boolean;
    registered: boolean;
    full_capacity: boolean;
    user_credit: number;
    marketer_data: string | any;
    events_all: MixedEventType[] | string;
    events_today: EventType[];
    tags?: ITag[];
}

export interface IWebinarMeetingsResponse {
    events_all: MixedEventType[] | string;
}

export interface IOrderResponse {
    link?: string;
}

export interface ISubmitRatingResponse {
    rating: number;
}

export interface IMeetingEnterOrReplayResponse {
    link: string;
}

export interface IAsanquizEnterResponse {
    link: string;
}

export interface IAsanquizCreateResponse {
    link: string;
}
