import { IMeeting, MeetingStatusEnum } from 'models/meeting';
import { AsanquizStateEnum, IAsanquiz } from 'models/asanquiz';

export type EventType = IMeeting | IAsanquiz;
export type MixedEventType = IMeetingCalendarEvent | IAsanquizCalendarEvent | IMeeting | IAsanquiz;
export type CalendarEventType = IMeetingCalendarEvent | IAsanquizCalendarEvent;

export interface IMeetingCalendarEvent {
    _customEnterUrl?: string; // It still exists but is deprecated
    _customOrganizationId?: string;
    _customReplayUrl?: string; // It still exists but is deprecated
    _customServerId?: string;
    _customStatus: MeetingStatusEnum;
    _customCanEnter: boolean;
    _customCanReplay: boolean;
    _customWebinarId: number;
    _customMeetingId: number;
    title: string;
    start: Date | string;
    end: Date | string;
    url?: string;
    color?: string;
    is_quiz: number | boolean;
    is_meeting: true; // Always true
    videoquiz_answer_link: string;
    videoquiz_ready: 'ready' | 'not_ready';
}

export interface IAsanquizCalendarEvent {
    _customActionButtonText: string;
    _customCanEnter: boolean;
    _customAsanquizId: number;
    _customWebinarId: number;
    _customOrganizationId?: number;
    _customStatus: AsanquizStateEnum;
    _customStatusFa: string;
    color: string;
    end: Date | string;
    is_asanquiz: true; // Always true
    start: Date | string;
    title: string;
    url?: string;
}

export interface IIcons {
    text: string;
    count: string | number;
    img: string;
}
