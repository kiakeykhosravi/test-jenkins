import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IWebinarResponse } from '../responses';
import { AxiosRequestConfig } from 'axios';

export default async function apiWebinar(
    isBackend = false,
    id: string,
    config?: AxiosRequestConfig,
): Promise<{
    data?: IWebinarResponse;
    status: 'error' | 'data';
    errors?;
}> {
    const url = isBackend ? endpoints.backend.webinar + id : endpoints.frontend.webinar + id;
    const result = await apiRequest<IWebinarResponse>('get', url, undefined, undefined, undefined, config);

    if (result.status === 'data') {
        return {
            data: result.data,
            status: result.status,
        };
    }

    return {
        status: 'error',
        errors: result.errors,
    };
}
