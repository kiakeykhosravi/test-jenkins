import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IOrderResponse } from '../responses';
import { IApiResponseType } from 'api/types';
import { toast } from 'react-toastify';

export default function apiOrder(
    webinar: string,
    params: {
        discount?: number;
        marketer_code?: string;
        webinar_code?: string;
        payment_type?: string;
    },
): Promise<IOrderResponse> {
    const url = endpoints.frontend.order.replace('{webinar}', webinar);

    return apiRequest<IOrderResponse>('post', url, { ...params })
        .then((response: IApiResponseType<IOrderResponse>) => {
            if (response.status === 'data') {
                toast.success(response.message || 'درحال انتقال به صفحه پرداخت ...');
                return response.data;
            }

            toast.error(response.message || 'خطایی در هنگام ثبت نام رخ داد');
            return null;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در هنگام ثبت نام رخ داد');
            return error;
        });
}
