import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IDiscountCheckResponse } from '../responses';
import { toast } from 'react-toastify';
import { IApiResponseType } from 'api/types';

export default function apiDiscountCheck(discount: string, webinar: string | number): Promise<IDiscountCheckResponse> {
    return apiRequest<IDiscountCheckResponse>(
        'get',
        endpoints.frontend.discounts_check_webinar.replace('{discount_code}', discount) + webinar,
    )
        .then((response: IApiResponseType<IDiscountCheckResponse>) => {
            if (response.status === 'data') {
                return response.data;
            }
            toast.error(response.message || 'خطایی در اعمال کد تخفیف رخ داد');
            return null;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در اعمال کد تخفیف رخ داد');
            return error;
        });
}
