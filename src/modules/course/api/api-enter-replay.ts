import apiRequest from 'api/api-request';
import { IMeetingEnterOrReplayResponse } from '../responses';
import endpoints from 'api/api-endpoints';
import { toast } from 'react-toastify';

export default function apiEnterReplay(meeting: number) {
    const url = endpoints.frontend.meeting_replay.replace('{meeting}', String(meeting));
    return apiRequest<IMeetingEnterOrReplayResponse>('get', url)
        .then((response) => {
            if (response.status === 'data') {
                toast.success(response.message || 'شما در حال انتقال به بازپخش می باشید', {
                    autoClose: 5000,
                });

                return response.data.link;
            }
            toast.error((response && response.message) || 'خطایی در ورود به بازپخش رخ داد', { autoClose: 15000 });

            return null;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در ورود به بازپخش رخ داد', { autoClose: 5000 });
        });
}
