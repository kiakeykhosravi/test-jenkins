import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { ISubmitRatingResponse } from '../responses';
import { IApiResponseType } from 'api/types';
import { toast } from 'react-toastify';

export default function apiSubmitRating(webinar: string, rating: number): Promise<ISubmitRatingResponse> {
    const url = endpoints.frontend.webinar_rate.replace('{webinar}', webinar);

    return apiRequest<ISubmitRatingResponse>('post', url, { rating })
        .then((response: IApiResponseType<ISubmitRatingResponse>) => {
            if (response.status === 'data') {
                toast.success(response.message);
                return response.data;
            }

            toast.error(response.message || 'خطایی در هنگام ثبت امتیاز داد');
            return null;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در هنگام ثبت امتیاز داد');
            return error;
        });
}
