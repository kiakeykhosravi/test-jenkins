import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IWebinarMeetingsResponse, IWebinarResponse } from '../responses';
import { toast } from 'react-toastify';

export default function apiWebinarMeetings(webinarId: string, startDate: string, endDate: string) {
    const url =
        endpoints.frontend.webinar_meetings.replace('{webinar}', webinarId) +
        `?startdate=${startDate}&enddate=${endDate}`;

    return apiRequest<IWebinarMeetingsResponse>('get', url)
        .then((response) => {
            if (response.status === 'data') {
                return response.data;
            }
            toast.error(
                (response && response.message) ||
                    'خطایی در دریافت جلسات این ماه رخ داد. لطفا کمی صبر کنید و سپس صفحه خود را رفرش کنید.',
                { autoClose: 7500 },
            );
            return null;
        })
        .catch((error) => {
            toast.error(
                (error && error.message) ||
                    'خطایی در دریافت جلسات این ماه رخ داد. لطفا کمی صبر کنید و سپس صفحه خود را رفرش کنید',
                { autoClose: 7500 },
            );
        });
}
