import apiRequest from 'api/api-request';
import { IAsanquizCreateResponse } from '../responses';
import endpoints from 'api/api-endpoints';
import { toast } from 'react-toastify';

export default function apiCreateAsanquiz(webinar: number) {
    const url = endpoints.frontend.asanquiz_create.replace('{webinar}', String(webinar));
    return apiRequest<IAsanquizCreateResponse>('post', url)
        .then((response) => {
            if (response.status === 'data') {
                toast.success(response.message || 'شما در حال انتقال به صفحه ایجاد آسان آزمون می باشید', {
                    autoClose: 5000,
                });

                return response.data.link;
            }
            toast.error((response && response.message) || 'خطایی در ایجاد آسان آزمون رخ داد.', { autoClose: 15000 });

            return null;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در ایجاد آسان آزمون رخ داد', { autoClose: 5000 });
        });
}
