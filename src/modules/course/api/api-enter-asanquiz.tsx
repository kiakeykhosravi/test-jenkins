import apiRequest from 'api/api-request';
import { IAsanquizEnterResponse } from '../responses';
import endpoints from 'api/api-endpoints';
import { toast } from 'react-toastify';

export default function apiEnterAsanquiz(webinar: number, asanquiz: number) {
    const url = endpoints.frontend.asanquiz_enter
        .replace('{webinar}', String(webinar))
        .replace('{asanquiz}', String(asanquiz));
    return apiRequest<IAsanquizEnterResponse>('get', url)
        .then((response) => {
            if (response.status === 'data') {
                toast.success(response.message || 'شما در حال انتقال به آسان آزمون می باشید', {
                    autoClose: 5000,
                });

                return response.data.link;
            }
            toast.error((response && response.message) || 'خطایی در ورود به آسان آزمون رخ داد.', { autoClose: 15000 });

            return null;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در ورود به آسان آزمون رخ داد', { autoClose: 5000 });
        });
}
