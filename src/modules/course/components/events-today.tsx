import React from 'react';
import styles from '../styles/meetings-today.module.scss';
import { EventType } from '../types';
import EventsTabMainSection from './events-tab-main-section';

export default function EventsToday(props: { events: EventType[] }) {
    return props.events && props.events.length ? (
        <div className={styles.meetings}>
            <div className={styles.title}>جلسات امروز</div>
            <EventsTabMainSection data={props.events} />
        </div>
    ) : (
        <div className={styles['no-meeting']}>امروز هیچ جلسه‌ای برای این کلاس تعیین نشده است</div>
    );
}
