import React, { useEffect, useRef, useState } from 'react';
import styles from '../styles/rating.module.scss';
import { getTrackBackground, Range } from 'react-range';
import toPersianNum from 'utils/to-persian-num';

export default function Rating({ totalRating, submitUserRating, currentUserRating }) {
    const emojiRefs = [useRef(null), useRef(null), useRef(null), useRef(null), useRef(null)];
    const [userRating, setUserRating] = useState(currentUserRating || 0);
    const [activeEmojiIndex, setActiveEmojiIndex] = useState(-1);

    const onChangeRatingHandler = (values: number[]) => {
        setUserRating(() => {
            const newIndex = Math.min(Math.floor(values[0] / 20), 4);
            if (activeEmojiIndex !== newIndex) {
                if (activeEmojiIndex !== -1) {
                    emojiRefs[activeEmojiIndex].current.style.opacity = '0';
                }
                setActiveEmojiIndex(newIndex);
                emojiRefs[newIndex].current.style.opacity = '1';
            }
            return values[0];
        });
    };

    const onClickSubmitRating = () => {
        if (userRating > 0) {
            submitUserRating(userRating);
        }
    };

    useEffect(() => {
        if (totalRating > 0) {
            const index = Math.min(Math.floor(totalRating / 20), 4);
            setActiveEmojiIndex(index);
            emojiRefs[index].current.style.opacity = '1';
        }
    }, []);

    const emojiImages = [
        '/images/emojies/slightly-frowning-face.png',
        '/images/emojies/neutral-face.png',
        '/images/emojies/slightly-smiling-face.png',
        '/images/emojies/smiling-face-with-heart-eyes.png',
        '/images/emojies/partying-face.png',
    ];
    return (
        <>
            <div className={styles.emojies} onClick={onClickSubmitRating}>
                {renderEmojies(emojiImages, emojiRefs)}
            </div>
            <div className={styles.rating}>
                <Range
                    step={1}
                    min={0}
                    max={100}
                    values={[userRating]}
                    onChange={onChangeRatingHandler}
                    renderTrack={({ props, children }) => {
                        return (
                            <div
                                {...props}
                                style={{
                                    ...props.style,
                                    height: '6px',
                                    width: '100%',
                                    background: getTrackBackground({
                                        colors: ['#4e13d1', '#ddd'],
                                        min: 0,
                                        max: 100,
                                        values: [userRating],
                                    }),
                                }}
                            >
                                {children}
                            </div>
                        );
                    }}
                    renderThumb={({ props, isDragged }) => (
                        <div
                            {...props}
                            style={{
                                ...props.style,
                                height: '20px',
                                width: '20px',
                                left: '0',
                                borderRadius: '50%',
                                backgroundColor: isDragged ? '#552cea' : '#4e13d1',
                            }}
                        />
                    )}
                />
            </div>
            {userRating !== -1 ? (
                <div className={styles['user-rating']}>
                    <span>نمره شما: </span>
                    <span>{toPersianNum(userRating)}</span>
                </div>
            ) : null}
            <div className={styles['rating-text']}>{toPersianNum(totalRating)}٪ پسندیده اند</div>
        </>
    );
}

function renderEmojies(emojiImages, emojiRefs) {
    return (
        <>
            <img
                src={emojiImages[0]}
                className={styles.emoji}
                alt="emoji"
                ref={emojiRefs[0]}
                style={{ opacity: '0' }}
            />
            <img
                src={emojiImages[1]}
                className={styles.emoji}
                alt="emoji"
                ref={emojiRefs[1]}
                style={{ opacity: '0' }}
            />
            <img
                src={emojiImages[2]}
                className={styles.emoji}
                alt="emoji"
                ref={emojiRefs[2]}
                style={{ opacity: '0' }}
            />
            <img
                src={emojiImages[3]}
                className={styles.emoji}
                alt="emoji"
                ref={emojiRefs[3]}
                style={{ opacity: '0' }}
            />
            <img
                src={emojiImages[4]}
                className={styles.emoji}
                alt="emoji"
                ref={emojiRefs[4]}
                style={{ opacity: '0' }}
            />
        </>
    );
}
