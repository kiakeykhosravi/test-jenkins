import React from 'react';
import styles from '../styles/description-tab-main-section.module.scss';
import { IWebinarResponse } from 'modules/course/responses';
import DOMPurify from 'isomorphic-dompurify';

export default function DescriptionTabMainSection(props: { data: IWebinarResponse }) {
    return (
        <>
            {props.data.webinar.description ? (
                <>
                    <div
                        className={styles.description}
                        dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(props.data.webinar.description.trim()) }}
                    />
                </>
            ) : (
                <div className={styles['no-description']}>
                    <img src="/images/notepad.png" className={styles['no-description-image']} />
                    <div className={styles['no-description-text']}>توضیحاتی برای این دوره ثبت نشده است</div>
                </div>
            )}
        </>
    );
}
