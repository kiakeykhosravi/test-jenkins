import React, { useContext } from 'react';
import styles from '../styles/meetings-tab-main-section.module.scss';
import { IMeeting, MeetingStatusEnum } from 'models/meeting';
import { Link as LinkIcon, CheckCircle, ArrowLeft, PlayFill } from 'react-bootstrap-icons';
import { Badge, Button } from 'react-bootstrap';
import Link from 'next/link';
import classNames from 'classnames';
import apiEnterMeeting from '../api/api-enter-meeting';
import { useRouter } from 'next/router';
import apiEnterReplay from '../api/api-enter-replay';
import { AuthContext } from 'contexts/auth-context';
import { EventType } from '../types';
import { IAsanquiz } from 'models/asanquiz';
import apiEnterAsanquiz from '../api/api-enter-asanquiz';

export default function EventsTabMainSection(props: { data: EventType[] }) {
    const cn = classNames;
    const router = useRouter();

    const authContext = useContext(AuthContext);

    // TODO: These two need to be received from api
    const meetingVideoquizAnswer_ready_status = 'STATUS_FILES_READY';
    const studentAnswersLink = '';

    const renderLinks = (meeting: IMeeting) => {
        const nodes = [];

        for (let i = 0; i < meeting.links.link.length; i++) {
            const node = (
                <div>
                    <a
                        href={meeting.links.link[i]}
                        target="_blank"
                        rel="noreferrer"
                        title={`لینک ${meeting.links.title[i]}`}
                    >
                        <LinkIcon className={styles['download-link-icon']} />
                        <span className={styles['download-link-title']}>{meeting.links.title[i]}</span>
                    </a>
                </div>
            );

            nodes.push(node);
        }

        return <>{nodes}</>;
    };

    const renderMeeting = (meeting: IMeeting) => (
        <>
            <div className={styles['info-row']}>
                {meeting.is_quiz &&
                meeting.status === MeetingStatusEnum.FINISHED &&
                authContext.user &&
                meeting.access_level === 'moderator' ? (
                    meetingVideoquizAnswer_ready_status ? (
                        <Link href={studentAnswersLink} passHref>
                            <Button variant="secondary" className={styles['quiz-answers-btn']}>
                                پاسخ های دانش آموزان
                            </Button>
                        </Link>
                    ) : (
                        <Button variant="secondary" className={styles['quiz-answers-not-ready-btn']}>
                            در حال آماده سازی پاسخ ها
                        </Button>
                    )
                ) : (
                    <div className={styles[`status-${meeting.status}`]}>
                        {meeting.can_enter && authContext.user ? (
                            <Button
                                className={cn('btn btn-success', styles['btn-success'], styles['button-enter'])}
                                onClick={(event) => {
                                    const element: any = event.target;
                                    element.setAttribute('disabled', true);
                                    apiEnterMeeting(meeting.id)
                                        .then((response) => {
                                            if (response) {
                                                router.push(response);
                                            } else {
                                                element.removeAttribute('disabled');
                                            }
                                        })
                                        .catch(() => {
                                            element.removeAttribute('disabled');
                                        });
                                }}
                            >
                                <ArrowLeft className={styles['btn-enter-icon']} />
                                ورود به جلسه
                            </Button>
                        ) : (
                            <span className={styles['status-text']}>{meeting.status_fa}</span>
                        )}
                    </div>
                )}
                <span className={styles['datetime-time']}>{meeting.datetime_fatime}</span>
                <span className={styles['datetime-date']}>{meeting.datetime_fadate}</span>
            </div>
            {meeting.can_replay && authContext.user ? (
                <div className={styles['button-replay']}>
                    <span>
                        <Button
                            variant="success"
                            className={cn(styles['btn-success'], styles['playback-btn'])}
                            onClick={(event) => {
                                const element: any = event.target;
                                element.setAttribute('disabled', true);

                                apiEnterReplay(meeting.id)
                                    .then((response) => {
                                        if (response) {
                                            router.push(response).catch(() => {
                                                element.removeAttribute('disabled');
                                            });
                                        } else {
                                            element.removeAttribute('disabled');
                                        }
                                    })
                                    .catch(() => {
                                        element.removeAttribute('disabled');
                                    });
                            }}
                        >
                            بازپخش
                            <PlayFill className={styles['playback-btn-icon']} />
                        </Button>
                    </span>
                </div>
            ) : null}
            {meeting.access_level && meeting.links && meeting.links.link.length && authContext.user ? (
                <div className={styles['link-container']}>{renderLinks(meeting)}</div>
            ) : null}
        </>
    );

    const renderAsanquiz = (asanquiz: IAsanquiz) => (
        <>
            <div className={styles['info-row']}>
                <div className={styles[`asanquiz-state-${asanquiz.state}`]}>{asanquiz.state_fa}</div>
                <span className={styles['datetime-time']}>{asanquiz.start_date_fadate}</span>
                <span className={styles['datetime-date']}>{asanquiz.start_date_fatime}</span>
            </div>
            <div>
                {asanquiz.can_enter && authContext.user ? (
                    <Button
                        className={cn(
                            'btn btn-success',
                            styles['btn-success'],
                            styles['button-enter'],
                            styles['asanquiz-enter'],
                        )}
                        onClick={(e) => {
                            const element: any = e.target;
                            element.setAttribute('disabled', true);
                            apiEnterAsanquiz(asanquiz.webinar_id, asanquiz.id)
                                .then((response) => {
                                    if (response) {
                                        router.push(response).catch(() => {
                                            element.removeAttribute('disabled');
                                        });
                                    } else {
                                        element.removeAttribute('disabled');
                                    }
                                })
                                .catch(() => {
                                    element.removeAttribute('disabled');
                                });
                        }}
                    >
                        <ArrowLeft className={styles['btn-enter-icon']} />
                        {asanquiz.action_button_text}
                    </Button>
                ) : null}
            </div>
        </>
    );

    const eventsList = props.data.map((event: EventType) => {
        const isAsanquiz = (event as IAsanquiz).is_asanquiz;
        const isMeeting = (event as IMeeting).is_meeting;
        const isQuiz = isMeeting && (event as IMeeting).is_quiz;
        const eventMeeting: IMeeting = isMeeting && (event as IMeeting);
        const eventAsanquiz: IAsanquiz = isAsanquiz && (event as IAsanquiz);

        return (
            <div className={styles.meeting} key={event.id}>
                <h5 className={styles['meeting-title']}>
                    {event.title}
                    {isQuiz || isAsanquiz ? (
                        <Badge variant="dark" className={styles['quiz-badge']}>
                            <CheckCircle className={styles['quiz-checkmark']} />
                            {isAsanquiz ? 'آسان آزمون' : 'آزمون ویدئویی'}
                        </Badge>
                    ) : null}
                </h5>
                {isMeeting ? renderMeeting(eventMeeting) : renderAsanquiz(eventAsanquiz)}
            </div>
        );
    });

    return eventsList.length ? (
        <div className={styles.meetings}>{eventsList}</div>
    ) : (
        <div className={styles['no-meeting']}>
            <img src="/images/notepad.png" className={styles['no-meeting-image']} />
            <div className={styles['no-meeting-text']}>هنوز هیچ جلسه‌ای برای این وبینار تعیین نشده است</div>
        </div>
    );
}
