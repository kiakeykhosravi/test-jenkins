import React, { useContext, useState } from 'react';
import { CalendarEventType, IAsanquizCalendarEvent, IMeetingCalendarEvent } from '../types';
import styles from '../styles/course-calendar.module.scss';
import BigCalendar from 'jalali-react-big-calendar';
import toPersianNum from 'utils/to-persian-num';
import { Badge, Button, Container, Form, Modal, Spinner } from 'react-bootstrap';
import classNames from 'classnames';
import {
    ArrowLeft,
    CheckCircle,
    ChevronLeft,
    ChevronRight,
    PlayFill,
    X as CloseIconCustom,
} from 'react-bootstrap-icons';
import useWindowDimensions from 'utils/use-window-dimensions';
import { MeetingStatusEnum, MeetingStatusFarsiEnum } from 'models/meeting';
import apiEnterMeeting from '../api/api-enter-meeting';
import { useRouter } from 'next/router';
import apiEnterReplay from '../api/api-enter-replay';
import { AuthContext, IAuthContextType } from 'contexts/auth-context';
import apiEnterAsanquiz from '../api/api-enter-asanquiz';
import apiCreateAsanquiz from '../api/api-create-asanquiz';
import apiWebinarMeetings from '../api/api-webinar-meetings';

const cn = classNames;

export default function CourseCalendar(props: {
    webinar_id: number;
    events: CalendarEventType[];
    canCreateAsanquiz: boolean;
}) {
    const router = useRouter();
    const { width } = useWindowDimensions();
    const [allEvents, setAllEvents] = useState<CalendarEventType[]>(props.events);
    const [existingEventsMonths, setExistingEventsMonths] = useState<Date[]>([]);
    const [calendarLoading, setCalendarLoading] = useState(false);
    const [dayEvents, setDayEvents] = useState<CalendarEventType[]>([]);
    const [showEventModal, setShowEventModal] = useState(false);
    const [showMovingModal, setShowMovingModal] = useState(false);

    const authContext = useContext(AuthContext);

    const renderModalEvents = () => {
        return dayEvents.map((event) => {
            return <>{EventBody(event, authContext, showMovingModal, setShowMovingModal)}</>;
        });
    };

    function EventComponent(event: { title: string; isAllDay?; event: CalendarEventType }) {
        const { width } = useWindowDimensions();

        const handleClick = () => {
            const thisDayEvents = allEvents.filter((item) => {
                const thisDay = new Date(event.event.start);
                const itemDay = new Date(item.start);
                return (
                    thisDay.getFullYear() === itemDay.getFullYear() &&
                    thisDay.getMonth() === itemDay.getMonth() &&
                    thisDay.getDate() === itemDay.getDate()
                );
            });
            if (thisDayEvents.length) {
                setDayEvents(thisDayEvents);
                setShowEventModal(true);
            }
        };

        return width < 992 ? (
            <div onClick={() => handleClick()} className={styles['circle-red-container']}>
                <div className={styles['circle-red']}>جلسات</div>
            </div>
        ) : (
            EventBody(event.event, authContext, showMovingModal, setShowMovingModal)
        );
    }

    const onClickCreateAsanquiz = (e) => {
        const element: HTMLElement = e.target;
        if (element.hasAttribute('disabled')) {
            return;
        }
        element.setAttribute('disabled', 'true');
        setShowMovingModal(true);
        apiCreateAsanquiz(props.webinar_id)
            .then((response) => {
                if (response) {
                    router.push(response).catch(() => {
                        element.removeAttribute('disabled');
                        setShowMovingModal(false);
                    });
                } else {
                    element.removeAttribute('disabled');
                    setShowMovingModal(false);
                }
            })
            .catch(() => {
                element.removeAttribute('disabled');
                setShowMovingModal(false);
            });
    };

    const onRangeChange = (range) => {
        if (existingEventsMonths.includes(range.start.getTime())) {
            return;
        }

        setCalendarLoading(true);
        setExistingEventsMonths([...existingEventsMonths, range.start.getTime()]);

        const startDate = range.start.getFullYear() + '-' + (range.start.getMonth() + 1) + '-' + range.start.getDate();
        const endDate = range.end.getFullYear() + '-' + (range.end.getMonth() + 1) + '-' + range.end.getDate();
        apiWebinarMeetings(props.webinar_id.toString(), startDate, endDate)
            .then((response) => {
                if (response) {
                    const newEvents = JSON.parse(response.events_all as string) as CalendarEventType[];

                    const allowedEvents = newEvents.filter((newEvent) => {
                        return !allEvents.some((existingEvent) => {
                            return (
                                (newEvent as IMeetingCalendarEvent)._customMeetingId ===
                                (existingEvent as IMeetingCalendarEvent)._customMeetingId
                            );
                        });
                    });
                    setAllEvents([...allEvents, ...allowedEvents]);
                }
            })
            .finally(() => {
                setCalendarLoading(false);
            });
    };

    return (
        <Container>
            <div className={styles['webinar-calendar']}>
                {props.canCreateAsanquiz ? (
                    <Button variant="primary" className={styles['asanquiz-add-btn']} onClick={onClickCreateAsanquiz}>
                        ایجاد آسان آزمون +
                    </Button>
                ) : null}
                <div className="position-relative">
                    <div className={calendarLoading ? styles['calendar-loading'] : 'd-none'}>
                        <Spinner animation="grow" variant={'primary'} />
                        <div className={'m-2 ' + styles['calendar-loading-text']}>درحال بارگذاری جلسات جدید...</div>
                    </div>
                    <BigCalendar
                        popup={false}
                        popupOffset={20}
                        onDrillDown={(event: Date) => {
                            const thisDayEvents = allEvents.filter((item) => {
                                const date = new Date(item.start);
                                return (
                                    date.getFullYear() === event.getFullYear() &&
                                    date.getMonth() === event.getMonth() &&
                                    date.getDate() === event.getDate()
                                );
                            });
                            if (thisDayEvents.length) {
                                setDayEvents(thisDayEvents);
                                setShowEventModal(true);
                            }
                        }}
                        onRangeChange={onRangeChange}
                        events={allEvents}
                        eventPropGetter={() => {
                            return {
                                className: styles['event-style'],
                            };
                        }}
                        slotPropGetter={() => {
                            return {
                                className: styles['slot-style'],
                            };
                        }}
                        components={{
                            event: EventComponent,
                            dateHeader: CustomDateHeader,
                            toolbar: CustomCalHeader,
                        }}
                        messages={{
                            showMore: function ShowMore(total) {
                                return width < 992 ? null : (
                                    <div className={styles['more-event-btn']}>{toPersianNum(total)} + بیشتر</div>
                                );
                            },
                        }}
                    />
                </div>
            </div>
            <Modal centered show={showEventModal} onHide={() => setShowEventModal(false)}>
                <Modal.Header>
                    <Modal.Title>
                        <span className={styles['ml-1']}>{'جلسات '}</span>
                        {dayEvents.length ? (
                            <>
                                <span className={styles['ml-1']}>
                                    {new Intl.DateTimeFormat('fa-IR', {
                                        weekday: 'long',
                                        timeZone: 'Asia/Tehran',
                                    }).format(new Date(dayEvents[0].start))}
                                </span>{' '}
                                <span className={styles['ml-1']}>
                                    {new Intl.DateTimeFormat('fa-IR', {
                                        day: '2-digit',
                                        timeZone: 'Asia/Tehran',
                                    }).format(new Date(dayEvents[0].start))}
                                </span>{' '}
                                <span className={styles['ml-1']}>
                                    {new Intl.DateTimeFormat('fa-IR', {
                                        month: 'long',
                                        timeZone: 'Asia/Tehran',
                                    }).format(new Date(dayEvents[0].start))}
                                </span>
                            </>
                        ) : null}
                        <span>:</span>
                    </Modal.Title>
                    <CloseIconCustom className={styles.close} onClick={() => setShowEventModal(false)} />
                </Modal.Header>
                <Modal.Body>
                    <div className={styles['modal-event-container']}>{renderModalEvents()}</div>
                </Modal.Body>
            </Modal>
            <Modal centered show={showMovingModal} onHide={() => setShowMovingModal(false)}>
                <Modal.Header>
                    <Modal.Body>
                        <h4 className={'text-center'}>{'درحال انتقال...'}</h4>
                    </Modal.Body>
                </Modal.Header>
            </Modal>
        </Container>
    );
}

function EventBody(event: CalendarEventType, authContext, showMovingModal, setShowMovingModal) {
    const hours = new Date(event.start).getHours().toLocaleString('fa-IR', { minimumIntegerDigits: 2 });
    const minutes = new Date(event.start).getMinutes().toLocaleString('fa-IR', { minimumIntegerDigits: 2 });

    return (
        <div className={styles.event} style={event.color ? { backgroundColor: event.color } : null}>
            <div>{event.title}</div>
            <div>
                {((event as IMeetingCalendarEvent).is_meeting && (event as IMeetingCalendarEvent).is_quiz) ||
                (event as IAsanquizCalendarEvent).is_asanquiz ? (
                    <Badge variant="dark" className={styles['quiz-badge']}>
                        <CheckCircle className={styles['quiz-checkmark']} />
                        {(event as IAsanquizCalendarEvent).is_asanquiz ? 'آسان آزمون' : 'آزمون ویدئویی'}
                    </Badge>
                ) : null}
                {hours}:{minutes}
            </div>
            {ActionButtons(event, authContext, showMovingModal, setShowMovingModal)}
        </div>
    );
}

function ActionButtons(event: CalendarEventType, authContext: IAuthContextType, showMovingModal, setShowMovingModal) {
    const nodes = [];
    const router = useRouter();
    const isMeeting = (event as IMeetingCalendarEvent).is_meeting;
    const isAsanquiz = (event as IAsanquizCalendarEvent).is_asanquiz;
    const eventAsanquiz: IAsanquizCalendarEvent = isAsanquiz && (event as IAsanquizCalendarEvent);
    const eventMeeting: IMeetingCalendarEvent = isMeeting && (event as IMeetingCalendarEvent);

    if (isMeeting && eventMeeting._customCanEnter && authContext.user) {
        nodes.push(
            <span
                className={cn(styles.status, styles['status-running'])}
                onClick={(e) => {
                    const element: any = e.target;
                    if (element.hasAttribute('disabled')) {
                        return;
                    }
                    element.setAttribute('disabled', true);
                    setShowMovingModal(true);
                    apiEnterMeeting(eventMeeting._customMeetingId)
                        .then((response) => {
                            if (response) {
                                router.push(response).catch(() => {
                                    element.removeAttribute('disabled');
                                    setShowMovingModal(false);
                                });
                            } else {
                                element.removeAttribute('disabled');
                                setShowMovingModal(false);
                            }
                        })
                        .catch(() => {
                            element.removeAttribute('disabled');
                            setShowMovingModal(false);
                        });
                }}
            >
                <ArrowLeft className={styles['btn-enter-icon']} />
                لینک کلاس
            </span>,
        );
    } else if (isMeeting && eventMeeting._customCanReplay && authContext.user) {
        nodes.push(
            <span
                className={cn(styles.status, styles['playback-btn'], showMovingModal ? styles.disabled : null)}
                onClick={(e) => {
                    const element: any = e.target;
                    if (element.hasAttribute('disabled')) {
                        return;
                    }
                    element.setAttribute('disabled', true);
                    setShowMovingModal(true);
                    apiEnterReplay(eventMeeting._customMeetingId)
                        .then((response) => {
                            if (response) {
                                router.push(response).catch(() => {
                                    element.removeAttribute('disabled');
                                    setShowMovingModal(false);
                                });
                            } else {
                                element.removeAttribute('disabled');
                                setShowMovingModal(false);
                            }
                        })
                        .catch(() => {
                            element.removeAttribute('disabled');
                            setShowMovingModal(false);
                        });
                }}
            >
                بازپخش
                <PlayFill className={styles['playback-btn-icon']} />
            </span>,
        );
    } else {
        if (isMeeting) {
            const statusText =
                event._customStatus === MeetingStatusEnum.FINISHED
                    ? MeetingStatusFarsiEnum.FINISHED
                    : event._customStatus === MeetingStatusEnum.PLANNED
                    ? MeetingStatusFarsiEnum.PLANNED
                    : event._customStatus === MeetingStatusEnum.CANCELED
                    ? MeetingStatusFarsiEnum.CANCELED
                    : MeetingStatusFarsiEnum.RUNNING;
            nodes.push(
                <span className={cn(styles.status, styles[`status-${event._customStatus}`])}>{statusText}</span>,
            );
        } else {
            nodes.push(
                <span className={cn(styles.status, styles[`asanquiz-state-${event._customStatus}`])}>
                    {eventAsanquiz._customStatusFa}
                </span>,
            );
        }
    }
    if (isMeeting && eventMeeting.is_quiz && event._customStatus === MeetingStatusEnum.FINISHED && authContext.user) {
        nodes.push(
            eventMeeting.videoquiz_ready === 'ready' ? (
                // TODO: this should call an api
                <Form method="POST">
                    <span className={styles['videoquiz-answers-btn']}>پاسخ های دانش‌ آموزان</span>
                </Form>
            ) : (
                <span className={styles['videoquiz-answers-not-ready-btn']}>در حال آماده سازی پاسخ ها</span>
            ),
        );
    } else if (isAsanquiz && eventAsanquiz._customCanEnter) {
        nodes.push(
            <span
                className={cn(styles.status, styles['status-running'])}
                onClick={(e) => {
                    const element: any = e.target;
                    if (element.hasAttribute('disabled')) {
                        return;
                    }
                    element.setAttribute('disabled', true);
                    setShowMovingModal(true);
                    apiEnterAsanquiz(eventAsanquiz._customWebinarId, eventAsanquiz._customAsanquizId)
                        .then((response) => {
                            if (response) {
                                router.push(response).catch(() => {
                                    element.removeAttribute('disabled');
                                    setShowMovingModal(false);
                                });
                            } else {
                                element.removeAttribute('disabled');
                                setShowMovingModal(false);
                            }
                        })
                        .catch(() => {
                            element.removeAttribute('disabled');
                            setShowMovingModal(false);
                        });
                }}
            >
                <ArrowLeft className={styles['btn-enter-icon']} />
                {eventAsanquiz._customActionButtonText}
            </span>,
        );
    }

    return <>{nodes}</>;
}

function CustomCalHeader(props) {
    return (
        <div className={cn(styles['rbc-toolbar'], 'rbc-toolbar')}>
            <h2 className={cn(styles['rbc-toolbar-label'], 'rbc-toolbar-label')}>{toPersianNum(props.label)}</h2>
            <span className="rbc-btn-group">
                <span
                    onClick={() => props.onNavigate('TODAY')}
                    className={cn(styles['toolbar-btn'], styles['today-btn'])}
                >
                    امروز
                </span>
                <span
                    onClick={() => props.onNavigate('PREV')}
                    className={cn(styles['toolbar-btn'], styles['prev-btn'])}
                >
                    <ChevronRight className={styles['toolbar-btn-icon']} />
                </span>
                <span
                    onClick={() => props.onNavigate('NEXT')}
                    className={cn(styles['toolbar-btn'], styles['next-btn'])}
                >
                    <ChevronLeft />
                </span>
            </span>
        </div>
    );
}

function CustomDateHeader({ date, label, onDrillDown }) {
    const { width } = useWindowDimensions();

    const handleDayClick = () => {
        // eslint-disable-next-line @typescript-eslint/no-empty-function
        date.preventDefault = () => {};
        onDrillDown(date);
    };

    if (width < 992) {
        return (
            <div onClick={handleDayClick} className={styles['day-number']}>
                <span>{toPersianNum(label)}</span>
            </div>
        );
    }

    return <div className={styles['day-number']}>{toPersianNum(label)}</div>;
}
