import React, { useContext, useState } from 'react';
import styles from '../styles/course-top-section.module.scss';
import { Breadcrumb, Button, Col, Container, Form, Row } from 'react-bootstrap';
import toPersianNum from 'utils/to-persian-num';
import Link from 'next/link';
import classNames from 'classnames';
import { IIcons } from '../types';
// import Rating from './rating'
import { IWebinarResponse } from '../responses';
import { OrganizationType } from 'models/organization';
import { WebinarStatusEnum } from 'models/webinar';
import { X as CloseIcon } from 'react-bootstrap-icons';
import apiDiscountCheck from '../api/api-discount-check';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';
import apiOrder from '../api/api-order';
// import apiSubmitRating from '../api/api-submit-rating';
import { AuthContext } from 'contexts/auth-context';
import toPrice from 'utils/to-price';
import SliderBanner from 'modules/shared/components/slider-banner';
import { ISlide } from 'models/slide';
import { IMedia } from 'models/media';
import ModalLogin from 'modules/auth/components/modal-login';
import toEnglishNum from 'utils/to-english-num';

// TODO: This section needs major refactor when apis are added
export default function CourseTopSection(props: {
    data: IWebinarResponse;
    showLoginModal;
    setShowLoginModal;
    shouldLogin: boolean;
    marketer;
    updateWebinar;
}) {
    const cn = classNames;
    const router = useRouter();

    // const [rating, setRating] = useState(props.data.webinar.rating || 0);
    // const [currentUserRating, setCurrentUserRating] = useState(0);
    const [discountField, setDiscountField] = useState('');
    const [marketerCode, setMarketerCode] = useState(props.marketer || '');
    const [discountAmount, setDiscountAmount] = useState(0);
    const [discountId, setDiscountId] = useState(0);
    const [orderLoading, setOrderLoading] = useState(false);
    const [isRegistered, setIsRegistered] = useState(props.data.registered);
    const [finalPrice, setFinalPrice] = useState(props.data.webinar.price);
    const [finalPriceWithDiscount, setFinalPriceWithDiscount] = useState(props.data.webinar.price_with_off);

    const statuses = {
        status: props.data.webinar.status,
        isEnrolled: isRegistered,
        isInstructor: props.data.is_instructor,
        isActive: props.data.webinar.active,
        isFull: props.data.full_capacity,
        discount: props.data.webinar.off,
        allow_order: props.data.webinar.allow_order,
        closed: props.data.webinar.closed,
        field_discount_code: props.data.webinar.field_discount_code,
    };

    const authContext = useContext(AuthContext);

    const showFields =
        statuses.isActive &&
        !statuses.isEnrolled &&
        !statuses.isFull &&
        statuses.status === WebinarStatusEnum.OPEN &&
        statuses.allow_order;

    // const submitUserRating = (userRating) => {
    //     setCurrentUserRating(userRating);
    //     apiSubmitRating(router.query.id as string, userRating);
    //     // TODO: Get new total rating from server
    //     // setRating(newvalueFromServer);
    // };

    const renderSidebarTop = () => {
        return (
            <>
                <Link href={`/organizations/${props.data.webinar.organization.slug}`}>
                    <a title={props.data.webinar.organization.title}>
                        <img
                            src={
                                (props.data.webinar.organization.logo && props.data.webinar.organization.logo.url) ||
                                '/images/avatar-placeholder.png'
                            }
                            alt={props.data.webinar.organization.title}
                            className={styles['organization-logo']}
                        />
                    </a>
                </Link>
                {/*
                TODO: This section is intentionally disabled because we still haven't properly utilized rating system
                    and it appears useless when placed like this

                {props.data.webinar.organization.type === OrganizationType.EDUCATIONAL_INSTITUTION ? (
                    <>
                        <hr />
                        <Rating
                            submitUserRating={submitUserRating}
                            totalRating={rating}
                            currentUserRating={currentUserRating}
                        />
                    </>
                ) : null}
                */}
            </>
        );
    };

    const renderSidebarIcons = () => {
        let meetingsCount = 0;
        let events = props.data.events_all;
        if (typeof props.data.events_all === 'string') {
            events = JSON.parse(props.data.events_all);
        }
        for (const event of events) {
            if (event['is_meeting']) {
                meetingsCount++;
            }
        }
        const icons = (): IIcons[] => {
            if (props.data.webinar.organization.type === OrganizationType.OTHER) {
                return [
                    /*{
                        text: 'طول دوره',
                        count: '200',
                        img: '/images/icons/clock.png',
                    },*/
                    {
                        text: 'تعداد جلسات',
                        count: Math.max(meetingsCount, props.data.webinar.meetings_count),
                        img: '/images/icons/sections.png',
                    },
                    {
                        text: 'تعداد ظرفیت',
                        count: props.data.webinar.max_user,
                        img: '/images/icons/crowd.png',
                    },
                ];
            } else {
                return [
                    {
                        text: 'طول دوره',
                        count: '--',
                        img: '/images/icons/clock.png',
                    },
                ];
            }
        };

        const renderMiniIcon = (text: string, count: string, img: string) => {
            return (
                <div className={styles['mini-icon-col']}>
                    <img className={styles['mini-icon-img']} src={img} alt="آیکون" />
                    <div className={styles['mini-icon-text']}>{text}</div>
                    <div className={styles['mini-icon-text']}>{count}</div>
                </div>
            );
        };

        if (icons().length === 1) {
            return (
                <div className={styles['icons-center']}>
                    {icons().map((item) => {
                        return renderMiniIcon(item.text, `${toPersianNum(item.count)} ساعت`, item.img);
                    })}
                </div>
            );
        } else {
            return (
                <div className={styles.icons}>
                    {icons().map((item) => {
                        if (item.text === 'طول دوره')
                            return renderMiniIcon(item.text, `${toPersianNum(item.count)} ساعت`, item.img);
                        else return renderMiniIcon(item.text, toPersianNum(item.count), item.img);
                    })}
                </div>
            );
        }
    };

    const onSubmitOrder = (event) => {
        if (orderLoading) {
            return;
        }
        setOrderLoading(true);
        event.preventDefault();
        const formData = new FormData(event.target);
        const parameters: {
            discount?: number;
            marketer_code?: string;
            webinar_code?: string;
            payment_type?: string;
        } = {};
        if (discountId) {
            parameters.discount = discountId;
        }
        if (formData.get('marketer_code')) {
            parameters.marketer_code = toEnglishNum(formData.get('marketer_code') as string);
        }
        if (formData.get('webinar_code')) {
            parameters.webinar_code = toEnglishNum(formData.get('webinar_code') as string);
        }
        if (formData.get('payment_type')) {
            parameters.payment_type = formData.get('payment_type') as string;
        }

        apiOrder(router.query.id as string, parameters)
            .then((response) => {
                if (response) {
                    if (response.link) {
                        router.push(response.link);
                    } else {
                        setIsRegistered(true);
                    }
                }
            })
            .finally(() => {
                setOrderLoading(false);
                props.updateWebinar();
            });
    };

    const renderActionButton = ({
        status,
        isEnrolled,
        isInstructor,
        isActive,
        isFull,
        discount,
        allow_order,
        closed,
        field_discount_code,
    }) => {
        const cn = classNames;
        const onClickApplyDiscount = (event) => {
            if (!discountField) {
                return;
            }
            const element = event.target;
            element.setAttribute('disabled', true);

            apiDiscountCheck(discountField, router.query.id as string)
                .then((response) => {
                    if (response) {
                        toast('کد تخفیف با موفقیت اعمال شد', { type: 'success' });
                        setDiscountAmount(response.discount_amount);
                        setFinalPrice(finalPrice - response.discount_amount);
                        setFinalPriceWithDiscount(finalPriceWithDiscount - response.discount_amount);
                        setDiscountId(response.discount.id);
                        element.value = '';
                    }
                })
                .finally(() => {
                    element.removeAttribute('disabled');
                });
        };

        let statusText = null;
        let priceText = null;
        let actionButton = null;
        let fieldCount = null;
        let paymentType = null;
        if (isInstructor) {
            statusText = <div className={styles['status-welcome']}>مدرس عزیز خوش آمدید</div>;
        } else if (isEnrolled) {
            statusText = <div className={styles['status-registered']}>شما در این دوره ثبت نام کرده اید</div>;
        } else if (!isActive) {
            statusText = <div className={styles['status-disabled']}>ثبت نام در این کلاس غیرفعال شده است</div>;
        } else if (isFull) {
            statusText = <div className={styles['status-full']}>متاسفانه ظرفیت این کلاس پر شده است</div>;
        } else if (status !== WebinarStatusEnum.OPEN || !allow_order) {
            statusText = <div className={styles['status-not-open']}>متاسفانه امکان ثبت نام وجود ندارد</div>;
        } else if (closed) {
            statusText = <div className={styles['status-closed']}>زمان دوره به پایان رسیده است</div>;
        } else {
            priceText = discount ? (
                <>
                    <div>
                        <span className={styles['old-price']}>{toPrice(props.data.webinar.price)} تومان</span>
                        <span className={styles.discount}>{toPersianNum(discount)}٪ تخفیف</span>
                    </div>
                    <div className={styles.price}>
                        {finalPriceWithDiscount ? `${toPrice(finalPriceWithDiscount)} تومان` : 'رایگان'}
                    </div>
                </>
            ) : finalPrice ? (
                <div className={styles.price}>{toPrice(finalPrice)} تومان</div>
            ) : !authContext.user ? (
                <div className={styles.price}>رایگان</div>
            ) : null;
            if (authContext.user) {
                const totalPrice = discount ? finalPriceWithDiscount : finalPrice;
                actionButton = (
                    <Button
                        type="submit"
                        variant="success"
                        className={cn(styles['action-button'], styles['action-enroll'])}
                        {...(orderLoading ? { disabled: true } : {})}
                    >
                        {totalPrice ? 'پرداخت و ثبت نام' : 'ثبت نام رایگان در دوره'}
                    </Button>
                );
            } else {
                actionButton = (
                    <>
                        <div className={styles['login-cta-text']}>
                            برای شرکت در وبینار باید ابتدا در سایت ثبت نام کنید
                        </div>
                        <Button
                            variant="info"
                            type="button"
                            className={cn(styles['action-button'], styles['login-btn'])}
                            title="ورود / ثبت نام در سایت"
                            onClick={() => props.setShowLoginModal(true)}
                        >
                            <span>ورود / ثبت نام</span>
                        </Button>
                    </>
                );
            }
            fieldCount =
                showFields && field_discount_code ? (
                    <div className={styles['discount-section']}>
                        <Form.Control
                            placeholder="کد تخفیف را وارد کنید"
                            className={cn(styles['input-field'], styles['discount-field'])}
                            onChange={(e) => setDiscountField(e.target.value)}
                        />
                        {discountAmount ? (
                            <div className={styles['discount-amount-container']}>
                                <div>
                                    تخفیف اعمال شده: <span>{toPrice(discountAmount)} تومان</span>
                                </div>
                                <Button
                                    variant="outline-danger"
                                    type="button"
                                    className={styles['discount-cancel-btn']}
                                    onClick={(e: any) => {
                                        setDiscountId(undefined);
                                        setFinalPrice(finalPrice + discountAmount);
                                        setFinalPriceWithDiscount(finalPriceWithDiscount + discountAmount);
                                        setDiscountAmount(0);
                                        toast('تخفیف اعمال شده با موفقیت حذف شد', { type: 'success' });
                                        e.target.setAttribute('disabled', true);
                                    }}
                                >
                                    حذف تخفیف
                                    <CloseIcon className={styles['discount-cancel-icon']} />
                                </Button>
                            </div>
                        ) : (
                            <Button className={cn(styles['discount-button'])} onClick={onClickApplyDiscount}>
                                اعمال کد تخفیف
                            </Button>
                        )}
                    </div>
                ) : null;
            paymentType = (
                <div style={props.data.webinar.price_with_off ? {} : { display: 'none' }}>
                    <div className={cn('mb-1', styles['textbox-info'])}>روش پرداخت</div>
                    <Form.Control as="select" name="payment_type" className={cn(styles['input-field'], 'form-select')}>
                        <option value="bank">بانکی</option>
                        <option value="credit">اعتبار حساب</option>
                    </Form.Control>
                    <div className={styles['textbox-info']} style={{ marginBottom: '1rem' }}>
                        اعتبار شما: {toPrice(props.data.user_credit)} تومان
                    </div>
                </div>
            );
        }

        return (
            <>
                {statusText}
                {fieldCount}
                <div>{priceText}</div>
                {paymentType}
                {actionButton}
            </>
        );
    };

    const renderSidebarBottom = () => {
        return (
            <div
                className={
                    props.data.webinar.organization.type !== OrganizationType.OTHER
                        ? styles['gov-sidebar-bottom']
                        : null
                }
            >
                <Form className={styles['register-section']} onSubmit={onSubmitOrder}>
                    {showFields ? (
                        <div>
                            {props.data.webinar.field_marketer_code ? (
                                <>
                                    {props.data.webinar.field_marketer_code_required ? (
                                        <div className={cn('text-danger mb-1', styles['textbox-info'])}>
                                            * کد معرف اجباری است
                                        </div>
                                    ) : null}
                                    <Form.Control
                                        placeholder="کد معرف"
                                        name="marketer_code"
                                        value={marketerCode}
                                        onChange={(e) => setMarketerCode(e.target.value)}
                                        className={styles['input-field']}
                                        dir="ltr"
                                    />
                                </>
                            ) : null}
                            {props.data.webinar.field_webinar_code_required ? (
                                <>
                                    <div className={cn('text-danger mb-1', styles['textbox-info'])}>
                                        * کد کلاس اجباری است
                                    </div>
                                    <Form.Control
                                        placeholder="کد کلاس را وارد کنید"
                                        name="webinar_code"
                                        className={styles['input-field']}
                                    />
                                </>
                            ) : null}
                        </div>
                    ) : null}
                    <div className={styles['action-section']}>{renderActionButton(statuses)}</div>
                </Form>
                {/*<div className={styles['phone-section']}>*/}
                {/*    <img className={styles['phone-icon']} alt="phone icon" src="/images/icons/telephone.png" />*/}
                {/*    <span className={styles['phone-number']}>{toPersianNum('09123456789')}</span>*/}
                {/*</div>*/}
            </div>
        );
    };

    const slides = (media: IMedia[]): ISlide[] => {
        return media.map((m) => {
            return {
                id: m.id,
                title: m.title,
                url: null,
                media_id: m.id,
                location: 'webinar_page',
                active: 1,
                media: m,
            };
        });
    };

    return (
        <div className="gradient-background">
            <Container className={styles.section}>
                <Row>
                    <Col lg={3} />
                    <Col lg={9}>
                        <h1 className={styles.header}>{props.data.webinar.title}</h1>
                        <Breadcrumb className={styles.breadcrumb}>
                            <Link href="/" passHref>
                                <Breadcrumb.Item href="/">خانه</Breadcrumb.Item>
                            </Link>
                            <Link href="/webinars" passHref>
                                <Breadcrumb.Item href="/webinars">دوره ها</Breadcrumb.Item>
                            </Link>
                            <Breadcrumb.Item active>{props.data.webinar.title}</Breadcrumb.Item>
                        </Breadcrumb>
                    </Col>
                </Row>
                <Row className={styles.main}>
                    <Col lg={3} className={styles['right-column-container']}>
                        <div className={styles['right-column']}>
                            {renderSidebarTop()}
                            <hr />
                            {renderSidebarIcons()}
                            <hr />
                            {renderSidebarBottom()}
                        </div>
                    </Col>
                    <Col lg={9} className={styles['left-column-container']}>
                        <div className={styles['left-column']}>
                            {props.data.webinar.media.length > 1 ? (
                                <SliderBanner
                                    slideClasses={styles.banner}
                                    slides={slides(props.data.webinar.media)}
                                    autoplay={{ autoplay: { delay: 10000 } }}
                                    swiperClasses="h-100"
                                />
                            ) : (
                                <img
                                    src={
                                        props.data.webinar.media &&
                                        props.data.webinar.media.length &&
                                        props.data.webinar.media[0].url
                                    }
                                    className={styles.banner}
                                    alt={`تصویر ${props.data.webinar.title}`}
                                />
                            )}
                        </div>
                    </Col>
                </Row>
                {/*<Row style={{ marginTop: '0.5rem' }}>*/}
                {/*    <Col md={4} lg={3} />*/}
                {/*    <Col md={8} lg={9}>*/}
                {/*        <div className={styles['black-bar']}>*/}
                {/*            <Button*/}
                {/*                variant="outline-secondary"*/}
                {/*                className={cn(styles['black-bar-button'], 'd-block d-lg-inline mx-auto')}*/}
                {/*                disabled*/}
                {/*            >*/}
                {/*                دوره به اتمام رسیده*/}
                {/*            </Button>*/}
                {/*            <span className={styles['black-bar-text']}>شروع: {date}</span>*/}
                {/*            <span className={styles['black-bar-text']}>ساعت: ۱۴ الی ۱۶</span>*/}
                {/*            <span className={styles['black-bar-text']}>*/}
                {/*                پخش آفلاین: <span className={styles['black-bar-text-red']}>ندارد</span>*/}
                {/*            </span>*/}
                {/*        </div>*/}
                {/*    </Col>*/}
                {/*</Row>*/}
            </Container>
            <ModalLogin
                closable={!props.shouldLogin}
                showModal={props.showLoginModal}
                setShowModal={props.setShowLoginModal}
            />
        </div>
    );
}
