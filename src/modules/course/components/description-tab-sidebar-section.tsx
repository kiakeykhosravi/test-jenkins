import React from 'react';
import styles from '../styles/description-tab-sidebar-section.module.scss';
import TagsList from 'modules/shared/components/tags-list';
import { IWebinarResponse } from '../responses';
import AsanImage from 'modules/shared/components/asan-image';

export default function DescriptionTabSidebarSection(props: { data: IWebinarResponse }) {
    return (
        <aside>
            <hr className="d-md-none" />
            {/*<div className={styles['participants-number']}>{toPersianNum('10')}</div>
            <div className={styles['participants-text']}>شرکت کننده</div>
            <hr />*/}
            <div className={styles['instructor-avatar-container']}>
                <AsanImage
                    src="/images/avatar-placeholder.png"
                    className={styles.avatar}
                    alt={'تصویر ' + props.data.webinar.instructor.fullname}
                />
                <span>
                    <span>
                        <span className={styles['instructor-name']}>{props.data.webinar.instructor.fullname}</span>
                        {/*<span className={styles['instructor-follow']}>دنبال کردن</span>*/}
                    </span>
                </span>
            </div>
            <p className={styles['description']}>{props.data.webinar.instructor.description}</p>
            {/*<Link href={`/instructors/${props.data.webinar.instructor.id}/${props.data.webinar.instructor.fullname}`}>*/}
            {/*    <a className={styles['instructor-link']}>مشاهده مشخصات کامل</a>*/}
            {/*</Link>*/}
            {props.data.tags ? (
                <>
                    <hr />
                    <div className={styles['tags-title']}>تگ های مربوط به این کلاس</div>
                    <TagsList data={props.data.tags} areLinks={true} />
                </>
            ) : null}
        </aside>
    );
}
