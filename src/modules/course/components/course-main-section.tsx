import React from 'react';
import { Col, Container, Nav, Row, Tab, Button } from 'react-bootstrap';
import styles from '../styles/course-main-section.module.scss';
import DescriptionTabMainSection from './description-tab-main-section';
import DescriptionTabSidebarSection from './description-tab-sidebar-section';
import { IWebinarResponse } from '../responses';
import { OrganizationType } from 'models/organization';
import CourseCalendar from './course-calendar';
import EventsToday from './events-today';
import EventsTabMainSection from './events-tab-main-section';
import { CalendarEventType, EventType } from '../types';
import apiCreateAsanquiz from '../api/api-create-asanquiz';
import { useRouter } from 'next/router';

export default function CourseMainSection(props: { data: IWebinarResponse }) {
    const router = useRouter();
    const isSchool = props.data.webinar.organization.type === OrganizationType.SCHOOL;

    const onClickCreateAsanquiz = (e) => {
        const element: HTMLElement = e.target;
        if (element.hasAttribute('disabled')) {
            return;
        }
        element.setAttribute('disabled', 'true');
        apiCreateAsanquiz(props.data.webinar.id)
            .then((response) => {
                if (response) {
                    router.push(response).catch(() => {
                        element.removeAttribute('disabled');
                    });
                } else {
                    element.removeAttribute('disabled');
                }
            })
            .catch(() => {
                element.removeAttribute('disabled');
            });
    };

    return (
        <Tab.Container
            id="course-tabs"
            defaultActiveKey={!props.data.registered || isSchool ? 'description' : 'meetings'}
        >
            <Container className="course-main-section-tabs">
                <Row>
                    <Nav className={styles.tabs}>
                        <Nav.Item>
                            <Nav.Link eventKey="description" className={styles['tab-item']}>
                                {isSchool ? 'تقویم' : 'توضیحات'}
                            </Nav.Link>
                        </Nav.Item>
                        {isSchool ? null : (
                            <Nav.Item>
                                <Nav.Link eventKey="meetings" className={styles['tab-item']}>
                                    جلسات
                                </Nav.Link>
                            </Nav.Item>
                        )}
                        {/*<Nav.Item>*/}
                        {/*    <Nav.Link eventKey="comments" className={styles['tab-item']}>*/}
                        {/*        نظرات*/}
                        {/*    </Nav.Link>*/}
                        {/*</Nav.Item>*/}
                        {/*<Nav.Item>*/}
                        {/*    <Nav.Link eventKey="files" className={styles['tab-item']}>*/}
                        {/*        فایل های دوره*/}
                        {/*    </Nav.Link>*/}
                        {/*</Nav.Item>*/}
                        {/*<Nav.Item className={styles['tab-bar-side-buttons']}>
                            <Button variant="outline-secondary" className={styles['tab-bar-side-btn']}>
                                <span>نشان کردن</span>
                                <Bookmark className={styles['tab-bar-icon']} />
                            </Button>
                            <Button variant="outline-secondary" className={styles['tab-bar-side-btn']}>
                                <span>اشتراک گذاری</span>
                                <Share className={styles['tab-bar-icon']} />
                            </Button>
                        </Nav.Item>*/}
                    </Nav>
                </Row>
            </Container>
            <hr className="mb-4" />
            <Container className={isSchool ? null : styles['container-limit']}>
                <Row className={styles['tab-pane-container']}>
                    <Col md={isSchool ? 12 : 8}>
                        <Tab.Content>
                            <Tab.Pane eventKey="description">
                                {isSchool ? (
                                    <>
                                        <EventsToday events={props.data.events_today} />
                                        <CourseCalendar
                                            webinar_id={props.data.webinar.id}
                                            events={JSON.parse(props.data.events_all as string) as CalendarEventType[]}
                                            canCreateAsanquiz={props.data.can_create_asanquiz}
                                        />
                                    </>
                                ) : (
                                    <DescriptionTabMainSection data={props.data} />
                                )}
                            </Tab.Pane>
                            {!isSchool ? (
                                <Tab.Pane eventKey="meetings">
                                    {props.data.can_create_asanquiz ? (
                                        <Button
                                            variant="primary"
                                            className={styles['asanquiz-add-btn']}
                                            onClick={onClickCreateAsanquiz}
                                        >
                                            ایجاد آسان آزمون +
                                        </Button>
                                    ) : null}
                                    <EventsTabMainSection data={props.data.events_all as EventType[]} />
                                </Tab.Pane>
                            ) : null}
                            {/*<Tab.Pane eventKey="comments">*/}
                            {/*    <div style={{ fontSize: '6rem' }}>بخش نظرات در انتظار طراحی</div>*/}
                            {/*</Tab.Pane>*/}
                            {/*<Tab.Pane eventKey="files">*/}
                            {/*    <div style={{ fontSize: '6rem' }}>بخش لیست فایل های دوره</div>*/}
                            {/*</Tab.Pane>*/}
                        </Tab.Content>
                    </Col>
                    <Col md={isSchool ? 12 : 4}>
                        <DescriptionTabSidebarSection data={props.data} />
                    </Col>
                </Row>
            </Container>
        </Tab.Container>
    );
}
