import React from 'react';
import ListSection from 'modules/shared/components/list-section';
import styles from '../styles/recommended-section.module.scss';
import { Container, Row } from 'react-bootstrap';
import classNames from 'classnames';
import { IWebinarCompact } from 'models/webinar-compact';

export default function RecommendedSection(props: { data: IWebinarCompact[] }) {
    return (
        <Container className={classNames('pb-5', styles.container)}>
            <h3 className="mb-4">کلاس های مرتبط</h3>
            <Row className="gx-5 gy-4">
                <ListSection data={props.data} />
            </Row>
        </Container>
    );
}
