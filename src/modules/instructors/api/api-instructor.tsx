import apiRequest from 'api/api-request';
import { IInstructorResponse } from '../responses';
import endpoints from 'api/api-endpoints';
import { IUser } from 'models/user';

export async function apiInstructor(id: string): Promise<{ status: 'data' | 'error'; data?: IUser }> {
    if (typeof id !== 'string') {
        return {
            status: 'error',
        };
    }

    const response = await apiRequest<IInstructorResponse>('get', endpoints.backend.instructor + `/${id}`);

    if (response.status === 'data') {
        return {
            data: response.data.instructor,
            status: 'data',
        };
    }

    return {
        status: 'error',
    };
}
