import React from 'react';
import Section from 'modules/shared/components/section';
import { Container, Row } from 'react-bootstrap';
import styles from '../styles/instructor-section.module.scss';
import classNames from 'classnames';
import { IUserSafe } from 'models/user-safe';
import LogoCard from 'modules/shared/components/logo-card';

// TODO: !!IMPORTANT!! This component and OrganizationSection share a lot in content. Merge them!
export default function InstructorSection(props: { data: IUserSafe[]; classes?: string }) {
    const cn = classNames;
    const renderTeacherCards = () => {
        if (props.data.length) {
            return props.data.map((item: IUserSafe) => {
                return <LogoCard data={item} key={item.id} />;
            });
        }
        return null;
    };
    return (
        <Section classes={props.classes}>
            <Container>
                <Row
                    className={cn(
                        styles['gx-3'],
                        styles['gy-4'],
                        styles['gx-lg-4'],
                        styles['gy-lg-5'],
                        styles['row'],
                        'gx-3 gy-4 gx-lg-4 gy-lg-5 justify-content-center',
                    )}
                >
                    {renderTeacherCards()}
                </Row>
            </Container>
        </Section>
    );
}
