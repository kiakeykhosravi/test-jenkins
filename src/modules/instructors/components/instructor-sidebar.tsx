import React from 'react';
import Link from 'next/link';
import styles from '../styles/instructor-sidebar.module.scss';
import { Globe } from 'react-bootstrap-icons';
import { IUser } from 'models/user';

export default function InstructorSidebar(props: { data: IUser }) {
    const imgSrc = '/images/avatar-placeholder.png';

    return (
        <div className={styles['sidebar']}>
            <img src={imgSrc} alt={props.data.fullname} className={styles['teacher-img']} />
            <div className={styles['name']}>{props.data.fullname}</div>
            {/*<div className={styles['role']}>{props.data.jobTitle || 'شغل استاد'}</div>*/}
            {/*<div className={styles['follow-btn']}>دنبال کردن</div>*/}
            {/*<div className={styles['follow-section']}>*/}
            {/*    <div className={styles['follower']}>*/}
            {/*        <div className={styles['count']}>{toPersianNum('120')}</div>*/}
            {/*        <div className={styles['txt']}>دنبال کننده</div>*/}
            {/*    </div>*/}
            {/*    <div className={styles['following']}>*/}
            {/*        <div className={styles['count']}>{toPersianNum('200')}</div>*/}
            {/*        <div className={styles['txt']}>دنبال کرده</div>*/}
            {/*    </div>*/}
            {/*</div>*/}
            <hr className={styles['hr']} />
            <div className={styles['link-section']}>
                <Globe className={styles['globe']} />
                <Link href={`/instructors/${props.data.id}`}>
                    <a title="لینک استاد" className={styles['link']}>
                        {`https://asanseminar.ir/instructors/${props.data.id}`}
                    </a>
                </Link>
            </div>
            <hr className={styles['hr']} />
            {/*<div className={styles['report-section']}>*/}
            {/*    <Megaphone className={styles['megaphone']} />*/}
            {/*    <Link href={'/report'}>*/}
            {/*        <a className={styles['report-link']}>گزارش دادن کاربر</a>*/}
            {/*    </Link>*/}
            {/*</div>*/}
        </div>
    );
}
