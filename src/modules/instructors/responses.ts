import { IUser } from 'models/user';

export interface IInstructorResponse {
    instructor: IUser;
}

export interface IInstructorsResponse {
    instructors: IUser[];
    lastpage: number;
}
