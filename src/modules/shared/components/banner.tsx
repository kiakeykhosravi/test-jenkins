import React from 'react';
import { Card } from 'react-bootstrap';

export default function Banner(props: {
    bannerClasses?: string;
    bodyClasses?: string;
    image?: { src: string; variant?: 'top' | 'bottom'; classes?: string };
    children?: any;
}) {
    return (
        <Card className={props.bannerClasses}>
            {props.image ? (
                <>
                    <Card.Img
                        src={props.image.src}
                        variant={props.image.variant}
                        className={props.image.classes}
                        alt={''}
                    />
                </>
            ) : null}
            <Card.Body className={props.bodyClasses}>{props.children}</Card.Body>
        </Card>
    );
}
