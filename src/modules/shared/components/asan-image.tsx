import React from 'react';
import classNames from 'classnames';
import 'lazysizes';

type MakeRequired<T, K extends keyof T> = Omit<T, K> & Required<{ [P in K]: T[P] }>;

type ImgProps = MakeRequired<JSX.IntrinsicElements['img'], 'alt' | 'src'>;

export default function AsanImage({ alt, src, className, ...otherProps }: ImgProps) {
    return (
        <>
            <noscript>
                <img className={className} src={src} alt={alt} {...otherProps} />
            </noscript>
            <img
                src="/images/lazy-placeholder-16x9.png"
                data-src={src}
                className={classNames(className, 'lazyload')}
                alt={alt}
                {...otherProps}
            />
        </>
    );
}
