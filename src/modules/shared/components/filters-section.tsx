import React, { useRef } from 'react';
import styles from '../styles/filters-section.module.scss';
import { Form } from 'react-bootstrap';
import classNames from 'classnames';

/*function CustomToggle({ eventKey, activeState, setActiveState }) {
    const decoratedOnClick = useAccordionToggle(eventKey, () => {
        setActiveState((prevState) => !prevState);
    });
    return (
        <div
            className={classNames(
                styles['accordion-toggle'],
                styles.select,
                'form-select',
                activeState ? styles['select-active'] : null,
            )}
            onClick={decoratedOnClick}
        >
            <span>فیلتر ها</span>
        </div>
    );
}*/

export default function FiltersSection(props: { filters; filterUpdate?; setLoading? }) {
    //const [filterBtnActive, setFilterBtnActive] = useState(false);
    const cn = classNames;
    const paidCheckboxRef = useRef(null);
    const freeCheckboxRef = useRef(null);

    const onChangeSortDateHandler = (event: any) => {
        if (props.filterUpdate) {
            props.setLoading && props.setLoading(true);
            props.filterUpdate({
                sortDate: event.target.selectedOptions[0].value,
            });
        }
    };

    const onChangeDateHandler = (event: any) => {
        if (props.filterUpdate) {
            props.setLoading && props.setLoading(true);
            props.filterUpdate({
                date: event.target.selectedOptions[0].value,
            });
        }
    };

    const onChangePriceHandler = (event: any) => {
        props.setLoading && props.setLoading(true);
        const el = event.target;
        let price = '';
        let doUpdate = false;
        if (el.id === 'filter-type-free') {
            if (el.checked) {
                paidCheckboxRef.current.checked = false;
                price = 'free';
                doUpdate = true;
            } else if (!paidCheckboxRef.current.checked) {
                doUpdate = true;
            }
        } else if (el.id === 'filter-type-not-free') {
            if (el.checked) {
                freeCheckboxRef.current.checked = false;
                price = 'notfree';
                doUpdate = true;
            } else if (!paidCheckboxRef.current.checked) {
                doUpdate = true;
            }
        }

        if (doUpdate && props.filterUpdate) {
            props.filterUpdate({
                price,
            });
        }
    };

    return (
        <Form className={styles.form}>
            <div className={styles.selects}>
                <div className={styles['filters-main-label']}>مرتب سازی و فیلتر ها:</div>
                <Form.Control
                    as="select"
                    id="sort-date"
                    className={cn('form-select', styles['select'])}
                    onChange={onChangeSortDateHandler}
                    value={props.filters.sortDate}
                >
                    <option value="">انتخاب کنید</option>
                    <option value="desc">تازه ترین</option>
                    <option value="asc">قدیمی ترین</option>
                </Form.Control>
                <Form.Control
                    as="select"
                    id="date"
                    className={cn('form-select', styles['select'])}
                    onChange={onChangeDateHandler}
                    value={props.filters.date}
                >
                    <option value="">انتخاب کنید</option>
                    <option value="year-1">یک سال اخیر</option>
                    <option value="month-6">۶ ماه اخیر</option>
                    <option value="month-3">۳ ماه اخیر</option>
                    <option value="month-1">یک ماه اخیر</option>
                    <option value="week-2">دو هفته اخیر</option>
                    <option value="week-1">هفته اخیر</option>
                    <option value="day-3">۳ روز اخیر</option>
                    <option value="day-1">یک روز قبل</option>
                    <option value="today">امروز</option>
                </Form.Control>
                <span>
                    <span className={styles['filter-label']}>هزینه: </span>
                    <Form.Check
                        className={styles.checkbox}
                        name="price"
                        id="filter-type-not-free"
                        label="پولی"
                        onChange={onChangePriceHandler}
                        ref={paidCheckboxRef}
                        checked={props.filters.price === 'notfree'}
                    />
                    <Form.Check
                        className={styles.checkbox}
                        name="price"
                        id="filter-type-free"
                        label="رایگان"
                        onChange={onChangePriceHandler}
                        ref={freeCheckboxRef}
                        checked={props.filters.price === 'free'}
                    />
                </span>
                {/*<Accordion className={styles.accordion}>
                    <CustomToggle eventKey="0" activeState={filterBtnActive} setActiveState={setFilterBtnActive} />
                    <Accordion.Collapse eventKey="0" className={styles['accordion-collapse']}>
                        <div className={styles.filters}>
                            <div>
                            </div>
                            <div className={styles['duration-filter']}>
                                <div className={styles['filter-label']}>طول کلاس</div>
                                <Form.Check
                                    id="filter-duration-15"
                                    className={styles.checkbox}
                                    name="class-duration"
                                    label="&lt; ۱۵ دقیقه"
                                />
                                <Form.Check
                                    id="filter-duration-1530"
                                    className={styles.checkbox}
                                    name="class-duration"
                                    label="۱۵ - ۳۰ دقیقه"
                                />
                                <Form.Check
                                    id="filter-duration-3060"
                                    className={styles.checkbox}
                                    name="class-duration"
                                    label="۳۰ - ۶۰ دقیقه"
                                />
                                <Form.Check
                                    id="filter-duration-60"
                                    className={styles.checkbox}
                                    name="class-duration"
                                    label="&gt; ۶۰ دقیقه"
                                />
                            </div>
                        </div>
                    </Accordion.Collapse>
                </Accordion>*/}
            </div>
        </Form>
    );
}
