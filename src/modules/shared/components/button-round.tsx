import React from 'react';
import { Button } from 'react-bootstrap';
import classNames from 'classnames';

export default function ButtonRound(props: {
    variant?: string;
    classes?: string;
    type?: 'button' | 'submit' | 'reset';
    otherProps?: any;
    children?: any;
}) {
    const type = props.type === undefined ? 'button' : props.type;
    const classes = classNames('button-round', { [props.classes]: props.classes !== undefined });
    return (
        <Button
            type={type}
            className={classes}
            variant={props.variant || 'outline-secondary'}
            {...(props.otherProps ? props.otherProps : {})}
        >
            {props.children}
        </Button>
    );
}
