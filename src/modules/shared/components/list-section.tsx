import React from 'react';
import CourseCard from './course-card';
import { Col } from 'react-bootstrap';
import { IWebinarCompact } from 'models/webinar-compact';
import { OrganizationType } from 'models/organization';

export default function ListSection(props: {
    data: IWebinarCompact[];
    organizationType?: OrganizationType;
    lastPage?: number;
}) {
    const renderCourseCards = () => {
        return props.data && props.data.length > 0 ? (
            props.data.map((item: IWebinarCompact) => {
                return (
                    <Col sm={6} lg={4} key={item.id} className={'d-flex'}>
                        <CourseCard data={item} />
                    </Col>
                );
            })
        ) : (
            <Col sm={6} lg={4}>
                <div style={{ fontSize: '1.1rem', padding: '2rem 0', textAlign: 'center' }}>
                    <img
                        src="/images/conference.png"
                        style={{ width: '100%', maxWidth: '200px', marginBottom: '1.5rem' }}
                        alt=""
                    />
                    <div>
                        {props.organizationType === OrganizationType.SCHOOL ||
                        props.organizationType === OrganizationType.EDUCATIONAL_INSTITUTION
                            ? 'متاسفانه‌‌ هیچ کلاسی یافت نشد'
                            : 'متاسفانه‌‌ هیچ وبیناری یافت نشد'}
                    </div>
                </div>
            </Col>
        );
    };

    return <>{renderCourseCards()}</>;
}
