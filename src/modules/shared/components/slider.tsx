import React from 'react';
import SwiperCore, { Autoplay, Navigation, Pagination } from 'swiper';
import { Swiper } from 'swiper/react';
import { ISliderOptions } from '../types';

SwiperCore.use([Autoplay, Navigation, Pagination]);

export default function Slider(props: ISliderOptions) {
    const options: ISliderOptions = {
        slidesPerView: 1,
        spaceBetween: 15,
        swiperClasses: '',
        paginationOptions: { pagination: { clickable: true } },
        autoplayOptions: { autoplay: false },
        navigationOptions: { navigation: false },
        breakpoints: null,
        centeredSlides: false,
        freeModeOptions: {
            freeMode: false,
        },
        scrollbarOptions: { scrollbar: false },
    };
    Object.assign(options, { ...props });

    return (
        <Swiper
            slidesPerView={options.slidesPerView}
            spaceBetween={options.spaceBetween}
            {...options.paginationOptions}
            {...options.autoplayOptions}
            {...options.navigationOptions}
            breakpoints={options.breakpoints}
            centeredSlides={options.centeredSlides}
            {...options.freeModeOptions}
            className={options.swiperClasses}
            watchOverflow={true}
        >
            {props.children}
        </Swiper>
    );
}
