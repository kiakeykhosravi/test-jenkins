import React, { useState } from 'react';
import styles from '../styles/asan-upload.module.scss';
import { Form } from 'react-bootstrap';
import {
    FileEarmarkFill,
    FileEarmarkFontFill,
    FileEarmarkImageFill,
    FileEarmarkPdfFill,
    FileEarmarkPlusFill,
    FileEarmarkZipFill,
} from 'react-bootstrap-icons';
import classNames from 'classnames';

type IconType = 'general' | 'plus' | 'text' | 'image' | 'pdf' | 'zip';

export default function AsanUpload(props: {
    name: string;
    iconType: IconType;
    containerClassName?;
    iconClassName?;
    labelText?: string;
    required?: boolean | string;
    register;
    onChange?;
}) {
    const cn = classNames;
    const [resumeFileName, setResumeFileName] = useState(null);
    const accept = () => {
        switch (props.iconType) {
            case 'image':
                return 'image/*';
            case 'pdf':
                return '.pdf';
            case 'plus':
                return '*';
            case 'text':
                return '.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,.txt';
            case 'zip':
                return '.zip,.rar,.7zip,application/octet-stream,application/zip,application/x-zip,application/x-zip-compressed';
            default:
                return '*';
        }
    };
    const renderIcon = () => {
        const fileInputIconStyle = cn(styles['file-input-icon'], props.iconClassName || '');
        switch (props.iconType) {
            case 'image':
                return <FileEarmarkImageFill className={fileInputIconStyle} />;
            case 'pdf':
                return <FileEarmarkPdfFill className={fileInputIconStyle} />;
            case 'plus':
                return <FileEarmarkPlusFill className={fileInputIconStyle} />;
            case 'text':
                return <FileEarmarkFontFill className={fileInputIconStyle} />;
            case 'zip':
                return <FileEarmarkZipFill className={fileInputIconStyle} />;
            default:
                return <FileEarmarkFill className={fileInputIconStyle} />;
        }
    };
    return (
        <div className={cn(styles['file-input'], props.containerClassName || '')}>
            <Form.File
                name={props.name || 'file'}
                onChange={(e) => {
                    setResumeFileName(
                        (e.target && e.target.files && e.target.files.length && e.target.files[0].name) || '',
                    );
                    if (props.onChange) {
                        props.onChange();
                    }
                }}
                className={styles['file-input-form']}
                ref={props.register({ required: props.required || false }) || null}
                accept={accept()}
            />
            <Form.Label className={styles['file-input-inner-content']}>
                <div className={styles['file-input-ui']}>
                    <div className={styles['file-input-ui-label']}>
                        {renderIcon()}
                        <span className={styles['fil-input-choose-label']}>{props.labelText || 'انتخاب فایل'}</span>
                    </div>
                    {resumeFileName ? <div className={styles['file-input-name']}>{resumeFileName}</div> : null}
                </div>
            </Form.Label>
        </div>
    );
}
