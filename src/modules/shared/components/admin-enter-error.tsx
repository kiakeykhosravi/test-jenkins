import styles from 'styles/error.module.scss';
import Head from 'next/head';
import { Image } from 'react-bootstrap';
import Link from 'next/link';
import React from 'react';

export default function AdminEnterError() {
    return (
        <div className={styles.main}>
            <Head>
                <title>خطا در ورود به ادمین</title>
            </Head>
            <div className={styles.image}>
                <Image src="/images/error-img.png" alt="learn" className={styles.logo} />
            </div>
            <div className={styles.error}>
                <br />
                <div className={styles['err-txt']}>
                    <h4>خطایی در ورود به پنل ادمین رخ داد</h4>
                </div>
            </div>
            <Image src="/images/asanseminar_full.png" alt="learn" className={styles['err-img']} />
            <div className={styles.return}>
                <Link href="/">
                    <a title="بازگشت به آسان سمینار" className="btn">
                        بازگشت به آسان سمینار
                    </a>
                </Link>
            </div>
        </div>
    );
}
