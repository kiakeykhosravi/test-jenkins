import React from 'react';
import Banner from './banner';
import Slider from './slider';
import { SwiperSlide } from 'swiper/react';
import styles from '../styles/slider-banner.module.scss';
import classNames from 'classnames';
import Link from 'next/link';
import { ISlide } from 'models/slide';

export default function SliderBanner(props: {
    bannerClasses?: string;
    bannerBodyClasses?: string;
    swiperClasses?: string;
    slideClasses?: string;
    autoplay?: { autoplay: boolean | { delay?: number; reverseDirection?: boolean } };
    slides: ISlide[];
    navigationOptions?;
    paginationOptions?;
}) {
    const options = {
        paginationOptions: props.paginationOptions ? props.paginationOptions : { pagination: { clickable: true } },
    };
    const renderSlides = (slides: ISlide[]) => {
        return slides.map((slide, index) => {
            return (
                <SwiperSlide className={styles.slide} key={index}>
                    {slide.url ? (
                        <Link href={slide.url}>
                            <a dir="rtl" title={slide.title}>
                                <img className={props.slideClasses} src={slide.media.url} alt={slide.title} />
                            </a>
                        </Link>
                    ) : (
                        <>
                            <img
                                className={props.slideClasses}
                                src={slide.media.url}
                                alt={slide.media.title}
                                dir="rtl"
                            />
                        </>
                    )}
                </SwiperSlide>
            );
        });
    };

    return (
        <Banner
            bannerClasses={classNames(styles.banner, props.bannerClasses)}
            bodyClasses={classNames(props.bannerBodyClasses, styles['banner-body'])}
        >
            <div dir="ltr" style={{ height: '100%' }}>
                <Slider
                    autoplayOptions={props.autoplay}
                    swiperClasses={props.swiperClasses}
                    navigationOptions={props.navigationOptions ? props.navigationOptions : {}}
                    paginationOptions={options.paginationOptions}
                >
                    {renderSlides(props.slides)}
                </Slider>
            </div>
        </Banner>
    );
}
