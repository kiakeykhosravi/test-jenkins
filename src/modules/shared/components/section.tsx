import React from 'react';
import Link from 'next/link';
import classNames from 'classnames';
import styles from '../styles/section.module.scss';

export default function Section(props: {
    title?: string;
    mainTitle?: boolean;
    titleClasses?: string;
    moreButton?: any;
    classes?: string;
    isCarousel?: boolean;
    itemCount?: number;
    itemsData?: any;
    children?: any;
}) {
    const cn = classNames;

    const renderTitle = () => {
        return props.title ? (
            props.mainTitle ? (
                <h1 className={classNames(styles.title, props.titleClasses)}>{props.title}</h1>
            ) : (
                <h2 className={classNames(styles.title, props.titleClasses)}>{props.title}</h2>
            )
        ) : null;
    };

    const renderMoreButton = () => {
        if (props.moreButton) {
            return (
                <div className={styles['more-button']}>
                    <Link href={'/webinars'}>
                        <a className={cn(props.moreButton.classes, styles['btn'])} title={props.moreButton.children}>
                            {props.moreButton.children}
                        </a>
                    </Link>
                </div>
            );
        }
        return null;
    };

    return (
        <div className={cn(styles['card-section'], props.classes)}>
            {renderTitle()}
            {props.children}
            {renderMoreButton()}
        </div>
    );
}
