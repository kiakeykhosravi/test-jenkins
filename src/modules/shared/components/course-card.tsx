import React from 'react';
import { Card } from 'react-bootstrap';
import styles from '../styles/course-card.module.scss';
//import { Bookmark, BookmarkFill } from 'react-bootstrap-icons';
import Link from 'next/link';
import classNames from 'classnames';
import { IWebinarCompact } from 'models/webinar-compact';
import toPrice from 'utils/to-price';
import AsanImage from './asan-image';

export default function CourseCard(props: {
    data: IWebinarCompact;
    cardClasses?: string;
    bodyClasses?: string;
    imgClasses?: string;
    showPrice?: boolean;
}) {
    const webinarLink = `/webinars/${props.data.id}/${props.data.title.trim().replace(/\s+/g, '-')}`;
    const showPrice = props.showPrice === undefined ? true : props.showPrice;
    const cn = classNames;

    // TODO: !! TO BE RECEIVED FROM API
    /*const [favorite, setFavorite] = useState(Math.random() > 0.5);
    const subscriberCount = Math.floor(Math.random() * 20) + 5;
    const time = Math.floor(Math.random() * 100) + 150;

    // TODO: Temporary. Must be changed after apis are added
    const favoriteCard = () => {
        setFavorite((prevState) => !prevState);
    };*/

    const renderPrice = () => {
        return (
            <Link href={`/webinars/${props.data.id}/${props.data.title}`}>
                <a title={props.data.title} className={styles['price-link']}>
                    <span className={styles.price}>
                        {props.data.price_with_off == 0 || props.data.price == 0
                            ? 'رایگان'
                            : toPrice(props.data.price_with_off) + ' تومان' || toPrice(props.data.price) + ' تومان'}
                    </span>
                </a>
            </Link>
        );
    };

    return (
        <Card className={cn(styles.card, props.cardClasses)}>
            <Link href={webinarLink}>
                <a title={props.data.title}>
                    <AsanImage
                        src={props.data.media && props.data.media.length && props.data.media[0].thumbnail_url}
                        className={cn(styles['card-img'], props.imgClasses)}
                        alt={props.data.title}
                    />
                </a>
            </Link>
            <Card.Body className={cn(styles['card-body'], props.bodyClasses)}>
                <div className={styles.main}>
                    {/*<div className={styles.top}>
                        <span className={styles.subscriber}>{toPersianNum(subscriberCount + ' شرکت کننده')}</span>
                        <span className={styles.time}>{toPersianNum(time + ' ساعت')}</span>
                    </div>*/}
                    <h3>
                        <Link href={webinarLink}>
                            <a className={styles.title} title={props.data.title}>
                                {props.data.title}
                            </a>
                        </Link>
                    </h3>
                    <div className={styles.bottom}>
                        <span>
                            {/*<Link href="/instructors">*/}
                            <span className={styles.name} title="اساتید">
                                مدرس:{' '}
                            </span>
                            {/*</Link>*/}
                            {props.data.instructor ? (
                                // <Link href={`/instructors/${props.data.instructor.id}`}>
                                <span className={styles.name} title={props.data.instructor.fullname}>
                                    {props.data.instructor.fullname}
                                </span>
                            ) : // </Link>
                            null}
                        </span>
                        {/*{favorite ? (
                            <BookmarkFill
                                className={styles.favorite}
                                style={{ color: '#e74c3c' }}
                                onClick={favoriteCard}
                            />
                        ) : (
                            <Bookmark className={styles.favorite} onClick={favoriteCard} />
                        )}*/}
                    </div>
                </div>
                <div>
                    <div className={styles.footer} style={showPrice ? { justifyContent: 'space-between' } : {}}>
                        <div className={styles.owner}>
                            <Link href={`/organizations/${props.data.organization.slug}`}>
                                <a title={props.data.organization.title}>
                                    <img
                                        src={
                                            (props.data.organization.logo && props.data.organization.logo.url) ||
                                            '/images/logos/asanseminar-logo.svg'
                                        }
                                        className={styles.logo}
                                        alt={props.data.organization.title}
                                    />
                                </a>
                            </Link>
                            <span className={styles.name}>
                                برگزارکننده:{' '}
                                <Link href={`/organizations/${props.data.organization.slug}`}>
                                    <a className={styles.name} title={props.data.organization.title}>
                                        {props.data.organization.title}
                                    </a>
                                </Link>
                            </span>
                        </div>
                        {showPrice ? renderPrice() : null}
                    </div>
                </div>
            </Card.Body>
        </Card>
    );
}
