import React from 'react';
import ReactPaginate from 'react-paginate';
import styles from '../styles/pagination.module.scss';
import toPersianNum from 'utils/to-persian-num';

export default function Pagination(props: {
    pageCount: number;
    page: string | string[];
    marginPagesDisplayed?: number;
    pageRangeDisplayed?: number;
    handlePageClick: any;
}) {
    return (
        <div className={styles['pagination-container']}>
            <ReactPaginate
                previousLabel={'قبلی'}
                nextLabel={'بعدی'}
                breakLabel={'...'}
                pageCount={props.pageCount}
                forcePage={Number(props.page) - 1}
                marginPagesDisplayed={props.marginPagesDisplayed || 2}
                pageRangeDisplayed={props.pageRangeDisplayed || 2}
                onPageChange={props.handlePageClick}
                pageLabelBuilder={(page) => toPersianNum(page)}
                containerClassName={styles.pagination}
                pageClassName={styles['page-item']}
                pageLinkClassName={styles['page-link']}
                activeClassName={styles.active}
                activeLinkClassName={styles['link-active']}
                disabledClassName={styles.disabled}
                previousClassName={styles.previous}
                previousLinkClassName={styles['previous-link']}
                nextClassName={styles.next}
                nextLinkClassName={styles['next-link']}
                breakClassName={styles['break-me']}
            />
        </div>
    );
}
