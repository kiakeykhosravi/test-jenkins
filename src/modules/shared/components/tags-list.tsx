import React, { useContext } from 'react';
import styles from '../styles/tags-list.module.scss';
import classNames from 'classnames';
import { ITag } from 'models/tag';
import Link from 'next/link';
import { Form } from 'react-bootstrap';
import { TagsFilterContext } from 'contexts/tags-filter-context';

export default function TagsList(props: {
    data: ITag[];
    containerClasses?: string;
    textClasses?: string;
    updateTags?;
    type?: 'blog' | 'webinar';
    areLinks?: boolean;
    simpleLink?: string;
    arePersistent?: boolean;
    tagUpdate?;
    limitAction?: boolean;
    toggleLimitAction?;
    isH3?: boolean;
    firstLinkedTag?: { tag: ITag; link: string };
}) {
    const cn = classNames;

    const tagsFilterContext = useContext(TagsFilterContext);

    const areLinks = props.areLinks === undefined ? false : props.areLinks;
    const arePersistent = props.arePersistent === undefined ? false : props.arePersistent;

    const isDefaultChecked = (tag: ITag) => {
        const urlSearchParams = new URLSearchParams(window.location.search);
        const params = Object.fromEntries(urlSearchParams.entries());
        const tags = () => {
            return params.tags ? params.tags.split(',') : [];
        };
        return (
            tags().find((t) => {
                return t === tag.title.replace(/\s+/g, '-');
            }) !== undefined
        );
    };

    const renderTags = () => {
        return props.data.map((data: ITag) => {
            if (areLinks) {
                return (
                    <li
                        className={cn(styles.title, props.textClasses)}
                        key={data.id}
                        onClick={() => {
                            tagsFilterContext.changeTags([data]);
                        }}
                    >
                        <Link href={`/webinars?tags=${data.title.replace(/\s+/g, '-')}`}>
                            <a title={data.title} className={styles['link-tag']}>
                                {data.title}
                            </a>
                        </Link>
                    </li>
                );
            }
            if (arePersistent) {
                return (
                    <li className={cn(styles['persistent-title'], props.textClasses)} key={data.id}>
                        <Form.Check
                            name={`tags`}
                            id={data.title}
                            label={data.title}
                            defaultChecked={isDefaultChecked(data)}
                            onChange={props.tagUpdate ? props.tagUpdate : null}
                        />
                    </li>
                );
            }
            if (props.type && props.type === 'blog') {
                return (
                    <li className={cn(styles.title, props.textClasses)} key={data.id}>
                        <Link href={`/blog/tag/${data.title.replace(/\s+/g, '-')}`}>
                            <a title={data.title} className={styles['link-tag']}>
                                {data.title}
                            </a>
                        </Link>
                    </li>
                );
            }
            if (props.simpleLink) {
                return (
                    <li className={cn(styles.title, props.textClasses)} key={data.id}>
                        <Link href={props.simpleLink}>
                            <a title={data.title} className={styles['link-tag']}>
                                {data.title}
                            </a>
                        </Link>
                    </li>
                );
            }
            if (props.isH3) {
                return (
                    <h3 className={cn(styles.title, props.textClasses)} key={data.title}>
                        {data.title}
                    </h3>
                );
            }
            return (
                <li className={cn(styles.title, props.textClasses)} key={data.title}>
                    {data.title}
                </li>
            );
        });
    };

    const renderActionLimit = () => {
        if (props.limitAction !== undefined) {
            return (
                <li className={cn(styles.title, styles['limit-action'])} onClick={() => props.toggleLimitAction()}>
                    {props.limitAction ? 'بیشتر' : 'کمتر'}
                </li>
            );
        }
    };

    return (
        <ul className={cn(styles.list, props.containerClasses)}>
            {props.firstLinkedTag ? (
                <Link href={props.firstLinkedTag.link}>
                    <a title={props.firstLinkedTag.tag.title} className={styles['link-tag']}>
                        <h3 className={cn(styles.title, props.textClasses)}>{props.firstLinkedTag.tag.title}</h3>
                    </a>
                </Link>
            ) : null}
            {renderTags()}
            {renderActionLimit()}
        </ul>
    );
}
