import React, { useState, useEffect, useRef } from 'react';
import { Form } from 'react-bootstrap';
import styles from '../styles/searchbar.module.scss';
import { Search } from 'react-bootstrap-icons';
import classNames from 'classnames';
import HeaderDropdown from 'modules/search/components/header-dropdown';
import { IApiResponseType } from 'api/types';
import { ISearchHeaderResponse } from 'modules/search/responses';
import { Router, useRouter } from 'next/router';
import { IOrganizationCompact } from 'models/organization-compact';
import { IWebinarCompact } from 'models/webinar-compact';
import { IUserSafe } from 'models/user-safe';
import { ITag } from 'models/tag';

type searchbarType = 'organizations' | 'organization_webinars' | 'webinars' | 'header' | 'instructors';

// TODO: Heavy refactoring required here
export default function Searchbar(props: {
    initialValue?: string;
    placeHolder?: string;
    containerClasses?: string;
    fieldClasses?: string;
    icon?: boolean;
    iconClasses?: string;
    nonEmptyClasses?: string;
    action?;
    method?: 'GET' | 'POST' | 'DIALOG';
    isHeader?: boolean;
    setData?;
    page?;
    resetPage?;
    customHandler?;
    setLoading?;
    type?: searchbarType;
    slug?: string;
}) {
    const cn = classNames;
    const hasIcon = props.icon === undefined || props.icon;
    const fieldClasses = cn(props.fieldClasses, styles['search-field']);
    const iconClasses = cn(props.iconClasses, styles['search-icon']);
    const searchInputRef = useRef(null);
    const isHeader = props.isHeader === undefined ? false : props.isHeader;

    const [containerClasses, setContainerClasses] = useState(
        cn(props.containerClasses, styles['search-field-container']),
    );

    const [input, setInput] = useState(props.initialValue || '');
    const [dropdownVisible, setDropdownVisible] = useState(false);
    const [debouncingTimer, setDebouncingTimer] = useState(null);
    const [dropDownLoading, setDropDownLoading] = useState(false);
    const [dropDownData, setDropdownData] = useState<{
        webinars: IWebinarCompact[];
        organizations: IOrganizationCompact[];
        instructors: IUserSafe[];
        tags: ITag[];
    }>({
        webinars: [],
        organizations: [],
        instructors: [],
        tags: [],
    });
    const [lastSearchPhrase, setLastSearchPhrase] = useState(props.initialValue || '');

    const [previousPage, setPreviousPage] = useState(props.page);

    useEffect(() => {
        setInput(props.initialValue);
    }, [props.initialValue]);

    useEffect(() => {
        // Note: Coercion is intentional
        if (!isHeader && props.page && previousPage != props.page) {
            setPreviousPage(props.page);
            const result: Promise<IApiResponseType<any>> = props.action(searchInputRef.current.value, props.page);

            setLastSearchPhrase(searchInputRef.current.value);

            result
                .then((response) => {
                    if (response.status === 'data') {
                        props.setData({ ...response.data });
                    }
                })
                .catch((error) => {
                    console.log('An error occurred in search api');
                });
        }
    }, [props.page]);

    useEffect(() => {
        if (props.initialValue) searchInputRef.current.defaultValue = props.initialValue;
        if (props.nonEmptyClasses && searchInputRef.current.value !== '') {
            setContainerClasses((prevState: string) => {
                return cn(prevState, props.nonEmptyClasses);
            });
        } else {
            setContainerClasses(cn(props.containerClasses, styles['search-container']));
        }

        Router.events.on('routeChangeStart', onClickBackdrop);
        Router.events.on('routeChangeComplete', routeChangeHandle);
        return () => {
            Router.events.off('routeChangeStart', onClickBackdrop);
            Router.events.off('routeChangeComplete', routeChangeHandle);
        };
    }, []);

    const onChangeHandler = (event: any): void => {
        setInput(event.target.value);
        props.setLoading && props.setLoading(true);
        if (event.target.value !== '') {
            if (props.nonEmptyClasses && containerClasses.indexOf(props.nonEmptyClasses) === -1) {
                setContainerClasses((prevState: string) => {
                    return cn(prevState, props.nonEmptyClasses);
                });
            }

            if (isHeader) {
                setDropdownVisible(true);
                setDropDownLoading(true);
            }

            clearTimeout(debouncingTimer);
            setDebouncingTimer(
                setTimeout(() => {
                    if (!props.action && !props.customHandler) {
                        return;
                    }

                    if (props.customHandler) {
                        if (lastSearchPhrase === event.target.value) {
                            return;
                        }
                        setLastSearchPhrase(event.target.value);

                        props.customHandler(event.target.value);
                    } else if (isHeader) {
                        if (lastSearchPhrase === event.target.value) {
                            setDropDownLoading(false);
                            return;
                        }

                        const result: Promise<IApiResponseType<ISearchHeaderResponse>> = props.action(
                            event.target.value,
                        );

                        setLastSearchPhrase(event.target.value);

                        result
                            .then((response) => {
                                if (response.status === 'data') {
                                    setDropdownData({ ...response.data });
                                }
                            })
                            .catch((error) => {
                                console.log('An error occurred in search api');
                            })
                            .finally(() => {
                                setDropDownLoading(false);
                            });
                    } else if (props.setData) {
                        if (lastSearchPhrase === event.target.value) {
                            return;
                        }

                        props.resetPage();

                        const result: Promise<IApiResponseType<any>> = props.action(event.target.value);

                        setLastSearchPhrase(event.target.value);

                        result
                            .then((response) => {
                                if (response.status === 'data') {
                                    props.setData({ ...response.data });
                                }
                            })
                            .catch((error) => {
                                console.log('An error occurred in search api');
                            });
                    }
                }, 1000),
            );
        } else if (!isHeader && (props.setData || props.customHandler)) {
            if (lastSearchPhrase === '') {
                return;
            }

            clearTimeout(debouncingTimer);
            setDebouncingTimer(
                setTimeout(() => {
                    setLastSearchPhrase('');

                    if (props.customHandler) {
                        props.customHandler(event.target.value);
                        return;
                    }

                    props.resetPage();

                    const result: Promise<IApiResponseType<any>> = props.action(event.target.value);

                    result
                        .then((response) => {
                            if (response.status === 'data') {
                                props.setData({ ...response.data });
                            }
                        })
                        .catch((error) => {
                            console.log('An error occurred in search api');
                        });
                }, 1000),
            );
        } else {
            clearTimeout(debouncingTimer);
            setDropdownVisible(false);
            setDropDownLoading(false);
            setContainerClasses(cn(props.containerClasses, styles['search-container']));
        }
    };

    const onFocusHandler = (event: any): void => {
        if (event.target.value !== '') {
            setDropdownVisible(true);
        }
    };

    const onClickBackdrop = (): void => {
        setDropdownVisible(false);
    };

    const routeChangeHandle = () => {
        if (props.type && props.type === 'organization_webinars' && props.slug) {
            if (!window.history.state.as.startsWith(`/organizations/${props.slug}`)) {
                setInput('');
            }
        }
    };

    return (
        <>
            <div className={cn(containerClasses)}>
                <Form.Group controlId="search-field" style={{ width: '100%' }}>
                    <div className={styles['search-field-container']}>
                        <Form.Control
                            value={input}
                            ref={searchInputRef}
                            type="text"
                            placeholder={props.placeHolder || 'جستجو کنید ...'}
                            className={cn(fieldClasses)}
                            onChange={
                                props.nonEmptyClasses || isHeader || props.action || props.customHandler
                                    ? onChangeHandler
                                    : null
                            }
                            onFocus={isHeader ? onFocusHandler : null}
                        />
                        {hasIcon ? <Search className={iconClasses} /> : null}
                    </div>
                </Form.Group>
                {renderSearchDropdown(
                    dropdownVisible,
                    dropDownLoading,
                    dropDownData.tags,
                    dropDownData.webinars,
                    dropDownData.organizations,
                    dropDownData.instructors,
                )}
            </div>
            {isHeader ? (
                <div className={cn({ [styles['backdrop-header']]: dropdownVisible })} onClick={onClickBackdrop} />
            ) : null}
        </>
    );
}

function renderSearchDropdown(
    visible = false,
    isLoading: boolean,
    tags = [],
    webinars = [],
    organizations = [],
    instructors = [],
) {
    return visible ? (
        <HeaderDropdown
            isLoading={isLoading}
            tags={tags}
            webinars={webinars}
            organizations={organizations}
            instructors={instructors}
        />
    ) : null;
}
