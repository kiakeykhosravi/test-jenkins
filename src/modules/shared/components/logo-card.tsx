import React from 'react';
import Link from 'next/link';
import styles from '../styles/logo-card.module.scss';
import { Card, Col } from 'react-bootstrap';
import { IUserSafe } from 'models/user-safe';
import AsanImage from 'modules/shared/components/asan-image';
import { IOrganizationCompact } from 'models/organization-compact';

export default function LogoCard(props: { data: IOrganizationCompact | IUserSafe }) {
    const media = 'logo' in props.data ? props.data.logo : null;
    const imgSrc =
        (media && (Array.isArray(media) ? media[0] : media).thumbnail_url) || '/images/avatar-placeholder.png';
    const name = 'fullname' in props.data ? props.data.fullname : props.data.title;
    const linkAddress =
        'fullname' in props.data
            ? `/instructors/${props.data.id}/${props.data.fullname}`
            : `/organizations/${props.data.slug}`;
    const linkTitle = 'fullname' in props.data ? 'صفحه استاد ' : 'دوره های سازمان ';
    const linkText = 'fullname' in props.data ? 'صفحه استاد' : 'دوره های این سازمان';

    return (
        <Col xs={12} sm={6} md={6} lg={4} xl={3} className={styles.col}>
            <Card className={styles.card}>
                <div className={styles.header}>
                    <Link href={linkAddress}>
                        <a title={`${linkTitle}${name}`}>
                            <AsanImage src={imgSrc} alt={name} className={styles.pic} />
                        </a>
                    </Link>
                </div>
                <div className={styles.body}>
                    <div className={styles.content}>
                        <div className={styles.name}>{name}</div>
                    </div>
                    <div className={styles.footer}>
                        <Link href={linkAddress}>
                            <a className={styles.link} title={`${linkTitle}${name}`}>
                                {linkText}
                            </a>
                        </Link>
                    </div>
                </div>
            </Card>
        </Col>
    );
}
