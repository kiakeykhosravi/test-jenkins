export interface ISliderOptions {
    slidesPerView?: number;
    spaceBetween?: number;
    swiperClasses?: string;
    paginationOptions?: {
        // eslint-disable-next-line prettier/prettier
        pagination: boolean
            | {
                  el?: any;
                  clickable?: boolean;
                  type?: 'bullets' | 'fraction' | 'progressbar';
              };
    };
    autoplayOptions?: {
        // eslint-disable-next-line prettier/prettier
        autoplay: boolean
            | {
                  delay?: number;
                  reverseDirection?: boolean;
              };
    };
    navigationOptions?: {
        // eslint-disable-next-line prettier/prettier
        navigation: boolean
            | {
                  nextEl?: string;
                  prevEl?: string;
              };
    };
    breakpoints?: Record<
        number,
        {
            width?: number;
            slidesPerView?: number;
            slidesPerGroup?: number;
            slidesPerColumn?: number;
            spaceBetween?: number;
        }
    >;
    centeredSlides?: boolean;
    freeModeOptions?: {
        freeMode: boolean;
        freeModeMinimumVelocity?: number;
        freeModeMomentum?: boolean;
        freeModeMomentumBounce?: boolean;
        freeModeMomentumBounceRatio?: number;
        freeModeMomentumRatio?: number;
        freeModeMomentumVelocityRatio?: number;
        freeModeSticky?: boolean;
    };
    scrollbarOptions?: {
        // eslint-disable-next-line prettier/prettier
        scrollbar: boolean
            | {
                  el?: string;
                  hide?: boolean;
                  draggable?: boolean;
              };
    };
    children?: any;
}
