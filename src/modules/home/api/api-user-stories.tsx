import apiRequest from 'api/api-request';
import { IUserStory } from 'models/user-story';
import { IUserStoryResponse } from '../responses';
import endpoints from 'api/api-endpoints';

export default async function apiUserStories(): Promise<{ data?: IUserStory[]; status: 'error' | 'data' }> {
    const response = await apiRequest<IUserStoryResponse>('get', endpoints.backend.home_user_stories);

    if (response.status === 'data') {
        return {
            data: response.data.feedbacks,
            status: response.status,
        };
    } else {
        return {
            status: 'error',
        };
    }
}
