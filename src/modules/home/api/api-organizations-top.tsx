import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IOrganizationsResponse } from '../responses';
import { IOrganizationCompact } from 'models/organization-compact';

export default async function apiOrganizationsTop(): Promise<{
    data?: IOrganizationCompact[];
    status: 'error' | 'data';
}> {
    const result = await apiRequest<IOrganizationsResponse>('get', endpoints.backend.home_organizations_top);

    if (result.status === 'data') {
        return {
            data: result.data.organizations,
            status: result.status,
        };
    }

    return {
        status: 'error',
    };
}
