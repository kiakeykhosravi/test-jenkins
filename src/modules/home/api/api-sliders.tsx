import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { ISliderResponse } from '../responses';
import { ISlide } from 'models/slide';

export default async function apiSliders(): Promise<{ data?: ISlide[]; status: 'error' | 'data' }> {
    const response = await apiRequest<ISliderResponse>('get', endpoints.backend.home_top_banners);

    if (response.status === 'data') {
        return {
            data: response.data.sliders,
            status: 'data',
        };
    }
    return {
        status: 'error',
    };
}
