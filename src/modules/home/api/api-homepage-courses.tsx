import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { ICoursesHomepageResponse } from '../responses';
import { IWebinarCompact } from 'models/webinar-compact';

export default async function apiHomepageCourses(): Promise<{ data?: IWebinarCompact[]; status: 'error' | 'data' }> {
    const response = await apiRequest<ICoursesHomepageResponse>('get', endpoints.backend.home_courses);

    if (response.status === 'data') {
        return {
            data: response.data.webinars,
            status: 'data',
        };
    }
    return {
        status: 'error',
    };
}
