import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IHomepageMetadataResponse } from '../responses';

export default async function apiHomepageMetadata(): Promise<{
    data?: IHomepageMetadataResponse;
    status: 'error' | 'data';
}> {
    const response = await apiRequest<IHomepageMetadataResponse>('get', endpoints.backend.home_metadata);

    if (response.status === 'data') {
        return {
            data: response.data,
            status: 'data',
        };
    }
    return {
        status: 'error',
    };
}
