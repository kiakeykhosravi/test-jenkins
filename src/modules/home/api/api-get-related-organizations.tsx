import endpoints from 'api/api-endpoints';
import { IRelatedOrganizationsResponse } from '../responses';
import { toast } from 'react-toastify';
import { AxiosRequestConfig } from 'axios';
import apiRequest from 'api/api-request';

export default async function apiGetRelatedOrganizations(
    isBackend = false,
    config?: AxiosRequestConfig,
): Promise<{ data?: IRelatedOrganizationsResponse; status: 'error' | 'data' }> {
    const url = isBackend ? endpoints.backend.related_organizations : endpoints.frontend.related_organizations;
    const response = await apiRequest<IRelatedOrganizationsResponse>(
        'get',
        url,
        undefined,
        undefined,
        undefined,
        config,
    );

    if (response.status === 'data') {
        return {
            data: response.data,
            status: response.status,
        };
    } else {
        if (response.errorMeta.response.status !== 401) {
            toast.error(response.message || 'خطایی در دریافت سازمان های مرتبط با شما رخ داد');
        }
        return {
            status: 'error',
        };
    }
}
