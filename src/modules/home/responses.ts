import { IUserStory } from 'models/user-story';
import { IWebinarCompact } from 'models/webinar-compact';
import { IBlog } from 'models/blog';
import { IOrganizationCompact } from 'models/organization-compact';
import { ISlide } from 'models/slide';

export interface IOrganizationsResponse {
    organizations: IOrganizationCompact[];
}

export interface IUserStoryResponse {
    feedbacks: IUserStory[];
}

export interface ICoursesHomepageResponse {
    webinars: IWebinarCompact[];
}

export interface IBlogHomepageResponse {
    blogs: IBlog[];
}

export interface ISliderResponse {
    sliders: ISlide[];
}

export interface IRelatedOrganizationsResponse {
    organizations: IOrganizationCompact[];
}

export interface IHomepageMetadataResponse {
    users_count: number;
    webinars_count: number;
    meetings_count: number;
}
