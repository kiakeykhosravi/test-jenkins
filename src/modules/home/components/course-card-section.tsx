import React from 'react';
import Section from 'modules/shared/components/section';
import CourseCard from 'modules/shared/components/course-card';
import Slider from 'modules/shared/components/slider';
import { SwiperSlide } from 'swiper/react';
import styles from '../styles/course-section.module.scss';
import classNames from 'classnames';
import ButtonRound from 'modules/shared/components/button-round';
import { Container } from 'react-bootstrap';
import Link from 'next/link';
import { IWebinarCompact } from 'models/webinar-compact';
import { ArrowLeft } from 'react-bootstrap-icons';

export default function CourseCardSection(props: { data: IWebinarCompact[]; classes?: string }) {
    const cn = classNames;

    const renderCourseCards = () => {
        return props.data.map((item: IWebinarCompact) => {
            return (
                <SwiperSlide key={item.id} className={styles['swiper-slide']} dir="rtl">
                    <CourseCard data={item} showPrice={false} imgClasses={styles['card-img-homepage']} />
                </SwiperSlide>
            );
        });
    };

    const breakpoints = {
        420: {
            slidesPerView: 1,
            spaceBetween: 8,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 25,
        },
        992: {
            slidesPerView: 3,
            spaceBetween: 15,
        },
        1200: {
            slidesPerView: 3,
            spaceBetween: 25,
        },
        1400: {
            slidesPerView: 4,
            spaceBetween: 30,
        },
    };

    return (
        <Section
            title="برگزاری وبینار و کلاس آنلاین آسان سمینار"
            mainTitle={true}
            titleClasses={styles.title}
            classes={cn(styles.section, 'gradient-background', props.classes)}
        >
            <Container>
                <div className={styles['card-container']} dir="ltr">
                    <Slider
                        spaceBetween={15}
                        paginationOptions={{ pagination: false }}
                        navigationOptions={{
                            navigation: {
                                nextEl: '.' + styles['slider-btn-next'],
                                prevEl: '.' + styles['slider-btn-prev'],
                            },
                        }}
                        breakpoints={breakpoints}
                        swiperClasses={styles['swiper-main']}
                        autoplayOptions={{
                            autoplay: {
                                delay: 20000,
                            },
                        }}
                    >
                        {renderCourseCards()}
                    </Slider>
                    <div className={cn('slider-btn-next', styles['slider-btn-next'])} />
                    <div className={cn('slider-btn-prev', styles['slider-btn-prev'])} />
                    <Link href="/webinars">
                        <a title="وبینار هایی که در آسان سمینار برگزار شده اند">
                            <ButtonRound variant="warning" type="button" classes={styles.button}>
                                <ArrowLeft className={styles['button-arrow']} />
                                <h2>وبینار هایی که در آسان سمینار برگزار شده اند</h2>
                            </ButtonRound>
                        </a>
                    </Link>
                </div>
            </Container>
        </Section>
    );
}
