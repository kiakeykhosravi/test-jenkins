import React from 'react';
import Banner from 'modules/shared/components/banner';
import SliderBanner from 'modules/shared/components/slider-banner';
import { Container, Row, Col } from 'react-bootstrap';
import styles from '../styles/banner-section.module.scss';
import classNames from 'classnames';
import Link from 'next/link';
import { ISlide } from 'models/slide';
import { IOrganizationCompact } from 'models/organization-compact';
import useWindowDimensions from 'utils/use-window-dimensions';
import YourMeetingsIndicator from 'modules/layout/components/your-meetings-indicator';

export default function BannerSection(props: { slides: ISlide[]; organizations?: IOrganizationCompact[] }) {
    const HARD_CODED_TOP_BANNER_ADDRESS = '/contact-us';
    const cn = classNames;
    const { width } = useWindowDimensions();

    const sliders = ((): { slider1: ISlide[]; slider2: ISlide[]; slider3: ISlide[]; slider4: ISlide[] } => {
        const slider1 = props.slides.filter((slider) => {
            return slider.location === 'top_thin_slider';
        });
        const slider2 = props.slides.filter((slider) => {
            return slider.location === 'main_big_slider';
        });
        const slider3 = props.slides.filter((slider) => {
            return slider.location === 'main_cta_top_slider';
        });
        const slider4 = props.slides.filter((slider) => {
            return slider.location === 'main_cta_down_slider';
        });
        return { slider1, slider2, slider3, slider4 };
    })();

    return (
        <Container className={styles['main-container']}>
            {
                //sliders.slider1.length || true ? (
                <Row className={styles['top-banner']}>
                    <Col>
                        <div className={styles['top-banner-mobile-padding']}>
                            <Banner bannerClasses={styles.banner} bodyClasses={styles['banner-body']}>
                                <Link href={HARD_CODED_TOP_BANNER_ADDRESS}>
                                    <a title="برگزاری وبینار">
                                        <img
                                            src={'/images/top-thin-banner.jpg'}
                                            className={styles.img}
                                            alt="برگزاری وبینار"
                                        />
                                    </a>
                                </Link>
                                {/*<img
                                    src={
                                        (sliders.slider1[0] && sliders.slider1[0].media && sliders.slider1[0].media.url) ||
                                        ''
                                    }
                                    alt={
                                        (sliders.slider1[0] &&
                                            sliders.slider1[0].media &&
                                            sliders.slider1[0].media.title) ||
                                        ''
                                    }
                                    className={styles.img}
                                />*/}
                            </Banner>
                        </div>
                    </Col>
                </Row>
                /*) : null*/
            }
            <Row className={styles['bottom-banners']}>
                <Col md={8}>
                    {sliders.slider2.length > 1 ? (
                        <div dir="ltr" style={{ position: 'relative' }}>
                            <SliderBanner
                                bannerClasses={cn(styles.banner, styles['right-banner'])}
                                slideClasses={styles.img}
                                slides={sliders.slider2}
                                // navigationOptions={{
                                //     navigation: {
                                //         nextEl: '.' + styles['slider-btn-next'],
                                //         prevEl: '.' + styles['slider-btn-prev'],
                                //     },
                                // }}
                                autoplay={{ autoplay: { delay: 10000 } }}
                                swiperClasses="h-100"
                            />
                            {/*<div className={cn('slider-btn-next', styles['slider-btn-next'])} />*/}
                            {/*<div className={cn('slider-btn-prev', styles['slider-btn-prev'])} />*/}
                        </div>
                    ) : (
                        <Banner
                            bannerClasses={cn(styles.banner, styles['right-banner'])}
                            bodyClasses={styles['banner-body']}
                        >
                            <img
                                src={
                                    (sliders.slider2[0] && sliders.slider2[0].media && sliders.slider2[0].media.url) ||
                                    ''
                                }
                                alt={
                                    (sliders.slider2[0] &&
                                        sliders.slider2[0].media &&
                                        sliders.slider2[0].media.title) ||
                                    ''
                                }
                                className={styles.img}
                            />
                        </Banner>
                    )}
                </Col>
                <Col md={4} className={styles['small-banners']}>
                    {sliders.slider3.length > 1 ? (
                        <SliderBanner
                            bannerClasses={cn(styles.banner, styles['left-top-banner'])}
                            slideClasses={styles.img}
                            slides={sliders.slider3}
                            autoplay={{ autoplay: { delay: 9000 } }}
                            swiperClasses="h-100"
                        />
                    ) : (
                        <Banner
                            bannerClasses={cn(styles.banner, styles['left-top-banner'])}
                            bodyClasses={styles['banner-body']}
                        >
                            <img
                                src={
                                    (sliders.slider3[0] && sliders.slider3[0].media && sliders.slider3[0].media.url) ||
                                    ''
                                }
                                alt={
                                    (sliders.slider3[0] &&
                                        sliders.slider3[0].media &&
                                        sliders.slider3[0].media.title) ||
                                    ''
                                }
                                className={styles.img}
                            />
                        </Banner>
                    )}
                    {sliders.slider4.length > 1 ? (
                        <SliderBanner
                            bannerClasses={cn(styles.banner, styles['left-bottom-banner'])}
                            slideClasses={styles.img}
                            slides={sliders.slider4}
                            autoplay={{ autoplay: { delay: 11000 } }}
                            swiperClasses="h-100"
                        />
                    ) : (
                        <Banner
                            bannerClasses={cn(styles.banner, styles['left-bottom-banner'])}
                            bodyClasses={styles['banner-body']}
                        >
                            <img
                                src={
                                    (sliders.slider4[0] && sliders.slider4[0].media && sliders.slider4[0].media.url) ||
                                    ''
                                }
                                alt={
                                    (sliders.slider4[0] &&
                                        sliders.slider4[0].media &&
                                        sliders.slider4[0].media.title) ||
                                    ''
                                }
                                className={styles.img}
                            />
                        </Banner>
                    )}
                </Col>
            </Row>
            {width <= 576 ? (
                <div className={styles['meetings-indicator-mobile']}>
                    <YourMeetingsIndicator isHeader={false} />
                </div>
            ) : null}
            {props.organizations ? (
                <div className={styles.organizations}>
                    {props.organizations.map((org) => {
                        return (
                            <Link href={`/organizations/${org.slug}`} key={org.title}>
                                <a title={org.title} className={styles['organization-link']}>
                                    <img src={org.logo.url} alt={org.title} className={styles['organization-img']} />
                                </a>
                            </Link>
                        );
                    })}
                </div>
            ) : null}
        </Container>
    );
}
