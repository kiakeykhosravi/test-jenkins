import React from 'react';
import styles from '../styles/user-story-card.module.scss';
import toPersianNum from 'utils/to-persian-num';
import { IUserStory } from 'models/user-story';

export default function UserStoryCard(props: { data: IUserStory }) {
    return (
        <div className={styles.card}>
            <div className={styles.text}>{toPersianNum(props.data.message)}</div>
            <div className={styles.info}>
                <div className={styles.user}>
                    <div className={styles.name}>{toPersianNum(props.data.author_name)}</div>
                    <div className={styles.organization}>{toPersianNum(props.data.organization_name)}</div>
                </div>
                <img
                    src={(props.data.media && props.data.media.url) || '/images/user-story-icon.jpg'}
                    className={styles.pic}
                    alt={props.data.organization_name}
                />
            </div>
        </div>
    );
}
