import React from 'react';
import Section from 'modules/shared/components/section';
import UserStoryCard from './user-story-card';
import { Container } from 'react-bootstrap';
import styles from '../styles/user-story-section.module.scss';
import classNames from 'classnames';
import { IUserStory } from 'models/user-story';
import Slider from 'modules/shared/components/slider';
import { SwiperSlide } from 'swiper/react';

export default function UserStorySection(props: { data: IUserStory[]; classes?: string }) {
    const cn = classNames;
    const renderUserStoryCards = () => {
        if (props.data.length) {
            return props.data.map((item: IUserStory) => {
                return (
                    <SwiperSlide key={item.id} className={styles['swiper-slide']} dir="rtl">
                        <UserStoryCard data={item} />
                    </SwiperSlide>
                );
            });
        }
        return null;
    };

    const breakpoints = {
        420: {
            slidesPerView: 1,
            spaceBetween: 10,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 25,
        },
        992: {
            slidesPerView: 2,
            spaceBetween: 35,
        },
        1200: {
            slidesPerView: 3,
            spaceBetween: 35,
        },
        1400: {
            slidesPerView: 3,
            spaceBetween: 50,
        },
    };

    return (
        <Section
            title="افرادی که به آسان سمینار اعتماد کرده اند."
            titleClasses={styles.title}
            classes={cn(props.classes, styles.section, 'gradient-background')}
        >
            <Container>
                <div className={styles['card-container']} dir="ltr">
                    <Slider
                        spaceBetween={15}
                        paginationOptions={{ pagination: false }}
                        navigationOptions={{
                            navigation: {
                                nextEl: '.' + styles['slider-btn-next'],
                                prevEl: '.' + styles['slider-btn-prev'],
                            },
                        }}
                        breakpoints={breakpoints}
                        swiperClasses={styles['swiper-main']}
                    >
                        {renderUserStoryCards()}
                    </Slider>
                    <div className={cn('slider-btn-next', styles['slider-btn-next'])} />
                    <div className={cn('slider-btn-prev', styles['slider-btn-prev'])} />
                </div>
            </Container>
        </Section>
    );
}
