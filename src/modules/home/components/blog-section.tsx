import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import styles from '../styles/blog-section.module.scss';
import Banner from 'modules/shared/components/banner';
import Link from 'next/link';
import ButtonRound from 'modules/shared/components/button-round';
import classNames from 'classnames';
import { ArrowLeft } from 'react-bootstrap-icons';
import AsanImage from 'modules/shared/components/asan-image';
import { IBlogCompact } from 'models/blog-compact';

export default function BlogSection(props: { data: IBlogCompact[] }) {
    const cn = classNames;
    const renderBlogCards = (blogCards: IBlogCompact[]) => {
        return blogCards.map((data: IBlogCompact) => {
            return (
                <Col lg={4} md={6} sm={12} key={data.id} className="g-5 g-lg-4 g-xl-5">
                    <Banner bodyClasses={styles['card-body']} bannerClasses={styles['card-banner']}>
                        <Link href={`/blog/${data.id}/${data.title.trim().replace(/\s+/g, '-')}`}>
                            <a title={data.title}>
                                <AsanImage
                                    src={data.media && data.media[0] && data.media[0].thumbnail_url}
                                    alt={data.title}
                                    className={styles.img}
                                />
                            </a>
                        </Link>
                    </Banner>
                </Col>
            );
        });
    };

    return (
        <Container className={styles.section}>
            <div className={styles.title}>مقالات آسان سمینار</div>
            <Row className={styles['cards-row']}>{renderBlogCards(props.data)}</Row>
            <Row>
                <Col className="text-center">
                    <Link href="/blog">
                        <a title="وبلاگ">
                            <ButtonRound type="button" classes={styles.button}>
                                همه مقالات آسان سمینار
                                <ArrowLeft className={styles['button-arrow']} />
                            </ButtonRound>
                        </a>
                    </Link>
                </Col>
            </Row>
        </Container>
    );
}
