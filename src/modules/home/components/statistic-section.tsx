import React from 'react';
import Section from 'modules/shared/components/section';
import StatisticCard from './statistic-card';
import classNames from 'classnames';
import styles from '../styles/statistic-section.module.scss';
import { Container, Row } from 'react-bootstrap';

export default function StatisticSection(props: { data?: any; classes?: string }) {
    const statisticCards = () => {
        if (props.data.length) {
            return props.data.map((item: any, index: number) => {
                return <StatisticCard data={item} key={index} />;
            });
        }
        return null;
    };
    return (
        <Section
            title="آمار ها"
            classes={classNames(props.classes, styles['bg-light'])}
            itemsData={props.data}
            titleClasses={styles.title}
        >
            <Container className={classNames(styles['container'], 'overflow-hidden')}>
                <Row className={classNames(styles['row'], styles['gx'])}>{statisticCards()}</Row>
            </Container>
        </Section>
    );
}
