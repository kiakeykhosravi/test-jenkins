import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import styles from '../styles/onelook-section.module.scss';
import classNames from 'classnames';
import Link from 'next/link';
import AsanImage from 'modules/shared/components/asan-image';

export default function OnelookSection() {
    const cn = classNames;

    return (
        <Container className={styles.section}>
            <h2 className={styles.title}>
                <span>آسان سمینار در یک نگاه</span>
            </h2>
            <Row>
                <Col md={6} className={styles['right-container']}>
                    <Link href="/team">
                        <a title="اهداف آسان سمینار">
                            <AsanImage
                                src="/images/home-onelook-right.png"
                                alt="تصویر اهداف آسان سمینار"
                                className={styles.right}
                            />
                        </a>
                    </Link>
                </Col>
                <Col md={6} className={styles['left-container']}>
                    <Link href="/team#team">
                        <a title="تیم ما">
                            <AsanImage src="/images/home-onelook-left.png" alt="در یک نگاه" className={styles.left} />
                        </a>
                    </Link>
                </Col>
            </Row>
        </Container>
    );
}
