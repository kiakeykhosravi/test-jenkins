import React, { useEffect, useRef } from 'react';
import styles from '../styles/statistic-card.module.scss';
import { Col, Image } from 'react-bootstrap';
import { CountUp } from 'use-count-up';
import toPersianNum from 'utils/to-persian-num';

export default function StatisticCard(props: { data?: any }) {
    const [statisticVisible, setStatisticVisible] = React.useState(false);
    const domRef = useRef();
    useEffect(() => {
        const observer = new IntersectionObserver((entries) => {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    setStatisticVisible(entry.isIntersecting);
                }
            });
        });
        observer.observe(domRef.current);
    }, []);
    const PersianCountUp = (value) => {
        return toPersianNum(value);
    };
    return (
        <Col lg={12} xl={4} className={styles['col']}>
            <div className={styles.card}>
                <div className={styles['count']} ref={domRef}>
                    <CountUp
                        {...(statisticVisible ? { isCounting: true } : { isCounting: false })}
                        end={props.data.count}
                        duration={props.data.duration}
                    >
                        {({ value }) => PersianCountUp(value)}
                    </CountUp>
                </div>
                <div className={styles['title']}>
                    {/*<Image src={props.data.icon} className={styles['logo']} alt={props.data.title} />*/}
                    <span>{toPersianNum(props.data.title)}</span>
                </div>
            </div>
        </Col>
    );
}
