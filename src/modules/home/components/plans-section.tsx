import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import ButtonRound from 'modules/shared/components/button-round';
import Link from 'next/link';
import styles from '../styles/plans-section.module.scss';
import classNames from 'classnames';
import { ClipboardData } from 'react-bootstrap-icons';
import AsanImage from 'modules/shared/components/asan-image';

export default function PlansSection() {
    const cn = classNames;

    return (
        <Container className={styles['main-container']}>
            <Row className={styles.main}>
                <Col md className="d-block d-md-none text-center">
                    <AsanImage
                        src="/images/homepage-plans-img.jpg"
                        alt="از هر کجای کشور هر مهارتی رو آموزش بده"
                        className={styles['left-img']}
                    />
                </Col>
                <Col md={7} lg={6} style={{ position: 'relative' }}>
                    <div className={styles['list-container']}>
                        <ul className={styles.list}>
                            <div className={styles['list-line-decorator']} />
                            <li className={styles['list-item']}>
                                <img
                                    className={styles['list-item-img']}
                                    src="/images/icons/checkmark-blue.svg"
                                    alt="مزیت"
                                />
                                <span>آموزشگاه آنلاین خودت رو راه اندازی کن</span>
                            </li>
                            <li className={styles['list-item']}>
                                <img
                                    className={styles['list-item-img']}
                                    src="/images/icons/checkmark-blue.svg"
                                    alt="مزیت"
                                />
                                <span>از هر کجای کشور هر مهارتی رو آموزش بده</span>
                            </li>
                            <li className={styles['list-item']}>
                                <img
                                    className={styles['list-item-img']}
                                    src="/images/icons/checkmark-blue.svg"
                                    alt="مزیت"
                                />
                                <span>با جلسات مدیریتی آنلاین از رقبا جلوتر باش</span>
                            </li>
                            <li className={styles['list-item']}>
                                <img
                                    className={styles['list-item-img']}
                                    src="/images/icons/checkmark-blue.svg"
                                    alt="مزیت"
                                />
                                <span>با مشاوره آنلاین به کل کشور، درآمد زایی کن</span>
                            </li>
                        </ul>
                    </div>
                    <div className={styles['important-list-container']}>
                        <img
                            className={cn('d-none d-sm-inline', styles['important-list-item-img'])}
                            src="/images/icons/checkmark-circle-outline.svg"
                            alt="نگران پشتیبانی نباش"
                        />
                        <div className={styles['important-list-item']}>
                            نیازی به اجاره سرور و استخدام تیم فنی نداری
                            <br />
                            نگران پشتیبانی هم نباش
                        </div>
                    </div>
                </Col>
                <Col md className="d-none d-md-block text-center">
                    <AsanImage
                        src="/images/homepage-plans-img.jpg"
                        alt="از هر کجای کشور هر مهارتی رو آموزش بده"
                        className={styles['left-img']}
                    />
                </Col>
            </Row>
            <Link href="/plans">
                <a title="شرایط و هزینه ها" className={styles['plans-button-link']}>
                    <ButtonRound variant="danger" type="button" classes={styles['plans-button']}>
                        <ClipboardData className={styles['plans-button-icon']} />
                        <h2>شرایط و هزینه های برگزاری وبینار و کلاس آنلاین</h2>
                    </ButtonRound>
                </a>
            </Link>
        </Container>
    );
}
