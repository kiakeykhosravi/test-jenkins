import React from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import Link from 'next/link';
import classNames from 'classnames';
import styles from '../styles/organizations-section.module.scss';
import { IOrganizationCompact } from 'models/organization-compact';
import AsanImage from 'modules/shared/components/asan-image';

export default function OrganizationsSection(props: { data: IOrganizationCompact[] }) {
    const cn = classNames;

    const orgPlaceholderImg = '/images/logos/private-class-logo.jpeg';

    const renderRow = (organizations: IOrganizationCompact[], start: number, end: number) => {
        const orgs = organizations.slice(start, end);

        return orgs.map((data) => {
            return (
                <Link href={`/organizations/${data.slug}`} key={data.id}>
                    <a title={data.title} className={styles['logo-link']}>
                        <AsanImage
                            src={(data.logo && data.logo.thumbnail_url) || orgPlaceholderImg}
                            alt={data.title}
                            style={{ maxWidth: '100%' }}
                        />
                    </a>
                </Link>
            );
        });
    };

    const renderMiddleSection = () => {
        return (
            <>
                <h3 className={styles.title}>
                    <span>به آسان سمینار اعتماد کرده اند ...</span>
                </h3>
                <Link href="/organizations">
                    <a title="پنل های سازمانی">
                        <Button variant="dark" type="button" className={styles.button}>
                            مشاهده همه پنل های سازمانی
                        </Button>
                    </a>
                </Link>
            </>
        );
    };

    return (
        <Container className={styles.section}>
            <Row>
                <Col className={styles['top-row']} style={props.data.length < 20 ? { justifyContent: 'center' } : null}>
                    {renderRow(props.data, 0, 20)}
                </Col>
            </Row>
            <Row>
                <Col className={styles['middle-row']}>
                    <div className={cn('d-none d-lg-flex', styles['middle-right'])}>
                        {renderRow(props.data, 20, 26)}
                    </div>
                    <div className={styles['middle-section']}>{renderMiddleSection()}</div>
                    <div className={cn('d-none  d-lg-flex', styles['middle-left'])}>
                        {renderRow(props.data, 26, 32)}
                    </div>
                </Col>
            </Row>
            <Row>
                <Col
                    className={styles['bottom-row']}
                    style={props.data.length < 20 ? { justifyContent: 'center' } : null}
                >
                    {renderRow(props.data, 32, 52)}
                </Col>
            </Row>
        </Container>
    );
}
