import { IBlog } from 'models/blog';
import { IBlogNews } from 'models/blog-news';
import { IBlogCompact } from 'models/blog-compact';
import { IBlogSimple } from 'models/blog-simple';

export interface ILatestBlogsResponse {
    blogs: IBlogCompact[];
}

export interface IMostLikedBlogsResponse {
    blogs: IBlogSimple[];
}

export interface IBlogsResponse {
    posts: IBlog[];
    lastpage: number;
}

export interface IBlogResponse {
    post: IBlog;
    related_posts: IBlogSimple[];
}

export interface IBlogSchemaMainResponse {
    schema: Record<string, any>;
}

export interface IOrganizationBlogResponse extends IBlogsResponse {
    organization_title: string;
}

export interface IBlogLatestNewsResponse {
    blog_news: IBlogNews[];
}
