import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IBlogResponse } from '../responses';
import { AxiosRequestConfig } from 'axios';

export default async function apiBlog(
    id: string,
    config?: AxiosRequestConfig,
): Promise<{ data?: IBlogResponse; status: 'error' | 'data' }> {
    const response = await apiRequest<IBlogResponse>('get', endpoints.backend.blogs + id, null, null, null, config);

    if (response.status === 'data') {
        return {
            data: response.data,
            status: 'data',
        };
    }
    return {
        status: 'error',
    };
}
