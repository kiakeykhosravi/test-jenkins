import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { ILatestBlogsResponse } from '../responses';
import { IBlogCompact } from 'models/blog-compact';

export default async function apiLatestBlogs(): Promise<{ data?: IBlogCompact[]; status: 'error' | 'data' }> {
    const response = await apiRequest<ILatestBlogsResponse>('get', endpoints.backend.blogs_latest);

    if (response.status === 'data') {
        return {
            data: response.data.blogs,
            status: 'data',
        };
    }
    return {
        status: 'error',
    };
}
