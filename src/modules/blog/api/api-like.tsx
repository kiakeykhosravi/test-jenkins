import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { toast } from 'react-toastify';

export default function apiLike(post: number) {
    const url = endpoints.frontend.like.replace('{post}', String(post));
    return apiRequest<Record<string, never>>('post', url)
        .then((response) => {
            if (response.status === 'data') {
                toast.success('شما این مقاله را پسندیدید! :)');
                return true;
            }

            toast.error(response.message || 'خطایی در هنگام لایک کردن رخ داد');
            return false;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در هنگام لایک کردن رخ داد');
            return false;
        });
}
