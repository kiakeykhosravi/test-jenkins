import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { toast } from 'react-toastify';

export default function apiUnlike(post: number) {
    const url = endpoints.frontend.unlike.replace('{post}', String(post));
    return apiRequest<Record<string, never>>('post', url)
        .then((response) => {
            if (response.status === 'data') {
                toast.info('شما دیگر این مقاله را نمی پسندید! :(');
                return true;
            }

            toast.error(response.message || 'خطایی در هنگام حذف لایک رخ داد');
            return false;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در هنگام حذف لایک رخ داد');
            return false;
        });
}
