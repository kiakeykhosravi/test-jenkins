import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { toast } from 'react-toastify';

export default function apiBlogComment(post: number, commentText: string) {
    const url = endpoints.frontend.comment_add.replace('{post}', String(post));
    return apiRequest('post', url, {
        text: commentText,
    })
        .then((response) => {
            if (response.status === 'data') {
                toast.success(response.message || 'دیدگاه شما با موفقیت ثبت شد و بعد از بازنگری نمایش داده خواهد شد', {
                    autoClose: 10000,
                });
                return true;
            }

            toast.error(response.message || 'خطایی در ارسال دیدگاه شما رخ داد', {
                autoClose: 7500,
            });
            return false;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در ارسال دیدگاه شما رخ داد', {
                autoClose: 7500,
            });
            return false;
        });
}
