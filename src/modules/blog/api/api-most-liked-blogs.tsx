import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IMostLikedBlogsResponse } from '../responses';
import { IBlogSimple } from 'models/blog-simple';

export default async function apiMostLikedBlogs(): Promise<{ data?: IBlogSimple[]; status: 'error' | 'data' }> {
    const response = await apiRequest<IMostLikedBlogsResponse>('get', endpoints.backend.blogs_most_liked);

    if (response.status === 'data') {
        return {
            data: response.data.blogs,
            status: 'data',
        };
    }
    return {
        status: 'error',
    };
}
