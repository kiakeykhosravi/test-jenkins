import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IBlogsResponse } from '../responses';
import { AxiosRequestConfig } from 'axios';

export default async function apiBlogSearch(
    query: string,
    page: number,
    tags?: string,
    categories?: string,
    config?: AxiosRequestConfig,
): Promise<{ data?: IBlogsResponse; status: 'error' | 'data' }> {
    let url = endpoints.backend.search_posts + '?q=' + query + '&page=' + page;
    if (tags) url = url + '&tags=' + tags;
    if (categories) url = url + '&categories=' + categories;
    const response = await apiRequest<IBlogsResponse>('get', url, undefined, undefined, undefined, config);

    if (response.status === 'data') {
        return {
            data: response.data,
            status: 'data',
        };
    }
    return {
        status: 'error',
    };
}
