import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IBlogsResponse } from '../responses';

export default async function apiBlogs(page: number): Promise<{ data?: IBlogsResponse; status: 'error' | 'data' }> {
    const response = await apiRequest<IBlogsResponse>('get', endpoints.backend.search_posts + '?page=' + page);

    if (response.status === 'data') {
        return {
            data: response.data,
            status: 'data',
        };
    }
    return {
        status: 'error',
    };
}
