import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IOrganizationBlogResponse } from 'modules/blog/responses';

export default async function apiOrganizationBlog(
    isBackend = false,
    organization_slug: string,
    page?: number,
    query?: string,
): Promise<{ data?: IOrganizationBlogResponse; status: 'error' | 'data' }> {
    const url = generateUrl(isBackend, organization_slug, query, page);
    const response = await apiRequest<IOrganizationBlogResponse>('get', url);

    if (response.status === 'data') {
        return {
            data: response.data,
            status: 'data',
        };
    }
    return {
        status: 'error',
    };
}

function generateUrl(isBackend: boolean, organizationSlug?: string, query?: string, page?: number) {
    const queryParamList: string[] = [];
    query !== undefined && query.length ? queryParamList.push(`q=${query}`) : '';
    page > 1 ? queryParamList.push(`page=${page}`) : '';

    let queryParams = queryParamList.length ? '?' : '';

    for (const param of queryParamList) {
        queryParams += param + '&';
    }

    return (
        (isBackend
            ? endpoints.backend.search_organization_blogs
            : endpoints.frontend.search_organization_blogs
        ).replace('{organization}', organizationSlug) + queryParams
    );
}
