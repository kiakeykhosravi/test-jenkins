import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IBlogLatestNewsResponse } from '../responses';

export default async function apiLatestNews() {
    const response = await apiRequest<IBlogLatestNewsResponse>('get', endpoints.frontend.latest_news);
    if (response.status === 'data') {
        return {
            data: response.data,
            status: 'data',
        };
    }

    return {
        status: 'error',
    };
}
