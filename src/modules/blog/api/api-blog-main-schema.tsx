import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IBlogSchemaMainResponse } from '../responses';

export default async function apiBlogMainSchema(): Promise<Record<string, any> | null> {
    const response = await apiRequest<IBlogSchemaMainResponse>('get', endpoints.backend.blogs_schema_main);

    if (response.status === 'data') {
        return response.data.schema;
    }

    return null;
}
