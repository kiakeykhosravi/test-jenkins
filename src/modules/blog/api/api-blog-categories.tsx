import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IBlogCategory } from 'models/blog-category';

export default async function apiBlogCategories(
    isBackend?: boolean,
    organizationSlug?: string,
): Promise<{ data?: IBlogCategory[]; status: 'error' | 'data' }> {
    let url = isBackend ? endpoints.backend.blog_categories : endpoints.frontend.blog_categories;

    if (!organizationSlug) {
        organizationSlug = process.env.NEXT_PUBLIC_ASANSEMINAR_SLUG;
    }
    url = url.replace('{organization_slug}', organizationSlug);

    const response = await apiRequest<IBlogCategory[]>('get', url);

    if (response.status === 'data') {
        return {
            data: response.data,
            status: 'data',
        };
    }
    return {
        status: 'error',
    };
}
