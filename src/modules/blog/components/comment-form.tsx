import React, { useContext, useState } from 'react';
import styles from '../styles/comment-section.module.scss';
import { Col, Form, Row, Button } from 'react-bootstrap';
import classNames from 'classnames';
import { useForm } from 'react-hook-form';
import { AuthContext } from 'contexts/auth-context';
import Link from 'next/link';
import { useRouter } from 'next/router';
import apiBlogComment from '../api/api-blog-comment';

export default function CommentForm(props: { postId: number }) {
    const cn = classNames;
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [commentText, setCommentText] = useState('');
    const { errors, register, handleSubmit } = useForm();
    const router = useRouter();

    const authContext = useContext(AuthContext);

    const onSubmitComment = (data: { comment: string }) => {
        if (isSubmitting) {
            return;
        }
        setIsSubmitting(true);

        apiBlogComment(props.postId, data.comment)
            // eslint-disable-next-line @typescript-eslint/no-empty-function
            .then((result) => {
                if (result) {
                    setCommentText('');
                }
            })
            .finally(() => {
                setIsSubmitting(false);
            });
    };

    return (
        <Form
            className={styles['comment-form']}
            onSubmit={handleSubmit(onSubmitComment)}
            {...(authContext.user ? {} : { disabled: true })}
        >
            {authContext.user ? (
                <Row className="mb-3">
                    <Col md={4}>
                        <div className="mb-2">{authContext.user.fullname}</div>
                    </Col>
                    <Col md={8}>
                        <div className="mb-2">{authContext.user.email || ''}</div>
                    </Col>
                </Row>
            ) : null}
            <Form.Control
                as="textarea"
                rows={3}
                placeholder="نظر خود را اینجا بنویسید"
                className={cn(
                    styles.input,
                    errors.comment && styles['field-error'],
                    !authContext.user && styles['comment-form-disabled'],
                )}
                name="comment"
                value={commentText}
                onChange={(e) => setCommentText(e.target.value)}
                ref={register({
                    required: 'نظر خود را وارد کنید!',
                })}
                {...(authContext.user ? {} : { disabled: true })}
            />
            {errors.comment && <div className={styles['error-text']}>{errors.comment.message}</div>}
            {authContext.user ? (
                <Button
                    type="submit"
                    variant="primary"
                    className={styles.submit}
                    {...(isSubmitting ? { disabled: true } : {})}
                >
                    {isSubmitting ? 'در حال ارسال ...' : 'ارسال دیدگاه'}
                </Button>
            ) : (
                <>
                    <div>جهت ارسال نظر باید ابتدا وارد سایت شوید</div>
                    <Link href={`/auth/login-nopass?return=${router.asPath}`}>
                        <a style={{ color: 'white' }} title="ورود / ثبت نام">
                            <Button type="button" variant="primary" className={styles.submit}>
                                ورود / ثبت نام
                            </Button>
                        </a>
                    </Link>
                </>
            )}
        </Form>
    );
}
