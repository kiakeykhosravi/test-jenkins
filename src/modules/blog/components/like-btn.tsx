import React, { useState } from 'react';
import styles from '../styles/like-btn.module.scss';
import { Heart, HeartFill } from 'react-bootstrap-icons';
import classNames from 'classnames';
import apiUnlike from '../api/api-unlike';
import apiLike from '../api/api-like';

export default function LikeBtn(props: {
    postId: number;
    containerClass?: string;
    isLiked: boolean;
    changeIsLiked: (value: boolean) => any;
}) {
    const cn = classNames;
    const [isTransitioning, setIsTransitioning] = useState(false);
    const [animating, setAnimating] = useState(false);
    const [submitting, setSubmitting] = useState(false);

    const onClickLike = () => {
        if (submitting || isTransitioning) {
            return;
        }
        setAnimating(true);
        setSubmitting(true);

        const api = props.isLiked ? apiUnlike : apiLike;

        api(props.postId)
            .then((result) => {
                if (result) {
                    setIsTransitioning(true);
                    props.changeIsLiked(!props.isLiked);
                }
            })
            .finally(() => {
                setSubmitting(false);
            });
    };

    const onHeartAnimationEnd = () => {
        setAnimating(false);
    };

    const onHeartStateTransitionEnd = () => {
        setIsTransitioning(false);
    };

    return (
        <span className={cn(styles.main, props.containerClass)} style={{ position: 'relative' }}>
            <Heart
                className={cn(styles.heart, styles['heart-backdrop'], animating && styles['heart-backdrop-moving'])}
                onAnimationEnd={onHeartAnimationEnd}
            />
            {props.isLiked ? (
                <HeartFill
                    className={cn(styles.heart, isTransitioning && styles.fadein)}
                    onClick={onClickLike}
                    onAnimationEnd={onHeartStateTransitionEnd}
                />
            ) : (
                <Heart
                    className={cn(styles.heart, isTransitioning && styles.fadein)}
                    onClick={onClickLike}
                    onAnimationEnd={onHeartStateTransitionEnd}
                />
            )}
        </span>
    );
}
