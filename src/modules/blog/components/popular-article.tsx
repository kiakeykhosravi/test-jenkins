import React from 'react';
import Link from 'next/link';
import styles from '../styles/popular-article.module.scss';
import AsanImage from 'modules/shared/components/asan-image';
import { IBlogCompact } from 'models/blog-compact';

export default function PopularArticle(props: { article: IBlogCompact }) {
    return (
        <div className={styles.main}>
            <div className={styles.background}>
                <h2 className={styles.title}>پربازدیدترین مقاله هفته</h2>
                <div className={styles['image-container']}>
                    <Link href={`/blog/${props.article.id}/${props.article.title.trim().replace(/\s+/g, '-')}`}>
                        <a title={props.article.title}>
                            <AsanImage
                                src={
                                    props.article.media && props.article.media.length
                                        ? props.article.media[0].url
                                        : '/images/avatar-placeholder.png'
                                }
                                alt={props.article.title}
                                className={styles.image}
                            />
                        </a>
                    </Link>
                </div>
            </div>
        </div>
    );
}
