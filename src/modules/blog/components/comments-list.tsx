import React from 'react';
import { IComment } from 'models/comment';
import styles from '../styles/comments-list.module.scss';

export default function CommentsList(props: { comments: IComment[] }) {
    return (
        <div className={props.comments && props.comments.length && styles.comments}>
            {props.comments &&
                props.comments.map((comment) => {
                    return (
                        <div className={styles.comment} key={comment.id}>
                            <div className={styles.header}>
                                <div className={styles.sender}>
                                    <span>نویسنده: </span>
                                    {comment.user.fullname}
                                </div>
                                <div className={styles.date}>
                                    <div>{comment.created_at_fadate}</div>
                                    <div>{comment.created_at_fatime}</div>
                                </div>
                            </div>
                            <div className={styles.text}>{comment.text}</div>
                        </div>
                    );
                })}
        </div>
    );
}
