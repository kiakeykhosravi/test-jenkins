import React from 'react';
import Link from 'next/link';
import styles from '../styles/article-card.module.scss';
import toPersianNum from 'utils/to-persian-num';
import DOMPurify from 'isomorphic-dompurify';
import { ChatLeftText, Heart } from 'react-bootstrap-icons';
import AsanImage from 'modules/shared/components/asan-image';
import { IBlog } from 'models/blog';
import { IBlogCompact } from 'models/blog-compact';

export default function ArticleCard(props: { data: IBlog | IBlogCompact; description: boolean }) {
    const commentsCount = props.data.comments_count || 0;
    const articleLink = `/blog/${props.data.id}/${props.data.title.trim().replace(/\s+/g, '-')}`;
    return (
        <div className={styles.main}>
            <div className={styles.card}>
                <div className={styles.body}>
                    <Link href={articleLink}>
                        <a title={props.data.title}>
                            <AsanImage
                                src={
                                    props.data.media && props.data.media.length
                                        ? props.data.media[0].thumbnail_url
                                        : '/images/avatar-placeholder.png'
                                }
                                alt={props.data.title}
                                className={styles.image}
                            />
                        </a>
                    </Link>
                    <Link href={articleLink}>
                        <a title={props.data.title}>
                            <h3 className={styles.title}>{toPersianNum(props.data.title)}</h3>
                        </a>
                    </Link>
                    {props.data.description ? (
                        <div
                            dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(props.data.description) }}
                            className={styles.description}
                        />
                    ) : null}
                </div>
                <div className={styles.footer}>
                    <div className={styles.author}>
                        <AsanImage
                            src="/images/avatar-placeholder.png"
                            alt="تیم محتوا"
                            className={styles['author-image']}
                        />
                        <span className={styles['author-link']}>تیم محتوا</span>
                    </div>
                    <div className={styles.date}>{props.data.created_at_fadate}</div>
                </div>
            </div>
            <Link href={articleLink}>
                <a title={props.data.title}>
                    <div className={styles['card-overlay']}>
                        <div className={styles['overlay-header']}>
                            <div className={styles['overlay-tag']}>
                                {props.data.tags[0] ? toPersianNum(props.data.tags[0].title) : 'مقاله'}
                            </div>
                        </div>
                        <div className={styles['overlay-footer']}>
                            <h5 className={styles['overlay-title']}>{props.data.title}</h5>
                            <div className={styles['overlay-subfooter']}>
                                {/*<div className={styles['overlay-duration']}>*/}
                                {/*    {toPersianNum(`${Math.floor(Math.random() * 10)} دقیقه`)}*/}
                                {/*</div>*/}
                                <div className={styles['overlay-icons']}>
                                    <div className={styles['overlay-comment']}>
                                        {toPersianNum(commentsCount)}
                                        <ChatLeftText className={styles['overlay-icon']} />
                                    </div>
                                    <span className={styles['overlay-icon']}>
                                        {toPersianNum(props.data.likes_count)}
                                        <Heart className={styles['overlay-icon']} />
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </Link>
        </div>
    );
}
