import React from 'react';
import Link from 'next/link';
import styles from '../styles/sidebar-box.module.scss';
import { IBlog } from 'models/blog';
import AsanImage from 'modules/shared/components/asan-image';
import { IBlogNews } from 'models/blog-news';
import { IMedia } from 'models/media';
import { IBlogCompact } from 'models/blog-compact';
import { IBlogSimple } from 'models/blog-simple';

export default function SidebarBox(props: {
    articles: IBlog[] | IBlogCompact[] | IBlogSimple[] | IBlogNews[];
    title: string;
    link?: string;
    isNews?: boolean;
}) {
    const renderArticles = () => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return props.articles.map(mapFunction);
    };

    const mapFunction = (article: (IBlog | IBlogSimple) | IBlogNews, index) => {
        if (index < 5) {
            const placeholderImg = '/images/avatar-placeholder.png';
            const link = props.isNews
                ? (article as IBlogNews).link
                : `/blog/${article.id}/${article.title.trim().replace(/\s+/g, '-')}`;
            const imgLink =
                (props.isNews
                    ? article.media && (article.media as IMedia).thumbnail_url
                    : article.media &&
                      (article.media as IMedia[]).length &&
                      (article.media as IMedia[])[0].thumbnail_url) || placeholderImg;

            return (
                <div className={styles.article} key={article.title}>
                    <Link href={link}>
                        <a title={article.title}>
                            <AsanImage src={imgLink} alt={article.title} className={styles['article-image']} />
                        </a>
                    </Link>
                    <div>
                        <Link href={link}>
                            <a title={article.title}>
                                <h6 className={styles['article-title']}>{article.title}</h6>
                            </a>
                        </Link>
                        <div className={styles['article-date']}>{article.created_at_fadate}</div>
                    </div>
                </div>
            );
        }
    };

    return props.articles && props.articles.length ? (
        <div className={styles['sidebar-box']}>
            <h5 className={styles.title}>{props.title}</h5>
            <div className={styles.articles}>{renderArticles()}</div>
            {props.link ? (
                <Link href={props.link}>
                    <a title="مشاهده همه" className={styles.link}>
                        مشاهده همه
                    </a>
                </Link>
            ) : null}
        </div>
    ) : null;
}
