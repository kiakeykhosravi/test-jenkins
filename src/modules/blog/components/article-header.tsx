import React from 'react';
import styles from '../styles/article-header.module.scss';
import { Image } from 'react-bootstrap';
import toPersianNum from 'utils/to-persian-num';
import { Calendar2Week } from 'react-bootstrap-icons';
import { IBlog } from 'models/blog';
import LikeBtn from './like-btn';

export default function ArticleHeader(props: {
    article: IBlog;
    isLiked: boolean;
    changeIsLiked: (value: boolean) => any;
    likesCountState: number;
}) {
    return (
        <div className={styles['header']}>
            <div className={styles['first-row']}>
                <h1 className={styles.title}>{toPersianNum(props.article.title)}</h1>
                <div className={styles.favorite}>
                    <span className={styles['like-count']}>{toPersianNum(props.likesCountState)}</span>
                    <LikeBtn
                        postId={props.article.id}
                        containerClass={styles.icon}
                        isLiked={props.isLiked}
                        changeIsLiked={props.changeIsLiked}
                    />
                    {/*<Bookmark className={styles.icon} />*/}
                </div>
            </div>
            <div className={styles['second-row']}>
                <div className={styles['right-column']}>
                    <div className={styles.author}>
                        <Image
                            src="/images/avatar-placeholder.png"
                            alt="تیم تولید محتوا"
                            className={styles['author-image']}
                        />
                        تیم تولید محتوا
                    </div>
                    <div className={styles.date}>
                        <Calendar2Week className={styles.icon} style={{ color: '#294c82' }} />
                        {props.article.created_at_fadate || ''}
                    </div>
                </div>
                <div className={styles['left-column']}>
                    <div>زمان مورد نیاز برای مطالعه: </div>
                    <div className={styles.time}>{`${toPersianNum(props.article.study_time || '۵')} دقیقه`}</div>
                </div>
            </div>
        </div>
    );
}
