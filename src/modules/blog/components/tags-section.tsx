import React from 'react';
import styles from '../styles/tags-section.module.scss';
import { ITag } from 'models/tag';
import Link from 'next/link';

export default function TagsSection(props: { tags: ITag[] }) {
    const renderTags = () => {
        return props.tags.map((tag) => {
            return (
                <Link href={`/blog/tag/${tag.title.replace(/\s+/g, '-')}`} key={tag.title}>
                    <a title={tag.title}>
                        <div key={tag.title} className={styles.tag}>
                            {tag.title}
                        </div>
                    </a>
                </Link>
            );
        });
    };
    return (
        <div className={styles.main}>
            <h5 className={styles.title}>برچسب ها</h5>
            <div className={styles.tags}>{renderTags()}</div>
        </div>
    );
}
