import React, { useEffect, useRef } from 'react';
import styles from '../styles/main-section.module.scss';
import { Col, Row } from 'react-bootstrap';
import classNames from 'classnames';
import ArticleCard from './article-card';
import SidebarBox from './sidebar-box';
import Pagination from 'modules/shared/components/pagination';
import { IBlog } from 'models/blog';
import router from 'next/router';
import Masonry from 'react-masonry-css';
import { IBlogNews } from 'models/blog-news';
import { IBlogCompact } from 'models/blog-compact';
import { IBlogSimple } from 'models/blog-simple';

export default function MainSection(props: {
    articles: IBlog[];
    lastPage: number;
    currentPage: number;
    mostLikedBlogs: IBlogSimple[];
    mostViewedBlogs: IBlogCompact[];
    latestNews: IBlogNews[];
}) {
    const breakpointColumnsObj = {
        default: 2,
        576: 1,
    };
    const scrollRef = useRef(null);

    useEffect(() => {
        if (router.asPath === '/blog') window.scrollTo(0, 0);
        else scrollRef.current.scrollIntoView();
    });

    const renderArticles = () => {
        return props.articles.map((article) => {
            return (
                <div key={article.title}>
                    <ArticleCard data={article} description={true} />
                </div>
            );
        });
    };
    return (
        <div ref={scrollRef} className={styles.main}>
            <h2 className={styles.title}>آخرین مقالات</h2>
            <Row>
                <Col xl={8}>
                    <Masonry
                        breakpointCols={breakpointColumnsObj}
                        className={styles['my-masonry-grid']}
                        columnClassName={styles['my-masonry-grid_column']}
                    >
                        {renderArticles()}
                    </Masonry>
                    <Pagination
                        pageCount={props.lastPage}
                        page={String(props.currentPage)}
                        handlePageClick={(data) =>
                            router.push(`/blog/page/${data.selected + 1}`, undefined, { scroll: false })
                        }
                    />
                </Col>
                <Col xl={4}>
                    <Row className={classNames(styles.sidebar, 'gx-md-3')}>
                        <Col sm={6} lg={4} xl={12}>
                            <SidebarBox articles={props.mostLikedBlogs} title="محبوب ترین ها" link="/blog/search" />
                        </Col>
                        <Col sm={6} lg={4} xl={12}>
                            <SidebarBox articles={props.mostViewedBlogs} title="پربازدید ترین ها" link="/blog/search" />
                        </Col>
                        <Col sm={6} lg={4} xl={12}>
                            <SidebarBox articles={props.latestNews} title="اخبار" isNews={true} />
                        </Col>
                    </Row>
                </Col>
            </Row>
        </div>
    );
}
