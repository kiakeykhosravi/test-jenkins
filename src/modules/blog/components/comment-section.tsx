import React, { useState } from 'react';
import styles from '../styles/comment-section.module.scss';
import { Accordion, Image } from 'react-bootstrap';
import classNames from 'classnames';
import { ChevronDown, ChevronUp } from 'react-bootstrap-icons';
import CommentForm from './comment-form';
import { IBlog } from 'models/blog';
import CommentsList from './comments-list';

export default function CommentSection(props: { article: IBlog }) {
    const cn = classNames;
    const [commentBoxOpen, setCommentBoxOpen] = useState(false);
    return (
        <div className={styles.main}>
            <h5 className={styles.title}>دیدگاه ها</h5>
            <Accordion defaultActiveKey="0" className={styles['comment-box']}>
                <Accordion.Toggle
                    eventKey="0"
                    className={cn(styles['accordion-toggle'], commentBoxOpen ? styles['toggle-down'] : null)}
                    onClick={() => {
                        setCommentBoxOpen((prevState) => !prevState);
                    }}
                >
                    <div className={styles.author}>
                        <Image src="/images/avatar-placeholder.png" className={styles['author-image']} />
                        <span className={styles['comment-box-title']}>دیدگاه شما</span>
                    </div>
                    {commentBoxOpen ? (
                        <ChevronUp className={styles['chevron']} />
                    ) : (
                        <ChevronDown className={styles['chevron']} />
                    )}
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="0" className={styles['accordion-collapse']}>
                    <>
                        {props.article.comments && !props.article.comments.length ? (
                            <div className="my-4 text-center fs-5">اولین نفری باشید که نظر می دهد!</div>
                        ) : null}
                        <CommentForm postId={props.article.id} />
                    </>
                </Accordion.Collapse>
                {props.article.comments && props.article.comments.length ? <hr /> : null}
                <CommentsList comments={props.article.comments || []} />
            </Accordion>
        </div>
    );
}
