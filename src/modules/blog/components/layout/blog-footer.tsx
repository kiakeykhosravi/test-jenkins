import React from 'react';
import styles from '../../styles/layout/footer.module.scss';
import { Envelope, Telephone } from 'react-bootstrap-icons';
import FooterLarge from './footer-large';
import { Col } from 'react-bootstrap';

export default function BlogFooter() {
    return (
        <div className={styles['blog-footer']}>
            <div className={styles.footer}>
                <div className={styles.right}>
                    <div className={styles['call-text']}>شماره تماس</div>
                    <div className={styles['other-text']}>هنگام برگزاری وبینار و کلاس آنلاین کنار شما هستیم</div>
                </div>
                <div className={styles.left}>
                    <div className={styles.email}>
                        <div className={styles['email-text']}>
                            <span>ایمیل: </span>
                            <a href="mailto:support@asanseminar.ir" className={styles['email-link']}>
                                support@asanseminar.ir
                            </a>
                        </div>
                        <Envelope className={styles.icon} />
                    </div>
                    <div className={styles.phone}>
                        <div className={styles['phone-text']}>۹۱۰۹۰۷۰۳ (۰۲۱)</div>
                        <Telephone className={styles.icon} />
                    </div>
                </div>
            </div>
            <FooterLarge />
        </div>
    );
}
