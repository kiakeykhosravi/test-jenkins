import React, { useState } from 'react';
import styles from '../../styles/layout/organization-blog-article-header.module.scss';
import { Container, Form, Image, Nav, Navbar } from 'react-bootstrap';
import { Diagram3Fill, HouseFill, Search, X } from 'react-bootstrap-icons';
import classNames from 'classnames';
import { useRouter } from 'next/router';

export default function OrganizationBlogArticleHeader() {
    const router = useRouter();
    const [searchInput, setSearchInput] = useState(router.query.q || '');
    const [search, setSearch] = useState(false);

    // apiBlogCategories(false, from router -> organization slug)

    const organizationHome = () => {
        const org = router.query.slug;
        return `/organizations/${org}`;
    };
    const organizationBlogHome = () => {
        const org = router.query.slug;
        return `/organizations/${org}/blog`;
    };
    return (
        <div className={styles.header}>
            <Container>
                <Navbar collapseOnSelect expand="lg" variant="dark" className={styles.navbar}>
                    <Navbar.Brand href="/">
                        <Image src={'/images/logos/asanseminar-logo.svg'} className={styles.brand} />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" className={styles.toggle} />
                    <Navbar.Collapse id="responsive-navbar-nav" className={styles.nav}>
                        <Nav className="mr-auto">
                            <Nav.Link href={organizationHome()} className={styles.link}>
                                <Diagram3Fill className={styles.icon} />
                                صفحه سازمان
                            </Nav.Link>
                            <Nav.Link href={organizationBlogHome()} className={styles.link}>
                                <HouseFill className={styles.icon} />
                                وبلاگ سازمان
                            </Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                    <Search className={styles.search} onClick={() => setSearch(true)} />
                    <Form
                        onSubmit={(e) => {
                            e.preventDefault();
                            router.push(`/organizations/${router.query.slug}/blog/search?q=${searchInput}&page=1`);
                        }}
                        className={classNames(styles.form, search ? styles.searching : null)}
                    >
                        <Form.Control
                            value={searchInput}
                            onChange={(e) => setSearchInput(e.target.value)}
                            placeholder={'جستجو...'}
                            className={styles.input}
                        />
                        <Search
                            onClick={() =>
                                router.push(`/organizations/${router.query.slug}/blog/search?q=${searchInput}&page=1`)
                            }
                            className={styles['mob-search']}
                        />
                        <X className={styles.close} onClick={() => setSearch(false)} />
                    </Form>
                </Navbar>
            </Container>
        </div>
    );
}
