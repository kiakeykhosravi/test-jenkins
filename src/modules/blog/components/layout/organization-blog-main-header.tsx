import React, { useState } from 'react';
import styles from '../../styles/layout/organization-blog-main-header.module.scss';
import { Container, Form, Image, Nav, Navbar } from 'react-bootstrap';
import { Diagram3Fill, HouseFill, Search } from 'react-bootstrap-icons';
import { useRouter } from 'next/router';

export default function OrganizationBlogMainHeader() {
    const router = useRouter();
    const [searchInput, setSearchInput] = useState(router.query.q || '');

    // apiBlogCategories(false, from router -> organization slug)

    const organizationHome = () => {
        const org = router.query.slug;
        return `/organizations/${org}`;
    };
    const organizationBlogHome = () => {
        const org = router.query.slug;
        return `/organizations/${org}/blog`;
    };
    return (
        <div className={styles.header}>
            <Container>
                <div className={styles['navbar-box']}>
                    <Navbar collapseOnSelect expand="lg" variant="dark" className={styles.navbar}>
                        <Navbar.Brand href="/">
                            <Image src={'/images/logos/asanseminar-logo.svg'} className={styles.brand} />
                        </Navbar.Brand>
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" className={styles.toggle} />
                        <Navbar.Collapse id="responsive-navbar-nav" className={styles.nav}>
                            <Nav className="mr-auto">
                                <Nav.Link href={organizationHome()} className={styles.link}>
                                    <Diagram3Fill className={styles.icon} />
                                    صفحه سازمان
                                </Nav.Link>
                                <Nav.Link href={organizationBlogHome()} className={styles.link}>
                                    <HouseFill className={styles.icon} />
                                    وبلاگ سازمان
                                </Nav.Link>
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>
                </div>
            </Container>
            {/*<Container>*/}
            {/*    <h1 className={styles['organization-name']}>{router.query.slug}</h1>*/}
            {/*</Container>*/}
            <Container>
                <Form
                    onSubmit={(e) => {
                        e.preventDefault();
                        router.push(`/organizations/${router.query.slug}/blog/search?q=${searchInput}&page=1`);
                    }}
                    className={styles['search-form']}
                >
                    <Search className={styles['search-icon']} />
                    <Form.Control
                        value={searchInput}
                        onChange={(e) => setSearchInput(e.target.value)}
                        placeholder={'دنبال چه مقاله ای هستی؟'}
                        className={styles['search-input']}
                    />
                    <div
                        onClick={() => router.push(`/organizations/${router.query.slug}/blog/search?q=${searchInput}`)}
                        className={styles['search-btn']}
                    >
                        جست و جو
                    </div>
                </Form>
            </Container>
        </div>
    );
}
