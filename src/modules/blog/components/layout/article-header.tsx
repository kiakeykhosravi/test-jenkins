import React, { useEffect, useState } from 'react';
import styles from '../../styles/layout/article-header.module.scss';
import { Container, Form, Image, Nav, Navbar, NavDropdown } from 'react-bootstrap';
import { HouseFill, Search, X } from 'react-bootstrap-icons';
import classNames from 'classnames';
import { useRouter } from 'next/router';
import { IBlogCategory } from 'models/blog-category';
import apiBlogCategories from '../../api/api-blog-categories';
import Link from 'next/link';
import Loader from 'modules/layout/components/loader';

export default function ArticleHeader() {
    const router = useRouter();
    const [categories, setCategories] = useState<{ data: IBlogCategory[]; status: boolean }>({
        data: [],
        status: false,
    });
    const [searchInput, setSearchInput] = useState(router.query.q || '');
    const [show, setShow] = useState([false, false, false, false]);
    const [otherShow, setOtherShow] = useState(false);
    const [search, setSearch] = useState(false);

    useEffect(() => {
        fetchBlogCategories();
    }, []);

    const fetchBlogCategories = async () => {
        const response = await apiBlogCategories(false);
        if (response.status === 'data') {
            setCategories({ data: response.data, status: true });
        } else {
            setCategories({ ...categories, status: true });
        }
    };

    const renderCategories = () => {
        return categories.data.map((cat, index) => {
            if (index < 4) {
                if (cat.sub_categories.length) {
                    return (
                        <NavDropdown
                            title={customDropDown(true, cat.title)}
                            id={`collapsible-nav-dropdown`}
                            className={classNames(styles.dropdown, 'blog-menu-dropdown')}
                            show={show[index]}
                            onMouseEnter={() => {
                                const temp = [false, false, false, false];
                                temp[index] = true;
                                setShow(temp);
                            }}
                            onMouseLeave={() => setShow([false, false, false, false])}
                            key={cat.title}
                        >
                            {cat.sub_categories.map((subCat) => customSubMenu(cat, subCat))}
                        </NavDropdown>
                    );
                } else
                    return (
                        <Nav.Link
                            href={`/blog/category/${cat.title.replace(/\s+/g, '-')}`}
                            className={styles.link}
                            key={cat.title}
                        >
                            {cat.title}
                        </Nav.Link>
                    );
            }
        });
    };

    const renderOtherCategories = () => {
        return (
            <NavDropdown
                title={customDropDown(false, 'سایر')}
                id={`collapsible-nav-dropdown`}
                className={classNames(styles.dropdown, 'blog-menu-dropdown')}
                show={otherShow}
                onMouseEnter={() => setOtherShow(true)}
                onMouseLeave={() => setOtherShow(false)}
            >
                {categories.data.map((cat, index) => {
                    if (index >= 4) {
                        return (
                            <NavDropdown.Item
                                href={`/blog/category/${cat.title.replace(/\s+/g, '-')}`}
                                className={styles['sub-item']}
                                key={cat.title}
                            >
                                {cat.title}
                            </NavDropdown.Item>
                        );
                    }
                })}
            </NavDropdown>
        );
    };

    const customDropDown = (isLink: boolean, title) => {
        return isLink ? (
            <Link href={`/blog/category/${title.replace(/\s+/g, '-')}`}>
                <a title={title}>
                    <div className={styles.link}>{title}</div>
                </a>
            </Link>
        ) : (
            <div className={styles.link}>{title}</div>
        );
    };

    const customSubMenu = (cat, subCat) => {
        return (
            <NavDropdown.Item
                href={`/blog/category/${subCat.title.replace(/\s+/g, '-')}`}
                className={styles['sub-item']}
            >
                {subCat.title}
            </NavDropdown.Item>
        );
    };

    if (categories.status) {
        return (
            <div className={styles.header}>
                <Container>
                    <Navbar collapseOnSelect expand="lg" variant="dark" className={styles.navbar}>
                        <Navbar.Brand href="/">
                            <Image src={'/images/logos/asanseminar-logo.svg'} className={styles.brand} />
                        </Navbar.Brand>
                        <Navbar.Toggle aria-controls="responsive-navbar-nav" className={styles.toggle} />
                        <Navbar.Collapse id="responsive-navbar-nav" className={styles.nav}>
                            <Nav className="mr-auto">
                                <Nav.Link href="/blog" className={styles.link}>
                                    <HouseFill className={styles.icon} />
                                    بلاگ آسان سمینار
                                </Nav.Link>
                                {renderCategories()}
                                {categories.data.length > 4 ? renderOtherCategories() : null}
                            </Nav>
                        </Navbar.Collapse>
                        <Search className={styles.search} onClick={() => setSearch(true)} />
                        <Form
                            onSubmit={(e) => {
                                e.preventDefault();
                                router.push(`/blog/search?q=${searchInput}&page=1`);
                            }}
                            className={classNames(styles.form, search ? styles.searching : null)}
                        >
                            <Form.Control
                                value={searchInput}
                                onChange={(e) => setSearchInput(e.target.value)}
                                placeholder={'جستجو...'}
                                className={styles.input}
                            />
                            <Search
                                onClick={() => router.push(`/blog/search?q=${searchInput}&page=1`)}
                                className={styles['mob-search']}
                            />
                            <X className={styles.close} onClick={() => setSearch(false)} />
                        </Form>
                    </Navbar>
                </Container>
            </div>
        );
    } else return <Loader />;
}
