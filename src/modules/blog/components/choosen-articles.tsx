import React from 'react';
import styles from '../styles/choosen-articles.module.scss';
import ArticleCard from './article-card';
import classNames from 'classnames';
import { Col, Row } from 'react-bootstrap';
import { IBlogCompact } from 'models/blog-compact';

export default function ChoosenArticles(props: { articles: IBlogCompact[] }) {
    const renderArticles = () => {
        return props.articles.map((article) => {
            return (
                <Col sm={6} lg={4} key={article.id}>
                    <ArticleCard data={article} description={false} />
                </Col>
            );
        });
    };
    return (
        <div className={styles.main}>
            <div className={styles.header}>
                <h2 className={styles.title}>مقالات منتخب</h2>
            </div>
            <Row className={classNames('gx-sm-4 gx-md-5 gy-5', styles['gy-5'], styles.articles)}>
                {renderArticles()}
            </Row>
        </div>
    );
}
