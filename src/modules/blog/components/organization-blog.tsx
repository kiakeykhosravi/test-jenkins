import React from 'react';
import styles from '../styles/organization-blog.module.scss';
import classNames from 'classnames';
import { Col, Container, Row } from 'react-bootstrap';
import { IBlog } from 'models/blog';
import router from 'next/router';
import Pagination from 'modules/shared/components/pagination';
import OrganizationBlogArticleCard from './organization-blog-article-card';

export default function OrganizationBlog(props: {
    articles: IBlog[];
    lastPage: number;
    currentPage: number;
    organization_slug: string;
    organization_title;
}) {
    const renderArticles = () => {
        if (props.articles.length) {
            return props.articles.map((article) => {
                return (
                    <Col sm={6} lg={4} key={article.id}>
                        <OrganizationBlogArticleCard data={article} description={false} />
                    </Col>
                );
            });
        } else
            return (
                <Container>
                    <div style={{ fontSize: '1.1rem', padding: '2rem 0', textAlign: 'center' }}>
                        <img
                            src="/images/notes.png"
                            style={{ width: '100%', maxWidth: '100px', marginBottom: '1.5rem' }}
                            alt=""
                        />
                        <div>متاسفانه‌‌ هیچ مقاله ای یافت نشد!</div>
                    </div>
                </Container>
            );
    };
    return (
        <div className={styles.main}>
            <div className={styles.header}>
                {props.articles.length ? (
                    <h1 className={styles.title}>{`مقالات ${props.organization_title}`}</h1>
                ) : null}
            </div>
            <Row className={classNames('gx-sm-4 gx-md-5 gy-5', styles['gy-5'], styles.articles)}>
                {renderArticles()}
            </Row>
            {props.lastPage > 1 ? (
                <Pagination
                    pageCount={props.lastPage}
                    page={String(props.currentPage)}
                    handlePageClick={(data) =>
                        router.push(`/organizations/${props.organization_slug}/blog/page/${data.selected + 1}`)
                    }
                />
            ) : null}
        </div>
    );
}
