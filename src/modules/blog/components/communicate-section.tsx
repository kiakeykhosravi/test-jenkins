import React, { useRef } from 'react';
import styles from '../styles/communicate-section.module.scss';
import { ChatLeftText, Link45deg, Linkedin } from 'react-bootstrap-icons';
import { IBlog } from 'models/blog';
import LikeBtn from './like-btn';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';
import toPersianNum from 'utils/to-persian-num';

export default function CommunicateSection(props: {
    article: IBlog;
    isLiked: boolean;
    changeIsLiked: (value: boolean) => any;
    likesCountState: number;
    basePath: string;
}) {
    const router = useRouter();
    const pageUrlElementRef = useRef(null);

    const shortenedUrl = (() => {
        const path = router.asPath;
        const index = path.indexOf(String(router.query.id));
        return props.basePath + path.slice(0, index + String(router.query.id).length);
    })();

    const onclickCopyLink = () => {
        pageUrlElementRef.current.select();
        document.execCommand('copy');
        pageUrlElementRef.current.blur();
        toast.success('لینک صفحه با موفقیت کپی شد');
    };

    return (
        <div className={styles.main}>
            <div className={styles['social-share']}>
                <h6 className={styles.title}>به اشتراک گذاری</h6>
                <div className={styles['social-icons']}>
                    <a
                        href={`https://www.linkedin.com/sharing/share-offsite/?url=${shortenedUrl}`}
                        title="به اشتراک گذاری در لینکدین"
                        onClick={() => {
                            toast.success('شما در حال انتقال به لینکدین هستید ...');
                        }}
                    >
                        <Linkedin className={styles['social-icon']} />
                    </a>
                    <span onClick={onclickCopyLink} className={styles['page-link-button']}>
                        کپی لینک صفحه
                        <Link45deg className={styles['social-icon']} />
                        <input
                            type="text"
                            value={shortenedUrl}
                            className={styles['hidden-page-link']}
                            ref={pageUrlElementRef}
                        />
                    </span>
                </div>
            </div>
            <div className={styles.actions}>
                <div className={styles.comments}>
                    <span className={styles['action-count']}>{toPersianNum(props.article.comments_count || '0')}</span>
                    <ChatLeftText className={styles['actions-icon']} />
                </div>
                <span className={styles['action-count']}>{toPersianNum(props.likesCountState)}</span>
                <LikeBtn
                    postId={props.article.id}
                    containerClass={styles['actions-icon']}
                    isLiked={props.isLiked}
                    changeIsLiked={props.changeIsLiked}
                />
            </div>
        </div>
    );
}
