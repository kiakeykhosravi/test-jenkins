import React from 'react';
import styles from '../styles/related-posts.module.scss';
import Link from 'next/link';
import { IBlogSimple } from 'models/blog-simple';
import AsanImage from 'modules/shared/components/asan-image';
import { SwiperSlide } from 'swiper/react';
import Slider from 'modules/shared/components/slider';

export default function RelatedPosts(props: { posts: IBlogSimple[] }) {
    const renderPosts = () => {
        return props.posts.map((post) => {
            return (
                <SwiperSlide key={post.id} className={styles['swiper-slide']} dir="rtl">
                    <Link href={`/blog/${post.id}/${post.title.trim().replace(/\s+/g, '-')}`}>
                        <a title={post.title} className={styles.link}>
                            <AsanImage
                                src={post.media && post.media[0] && post.media[0].thumbnail_url}
                                alt={post.title}
                                className={styles.img}
                            />
                        </a>
                    </Link>
                </SwiperSlide>
            );
        });
    };

    const breakpoints = {
        420: {
            slidesPerView: 1,
            spaceBetween: 8,
        },
        576: {
            slidesPerView: 2,
            spaceBetween: 10,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        992: {
            slidesPerView: 3,
            spaceBetween: 15,
        },
        1200: {
            slidesPerView: 2,
            spaceBetween: 15,
        },
        1400: {
            slidesPerView: 3,
            spaceBetween: 20,
        },
    };

    return (
        <div className={styles.main}>
            <h5 className={styles.title}>مقاله های مرتبط</h5>
            <Slider
                spaceBetween={15}
                paginationOptions={{ pagination: false }}
                navigationOptions={{
                    navigation: {
                        nextEl: '.' + styles['slider-btn-next'],
                        prevEl: '.' + styles['slider-btn-prev'],
                    },
                }}
                breakpoints={breakpoints}
                swiperClasses={styles['swiper-main']}
                autoplayOptions={{
                    autoplay: {
                        delay: 10000,
                    },
                }}
            >
                {renderPosts()}
            </Slider>
            {/*<div className={styles.posts}>{renderPosts()}</div>*/}
        </div>
    );
}
