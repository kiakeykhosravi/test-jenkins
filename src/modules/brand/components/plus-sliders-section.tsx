import React from 'react';
import styles from '../styles/plus-sliders-section.module.scss';
import { IOrganization } from 'models/organization';
import { Col, Container, Row } from 'react-bootstrap';
import { IOrganizationSlider } from 'models/organization-slider';
import PlusSlider from './plus-slider';

export default function PlusSlidersSection(props: { data: IOrganization; sliders: IOrganizationSlider[] }) {
    const renderSliders = () => {
        return props.sliders.map((slider, index) => {
            return slider.products.length ? (
                <div className="mb-4" key={index}>
                    <h3 className="text-center">{slider.title}</h3>
                    <PlusSlider data={props.data} slider={slider} />
                </div>
            ) : null;
        });
    };
    return (
        <div className={styles.main} id="products">
            {props.sliders.length ? (
                <>
                    <h2 className={styles.title}>محصولات و خدمات</h2>
                    <Container>
                        <Row>
                            <Col>{renderSliders()}</Col>
                        </Row>
                    </Container>
                </>
            ) : null}
        </div>
    );
}
