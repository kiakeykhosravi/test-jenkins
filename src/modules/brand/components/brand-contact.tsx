import React, { useEffect, useRef, useState } from 'react';
import styles from '../styles/brand-contact.module.scss';
import { Modal, Row } from 'react-bootstrap';
import { IOrganization } from 'models/organization';
import toPersianNum from 'utils/to-persian-num';
import AsanImage from 'modules/shared/components/asan-image';
import { Link45deg, ShareFill } from 'react-bootstrap-icons';
import { toast } from 'react-toastify';
import classNames from 'classnames';
import BrandpageFooter from 'modules/organizations/components/layout/brandpage-footer';
import { useRouter } from 'next/router';

export default function BrandContact(props: {
    organization: IOrganization;
    contactRef;
    footer: boolean;
    showBrand: boolean;
    isPremium?: boolean;
    isPlus?: boolean;
}) {
    return (
        <div className={classNames(styles.main, props.isPremium ? styles.premium : '')} ref={props.contactRef}>
            <div className={styles['contact-section']}>
                {BrandContactSection(props.organization, props.showBrand, props.isPlus)}
            </div>
            {props.footer ? <BrandpageFooter /> : null}
        </div>
    );
}

export const BrandContactSection = (organization: IOrganization, showBrand: boolean, isPlus?: boolean) => {
    const [showModal, setShowModal] = useState(false);
    const [pageLink, setPageLink] = useState('');
    const pageUrlElementRef = useRef(null);
    // const pageLink = organization.short_link || `/organizations/${organization.slug}`;

    useEffect(() => {
        if (window !== undefined) {
            setPageLink(organization.short_link || `${window.location.host}/organizations/${organization.slug}`);
        }
    }, []);

    const contactSection = () => {
        let phoneNumbers = organization.phone_number;
        if (!Array.isArray(phoneNumbers)) {
            phoneNumbers = phoneNumbers ? [phoneNumbers] : [];
        }

        let mobileNumbers = organization.mobile_number;
        if (!Array.isArray(mobileNumbers)) {
            mobileNumbers = mobileNumbers ? [mobileNumbers] : [];
        }

        const phoneNumberElements = phoneNumbers
            ? phoneNumbers.map((item: string, index: number) => {
                  return (
                      <div className={styles['social-item']} key={index}>
                          <a
                              href={`tel:${item.trim()}`}
                              title="تلفن"
                              target="_blank"
                              rel="noreferrer"
                              className={styles['social-item-link']}
                          >
                              <div className={styles['social-item-icon']}>
                                  <AsanImage
                                      src={'/images/logos/socialmedias/telephone.svg'}
                                      alt={'تلفن'}
                                      className={styles['social-item-img']}
                                  />
                              </div>
                              <div className={styles['social-item-text']}>{toPersianNum(item.trim())}</div>
                          </a>
                      </div>
                  );
              })
            : null;

        const mobileNumberElements = mobileNumbers
            ? mobileNumbers.map((item: string, index: number) => {
                  return (
                      <div className={styles['social-item']} key={index}>
                          <a
                              href={`tel:${item.trim()}`}
                              title="موبایل"
                              target="_blank"
                              rel="noreferrer"
                              className={styles['social-item-link']}
                          >
                              <div className={styles['social-item-icon']}>
                                  <AsanImage
                                      src="/images/logos/socialmedias/mobile.png"
                                      alt="موبایل"
                                      className={styles['social-item-img']}
                                  />
                              </div>
                              <div className={styles['social-item-text']}>{toPersianNum(item.trim())}</div>
                          </a>
                      </div>
                  );
              })
            : null;

        return (
            <>
                {phoneNumberElements}
                {mobileNumberElements}
            </>
        );
    };

    const onclickCopyLink = () => {
        pageUrlElementRef.current.select();
        document.execCommand('copy');
        pageUrlElementRef.current.blur();
        toast.success('لینک صفحه با موفقیت کپی شد');
    };

    const share = () => {
        const pageInfo = {
            url: organization.short_link || `/organizations/${organization.slug}`,
            title: `صفحه ${organization.title}`,
            text: `صفحه ${organization.title} را دنبال کنید.`,
        };
        if (navigator.share) {
            navigator.share(pageInfo);
        } else setShowModal(true);
    };

    const internationalNumber = (number: string) => {
        let correctedNumber = number;
        if (number[0] === '0') {
            correctedNumber = '+98' + number.substring(1);
        }
        return correctedNumber;
    };

    const renderSocials = (href, title, imgSrc, text, imgClass?) => {
        const imageClasses = classNames(styles['social-item-img'], imgClass || null);
        return (
            <div className={styles['social-item']}>
                <a href={href} title={title} target="_blank" rel="noreferrer" className={styles['social-item-link']}>
                    <div className={styles['social-item-icon']}>
                        <AsanImage src={imgSrc} alt={text} className={imageClasses} />
                    </div>
                    <div className={styles['social-item-text']}>{text}</div>
                </a>
            </div>
        );
    };

    return (
        <>
            {showBrand ? (
                <div className={styles.brand}>
                    <AsanImage src={organization.logo.url} alt="لوگو" className={styles['brand-logo']} />
                    <h2 className={styles['brand-title']}>{organization.slogan || organization.title}</h2>
                </div>
            ) : null}
            <div className={styles.box}>
                <div className={styles.contact}>
                    <div className={styles['social-title']}>تنها با یک کلیک با ما در ارتباط باشید</div>
                    <Row className={styles['social-box']}>
                        {organization.email ? (
                            <div className={classNames(styles['social-item'], styles['social-item-large'])}>
                                <a
                                    href={`mailto:${organization.email}`}
                                    title={`ایمیل ${organization.title}`}
                                    target="_blank"
                                    rel="noreferrer"
                                    className={styles['social-item-link']}
                                >
                                    <div className={styles['social-item-icon']}>
                                        <AsanImage src="/images/logos/socialmedias/gmail.png" alt="ایمیل" />
                                    </div>
                                    <div className={styles['social-item-text']}>{organization.email}</div>
                                </a>
                            </div>
                        ) : null}

                        {organization.whatsapp
                            ? renderSocials(
                                  `whatsapp://send?phone=${internationalNumber(organization.whatsapp)}`,
                                  `واتساپ ${organization.title}`,
                                  '/images/logos/socialmedias/whatsapp-icon.png',
                                  'واتساپ',
                              )
                            : null}

                        {organization.whatsapp_business
                            ? renderSocials(
                                  organization.whatsapp,
                                  `واتساپ بیزینس ${organization.title}`,
                                  '/images/logos/socialmedias/whatsapp-business.svg',
                                  'واتساپ بیزینس',
                              )
                            : null}

                        {organization.instagram
                            ? renderSocials(
                                  `https://instagram.com/${organization.instagram}`,
                                  `اینستاگرام ${organization.title}`,
                                  '/images/logos/socialmedias/instagram-full.svg',
                                  'اینستاگرام',
                              )
                            : null}

                        {organization.linkedin
                            ? renderSocials(
                                  organization.linkedin,
                                  `لینکدین ${organization.title}`,
                                  '/images/logos/socialmedias/linkedin-full_org.svg',
                                  'لینکدین',
                              )
                            : null}

                        {organization.aparat
                            ? renderSocials(
                                  `https://aparat.com/${organization.aparat}`,
                                  `آپارات ${organization.title}`,
                                  '/images/logos/socialmedias/aparat.svg',
                                  'آپارات',
                              )
                            : null}

                        {organization.website
                            ? renderSocials(
                                  organization.website,
                                  `وبسایت ${organization.title}`,
                                  '/images/logos/socialmedias/address.svg',
                                  'وبسایت',
                              )
                            : null}

                        {contactSection()}

                        <div className={styles['social-item']} onClick={onclickCopyLink}>
                            <a title={`کپی لینک صفحه`} className={styles['social-item-link']}>
                                <div className={styles['social-item-icon']}>
                                    <Link45deg className={styles['social-item-img']} />
                                    <input
                                        type="text"
                                        value={pageLink}
                                        className={styles['hidden-page-link']}
                                        ref={pageUrlElementRef}
                                    />
                                </div>
                                <div className={styles['social-item-text']}>کپی لینک صفحه</div>
                            </a>
                        </div>

                        <div
                            className={classNames(styles['social-item'], styles['social-item-share'])}
                            onClick={() => share()}
                        >
                            <a title={`به اشتراک گذاری`} className={styles['social-item-link']}>
                                <div className={styles['social-item-icon']}>
                                    <ShareFill className={styles['social-item-img']} />
                                </div>
                                <div className={styles['social-item-text']}>به اشتراک گذاری</div>
                            </a>
                        </div>

                        {isPlus && organization.address ? (
                            <div className={styles.address}>
                                <span className={styles['address-label']}>آدرس: </span>
                                <span>{toPersianNum(organization.address || '')}</span>
                            </div>
                        ) : null}
                    </Row>
                </div>
            </div>
            <Modal
                centered
                size={'sm'}
                show={showModal}
                onHide={() => setShowModal(false)}
                dialogClassName={styles['modal-dialog']}
                contentClassName={styles.modal}
            >
                <Modal.Body className={styles['modal-body']}>
                    <div className={styles['modal-share']}>
                        <ShareFill className={styles['modal-share-icon']} />
                    </div>
                    <div className={styles['modal-share-icons-container']}>
                        <a
                            href={`whatsapp://send?text=${organization.short_link}`}
                            title={'اشتراک در واتساپ'}
                            target="_blank"
                            rel="noreferrer"
                            className={styles['modal-share-item-icon']}
                        >
                            <AsanImage
                                src={'/images/logos/socialmedias/whatsapp-filled.png'}
                                alt={'واتساپ'}
                                className={classNames(styles['modal-social-icon'], styles['whatsapp-icon'])}
                            />
                        </a>
                        <a
                            href={`https://www.linkedin.com/sharing/share-offsite/?url=${organization.short_link}`}
                            title={'اشتراک در لینکدین'}
                            target="_blank"
                            rel="noreferrer"
                            className={styles['modal-share-item-icon']}
                        >
                            <AsanImage
                                src={'/images/logos/socialmedias/linkedin-circled.png'}
                                alt={'لینکدین'}
                                className={styles['modal-social-icon']}
                            />
                        </a>
                        <a
                            title={'کپی لینک صفحه'}
                            className={styles['modal-share-item-icon']}
                            onClick={onclickCopyLink}
                        >
                            <AsanImage
                                src={'/images/logos/socialmedias/link.png'}
                                alt={'لینک'}
                                className={styles['modal-social-icon']}
                            />
                        </a>
                    </div>
                </Modal.Body>
            </Modal>
        </>
    );
};
