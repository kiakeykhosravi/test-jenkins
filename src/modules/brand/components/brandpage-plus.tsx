import React from 'react';
import PlusIntroduction from './plus-introduction';
import BrandpageHeader from 'modules/brandpage/components/layout/brandpage-header';
import BrandpageFooter from 'modules/brandpage/components/layout/brandpage-footer';
import { IOrganization } from 'models/organization';
import BrandContact from './brand-contact';
import PlusSlidersSection from './plus-sliders-section';
import { IOrganizationSlider } from 'models/organization-slider';

export default function BrandpagePlus(props: { organizationData: IOrganization; sliders: IOrganizationSlider[] }) {
    return (
        <>
            <BrandpageHeader />
            <PlusIntroduction data={props.organizationData} />
            <BrandContact
                organization={props.organizationData}
                contactRef={null}
                showBrand={false}
                footer={false}
                isPlus={true}
            />
            <PlusSlidersSection data={props.organizationData} sliders={props.sliders} />
            <BrandpageFooter />
        </>
    );
}
