import React from 'react';
import { IOrganization } from 'models/organization';
import PlusProductCard from './plus-product-card';
import { IOrganizationSlider } from 'models/organization-slider';
import Slider from 'modules/shared/components/slider';
import { SwiperSlide } from 'swiper/react';
import { IOrganizationSliderProduct } from 'models/organization-slider-product';

export default function PlusSlider(props: { data: IOrganization; slider: IOrganizationSlider }) {
    const sliderBreakpoints = {
        576: {
            slidesPerView: 2,
            spaceBetween: 10,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        992: {
            slidesPerView: 3,
            spaceBetween: 25,
        },
    };

    const renderSliderProducts = (products: IOrganizationSliderProduct[]) => {
        return products.map((product, index) => {
            return (
                <SwiperSlide className="d-flex justify-content-center" key={index}>
                    <PlusProductCard organizationSlug={props.data.slug} data={product} />
                </SwiperSlide>
            );
        });
    };

    return (
        <>
            <Slider
                breakpoints={sliderBreakpoints}
                spaceBetween={20}
                centeredSlides={false}
                swiperClasses="pb-5 pb-sm-4"
                paginationOptions={{
                    pagination: true,
                }}
            >
                {renderSliderProducts(props.slider.products)}
            </Slider>
        </>
    );
}
