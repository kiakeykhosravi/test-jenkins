import React from 'react';
import styles from '../styles/brand-image-slider.module.scss';
import { IMedia } from 'models/media';
import { SwiperSlide } from 'swiper/react';
import classNames from 'classnames';
import Slider from 'modules/shared/components/slider';
import Banner from 'modules/shared/components/banner';
import useSwiperRef from 'utils/use-swiper-ref';

export default function BrandImageSlider(props: {
    images: IMedia[];
    sliderClass?;
    bannerClass?;
    bodyClass?;
    imgClass?;
    autoplay?;
}) {
    const [paginationEl, paginationRef] = useSwiperRef();
    const autoplayOption = props.autoplay ? props.autoplay : { autoplay: false };

    const renderSlides = (images: IMedia[]) => {
        return images.map((slide, index) => {
            return (
                <SwiperSlide className={styles.slide} key={index}>
                    <img
                        className={classNames(styles['slide-img'], props.imgClass || '')}
                        src={slide.url}
                        alt={slide.title}
                        dir="rtl"
                    />
                </SwiperSlide>
            );
        });
    };
    return (
        <div className={classNames(styles.slider, props.sliderClass || '')}>
            {props.images && props.images.length > 1 ? (
                <>
                    <Slider
                        slidesPerView={1}
                        spaceBetween={30}
                        centeredSlides={false}
                        swiperClasses={classNames('h-100', styles['swiper-container'])}
                        paginationOptions={{
                            pagination: {
                                el: paginationEl,
                                clickable: true,
                            },
                        }}
                        autoplayOptions={autoplayOption}
                    >
                        {renderSlides(props.images)}
                    </Slider>
                    <div className={styles.pagination} ref={paginationRef} />
                </>
            ) : (
                <Banner
                    bannerClasses={classNames(styles.banner, props.bannerClass || '')}
                    bodyClasses={classNames(styles['banner-body'], props.bodyClass || '')}
                >
                    <img
                        src={
                            (props.images && props.images.length && props.images[0].url) ||
                            '/images/webinar-default-image.jpeg'
                        }
                        alt={(props.images && props.images.length && props.images[0].title) || ''}
                        className={classNames(styles['slide-img'], props.imgClass || '')}
                    />
                </Banner>
            )}
        </div>
    );
}
