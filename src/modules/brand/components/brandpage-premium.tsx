import React, { useRef } from 'react';
import styles from '../styles/brandpage-premium.module.scss';
import PremiumMain from './premium-main';
import BrandContact from './brand-contact';
import { IOrganization } from 'models/organization';

export default function BrandpagePremium(props: { organizationData: IOrganization }) {
    const contactRef = useRef(null);

    const executeScroll = () => contactRef.current.scrollIntoView();

    return (
        <div className={styles.brand}>
            <PremiumMain organization={props.organizationData} executeScroll={executeScroll} />
            <BrandContact
                organization={props.organizationData}
                contactRef={contactRef}
                showBrand={true}
                footer={true}
                isPremium={true}
            />
        </div>
    );
}
