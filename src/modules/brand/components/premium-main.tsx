import React from 'react';
import styles from '../styles/premium-main.module.scss';
import AsanImage from 'modules/shared/components/asan-image';
import toPersianNum from 'utils/to-persian-num';
import { ChevronDown } from 'react-bootstrap-icons';
import { BrandContactSection } from './brand-contact';
import { IOrganization } from 'models/organization';
import { Col, Container, Row } from 'react-bootstrap';
import BrandImageSlider from './brand-image-slider';
import BrandpageHeader from 'modules/brandpage/components/layout/brandpage-header';

export default function PremiumMain(props: { organization: IOrganization; executeScroll }) {
    return (
        <div className={styles.main}>
            <BrandpageHeader />
            <div className={styles.header}>
                <div className={styles.content}>
                    <div className={styles.top}>
                        <div className={styles['top-bg']} />
                        <div className={styles.logo}>
                            <AsanImage src={props.organization.logo.url} alt={'لوگو'} className={styles['logo-img']} />
                        </div>
                        <AsanImage src={'/images/icons/dots4-4.svg'} alt={'dots4-4'} className={styles['dots4-4']} />
                        <h1 className={styles['name']}>{toPersianNum(props.organization.title)}</h1>
                        <div className={styles['double-quotes']}>
                            <AsanImage
                                src={'/images/icons/double-quotes.svg'}
                                alt={'double-quotes'}
                                className={styles['double-quotes-img']}
                            />
                        </div>
                        <div className={styles['header-about-section']}>
                            <div className={styles['header-about-right']} />
                            <p className={styles['description']}>{toPersianNum(props.organization.description)}</p>
                            <div className={styles['header-about-left']} />
                        </div>
                        <AsanImage src={'/images/icons/dots2-4.svg'} alt={'dots2-4'} className={styles['dots2-4']} />
                    </div>
                    <Container className={styles['main-desktop']}>
                        <Row className={styles.bottom}>
                            <Col md={5} className={styles['right-contact']}>
                                {BrandContactSection(props.organization, true)}
                            </Col>
                            <Col md={7} className={styles['left-main']}>
                                <p className={styles['description']}>{toPersianNum(props.organization.description)}</p>
                                <BrandImageSlider images={props.organization.media} />
                            </Col>
                        </Row>
                    </Container>
                    <div className={styles['mobile-slider']}>
                        <BrandImageSlider images={props.organization.media} />
                    </div>
                </div>
                <ChevronDown className={styles.arrow} onClick={() => props.executeScroll()} />
            </div>
        </div>
    );
}
