import React from 'react';
import styles from '../styles/plus-introduction.module.scss';
import AsanImage from 'modules/shared/components/asan-image';
import toPersianNum from 'utils/to-persian-num';
import BrandImageSlider from './brand-image-slider';
import { IOrganization } from 'models/organization';
import { Col, Container, Row } from 'react-bootstrap';
import { BrandContactSection } from './brand-contact';
import useWindowDimensions from 'utils/use-window-dimensions';

export default function PlusIntroduction(props: { data: IOrganization }) {
    const { width } = useWindowDimensions();
    return (
        <div className={styles.main}>
            <div className={styles['top-bg']} />
            <div className={styles.logo}>
                <AsanImage src={props.data.logo.url} alt={`لوگو ${props.data.title}`} className={styles['logo-img']} />
            </div>
            <h1 className={styles.title}>{toPersianNum(props.data.title)}</h1>
            <Container className={styles.mobile}>
                <div className={styles.slider}>
                    <BrandImageSlider images={props.data.media} />
                </div>
                <div className={styles['description-box']}>
                    <p className={styles.description}>{toPersianNum(props.data.description)}</p>
                    <img
                        src={width < 450 ? '/images/vectors/vector-1.png' : '/images/vectors/vector-1-lg.png'}
                        alt="وکتور1"
                        className={styles['description-img']}
                    />
                </div>
            </Container>
            <Container className={styles.desktop}>
                <Row className={styles['desktop-row']}>
                    <Col md={5} className={styles['right-contact']}>
                        {BrandContactSection(props.data, true, true)}
                        {/*<div className={styles['request-btn']}>ثبت سفارش / مشاوره</div>*/}
                    </Col>
                    <Col md={7} className={styles['left-main']}>
                        <div className={styles.slider}>
                            <BrandImageSlider images={props.data.media} />
                        </div>
                        {width > 991 ? (
                            <div className={styles['description-box']}>
                                <p className={styles.description}>{toPersianNum(props.data.description)}</p>
                                <img
                                    src="/images/vectors/vector-1-lg.png"
                                    alt="وکتور1"
                                    className={styles['description-img']}
                                />
                            </div>
                        ) : null}
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
