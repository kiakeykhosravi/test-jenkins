import React from 'react';
import styles from '../styles/brandpage-product.module.scss';
import { Breadcrumb, Col, Container, Row } from 'react-bootstrap';
import BrandImageSlider from './brand-image-slider';
import Link from 'next/link';
import Slider from 'modules/shared/components/slider';
import { SwiperSlide } from 'swiper/react';
import AsanImage from 'modules/shared/components/asan-image';
import { IOrganizationProduct } from 'models/organization-product';
import { IOrganizationSliderProduct } from 'models/organization-slider-product';
import DOMPurify from 'isomorphic-dompurify';

export default function BrandpageProduct(props: {
    product: IOrganizationProduct;
    relatedProducts: IOrganizationSliderProduct[];
    slug: string;
}) {
    const renderRelatedServices = () => {
        return props.relatedProducts.map((product, index) => {
            const link = product.is_external
                ? product.external_url
                : `/organizations/${props.slug}/product/${product.id}`;
            return (
                <SwiperSlide key={index} className={styles['swiper-slide']} dir="rtl">
                    <Link href={link}>
                        <a
                            title={'تصویر' + product.title}
                            className={styles.link}
                            {...(product.is_external ? { target: '_blank' } : {})}
                        >
                            <AsanImage src={product.media.url} alt={'تصویر' + product.title} className={styles.img} />
                        </a>
                    </Link>
                </SwiperSlide>
            );
        });
    };
    const breakpoints = {
        420: {
            slidesPerView: 1,
            spaceBetween: 8,
        },
        576: {
            slidesPerView: 2,
            spaceBetween: 10,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 20,
        },
        992: {
            slidesPerView: 3,
            spaceBetween: 20,
        },
        1400: {
            slidesPerView: 3,
            spaceBetween: 20,
        },
    };
    return (
        <div className={styles.main}>
            <Container>
                <Breadcrumb className={styles.breadcrumb}>
                    <Link href={`/organizations/${props.slug}`} passHref>
                        <Breadcrumb.Item href="/organizations/test-brandpage">
                            <img src="/images/logos/asanseminar-logo.svg" alt="خانه" className={styles.logo} />
                            <span>صفحه اصلی</span>
                        </Breadcrumb.Item>
                    </Link>
                    <Link href={`/organizations/${props.slug}#products`} passHref>
                        <Breadcrumb.Item href="/organizations/test-brandpage#products">محصولات و خدمات</Breadcrumb.Item>
                    </Link>
                </Breadcrumb>
                <Row className="mb-4">
                    <Col lg={8} className="offset-lg-2 mb-4">
                        <h1 className={styles.title}>{props.product.title}</h1>
                        <BrandImageSlider
                            images={props.product.media}
                            sliderClass={styles.slider}
                            imgClass={styles.img}
                            autoplay={{
                                autoplay: {
                                    delay: 8000,
                                },
                            }}
                        />
                    </Col>
                    <Col xs={12} className={styles.section}>
                        <div
                            className={styles.description}
                            dangerouslySetInnerHTML={{
                                __html: DOMPurify.sanitize(props.product.description),
                            }}
                        />
                    </Col>
                </Row>
                <div className={styles.section}>
                    {props.relatedProducts.length ? (
                        <>
                            <h2 className={styles['secondary-title']}>محصولات بیشتر</h2>
                            <Slider
                                spaceBetween={15}
                                paginationOptions={{ pagination: false }}
                                navigationOptions={{
                                    navigation: {
                                        nextEl: '.' + styles['slider-btn-next'],
                                        prevEl: '.' + styles['slider-btn-prev'],
                                    },
                                }}
                                breakpoints={breakpoints}
                                swiperClasses={styles['swiper-main']}
                                autoplayOptions={{
                                    autoplay: {
                                        delay: 80000,
                                    },
                                }}
                            >
                                {renderRelatedServices()}
                            </Slider>
                        </>
                    ) : null}
                </div>
            </Container>
        </div>
    );
}
