import React from 'react';
import styles from '../styles/plus-products-card.module.scss';
import Link from 'next/link';
import { Card } from 'react-bootstrap';
import toPersianNum from 'utils/to-persian-num';
import { IOrganizationSliderProduct } from 'models/organization-slider-product';

export default function PlusProductCard(props: { data: IOrganizationSliderProduct; organizationSlug: string }) {
    const link = props.data.is_external
        ? props.data.external_url
        : `/organizations/${props.organizationSlug}/product/${props.data.id}`;

    return (
        <Card className={styles.card} dir="rtl">
            <div className={styles['card-header']}>
                <Link href={link}>
                    <a title={props.data.title} {...(props.data.is_external ? { target: '_blank' } : {})}>
                        <img
                            src={props.data.media.url || ''}
                            alt={props.data.media.title || ''}
                            className={styles.banner}
                        />
                    </a>
                </Link>
            </div>
            <Card.Body className={styles['card-body']}>
                <h3 className={styles.title}>
                    <Link href={link}>
                        <a
                            title={props.data.title}
                            className={styles.title}
                            {...(props.data.is_external ? { target: '_blank' } : {})}
                        >
                            {props.data.title}
                        </a>
                    </Link>
                </h3>
                <p className={styles.description}>{props.data.short_description}</p>
                <div className={styles['card-footer']}>
                    <div className={styles['service-code']}>
                        {props.data.custom_product_id ? (
                            <>
                                <span>کد محصول: </span>
                                <span>{toPersianNum(props.data.custom_product_id)}</span>
                            </>
                        ) : null}
                    </div>
                    <Link href={link}>
                        <a
                            title={props.data.title}
                            className={styles.more}
                            {...(props.data.is_external ? { target: '_blank' } : {})}
                        >
                            <span className={styles['more-text']}>مشاهده کامل</span>
                            <img
                                src="/images/brandpage/arrow-left.png"
                                alt="مشاهده کامل"
                                className={styles['more-icon']}
                            />
                        </a>
                    </Link>
                </div>
            </Card.Body>
        </Card>
    );
}
