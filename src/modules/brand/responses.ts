import { IOrganizationProduct } from 'models/organization-product';
import { IOrganizationSliderProduct } from 'models/organization-slider-product';

export interface IProductResponse {
    product: IOrganizationProduct;
    related_products: IOrganizationSliderProduct[];
}
