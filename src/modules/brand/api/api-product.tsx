import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IProductResponse } from '../responses';

export default async function apiProduct(
    id: string,
): Promise<{
    data?: IProductResponse;
    status: 'error' | 'data';
}> {
    const url = endpoints.backend.organization_product.replace('{organization_product_id}', id);
    const result = await apiRequest<IProductResponse>('get', url);

    if (result.status === 'data') {
        return {
            data: result.data,
            status: result.status,
        };
    }

    return {
        status: 'error',
    };
}
