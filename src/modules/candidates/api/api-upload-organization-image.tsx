import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IMedia } from 'models/media';
import { AxiosError } from 'axios';
import { IUploadOrganizationImageResponse } from '../responses';

export default async function apiUploadOrganizationImage(
    formData: any,
): Promise<{
    data?: IMedia;
    status: 'error' | 'data';
    message: string;
    errors?: Record<string, any>;
    errorMeta?: AxiosError;
}> {
    const response = await apiRequest<IUploadOrganizationImageResponse>(
        'post',
        endpoints.frontend.upload_organization_image,
        formData,
    );

    if (response.status === 'data') {
        return {
            data: response.data.media,
            status: 'data',
            message: response.message,
        };
    }
    return {
        status: 'error',
        message: response.message,
        errors: response.errors,
        errorMeta: response.errorMeta,
    };
}
