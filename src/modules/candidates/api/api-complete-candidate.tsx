import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { AxiosError } from 'axios';

export default async function apiCompleteCandidate(
    id,
    data,
): Promise<{
    data?;
    status: 'error' | 'data';
    message: string;
    errors?: Record<string, any>;
    errorMeta?: AxiosError;
}> {
    const response = await apiRequest('post', endpoints.frontend.candidate_complete.replace('{id}', id), data);

    if (response.status === 'data') {
        return {
            data: response.data,
            status: 'data',
            message: response.message,
        };
    }
    return {
        status: 'error',
        message: response.message,
        errors: response.errors,
        errorMeta: response.errorMeta,
    };
}
