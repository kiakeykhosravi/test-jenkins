import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { ICandidate } from 'models/candidate';
import { ICandidateResponse } from '../responses';

export default async function apiShowCandidate(id): Promise<{ data?: ICandidate; status: 'error' | 'data' }> {
    const response = await apiRequest<ICandidateResponse>('get', endpoints.backend.candidate_show.replace('{id}', id));

    if (response.status === 'data') {
        return {
            data: response.data.candidate,
            status: 'data',
        };
    }
    return {
        status: 'error',
    };
}
