import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IBrandpagePayRetryResponse } from '../responses';

export default async function apiBrandpagePayRetry(data: {
    secret: string;
}): Promise<{
    data?: string;
    status: 'error' | 'data';
    message: string;
}> {
    const url = endpoints.frontend.retry_brandpage.replace('{secret}', data.secret);

    const response = await apiRequest<IBrandpagePayRetryResponse>('post', url);

    if (response.status === 'data') {
        return {
            data: response.data.link,
            status: 'data',
            message: response.message,
        };
    }
    return {
        status: 'error',
        message: response.message,
    };
}
