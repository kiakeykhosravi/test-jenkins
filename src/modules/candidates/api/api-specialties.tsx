import { ISpecialtiesResponse } from '../responses';
import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { ISpecialty } from 'models/specialty';
import { toast } from 'react-toastify';

export default async function apiSpecialties(): Promise<{ data?: ISpecialty[]; status: 'data' | 'error' }> {
    const response = await apiRequest<ISpecialtiesResponse>('get', endpoints.frontend.specialties);

    if (response.status === 'data') {
        return {
            data: response.data.specialties,
            status: 'data',
        };
    }

    toast.error(response.message);

    return {
        status: 'error',
    };
}
