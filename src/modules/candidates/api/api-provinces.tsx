import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { ICitiesResponse } from '../responses';
import { IProvince } from 'models/province';

export default async function apiProvinces(): Promise<{ data?: IProvince[]; status: 'error' | 'data' }> {
    const response = await apiRequest<ICitiesResponse>('get', endpoints.backend.provinces);

    if (response.status === 'data') {
        return {
            data: response.data.provinces,
            status: 'data',
        };
    }
    return {
        status: 'error',
    };
}
