import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IRegisterCandidateResponse } from '../responses';

export default async function apiRegisterCandidate(
    data,
): Promise<{
    data?: string;
    status: 'error' | 'data';
    message: string;
}> {
    const response = await apiRequest<IRegisterCandidateResponse>('post', endpoints.frontend.register_candidate, data);

    if (response.status === 'data') {
        return {
            data: response.data.link,
            status: 'data',
            message: response.message,
        };
    }
    return {
        status: 'error',
        message: response.message,
    };
}
