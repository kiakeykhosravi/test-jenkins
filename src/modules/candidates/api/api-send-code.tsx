import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { ISendAndVerifyCodeResponse } from '../responses';

export default async function apiSendCode(data: {
    mobile: string;
}): Promise<{
    data?: string;
    status: 'error' | 'data';
    message: string;
}> {
    const response = await apiRequest<ISendAndVerifyCodeResponse>('post', endpoints.frontend.send_code, data);

    if (response.status === 'data') {
        return {
            data: response.data.secret,
            status: 'data',
            message: response.message,
        };
    }
    return {
        status: 'error',
        message: response.message,
    };
}
