import React from 'react';
import { Form } from 'react-bootstrap';

export default function UserCount(props: { formRegisterer }) {
    return (
        <Form.Control as="select" name="size" className="form-select" ref={props.formRegisterer()}>
            <option value="1 - 20">۱ - ۲۰ نفر</option>
            <option value="21 - 50">۲۱ - ۵۰ نفر</option>
            <option value="51 - 100">۵۱ - ۱۰۰ نفر</option>
            <option value="101 - 200">۱۰۱ - ۲۰۰ نفر</option>
            <option value="200+">۲۰۰+ نفر</option>
        </Form.Control>
    );
}
