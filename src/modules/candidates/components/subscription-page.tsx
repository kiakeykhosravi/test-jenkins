import React, { useState } from 'react';
import classNames from 'classnames';
import { useForm } from 'react-hook-form';
import toEnglishNum from 'utils/to-english-num';
import { toast } from 'react-toastify';
import styles from '../styles/candidates.module.scss';
import { Button, Container, Form } from 'react-bootstrap';
import Link from 'next/link';
import apiRegisterCandidate from '../api/api-register-candidate';
import { CandidateTypeEnum } from 'models/candidate';
import Head from 'next/head';

export default function SubscriptionPage() {
    const cn = classNames;
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [succeed, setSucceed] = useState(false);
    const [succeedMessage, setSucceedMessage] = useState('با موفقیت انجام شد!');
    const { handleSubmit, errors, register } = useForm();

    const onSubmitForm = async (data) => {
        setIsSubmitting(true);
        const formData = {
            owner_name: data.owner_name,
            mobile_number: [toEnglishNum(data.whatsapp)],
            email: data.email,
            custom_specialty: data.custom_specialty,
            type: CandidateTypeEnum.SUBSCRIPTION,
        };
        const response = await apiRegisterCandidate(formData);
        if (response.status === 'data') {
            setIsSubmitting(false);
            setSucceed(true);
            setSucceedMessage(response.message);
        } else {
            setIsSubmitting(false);
            toast.warning(response.message);
        }
    };

    const headTag = () => {
        return (
            <Head>
                <title>فرم مشاوره</title>
                <meta name="robots" content="index, follow" />
                <link rel="canonical" href="https://www.asanseminar.ir" />
                <meta name="description" content="فرم مشاوره آسان سمینار" />
                <meta property="og:url" content="https://asanseminar.ir" />
                <meta property="og:title" content="فرم مشاوره" />
                <meta property="og:description" content="فرم مشاوره آسان سمینار" />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="فرم مشاوره" />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
            </Head>
        );
    };

    if (succeed)
        return (
            <>
                {headTag()}
                <div className={styles.body}>
                    <Container className={styles.container}>
                        <div className={cn(styles.main, 'col-12 col-lg-10 col-xl-9')}>
                            <h3 className={'text-center text-white mb-5'}>{succeedMessage}</h3>
                            <Link href={'/'}>
                                <a className={cn(styles.link, 'btn btn-primary')} title={'صفحه اصلی'}>
                                    صفحه اصلی
                                </a>
                            </Link>
                            <a
                                className={cn(styles.link, 'btn btn-secondary')}
                                title={'صفحه فرم مشاوره'}
                                onClick={() => setSucceed(false)}
                            >
                                ثبت یک درخواست دیگر
                            </a>
                        </div>
                    </Container>
                </div>
            </>
        );
    return (
        <>
            {headTag()}
            <div className={styles.body}>
                <Container className={styles.container}>
                    <div className={cn(styles.main, 'col-12 col-lg-10 col-xl-9')}>
                        <Link href="/">
                            <a title="آسان سمینار" className={styles['asanseminar-logo-link']}>
                                <img
                                    src="/images/asanseminar_full.png"
                                    alt="آسان سمینار"
                                    className={styles['asanseminar-logo']}
                                />
                            </a>
                        </Link>
                        <h1 className={styles['title-bar']}>فرم مشاوره</h1>
                        <div className={styles.box}>
                            <Form onSubmit={handleSubmit(onSubmitForm)}>
                                <Form.Group controlId="ownerName" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        نام و نام خانوادگی *
                                    </Form.Label>
                                    <div className="col-12 col-md-7 col-lg-8">
                                        <Form.Control
                                            className={errors.owner_name && styles['field-error']}
                                            name="owner_name"
                                            ref={register({
                                                required: 'نام و نام خانوادگی الزامی می باشد',
                                                minLength: {
                                                    value: 3,
                                                    message: 'نام و نام خانوادگی باید حداقل سه حرف داشته باشد',
                                                },
                                            })}
                                        />
                                    </div>
                                    {errors.owner_name && (
                                        <div className={cn(styles['error-message'], 'col-12 col-md-8 offset-md-4')}>
                                            {errors.owner_name.message}
                                        </div>
                                    )}
                                </Form.Group>

                                <Form.Group controlId="whatsapp" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        شماره تماس، همراه یا واتساپ *
                                    </Form.Label>
                                    <div className="col-12 col-md-4">
                                        <Form.Control
                                            className={cn('text-left', { [styles['field-error']]: errors.whatsapp })}
                                            style={{ direction: 'ltr' }}
                                            name="whatsapp"
                                            ref={register({
                                                required: 'شماره همراه یا واتساپ الزامی می باشد',
                                            })}
                                        />
                                    </div>
                                    {errors.whatsapp && (
                                        <div className={cn(styles['error-message'], 'col-12 col-md-8 offset-md-4')}>
                                            {errors.whatsapp.message}
                                        </div>
                                    )}
                                </Form.Group>

                                <Form.Group controlId="email" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        ایمیل
                                    </Form.Label>
                                    <div className="col-12 col-md-4">
                                        <Form.Control
                                            type={'email'}
                                            className={cn('text-left', { [styles['field-error']]: errors.email })}
                                            name="email"
                                            ref={register()}
                                        />
                                    </div>
                                </Form.Group>

                                <Form.Group controlId="custom_specialty" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        زمینه کاری
                                    </Form.Label>
                                    <div className="col-12 col-md-4">
                                        <Form.Control
                                            className={errors.specialty && styles['field-error']}
                                            name="custom_specialty"
                                            ref={register()}
                                        />
                                    </div>
                                </Form.Group>

                                <div className="row">
                                    <div className="col-12 col-md-4" />
                                    <div className="col-12 col-md-8" />
                                </div>

                                <div style={{ textAlign: 'center' }}>
                                    <Button
                                        type="submit"
                                        className="btn btn-dark"
                                        style={{
                                            width: '100%',
                                            marginTop: '1rem',
                                            padding: '0.5rem 0',
                                            fontSize: '1.1rem',
                                        }}
                                        {...(isSubmitting ? { disabled: true } : {})}
                                    >
                                        {isSubmitting ? 'در حال ارسال ...' : 'ارسال فرم مشاوره'}
                                    </Button>
                                </div>
                            </Form>
                        </div>
                    </div>
                </Container>
            </div>
        </>
    );
}
