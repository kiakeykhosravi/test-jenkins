import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import classNames from 'classnames';
import { useForm } from 'react-hook-form';
import { Button, Container, Form } from 'react-bootstrap';
import styles from '../styles/candidates.module.scss';
import apiUploadOrganizationImage from '../api/api-upload-organization-image';
import { toast } from 'react-toastify';
import apiCompleteCandidate from '../api/api-complete-candidate';
import Custom404 from 'pages/404';
import CreatableSelect from 'react-select/creatable';
import makeAnimated from 'react-select/animated';
import { ICandidate } from 'models/candidate';
import Head from 'next/head';
import AsanUpload from 'modules/shared/components/asan-upload';
import { ISpecialty } from 'models/specialty';
import toEnglishNum from 'utils/to-english-num';
import { Plus, X } from 'react-bootstrap-icons';
import toPersianNum from 'utils/to-persian-num';

const animatedComponents = makeAnimated();

export default function FinalBrandpageCandidates(props: {
    specialties: ISpecialty[];
    candidate: ICandidate;
    paramsID;
}) {
    const cn = classNames;
    const { register, errors, handleSubmit } = useForm();
    const [phoneFields, setPhoneFields] = useState(
        (props.candidate.phone_number && props.candidate.phone_number.slice(1)) || [],
    );
    const [specialty, setSpecialty] = useState<{ id: number; title: string; isNew: boolean }>({
        id: 0,
        title: '',
        isNew: false,
    });
    const allSpecialties = props.specialties.filter((s) => s.active);
    const allActiveSpecialties: { value: number; label: string }[] = allSpecialties.map((s) => {
        return { value: s.id, label: s.title };
    });
    const [isSubmitting, setIsSubmitting] = useState(false);

    const handleFieldsChange = (i, e, type) => {
        if (type === 'phone') {
            const newFormValues = phoneFields.map((p, index) => (i === index ? e.target.value : p));
            setPhoneFields(newFormValues);
        }
    };

    const addFormFields = (type) => {
        if (type === 'phone') {
            setPhoneFields([...phoneFields, '']);
        }
    };

    const removeFormFields = (i, type) => {
        if (type === 'phone') {
            const newFormValues = phoneFields.filter((p, index) => i !== index);
            setPhoneFields(newFormValues);
        }
    };

    const handleSpecialtyChange = (newValue) => {
        if (newValue) {
            if (newValue.__isNew__) {
                setSpecialty({
                    id: 0,
                    title: newValue.label,
                    isNew: true,
                });
            } else {
                setSpecialty({
                    id: newValue.value,
                    title: newValue.label,
                    isNew: false,
                });
            }
        } else {
            setSpecialty({
                id: 0,
                title: '',
                isNew: false,
            });
        }
    };

    const renderContactField = (name, label, placeholder, controlContainerClasses, controlClasses?, direction?) => {
        return (
            <Form.Group controlId={name} className={cn('row', styles['field-row'])}>
                <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>{label}</Form.Label>
                <div className={controlContainerClasses}>
                    <Form.Control
                        name={name}
                        placeholder={placeholder}
                        className={controlClasses || ''}
                        dir={direction || 'rtl'}
                        ref={register()}
                        defaultValue={props.candidate[name]}
                    />
                </div>
                <div className="col-12 col-md-8 offset-md-4" />
            </Form.Group>
        );
    };

    const onSubmitForm = async (data) => {
        if (isSubmitting) {
            return;
        }
        setIsSubmitting(true);

        const phone_number = [toEnglishNum(data.firstPhone)];
        phoneFields.map((p) => {
            if (p !== '') {
                phone_number.push(toEnglishNum(p));
            }
        });

        const formData = {
            slogan: data.slogan,
            phone_number,
            whatsapp: toEnglishNum(data.whatsapp),
            specialty_id: !specialty.isNew && specialty.id !== 0 ? specialty.id : null,
            custom_specialty: specialty.isNew ? specialty.title : null,
            linkedin: data.linkedin,
            whatsapp_business: data.whatsapp_business,
            description: data.description,
            logo_id: null,
            instagram: data.instagram,
            aparat: data.aparat,
            website: data.website,
        };

        if (data.logo.length) {
            if (data.logo[0].size <= 20971520) {
                const fileFormData = new FormData();
                fileFormData.append('fileupload', data.logo[0]);
                const upload_result = await apiUploadOrganizationImage(fileFormData);
                if (upload_result.status === 'data') {
                    formData.logo_id = upload_result.data.id;
                } else {
                    setIsSubmitting(false);
                    toast.error('آپلود لوگو با مشکل مواجه شد!');
                    return;
                }
            } else {
                setIsSubmitting(false);
                toast.error('حجم فایل نباید بزرگتر از ۲۰ مگابایت باشد!');
                return;
            }
        }
        const response = await apiCompleteCandidate(props.paramsID, formData);
        if (response.status === 'data') {
            setIsSubmitting(false);
            toast.success(response.message);
        } else {
            setIsSubmitting(false);
            toast.error(response.message);
        }
    };

    if (props.candidate === null) {
        return <Custom404 />;
    } else {
        return (
            <>
                <Head>
                    <title>تکمیل ثبت نام برندپیج</title>
                    <meta name="robots" content="index, follow" />
                    <meta name="description" content="تکمیل ثبت نام برندپیج" />
                    <meta property="og:url" content="https://asanseminar.ir" />
                    <meta property="og:title" content="تکمیل ثبت نام برندپیج" />
                    <meta property="og:description" content="تکمیل ثبت نام برندپیج" />
                    <meta property="og:type" content="website" />
                    <meta property="og:image" content="/images/logos/brandpage-logo.jpg" />
                    <meta property="og:image:type" content="image/jpg" />
                    <meta property="og:image:alt" content="لوگو برندپیج" />
                    <meta property="og:site_name" content="آسان سمینار" />
                    <meta property="og:locale" content="fa_IR" />
                    <meta name="twitter:card" content="summary_large_image" />
                    <meta name="twitter:title" content="تکمیل ثبت نام برندپیج" />
                    <meta name="twitter:image:alt" content="لوگو برندپیج" />
                </Head>
                <div className={styles.body}>
                    <Container>
                        <div className="row justify-content-center">
                            <div className="col-12 col-lg-10 col-xl-9 mb-3 mt-3">
                                <Link href="/">
                                    <a title="برندپیج" className={styles['asanseminar-logo-link']}>
                                        <img
                                            src="/images/logos/brandpage-logo.jpg"
                                            alt="آسان سمینار"
                                            className={styles['asanseminar-logo']}
                                            style={{ borderRadius: '50%' }}
                                        />
                                    </a>
                                </Link>
                                <h1 className={styles['title-bar']}>تکمیل ثبت نام برندپیج</h1>
                                <div className={styles.box}>
                                    <Form onSubmit={handleSubmit(onSubmitForm)}>
                                        <Form.Group className={cn('row', styles['field-row'])}>
                                            <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                نام و نام خانوادگی
                                            </Form.Label>
                                            <div className="col-11 col-md-7 col-lg-7">
                                                <div className="value">{props.candidate.owner_name}</div>
                                            </div>
                                        </Form.Group>
                                        <Form.Group className={cn('row', styles['field-row'])}>
                                            <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                عنوان برند
                                            </Form.Label>
                                            <div className="col-11 col-md-7 col-lg-7">
                                                <div className="value">{props.candidate.name}</div>
                                            </div>
                                        </Form.Group>
                                        <Form.Group controlId="slogan" className={cn('row', styles['field-row'])}>
                                            <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                شعار برند
                                            </Form.Label>
                                            <div className="col-11 col-md-7 col-lg-7">
                                                <Form.Control
                                                    type="text"
                                                    className={errors.slogan && styles['field-error']}
                                                    name="slogan"
                                                    ref={register()}
                                                    defaultValue={props.candidate.slogan}
                                                />
                                            </div>
                                            {errors.slogan && (
                                                <div
                                                    className={cn(
                                                        styles['error-message'],
                                                        'col-12 col-md-8 offset-md-4',
                                                    )}
                                                >
                                                    {errors.slogan.message}
                                                </div>
                                            )}
                                        </Form.Group>

                                        <Form.Group controlId="firstPhone" className={cn('row', styles['field-row'])}>
                                            <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                شماره تماس ثابت
                                            </Form.Label>
                                            <div className="col-11 col-md-7 col-lg-7">
                                                <Form.Control
                                                    className={cn('text-left', {
                                                        [styles['field-error']]: errors.firstPhone,
                                                    })}
                                                    style={{ direction: 'ltr' }}
                                                    name="firstPhone"
                                                    ref={register()}
                                                    defaultValue={
                                                        props.candidate.phone_number &&
                                                        props.candidate.phone_number.length &&
                                                        props.candidate.phone_number[0]
                                                    }
                                                />
                                            </div>
                                            {!phoneFields.length ? (
                                                <div className={cn(styles['field-action-btn-col'], 'col-1')}>
                                                    <Plus
                                                        className={cn(styles['field-action-btn'], styles['add-btn'])}
                                                        onClick={() => addFormFields('phone')}
                                                    />
                                                </div>
                                            ) : null}
                                            {errors.firstPhone && (
                                                <div
                                                    className={cn(
                                                        styles['error-message'],
                                                        'col-12 col-md-8 offset-md-4',
                                                    )}
                                                >
                                                    {errors.firstPhone.message}
                                                </div>
                                            )}
                                        </Form.Group>

                                        {phoneFields.map((element, index) => (
                                            <Form.Group
                                                controlId={`phone-${index + 2}`}
                                                className={cn('row', styles['field-row'])}
                                                key={index}
                                            >
                                                <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                    {`شماره ثابت ${toPersianNum(index + 2)}`}
                                                </Form.Label>
                                                <div className="col-11 col-md-7 col-lg-7 position-relative">
                                                    <Form.Control
                                                        className={cn(styles['more-field-input'])}
                                                        style={{ direction: 'ltr' }}
                                                        name={`phone-${index + 2}`}
                                                        value={phoneFields[index]}
                                                        onChange={(e) => handleFieldsChange(index, e, 'phone')}
                                                    />
                                                    <div
                                                        className={cn(
                                                            styles['field-action-btn-col'],
                                                            styles['remove-btn-col'],
                                                        )}
                                                    >
                                                        <X
                                                            className={cn(
                                                                styles['field-action-btn'],
                                                                styles['remove-btn'],
                                                            )}
                                                            onClick={() => removeFormFields(index, 'phone')}
                                                        />
                                                    </div>
                                                </div>

                                                {phoneFields.length === index + 1 && phoneFields.length < 3 ? (
                                                    <div className={cn(styles['field-action-btn-col'], 'col-1')}>
                                                        <Plus
                                                            className={cn(
                                                                styles['field-action-btn'],
                                                                styles['add-btn'],
                                                            )}
                                                            onClick={() => addFormFields('phone')}
                                                        />
                                                    </div>
                                                ) : null}
                                            </Form.Group>
                                        ))}

                                        <Form.Group controlId="whatsapp" className={cn('row', styles['field-row'])}>
                                            <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                شماره واتساپ
                                            </Form.Label>
                                            <div className="col-11 col-md-7 col-lg-7">
                                                <Form.Control
                                                    className={cn('text-left', {
                                                        [styles['field-error']]: errors.whatsapp,
                                                    })}
                                                    style={{ direction: 'ltr' }}
                                                    name="whatsapp"
                                                    ref={register()}
                                                    defaultValue={props.candidate.whatsapp}
                                                />
                                            </div>
                                            {errors.whatsapp && (
                                                <div
                                                    className={cn(
                                                        styles['error-message'],
                                                        'col-12 col-md-8 offset-md-4',
                                                    )}
                                                >
                                                    {errors.whatsapp.message}
                                                </div>
                                            )}
                                        </Form.Group>

                                        <Form.Group controlId="logo" className={cn('row', styles['field-row'])}>
                                            <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                لوگو
                                            </Form.Label>
                                            <div className="col-11 col-md-7 col-lg-7">
                                                <AsanUpload name="logo" iconType="image" register={register} />
                                            </div>
                                        </Form.Group>

                                        <Form.Group controlId="specialty" className={cn('row', styles['field-row'])}>
                                            <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                زمینه کاری / تخصص / رشته
                                            </Form.Label>
                                            <div className="col-11 col-md-7 col-lg-7">
                                                <CreatableSelect
                                                    isClearable
                                                    onChange={handleSpecialtyChange}
                                                    options={allActiveSpecialties}
                                                    closeMenuOnSelect={false}
                                                    components={animatedComponents}
                                                    blurInputOnSelect={true}
                                                    defaultValue={null}
                                                    isMulti={false}
                                                    isRtl
                                                    className="w-remain"
                                                    placeholder=""
                                                    noOptionsMessage={() => 'برای افزودن گزینه تایپ کنید'}
                                                    formatCreateLabel={(inputValue) => `ایجاد ' ${inputValue} '`}
                                                />
                                            </div>
                                        </Form.Group>

                                        {renderContactField(
                                            'instagram',
                                            'صفحه اینستاگرام',
                                            'نام صفحه',
                                            'col-11 col-md-7 col-lg-7',
                                            'text-left',
                                            'ltr',
                                        )}

                                        {renderContactField(
                                            'whatsapp_business',
                                            'آدرس واتساپ بیزنس',
                                            'https://wa.me/****',
                                            'col-11 col-md-7 col-lg-7',
                                            'text-left',
                                            'ltr',
                                        )}

                                        {renderContactField(
                                            'aparat',
                                            'آدرس آپارات',
                                            'نام کانال',
                                            'col-11 col-md-7 col-lg-7',
                                            'text-left',
                                            'ltr',
                                        )}

                                        {renderContactField(
                                            'website',
                                            'آدرس وبسایت شما',
                                            'آدرس کامل وبسایت',
                                            'col-11 col-md-7 col-lg-7',
                                            'text-left',
                                            'ltr',
                                        )}

                                        {renderContactField(
                                            'linkedin',
                                            'صفحه لینکدین',
                                            'آدرس کامل صفحه لینکدین',
                                            'col-11 col-md-7 col-lg-7',
                                            'text-left',
                                            'ltr',
                                        )}

                                        <Form.Group controlId="description" className={cn('row', styles['field-row'])}>
                                            <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                توضیحات
                                            </Form.Label>
                                            <div className="col-11 col-md-7 col-lg-7">
                                                <Form.Control
                                                    as="textarea"
                                                    className="form-control"
                                                    name="description"
                                                    rows={3}
                                                    ref={register()}
                                                />
                                            </div>
                                        </Form.Group>

                                        <Button
                                            type="submit"
                                            variant="primary"
                                            className={styles['submit-button']}
                                            {...(isSubmitting ? { disabled: true } : {})}
                                        >
                                            {isSubmitting ? 'در حال ارسال ...' : 'ارسال فرم'}
                                        </Button>
                                    </Form>
                                </div>
                            </div>
                        </div>
                    </Container>
                </div>
            </>
        );
    }
}
