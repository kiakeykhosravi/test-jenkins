import React from 'react';
import { Form } from 'react-bootstrap';
import { IProvince } from 'models/province';

export default function Cities(props: { activeProvince: IProvince; formRegisterer }) {
    const renderCities = () => {
        if (props.activeProvince && props.activeProvince.cities.length) {
            return props.activeProvince.cities.map((city) => {
                return (
                    <option value={city.id} key={city.id}>
                        {city.name}
                    </option>
                );
            });
        } else return null;
    };

    return (
        <Form.Control as="select" name="city_id" className="form-select" ref={props.formRegisterer()}>
            {renderCities()}
        </Form.Control>
    );
}
