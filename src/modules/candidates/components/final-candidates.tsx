import React, { useState } from 'react';
import classNames from 'classnames';
import { useForm } from 'react-hook-form';
import { Alert, Button, Container, Form } from 'react-bootstrap';
import styles from '../styles/candidates.module.scss';
import apiUploadOrganizationImage from '../api/api-upload-organization-image';
import { toast } from 'react-toastify';
import apiCompleteCandidate from '../api/api-complete-candidate';
import Custom404 from 'pages/404';
import Link from 'next/link';
import { ICandidate } from 'models/candidate';
import Head from 'next/head';
import AsanUpload from 'modules/shared/components/asan-upload';

export default function FinalCandidates(props: { candidate: ICandidate; paramsID }) {
    const cn = classNames;
    const { register, errors, handleSubmit } = useForm();
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [logo, setLogo] = useState(undefined);
    const [uploaded, setUploaded] = useState(false);
    const [formCandidateData, setFormCandidateData] = useState<any>(props.candidate);

    const renderContactField = (name, label, placeholder, controlContainerClasses, controlClasses?, direction?) => {
        return (
            <Form.Group controlId={name} className={cn('row', styles['field-row'])}>
                <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>{label}</Form.Label>
                <div className={controlContainerClasses}>
                    <Form.Control
                        name={name}
                        placeholder={placeholder}
                        className={controlClasses || ''}
                        dir={direction || 'rtl'}
                        ref={register()}
                        value={formCandidateData[name] || ''}
                        onChange={(e) => {
                            setFormCandidateData((prevState) => ({
                                ...prevState,
                                [name]: e.target.value,
                            }));
                        }}
                    />
                </div>
                <div className="col-12 col-md-8 offset-md-4" />
            </Form.Group>
        );
    };

    const onLogoChange = () => setUploaded(false);

    const uploadHandler = async (file) => {
        const formData = new FormData();
        formData.append('fileupload', file);
        const response = await apiUploadOrganizationImage(formData);
        if (response.status === 'data') {
            setUploaded(true);
            setLogo(response.data.id);
            return response.data.id;
        } else {
            setUploaded(false);
            toast.warning('مشکلی در آپلود به وجود آمد!');
            return null;
        }
    };

    const onSubmitForm = async (data) => {
        setIsSubmitting(true);
        const formData = {
            email: data.email,
            address: data.address,
            postal_code: data.postal_code,
            registration_number: data.registration_number,
            logo_id: undefined,
            instagram: data.instagram,
            aparat: data.aparat,
            whatsapp: data.whatsapp,
            website: data.website,
            linkedin: data.linkedin,
        };
        if (!uploaded && data.logo.length) {
            if (data.logo[0].size <= 20971520) {
                const logoID = await uploadHandler(data.logo[0]);
                if (logoID !== null) {
                    formData.logo_id = logoID;
                    sendForm(formData);
                } else {
                    setIsSubmitting(false);
                    toast.warning('آپلود فایل با مشکل مواجه شد!');
                }
            } else {
                setIsSubmitting(false);
                toast.warning('حجم فایل نباید بزرگتر از ۲۰ مگابایت باشد!');
            }
        } else {
            if (!data.logo.length) {
                setUploaded(false);
                formData.logo_id = undefined;
            } else formData.logo_id = logo;
            sendForm(formData);
        }
    };

    const sendForm = async (formData) => {
        const response = await apiCompleteCandidate(props.paramsID, formData);
        if (response.status === 'data') {
            setIsSubmitting(false);
            toast.success(response.message);
        } else {
            setIsSubmitting(false);
            toast.warning(response.message);
        }
    };

    if (props.candidate === null) return <Custom404 />;
    else
        return (
            <>
                <Head>
                    <title>تکمیل پیش ثبت نام</title>
                    <meta name="robots" content="index, follow" />
                    <link rel="canonical" href="https://www.asanseminar.ir" />
                    <meta name="description" content="تکمیل پیش ثبت نام آسان سمینار" />
                    <meta property="og:url" content="https://asanseminar.ir" />
                    <meta property="og:title" content="تکمیل پیش ثبت نام" />
                    <meta property="og:description" content="تکمیل پیش ثبت نام آسان سمینار" />
                    <meta property="og:type" content="website" />
                    <meta property="og:image" content="/images/asanseminar_logo.png" />
                    <meta property="og:image:type" content="image/png" />
                    <meta property="og:image:alt" content="لوگو آسان سمینار" />
                    <meta property="og:site_name" content="آسان سمینار" />
                    <meta property="og:locale" content="fa_IR" />
                    <meta name="twitter:card" content="summary_large_image" />
                    <meta name="twitter:title" content="تکمیل پیش ثبت نام" />
                    <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
                </Head>
                <div className={styles.body}>
                    <Container>
                        <div className="row justify-content-center">
                            <div className="col-12 col-lg-10 col-xl-9">
                                <Link href="/">
                                    <a title="آسان سمینار" className={styles['asanseminar-logo-link']}>
                                        <img
                                            src="/images/asanseminar_full.png"
                                            alt="آسان سمینار"
                                            className={styles['asanseminar-logo']}
                                        />
                                    </a>
                                </Link>
                                <h1 className={styles['title-bar']}>تکمیل پیش ثبت نام</h1>
                                <div className={styles.box}>
                                    <Alert variant="success">
                                        <div className={styles['error-message']}>
                                            راهنمایی : لطفا ابتدا به آموزش صوتی این صفحه گوش دهید
                                        </div>
                                        <audio
                                            controls={true}
                                            style={{ width: '100%', marginTop: '1rem' }}
                                            src="/audios/candidates_help.mp3"
                                        />
                                    </Alert>
                                    <Form onSubmit={handleSubmit(onSubmitForm)}>
                                        <Form.Group className={cn('row', styles['field-row'])}>
                                            <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                نام سازمان یا آموزشگاه
                                            </Form.Label>
                                            <div className="col-12 col-md-6 col-lg-7">
                                                <div className="value">{props.candidate.name}</div>
                                            </div>
                                        </Form.Group>
                                        <Form.Group className={cn('row', styles['field-row'])}>
                                            <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                نام و نام خانوادگی
                                            </Form.Label>
                                            <div className="col-12 col-md-5 col-lg-6">
                                                <div className="value">{props.candidate.owner_name}</div>
                                            </div>
                                        </Form.Group>
                                        <Form.Group controlId="email" className={cn('row', styles['field-row'])}>
                                            <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                ایمیل
                                            </Form.Label>
                                            <div className="col-12 col-md-4 col-lg-6">
                                                <Form.Control
                                                    name="email"
                                                    className="text-left"
                                                    ref={register({
                                                        required: 'آدرس ایمیل الزامی می باشد',
                                                    })}
                                                    value={formCandidateData.email || ''}
                                                    onChange={(e) => {
                                                        setFormCandidateData((prevState) => ({
                                                            ...prevState,
                                                            email: e.target.value,
                                                        }));
                                                    }}
                                                />
                                            </div>
                                            {errors.email && (
                                                <div
                                                    className={cn(
                                                        styles['error-message'],
                                                        'col-12 col-md-8 offset-md-4',
                                                    )}
                                                >
                                                    {errors.email.message}
                                                </div>
                                            )}
                                        </Form.Group>
                                        <Form.Group controlId="address" className={cn('row', styles['field-row'])}>
                                            <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                نشانی
                                            </Form.Label>
                                            <div className="col-12 col-md-5 col-lg-6">
                                                <Form.Control
                                                    as="textarea"
                                                    name="address"
                                                    rows={2}
                                                    ref={register({
                                                        required: 'نشانی الزامی می باشد',
                                                    })}
                                                    value={formCandidateData.address || ''}
                                                    onChange={(e) => {
                                                        setFormCandidateData((prevState) => ({
                                                            ...prevState,
                                                            address: e.target.value,
                                                        }));
                                                    }}
                                                />
                                            </div>
                                            {errors.address && (
                                                <div
                                                    className={cn(
                                                        styles['error-message'],
                                                        'col-12 col-md-8 offset-md-4',
                                                    )}
                                                >
                                                    {errors.address.message}
                                                </div>
                                            )}
                                        </Form.Group>
                                        <Form.Group controlId="postal_code" className={cn('row', styles['field-row'])}>
                                            <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                کد پستی
                                            </Form.Label>
                                            <div className="col-12 col-md-3 col-lg-6">
                                                <Form.Control
                                                    name="postal_code"
                                                    className="text-left"
                                                    ref={register({
                                                        required: 'کد پستی الزامی می باشد',
                                                    })}
                                                    value={formCandidateData.postal_code || ''}
                                                    onChange={(e) => {
                                                        setFormCandidateData((prevState) => ({
                                                            ...prevState,
                                                            postal_code: e.target.value,
                                                        }));
                                                    }}
                                                />
                                            </div>
                                            {errors.postal_code && (
                                                <div
                                                    className={cn(
                                                        styles['error-message'],
                                                        'col-12 col-md-8 offset-md-4',
                                                    )}
                                                >
                                                    {errors.postal_code.message}
                                                </div>
                                            )}
                                        </Form.Group>
                                        <Form.Group
                                            controlId="registration_number"
                                            className={cn('row', styles['field-row'])}
                                        >
                                            <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                شماره ثبت
                                            </Form.Label>
                                            <div className="col-12 col-md-5 col-lg-6">
                                                <Form.Control
                                                    name="registration_number"
                                                    ref={register({
                                                        required: 'شماره ثبت الزامی می باشد',
                                                    })}
                                                    value={formCandidateData.registration_number || ''}
                                                    onChange={(e) => {
                                                        setFormCandidateData((prevState) => ({
                                                            ...prevState,
                                                            registration_number: e.target.value,
                                                        }));
                                                    }}
                                                />
                                            </div>
                                            {errors.registration_number && (
                                                <div
                                                    className={cn(
                                                        styles['error-message'],
                                                        'col-12 col-md-8 offset-md-4',
                                                    )}
                                                >
                                                    {errors.registration_number.message}
                                                </div>
                                            )}
                                        </Form.Group>
                                        <Form.Group controlId="logo" className={cn('row', styles['field-row'])}>
                                            <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                لوگو
                                            </Form.Label>
                                            <div className="col-12 col-md-5 col-lg-6">
                                                <AsanUpload
                                                    name="logo"
                                                    iconType="image"
                                                    register={register}
                                                    onChange={onLogoChange}
                                                />
                                            </div>
                                        </Form.Group>
                                        <Alert variant="info">
                                            فایل آپلود شده پردازش می‌شود، به همین دلیل ممکن است حدود ۱۵ ثانیه با تاخیر
                                            آن را در لیست فایل‌های آپلود کرده‌ی خود مشاهده کنید.
                                            <br />
                                            (حداکثر حجم فایل ۲۰ مگابایت)
                                        </Alert>
                                        <hr />
                                        <div className={styles['organization-contacts-title']}>اطلاعات تماس سازمان</div>
                                        <Alert variant="info">
                                            <strong>توضیحات در مورد پر کردن فرم اطلاعات تماس:</strong>
                                            <br />
                                            ۱: فیلد هایی که جلوشون ستاره دارن فقط کافیه نام کاربری رو وارد کنید
                                            <br />
                                            ۲: فیلد وب سایت رو کامل وارد کنید
                                            <br />
                                            <strong style={{ color: 'green' }}>
                                                ۳: مهم!! در فیلد واتساپ لطفاٌ اول کد کشور رو بدون صفر های اولش وارد کنید
                                                بعد شماره مورد نظر مثال: 989101234567
                                            </strong>
                                            <br />
                                            ۴: فیلد ها الزامی نیستند و اگر خالی بذارید مشکلی پیش نمیاد :)
                                            <br />
                                            ۵: این فیلد ها برای نمایش بیشتر سازمان در موتور های جستجو موثر هست پس ازشون
                                            استفاده کنید :)
                                        </Alert>
                                        {renderContactField(
                                            'instagram',
                                            'صفحه اینستاگرام',
                                            'instagram.com/****',
                                            'col-12 col-md-6 col-lg-7',
                                            'text-left',
                                            'ltr',
                                        )}
                                        {/*{renderContactField(
                                        'telegram',
                                        'صفحه تلگرام',
                                        'https://t.me/****',
                                        'col-12 col-md-6 col-lg-7',
                                        'text-left',
                                        'ltr',
                                    )}
                                    {renderContactField(
                                        'twitter',
                                        'صفحه توییتر',
                                        'twitter.com/****',
                                        'col-12 col-md-6 col-lg-7',
                                        'text-left',
                                        'ltr',
                                    )}*/}
                                        {renderContactField(
                                            'aparat',
                                            'کانال آپارات',
                                            'aparat.com/****',
                                            'col-12 col-md-6 col-lg-7',
                                            'text-left',
                                            'ltr',
                                        )}
                                        {/*{renderContactField(
                                        'youtube',
                                        'کانال یوتوب',
                                        'لینک کامل آدرس یوتوب رو قرار بدید!',
                                        'col-12 col-md-6 col-lg-7',
                                    )}*/}
                                        {renderContactField(
                                            'linkedin',
                                            'صفحه لینکدین',
                                            'لینک کامل آدرس لینکدین رو قرار بدید',
                                            'col-12 col-md-7 col-lg-7',
                                            'text-left',
                                            'ltr',
                                        )}
                                        {/*{renderContactField(
                                        'whatsapp',
                                        'آدرس واتس آپ بیزنسس',
                                        'لینک واتس آپ بیزنسس',
                                        'col-12 col-md-6 col-lg-7',
                                        'text-left',
                                        'ltr',
                                    )}*/}
                                        {renderContactField(
                                            'website',
                                            'آدرس وب سایت',
                                            'لینک کامل وب آدرس وب سایت رو قرار بدید!',
                                            'col-12 col-md-6 col-lg-7',
                                        )}
                                        <hr />
                                        <Button
                                            variant="dark"
                                            type="submit"
                                            className={styles['submit-button']}
                                            {...(isSubmitting ? { disabled: true } : {})}
                                        >
                                            {isSubmitting ? 'در حال ارسال ...' : 'ارسال فرم تکمیلی'}
                                        </Button>
                                    </Form>
                                </div>
                            </div>
                        </div>
                    </Container>
                </div>
            </>
        );
}
