import React, { useEffect, useState } from 'react';
import classNames from 'classnames';
import { IProvince } from 'models/province';
import { useForm } from 'react-hook-form';
import toEnglishNum from 'utils/to-english-num';
import apiRegisterCandidate from '../api/api-register-candidate';
import { toast } from 'react-toastify';
import styles from '../styles/candidates.module.scss';
import { Button, Container, Form } from 'react-bootstrap';
import Link from 'next/link';
import Provinces from './provinces';
import Cities from './cities';
import UserCount from './user-count';
import Shifts from './shifts';
import toPersianNum from 'utils/to-persian-num';
import { Plus, X } from 'react-bootstrap-icons';
import { CandidateTypeEnum, CandidateTypeEnToFaEnum } from 'models/candidate';
import { useRouter } from 'next/router';
import Head from 'next/head';

export default function InitialCandidates(props: { provinces: IProvince[]; type: CandidateTypeEnum }) {
    const cn = classNames;
    const [activeProvince, setActiveProvince] = useState<IProvince>(props.provinces[0]);
    const [userCountText, setUserCountText] = useState('تعداد دانش آموزان/کارآموزان');
    const [mobileFields, setMobileFields] = useState([]);
    const [phoneFields, setPhoneFields] = useState([]);
    const [showShifts, setShowShifts] = useState(true);
    const [showSpecialty, setShowSpecialty] = useState(false);
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [succeed, setSucceed] = useState(false);
    const [succeedMessage, setSucceedMessage] = useState('با موفقیت انجام شد!');
    const [referrer, setReferrer] = useState<number | null>(null);
    const { handleSubmit, errors, register } = useForm();
    const router = useRouter();

    useEffect(() => {
        const type = props.type;
        setShowSpecialty(type === CandidateTypeEnum.EDUCATIONAL_INSTITUTION || type === CandidateTypeEnum.OTHER);

        if (type === CandidateTypeEnum.SCHOOL) {
            setUserCountText('تعداد دانش آموزان/کارآموزان');
        } else if (type === CandidateTypeEnum.EDUCATIONAL_INSTITUTION) {
            setUserCountText('تعداد دانش آموختگان');
        } else {
            setUserCountText('تعداد اعضا');
        }

        setShowShifts(type !== CandidateTypeEnum.OTHER);
    }, []);

    useEffect(() => {
        let ref;
        if (typeof router.query.referrer !== 'string' || !parseInt(router.query.referrer as string, 10)) {
            ref = null;
        } else {
            ref = parseInt(router.query.referrer as string);
        }

        setReferrer(ref);
    }, [router.query.referrer]);

    const typeFa = () => {
        return CandidateTypeEnToFaEnum[props.type];
    };

    const handleFieldsChange = (i, e, type) => {
        if (type === 'mobile') {
            const newFormValues = mobileFields.map((m, index) => (i === index ? e.target.value : m));
            setMobileFields(newFormValues);
        } else {
            const newFormValues = phoneFields.map((p, index) => (i === index ? e.target.value : p));
            setPhoneFields(newFormValues);
        }
    };
    const addFormFields = (type) => {
        if (type === 'mobile') {
            setMobileFields([...mobileFields, '']);
        } else {
            setPhoneFields([...phoneFields, '']);
        }
    };

    const removeFormFields = (i, type) => {
        if (type === 'mobile') {
            const newFormValues = mobileFields.filter((m, index) => i !== index);
            setMobileFields(newFormValues);
        } else {
            const newFormValues = phoneFields.filter((p, index) => i !== index);
            setPhoneFields(newFormValues);
        }
    };

    const provinceChangeHandler = (event) => {
        for (let i = 0; i < props.provinces.length; i++) {
            if (props.provinces[i].id == event.target.value) {
                setActiveProvince(props.provinces[i]);
                break;
            }
        }
    };

    const onSubmitForm = async (data) => {
        setIsSubmitting(true);
        const mobile_number = [toEnglishNum(data.firstMobile)];
        mobileFields.map((m) => {
            if (m !== '') mobile_number.push(toEnglishNum(m));
        });
        const phone_number = [toEnglishNum(data.firstPhone)];
        phoneFields.map((p) => {
            if (p !== '') phone_number.push(toEnglishNum(p));
        });
        const formData = {
            name: data.name,
            owner_name: data.owner_name,
            mobile_number,
            phone_number,
            whatsapp: toEnglishNum(data.whatsapp),
            city_id: data.city_id,
            size: data.size,
            class_times: data.class_times,
            type: props.type,
            custom_specialty: data.custom_specialty,
            description: data.description,
            marketer_id: referrer || null,
        };

        const response = await apiRegisterCandidate(formData);
        if (response.status === 'data') {
            setIsSubmitting(false);
            setSucceedMessage(response.message);
            setSucceed(true);
        } else {
            setIsSubmitting(false);
            toast.error(response.message);
        }
    };

    const headTag = () => {
        return (
            <Head>
                <title>{`پیش ثبت نام سامانه برای ${typeFa()}`}</title>
                <meta name="robots" content="index, follow" />
                <link rel="canonical" href="https://www.asanseminar.ir" />
                <meta name="description" content={`پیش ثبت نام سامانه برای ${typeFa()}`} />
                <meta property="og:url" content="https://asanseminar.ir" />
                <meta property="og:title" content={`پیش ثبت نام سامانه برای ${typeFa()}`} />
                <meta property="og:description" content={`پیش ثبت نام سامانه برای ${typeFa()}`} />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content={`پیش ثبت نام سامانه برای ${typeFa()}`} />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
            </Head>
        );
    };

    if (succeed) {
        return (
            <>
                {headTag()}
                <div className={styles.body}>
                    <Container className={styles.container}>
                        <div className={cn(styles['success-box'], 'col-12 col-lg-10 col-xl-9')}>
                            <h3 className={styles['success-box-header']}>{succeedMessage}</h3>
                            <div className="text-center">
                                <Link href="/">
                                    <a className={cn(styles.link, 'btn btn-primary')} title="صفحه اصلی">
                                        صفحه اصلی
                                    </a>
                                </Link>
                                <a
                                    className={cn(styles.link, 'btn btn-primary')}
                                    title="صفحه فرم مشاوره"
                                    onClick={() => setSucceed(false)}
                                >
                                    ثبت یک درخواست دیگر
                                </a>
                            </div>
                        </div>
                    </Container>
                </div>
            </>
        );
    }

    return (
        <>
            {headTag()}
            <div className={styles.body}>
                <Container className={styles.container}>
                    <div className={cn(styles.main, 'col-12 col-lg-10 col-xl-9')}>
                        <Link href="/">
                            <a title="آسان سمینار" className={styles['asanseminar-logo-link']}>
                                <img
                                    src="/images/asanseminar_full.png"
                                    alt="آسان سمینار"
                                    className={styles['asanseminar-logo']}
                                />
                            </a>
                        </Link>
                        <h1 className={styles['title-bar']}>{`پیش ثبت نام سامانه برای ${typeFa()}`}</h1>
                        <div className={styles.box}>
                            <Form onSubmit={handleSubmit(onSubmitForm)}>
                                <Form.Group controlId="name" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        {`نام ${typeFa()} *`}
                                    </Form.Label>
                                    <div className="col-12 col-md-7 col-lg-8">
                                        <Form.Control
                                            type="text"
                                            className={errors.name && styles['field-error']}
                                            name="name"
                                            ref={register({
                                                required: `نام ${typeFa()} الزامی می باشد`,
                                                minLength: {
                                                    value: 3,
                                                    message: `نام ${typeFa()} باید حداقل سه حرف داشته باشد`,
                                                },
                                            })}
                                        />
                                    </div>
                                    {errors.name && (
                                        <div className={cn(styles['error-message'], 'col-12 col-md-8 offset-md-4')}>
                                            {errors.name.message}
                                        </div>
                                    )}
                                </Form.Group>

                                <Form.Group controlId="ownerName" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        نام و نام خانوادگی *
                                    </Form.Label>
                                    <div className="col-12 col-md-7 col-lg-8">
                                        <Form.Control
                                            className={errors.owner_name && styles['field-error']}
                                            name="owner_name"
                                            ref={register({
                                                required: 'نام و نام خانوادگی الزامی می باشد',
                                                minLength: {
                                                    value: 3,
                                                    message: 'نام و نام خانوادگی باید حداقل سه حرف داشته باشد',
                                                },
                                            })}
                                        />
                                    </div>
                                    {errors.owner_name && (
                                        <div className={cn(styles['error-message'], 'col-12 col-md-8 offset-md-4')}>
                                            {errors.owner_name.message}
                                        </div>
                                    )}
                                </Form.Group>

                                <Form.Group controlId="firstMobile" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        شماره همراه *
                                    </Form.Label>
                                    <div className={mobileFields.length ? 'col-12 col-md-4' : 'col-11 col-md-4'}>
                                        <Form.Control
                                            className={cn('text-left', { [styles['field-error']]: errors.firstMobile })}
                                            name="firstMobile"
                                            ref={register({
                                                required: 'شماره همراه الزامی می باشد',
                                                pattern: {
                                                    value: /^((?:98|\+98|0098|0)?9[0-9]{9})|((?:۹۸|\+۹۸|۰۰۹۸|۰)?۹[۰۹۸۷۶۵۴۳۲۱]{9})$/, // 09(0[1-2]|1[0-9]|3[0-9]|2[0-1])-?[0-9]{3}-?[0-9]{4}
                                                    message: 'فرمت شماره موبایل اشتباه است!',
                                                },
                                            })}
                                        />
                                    </div>
                                    {!mobileFields.length ? (
                                        <div className={cn(styles['field-action-btn-col'], 'col-1')}>
                                            <Plus
                                                className={cn(styles['field-action-btn'], styles['add-btn'])}
                                                onClick={() => addFormFields('mobile')}
                                            />
                                        </div>
                                    ) : null}
                                    {errors.firstMobile && (
                                        <div className={cn(styles['error-message'], 'col-12 col-md-8 offset-md-4')}>
                                            {errors.firstMobile.message}
                                        </div>
                                    )}
                                </Form.Group>

                                {mobileFields.map((element, index) => (
                                    <Form.Group
                                        controlId={`mobile-${index + 2}`}
                                        className={cn('row', styles['field-row'])}
                                        key={index}
                                    >
                                        <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                            {`شماره همراه ${toPersianNum(index + 2)}`}
                                        </Form.Label>
                                        <div
                                            className={
                                                mobileFields.length !== index + 1 || index === 2
                                                    ? 'col-12 col-md-4 position-relative'
                                                    : 'col-11 col-md-4 position-relative'
                                            }
                                        >
                                            <Form.Control
                                                className={cn(styles['more-field-input'])}
                                                name={`mobile-${index + 2}`}
                                                value={mobileFields[index]}
                                                onChange={(e) => handleFieldsChange(index, e, 'mobile')}
                                            />
                                            <div
                                                className={cn(styles['field-action-btn-col'], styles['remove-btn-col'])}
                                            >
                                                <X
                                                    className={cn(styles['field-action-btn'], styles['remove-btn'])}
                                                    onClick={() => removeFormFields(index, 'mobile')}
                                                />
                                            </div>
                                        </div>
                                        {mobileFields.length === index + 1 && mobileFields.length < 3 ? (
                                            <div className={cn(styles['field-action-btn-col'], 'col-1')}>
                                                <Plus
                                                    className={cn(styles['field-action-btn'], styles['add-btn'])}
                                                    onClick={() => addFormFields('mobile')}
                                                />
                                            </div>
                                        ) : null}
                                    </Form.Group>
                                ))}

                                <Form.Group controlId="firstPhone" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        شماره تماس ثابت *
                                    </Form.Label>
                                    <div className={phoneFields.length ? 'col-12 col-md-4' : 'col-11 col-md-4'}>
                                        <Form.Control
                                            className={cn('text-left', { [styles['field-error']]: errors.firstPhone })}
                                            name="firstPhone"
                                            ref={register({
                                                required: 'شماره تماس الزامی می باشد',
                                            })}
                                        />
                                    </div>
                                    {!phoneFields.length ? (
                                        <div className={cn(styles['field-action-btn-col'], 'col-1')}>
                                            <Plus
                                                className={cn(styles['field-action-btn'], styles['add-btn'])}
                                                onClick={() => addFormFields('phone')}
                                            />
                                        </div>
                                    ) : null}
                                    {errors.firstPhone && (
                                        <div className={cn(styles['error-message'], 'col-12 col-md-8 offset-md-4')}>
                                            {errors.firstPhone.message}
                                        </div>
                                    )}
                                </Form.Group>

                                {phoneFields.map((element, index) => (
                                    <Form.Group
                                        controlId={`phone-${index + 2}`}
                                        className={cn('row', styles['field-row'])}
                                        key={index}
                                    >
                                        <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                            {`شماره ثابت ${toPersianNum(index + 2)}`}
                                        </Form.Label>
                                        <div
                                            className={
                                                phoneFields.length !== index + 1 || index === 2
                                                    ? 'col-12 col-md-4 position-relative'
                                                    : 'col-11 col-md-4 position-relative'
                                            }
                                        >
                                            <Form.Control
                                                className={cn(styles['more-field-input'])}
                                                name={`phone-${index + 2}`}
                                                value={phoneFields[index]}
                                                onChange={(e) => handleFieldsChange(index, e, 'phone')}
                                            />
                                            <div
                                                className={cn(styles['field-action-btn-col'], styles['remove-btn-col'])}
                                            >
                                                <X
                                                    className={cn(styles['field-action-btn'], styles['remove-btn'])}
                                                    onClick={() => removeFormFields(index, 'phone')}
                                                />
                                            </div>
                                        </div>

                                        {phoneFields.length === index + 1 && phoneFields.length < 3 ? (
                                            <div className={cn(styles['field-action-btn-col'], 'col-1')}>
                                                <Plus
                                                    className={cn(styles['field-action-btn'], styles['add-btn'])}
                                                    onClick={() => addFormFields('phone')}
                                                />
                                            </div>
                                        ) : null}
                                    </Form.Group>
                                ))}

                                <Form.Group controlId="whatsapp" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        شماره واتساپ
                                    </Form.Label>
                                    <div className="col-12 col-md-4">
                                        <Form.Control
                                            className={cn('text-left', { [styles['field-error']]: errors.whatsapp })}
                                            name="whatsapp"
                                            ref={register()}
                                        />
                                    </div>
                                    {errors.whatsapp && (
                                        <div className={cn(styles['error-message'], 'col-12 col-md-8 offset-md-4')}>
                                            {errors.whatsapp.message}
                                        </div>
                                    )}
                                </Form.Group>

                                <Form.Group controlId="province_id" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        استان *
                                    </Form.Label>
                                    <div className="col-12 col-md-4">
                                        <Provinces
                                            provinces={props.provinces}
                                            changeHandler={provinceChangeHandler}
                                            formRegisterer={register}
                                        />
                                    </div>
                                </Form.Group>

                                <Form.Group controlId="city_id" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        شهر *
                                    </Form.Label>
                                    <div className="col-12 col-md-4">
                                        <Cities activeProvince={activeProvince} formRegisterer={register} />
                                    </div>
                                </Form.Group>

                                <Form.Group controlId="size">
                                    <div className={cn('row', styles['field-row'])}>
                                        <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                            {userCountText} *
                                        </Form.Label>
                                        <div className="col-12 col-md-4">
                                            <UserCount formRegisterer={register} />
                                        </div>
                                    </div>
                                </Form.Group>

                                {showShifts ? (
                                    <Form.Group controlId="class_times">
                                        <div className={cn('row', styles['field-row'])}>
                                            <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                شیفت کلاس‌ها *
                                            </Form.Label>
                                            <div className="col-12 col-md-4">
                                                <Shifts formRegisterer={register} />
                                            </div>
                                        </div>
                                    </Form.Group>
                                ) : null}

                                {showSpecialty ? (
                                    <Form.Group controlId="custom_specialty" className={cn('row', styles['field-row'])}>
                                        <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                            تخصص
                                        </Form.Label>
                                        <div className="col-12 col-md-4">
                                            <Form.Control name="custom_specialty" ref={register()} />
                                        </div>
                                    </Form.Group>
                                ) : null}

                                <Form.Group controlId="description" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        توضیحات
                                    </Form.Label>
                                    <div className="col-12 col-md-7 col-lg-8">
                                        <Form.Control
                                            as="textarea"
                                            className="form-control"
                                            name="description"
                                            rows={3}
                                            ref={register()}
                                        />
                                    </div>
                                </Form.Group>

                                <div className="row">
                                    <div className="col-12 col-md-4" />
                                    <div className="col-12 col-md-8" />
                                </div>

                                <div style={{ textAlign: 'center' }}>
                                    <Button
                                        type="submit"
                                        variant="primary"
                                        className={styles['submit-button']}
                                        {...(isSubmitting ? { disabled: true } : {})}
                                    >
                                        {isSubmitting ? 'در حال ارسال ...' : 'ارسال فرم پیش ثبت نام'}
                                    </Button>
                                </div>
                            </Form>
                        </div>
                    </div>
                </Container>
            </div>
        </>
    );
}
