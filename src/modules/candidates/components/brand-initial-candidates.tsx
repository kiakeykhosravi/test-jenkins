import React, { useContext, useEffect, useRef, useState } from 'react';
import classNames from 'classnames';
import { IProvince } from 'models/province';
import { useForm } from 'react-hook-form';
import toEnglishNum from 'utils/to-english-num';
import apiRegisterCandidate from '../api/api-register-candidate';
import { toast } from 'react-toastify';
import styles from '../styles/candidates.module.scss';
import { Button, Container, Form, Tooltip, OverlayTrigger } from 'react-bootstrap';
import Link from 'next/link';
import Provinces from './provinces';
import Cities from './cities';
import toPersianNum from 'utils/to-persian-num';
import { Plus, X } from 'react-bootstrap-icons';
import { ISpecialty } from 'models/specialty';
import { CandidateTypeEnToFaEnum, CandidateTypeEnum } from 'models/candidate';
import { useRouter } from 'next/router';
import apiSendCode from '../api/api-send-code';
import apiVerifyCode from '../api/api-verify-code';
import { setInterval } from 'timers';
import { AuthContext } from 'contexts/auth-context';
import Head from 'next/head';

export default function BrandInitialCandidates(props: {
    provinces: IProvince[];
    specialties: ISpecialty[];
    type: CandidateTypeEnum;
}) {
    const VERIFICATION_TIMEOUT = 180;

    const cn = classNames;
    const { handleSubmit, errors, register, trigger, getValues } = useForm();
    const [activeProvince, setActiveProvince] = useState<IProvince>(props.provinces[0]);
    const [mobileFields, setMobileFields] = useState([]);
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [referrer, setReferrer] = useState<number | null>(null);
    const [secret, setSecret] = useState(null);
    const [verifyingMobileNumber, setVerifyingMobileNumber] = useState('');
    const [mobileVerified, setMobileVerified] = useState(false);
    const [isSendingCode, setIsSendingCode] = useState(false);
    const [verificationTimerValue, setVerificationTimerValue] = useState(0);
    const [verificationTimerInterval, setVerificationTimerInterval] = useState(null);

    const fullnameRef = useRef(null);
    const firstMobileRef = useRef(null);
    const emailRef = useRef(null);
    const authContext = useContext(AuthContext);
    const router = useRouter();

    useEffect(() => {
        let ref;
        if (typeof router.query.referrer !== 'string' || !parseInt(router.query.referrer as string, 10)) {
            ref = null;
        } else {
            ref = parseInt(router.query.referrer as string);
        }

        setReferrer(ref);
    }, [router.query.referrer]);

    useEffect(() => {
        if (authContext.user) {
            firstMobileRef.current.value = authContext.user.mobile;
            setMobileFields(['']);
            fullnameRef.current.value = authContext.user.fullname;
            emailRef.current.value = authContext.user.email;

            firstMobileRef.current.setAttribute('readonly', true);
            fullnameRef.current.setAttribute('readonly', true);
            if (authContext.user.email) {
                emailRef.current.setAttribute('readonly', true);
            }
        }
    }, [authContext.user]);

    const typeFa = () => {
        return CandidateTypeEnToFaEnum[props.type];
    };

    const handleFieldsChange = (i, e, type) => {
        if (type === 'mobile') {
            const newFormValues = mobileFields.map((m, index) => (i === index ? e.target.value : m));
            setMobileFields(newFormValues);
        }
    };

    const addFormFields = (type) => {
        if (type === 'mobile') {
            setMobileFields([...mobileFields, '']);
        }
    };

    const removeFormFields = (i, type) => {
        if (type === 'mobile') {
            const newFormValues = mobileFields.filter((m, index) => i !== index);
            setMobileFields(newFormValues);
        }
    };

    const provinceChangeHandler = (event) => {
        for (let i = 0; i < props.provinces.length; i++) {
            if (props.provinces[i].id == event.target.value) {
                setActiveProvince(props.provinces[i]);
                break;
            }
        }
    };

    const runTimer = () => {
        const interval = setInterval(() => {
            if (verificationTimerValue < 0) {
                clearInterval(verificationTimerInterval);
            } else {
                setVerificationTimerValue((prevValue) => prevValue - 1);
            }
        }, 1000);

        setVerificationTimerInterval(interval);
    };

    const sendCode = async () => {
        if (isSendingCode) {
            return;
        }

        const isValid = await trigger();
        if (!isValid) {
            return;
        }

        setIsSendingCode(true);

        const mobile = getValues('firstMobile');
        setVerifyingMobileNumber(mobile);

        const response = await apiSendCode({
            mobile: mobile,
        });

        if (response.status === 'data') {
            setVerificationTimerValue(VERIFICATION_TIMEOUT);
            setSecret(response.data);
            runTimer();
            toast.success(response.message);
        } else {
            toast.error(response.message);
        }

        setIsSendingCode(false);
    };

    const resendCode = async () => {
        if (mobileVerified) {
            toast.info('این شماره موبایل تایید شده است و نیازی به تایید مجدد ندارد!');
            return;
        }

        if (!verificationTimerValue) {
            return;
        }

        await sendCode();
    };

    const verifyCode = async (code: string) => {
        if (!secret || !code) {
            return false;
        }

        if (mobileVerified) {
            return true;
        }

        const response = await apiVerifyCode({
            secret: secret,
            code: code,
        });

        const success = response.status === 'data';
        if (success) {
            setMobileVerified(true);
            toast.success(response.message);
        } else {
            toast.error(response.message);
        }

        return success;
    };

    const onSubmitForm = async (data) => {
        if (isSubmitting || !secret) {
            return;
        }
        setIsSubmitting(true);

        const isVerified = await verifyCode(toEnglishNum(data.code));
        if (!isVerified) {
            setIsSubmitting(false);
            return;
        }

        const mobile_number = [toEnglishNum(data.firstMobile)];
        mobileFields.map((m) => {
            if (m !== '') mobile_number.push(toEnglishNum(m));
        });

        const formData = {
            city_id: data.city_id,
            name: data.name,
            owner_name: data.owner_name,
            mobile_number,
            email: data.email,
            type: props.type,
            marketer_id: referrer || null,
            secret: secret,
        };

        const response = await apiRegisterCandidate(formData);
        if (response.status === 'data') {
            setIsSubmitting(false);
            toast.success(response.message);
            await router.push(response.data);
        } else {
            setIsSubmitting(false);
            toast.error(response.message);
        }
    };

    const renderTimer = () => {
        const seconds = verificationTimerValue % 60;
        return verificationTimerValue > 0
            ? Math.floor(verificationTimerValue / 60) + ':' + (seconds > 10 ? seconds : '0' + seconds)
            : '';
    };

    return (
        <>
            <Head>
                <title>{`پیش ثبت نام سامانه برای ${typeFa()}`}</title>
                <meta name="robots" content="index, follow" />
                <link rel="canonical" href="https://www.asanseminar.ir" />
                <meta name="description" content={`پیش ثبت نام سامانه برای ${typeFa()}`} />
                <meta property="og:url" content="https://asanseminar.ir" />
                <meta property="og:title" content={`پیش ثبت نام سامانه برای ${typeFa()}`} />
                <meta property="og:description" content={`پیش ثبت نام سامانه برای ${typeFa()}`} />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content={`پیش ثبت نام سامانه برای ${typeFa()}`} />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
            </Head>
            <div className={styles.body}>
                <Container className={styles.container}>
                    <div className={cn(styles.main, 'col-12 col-lg-10 col-xl-9')}>
                        <Link href="/">
                            <a title="آسان سمینار" className={styles['asanseminar-logo-link']}>
                                <img
                                    src="/images/asanseminar_full.png"
                                    alt="آسان سمینار"
                                    className={styles['asanseminar-logo']}
                                />
                            </a>
                        </Link>
                        <h1 className={styles['title-bar']}>{`پیش ثبت نام سامانه برای ${typeFa()}`}</h1>
                        <div className={styles.box}>
                            <Form onSubmit={handleSubmit(onSubmitForm)}>
                                <Form.Group controlId="name" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        عنوان برند *
                                    </Form.Label>
                                    <div className="col-12 col-md-7 col-lg-8">
                                        <Form.Control
                                            type="text"
                                            className={errors.name && styles['field-error']}
                                            name="name"
                                            ref={register({
                                                required: 'عنوان برند الزامی می باشد',
                                                minLength: {
                                                    value: 3,
                                                    message: 'عنوان برند باید حداقل سه حرف داشته باشد',
                                                },
                                            })}
                                        />
                                    </div>
                                    {errors.name && (
                                        <div className={cn(styles['error-message'], 'col-12 col-md-8 offset-md-4')}>
                                            {errors.name.message}
                                        </div>
                                    )}
                                </Form.Group>

                                <Form.Group controlId="ownerName" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        نام و نام خانوادگی *
                                    </Form.Label>
                                    <div className="col-12 col-md-7 col-lg-8">
                                        <OverlayTrigger
                                            key="fullname-tooltip"
                                            placement="top"
                                            delay={{ show: 500, hide: 0 }}
                                            overlay={
                                                <Tooltip
                                                    id="fullname-tooltip"
                                                    style={{ display: authContext.user ? '' : 'none' }}
                                                >
                                                    جهت ثبت برندپیج برای شخص دیگر لطفا ابتدا خارج شوید
                                                </Tooltip>
                                            }
                                        >
                                            <Form.Control
                                                className={errors.owner_name && styles['field-error']}
                                                name="owner_name"
                                                ref={(e) => {
                                                    register(e, {
                                                        required: 'نام و نام خانوادگی الزامی می باشد',
                                                        minLength: {
                                                            value: 3,
                                                            message: 'نام و نام خانوادگی باید حداقل سه حرف داشته باشد',
                                                        },
                                                    });
                                                    fullnameRef.current = e;
                                                }}
                                            />
                                        </OverlayTrigger>
                                    </div>
                                    {errors.owner_name && (
                                        <div className={cn(styles['error-message'], 'col-12 col-md-8 offset-md-4')}>
                                            {errors.owner_name.message}
                                        </div>
                                    )}
                                </Form.Group>

                                <Form.Group controlId="firstMobile" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        شماره همراه *
                                    </Form.Label>
                                    <div
                                        className={
                                            mobileFields.length
                                                ? 'col-12 col-md-7 col-lg-8'
                                                : 'col-11 col-md-7 col-lg-7'
                                        }
                                    >
                                        <OverlayTrigger
                                            key="mobile-tooltip"
                                            placement="top"
                                            delay={{ show: 500, hide: 0 }}
                                            overlay={
                                                <Tooltip
                                                    id="mobile-tooltip"
                                                    style={{ display: authContext.user ? '' : 'none' }}
                                                >
                                                    جهت ثبت برندپیج برای شخص دیگر لطفا ابتدا خارج شوید
                                                </Tooltip>
                                            }
                                        >
                                            <Form.Control
                                                className={cn('text-left', {
                                                    [styles['field-error']]: errors.firstMobile,
                                                })}
                                                style={{ direction: 'ltr' }}
                                                name="firstMobile"
                                                ref={(e) => {
                                                    register(e, {
                                                        required: 'شماره همراه الزامی می باشد',
                                                        pattern: {
                                                            value: /^((?:98|\+98|0098|0)?9[0-9]{9})|((?:۹۸|\+۹۸|۰۰۹۸|۰)?۹[۰۹۸۷۶۵۴۳۲۱]{9})$/, // 09(0[1-2]|1[0-9]|3[0-9]|2[0-1])-?[0-9]{3}-?[0-9]{4}
                                                            message: 'فرمت شماره موبایل اشتباه است!',
                                                        },
                                                    });
                                                    firstMobileRef.current = e;
                                                }}
                                                onChange={() => {
                                                    setMobileVerified(false);
                                                    setSecret(null);
                                                }}
                                                {...(isSubmitting || isSendingCode ? { disabled: true } : {})}
                                            />
                                        </OverlayTrigger>
                                    </div>
                                    {!mobileFields.length ? (
                                        <div className={cn(styles['field-action-btn-col'], 'col-1')}>
                                            <Plus
                                                className={cn(styles['field-action-btn'], styles['add-btn'])}
                                                onClick={() => addFormFields('mobile')}
                                            />
                                        </div>
                                    ) : null}
                                    {errors.firstMobile && (
                                        <div className={cn(styles['error-message'], 'col-12 col-md-8 offset-md-4')}>
                                            {errors.firstMobile.message}
                                        </div>
                                    )}
                                </Form.Group>

                                {mobileFields.map((element, index) => (
                                    <Form.Group
                                        controlId={`mobile-${index + 2}`}
                                        className={cn('row', styles['field-row'])}
                                        key={index}
                                    >
                                        <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                            {`شماره همراه ${toPersianNum(index + 2)}`}
                                        </Form.Label>
                                        <div
                                            className={
                                                mobileFields.length !== index + 1 || index === 2
                                                    ? 'col-12 col-md-7 col-lg-8 position-relative'
                                                    : 'col-11 col-md-7 col-lg-7 position-relative'
                                            }
                                        >
                                            <Form.Control
                                                className={cn(styles['more-field-input'])}
                                                style={{ direction: 'ltr' }}
                                                name={`mobile-${index + 2}`}
                                                value={mobileFields[index]}
                                                onChange={(e) => handleFieldsChange(index, e, 'mobile')}
                                            />
                                            <div
                                                className={cn(styles['field-action-btn-col'], styles['remove-btn-col'])}
                                            >
                                                <X
                                                    className={cn(styles['field-action-btn'], styles['remove-btn'])}
                                                    onClick={() => removeFormFields(index, 'mobile')}
                                                />
                                            </div>
                                        </div>
                                        {mobileFields.length === index + 1 && mobileFields.length < 3 ? (
                                            <div className={cn(styles['field-action-btn-col'], 'col-1')}>
                                                <Plus
                                                    className={cn(styles['field-action-btn'], styles['add-btn'])}
                                                    onClick={() => addFormFields('mobile')}
                                                />
                                            </div>
                                        ) : null}
                                    </Form.Group>
                                ))}

                                <Form.Group controlId="email" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        ایمیل *
                                    </Form.Label>
                                    <div className="col-12 col-md-7 col-lg-8">
                                        <OverlayTrigger
                                            key="email-tooltip"
                                            placement="top"
                                            delay={{ show: 500, hide: 0 }}
                                            overlay={
                                                <Tooltip
                                                    id="email-tooltip"
                                                    style={{
                                                        display:
                                                            authContext.user && authContext.user.email ? '' : 'none',
                                                    }}
                                                >
                                                    جهت ثبت برندپیج برای شخص دیگر لطفا ابتدا خارج شوید
                                                </Tooltip>
                                            }
                                        >
                                            <Form.Control
                                                className={cn('text-left', { [styles['field-error']]: errors.email })}
                                                style={{ direction: 'ltr' }}
                                                name="email"
                                                ref={(e) => {
                                                    register(e, {
                                                        required: 'ایمیل الزامی می باشد',
                                                    });
                                                    emailRef.current = e;
                                                }}
                                            />
                                        </OverlayTrigger>
                                    </div>
                                    {errors.email && (
                                        <div className={cn(styles['error-message'], 'col-12 col-md-8 offset-md-4')}>
                                            {errors.email.message}
                                        </div>
                                    )}
                                </Form.Group>

                                <Form.Group controlId="province_id" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        استان *
                                    </Form.Label>
                                    <div className="col-12 col-md-7 col-lg-8">
                                        <Provinces
                                            provinces={props.provinces}
                                            changeHandler={provinceChangeHandler}
                                            formRegisterer={register}
                                        />
                                    </div>
                                </Form.Group>

                                <Form.Group controlId="city_id" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        شهر *
                                    </Form.Label>
                                    <div className="col-12 col-md-7 col-lg-8">
                                        <Cities activeProvince={activeProvince} formRegisterer={register} />
                                    </div>
                                </Form.Group>

                                {secret ? (
                                    <>
                                        <div className={styles['verification-box']}>
                                            <Form.Group controlId="code" className={cn('row', styles['field-row'])}>
                                                <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                                    کد تایید
                                                </Form.Label>
                                                <div className="col-12 col-md-3">
                                                    <Form.Control
                                                        className={cn('text-left', {
                                                            [styles['field-error']]: errors.code,
                                                        })}
                                                        name="code"
                                                        ref={register({
                                                            required: 'کد تایید ارسال شده را وارد کنید',
                                                            pattern: {
                                                                value: /[0-9]{6}/,
                                                                message: 'کد تایید باید عددی و به طول 6 رقم باشد',
                                                            },
                                                        })}
                                                    />
                                                </div>
                                                <Form.Label className={cn('col-12 col-md-5', styles['field-title'])}>
                                                    کد تایید به این شماره ارسال شده است: {verifyingMobileNumber}
                                                </Form.Label>
                                                {errors.code && (
                                                    <div
                                                        className={cn(
                                                            styles['error-message'],
                                                            'col-12 col-md-8 offset-md-4',
                                                        )}
                                                    >
                                                        {errors.code.message}
                                                    </div>
                                                )}
                                            </Form.Group>
                                            <Button
                                                type="button"
                                                variant="secondary"
                                                className={styles['resend-button']}
                                                {...(verificationTimerValue > 0 ? { disabled: true } : {})}
                                                onClick={resendCode}
                                            >
                                                {isSendingCode
                                                    ? 'در حال ارسال مجدد ...'
                                                    : 'ارسال مجدد کد ' + renderTimer()}
                                            </Button>
                                        </div>
                                        <Button
                                            type="submit"
                                            variant="primary"
                                            className={styles['submit-button']}
                                            {...(isSubmitting ? { disabled: true } : {})}
                                        >
                                            {isSubmitting ? 'در حال انجام ...' : 'تایید کد و پرداخت'}
                                        </Button>
                                    </>
                                ) : (
                                    <Button
                                        type="button"
                                        variant="primary"
                                        className={styles['submit-button']}
                                        {...(isSendingCode ? { disabled: true } : {})}
                                        onClick={sendCode}
                                    >
                                        {isSendingCode ? 'در حال ارسال ...' : 'ارسال پیامک تایید'}
                                    </Button>
                                )}
                            </Form>
                        </div>
                    </div>
                </Container>
            </div>
        </>
    );
}
