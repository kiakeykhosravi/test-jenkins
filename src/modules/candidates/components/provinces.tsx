import React from 'react';
import { Form } from 'react-bootstrap';
import { IProvince } from '../../../models/province';

export default function Provinces(props: { provinces: IProvince[]; changeHandler; formRegisterer }) {
    const renderProvinces = () => {
        if (props.provinces.length) {
            return props.provinces.map((province) => {
                return (
                    <option value={province.id} key={province.id}>
                        {province.name}
                    </option>
                );
            });
        } else return null;
    };
    return (
        <Form.Control
            as="select"
            name="province_id"
            className="form-select"
            onChange={props.changeHandler}
            ref={props.formRegisterer()}
        >
            {renderProvinces()}
        </Form.Control>
    );
}
