import React from 'react';
import { Form } from 'react-bootstrap';

export default function Groups(props: { changeHandler; formRegisterer }) {
    return (
        <Form.Control
            as="select"
            name="type"
            className="form-select"
            onChange={props.changeHandler}
            ref={props.formRegisterer()}
        >
            <option value="school">مدرسه</option>
            <option value="university">دانشگاه</option>
            <option value="institution">موسسه/آموزشگاه فنی‌و‌حرفه‌ای</option>
            <option value="institution_private">موسسه/آموزشگاه آزاد علمی</option>
            <option value="institution_language">موسسه/آموزشگاه زبان</option>
            <option value="organization">سازمان</option>
        </Form.Control>
    );
}
