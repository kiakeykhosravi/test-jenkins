import React from 'react';
import { Form } from 'react-bootstrap';

export default function Shifts(props: { formRegisterer }) {
    return (
        <Form.Control as="select" name="class_times" className="form-select" ref={props.formRegisterer()}>
            <option value="صبح">صبح</option>
            <option value="عصر">عصر</option>
            <option value="هردو">هردو</option>
        </Form.Control>
    );
}
