import { IProvince } from 'models/province';
import { ICandidate } from 'models/candidate';
import { IMedia } from 'models/media';
import { ISpecialty } from 'models/specialty';

export interface ICitiesResponse {
    provinces: IProvince[];
}

export interface ICandidateResponse {
    candidate: ICandidate;
}

export interface IRegisterCandidateResponse {
    link?: string;
}

export interface IUploadOrganizationImageResponse {
    media: IMedia;
}

export interface ISpecialtiesResponse {
    specialties: ISpecialty[];
}

export interface ISendAndVerifyCodeResponse {
    secret: string;
}

export interface IBrandpagePayRetryResponse {
    link: string;
}
