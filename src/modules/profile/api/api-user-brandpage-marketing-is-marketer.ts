import apiRequest from 'api/api-request';
import { IUserBrandpageMarketingIsMarketer } from '../responses';
import endpoints from 'api/api-endpoints';
import { AxiosRequestConfig } from 'axios';

export default async function apiUserBrandpageMarketingIsMarketer(
    isBackend = false,
    config?: AxiosRequestConfig,
): Promise<{
    is_marketer: boolean;
}> {
    const url = isBackend
        ? endpoints.backend.profile_marketing_brandpage_is_marketer
        : endpoints.frontend.profile_marketing_brandpage_is_marketer;

    const response = await apiRequest<IUserBrandpageMarketingIsMarketer>(
        'get',
        url,
        undefined,
        undefined,
        undefined,
        config,
    );

    if (response.status === 'data') {
        return response.data;
    }

    return null;
}
