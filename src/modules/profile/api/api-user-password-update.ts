import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { toast } from 'react-toastify';

export default function apiUserPasswordUpdate(data: {
    password: string;
    password_confirmation: string;
}): Promise<boolean> {
    return apiRequest<void>('put', endpoints.frontend.profile_password_update, {
        password: data.password,
        password_confirmation: data.password_confirmation,
    })
        .then((response) => {
            if (response) {
                if (response.status === 'data') {
                    toast.success(response.message || 'بروز رسانی رمز عبور با موفقیت انجام شد', {
                        autoClose: 10000,
                    });
                    return true;
                }
            }
            toast.error('خطایی در بروز رسانی رمز عبور شما رخ داد', {
                autoClose: 5000,
            });

            return false;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در بروز رسانی رمز عبور شما رخ داد', {
                autoClose: 5000,
            });

            return false;
        });
}
