import endpoints from 'api/api-endpoints';
import { toast } from 'react-toastify';
import apiRequest from 'api/api-request';
import { ICreditResponse } from '../responses';
import { AxiosRequestConfig } from 'axios';

export default async function apiGetCredit(
    isBackend = false,
    config?: AxiosRequestConfig,
): Promise<{ data?: ICreditResponse; status: 'error' | 'data' }> {
    const url = isBackend ? endpoints.backend.credit : endpoints.frontend.credit;
    const response = await apiRequest<ICreditResponse>('get', url, undefined, undefined, undefined, config);

    if (response.status === 'data') {
        return {
            data: response.data,
            status: response.status,
        };
    } else {
        toast.error(response.message || 'خطایی در دریافت اعتبار شما رخ داد');
        return {
            status: 'error',
        };
    }
}
