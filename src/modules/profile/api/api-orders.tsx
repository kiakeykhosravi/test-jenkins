import endpoints from 'api/api-endpoints';
import { IOrdersResponse } from '../responses';
import { toast } from 'react-toastify';
import apiRequest from 'api/api-request';
import { AxiosRequestConfig } from 'axios';

export default function apiOrders(
    isBackend = false,
    page?: number,
    config?: AxiosRequestConfig,
): Promise<IOrdersResponse | null> {
    const url = (isBackend ? endpoints.backend.orders : endpoints.frontend.orders) + (page > 1 ? `?page=${page}` : '');

    return apiRequest<IOrdersResponse>('get', url, undefined, undefined, undefined, config)
        .then((response) => {
            if (response) {
                if (response.status === 'data') {
                    return response.data;
                }
                toast.error(response.message || 'خطایی در دریافت دوره های مرتبط با شما رخ داد');
            } else {
                toast.error('خطایی در دریافت دوره های مرتبط با شما رخ داد');
            }

            return null;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در دریافت دوره های مرتبط با شما رخ داد');
            return null;
        });
}
