import endpoints from 'api/api-endpoints';
import apiRequest from 'api/api-request';
import { IUserCandidatesResponse } from '../responses';
import { toast } from 'react-toastify';
import { AxiosRequestConfig } from 'axios';

export default async function apiUserCandidates(
    isBackend = false,
    config?: AxiosRequestConfig,
): Promise<null | { data: any; message: string }> {
    const url = isBackend ? endpoints.backend.profile_candidates : endpoints.frontend.profile_candidates;

    const result = apiRequest<IUserCandidatesResponse>('get', url, undefined, undefined, undefined, config);

    return result
        .then((response) => {
            if (response.status === 'data') {
                return {
                    data: response.data,
                    message: response.message,
                };
            }
            if (
                !response.errorMeta ||
                !response.errorMeta.response ||
                (response.errorMeta && response.errorMeta.response && response.errorMeta.response.status !== 401)
            ) {
                toast.error(response.message || 'خطایی در دریافت اطلاعات درخواست‌های شما رخ داد');
            }
            return null;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در دریافت اطلاعات درخواست‌های شما رخ داد');
            return null;
        });
}
