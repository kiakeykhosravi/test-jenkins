import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IUserUpdateResponse } from '../responses';
import { toast } from 'react-toastify';
import { IUser } from 'models/user';

export default function apiUserUpdate(data: {
    fullname: string;
    email: string;
    nationalCode: string;
    whatsapp: string;
}): Promise<IUser | null> {
    return apiRequest<IUserUpdateResponse>('put', endpoints.frontend.profile_user, {
        fullname: data.fullname,
        email: data.email,
        national_code: data.nationalCode,
        whatsapp: data.whatsapp,
    })
        .then((response) => {
            if (response) {
                if (response.status === 'data') {
                    toast.success(response.message || 'بروز رسانی اطلاعات کاربری با موفقیت انجام شد', {
                        autoClose: 10000,
                    });
                    return response.data.user;
                }
                toast.error(response.message || 'خطایی در بروز رسانی اطلاعات کاربری شما رخ داد', {
                    autoClose: 5000,
                });
            }
            return null;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در بروز رسانی اطلاعات کاربری شما رخ داد', {
                autoClose: 5000,
            });
            return null;
        });
}
