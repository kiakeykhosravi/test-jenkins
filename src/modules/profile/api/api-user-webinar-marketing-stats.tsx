import endpoints from 'api/api-endpoints';
import { toast } from 'react-toastify';
import { IUserWebinarMarketingStatsResponse } from '../responses';
import { AxiosRequestConfig } from 'axios';
import apiRequest from 'api/api-request';

export default function apiUserWebinarMarketingStats(
    isBackend = false,
    config?: AxiosRequestConfig,
): Promise<IUserWebinarMarketingStatsResponse> {
    const url = isBackend
        ? endpoints.backend.user_marketing_webinars_stats
        : endpoints.frontend.user_marketing_webinars_stats;

    return apiRequest<IUserWebinarMarketingStatsResponse>('get', url, undefined, undefined, undefined, config)
        .then((response) => {
            if (response) {
                if (response.status === 'data') {
                    return response.data;
                }
                toast.error(response.message || 'خطایی در دریافت وبینارهای مرتبط با شما رخ داد');
            } else {
                toast.error('خطایی در دریافت وبینارهای مرتبط با شما رخ داد');
            }

            return null;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در دریافت وبینارهای مرتبط با شما رخ داد');
            return null;
        });
}
