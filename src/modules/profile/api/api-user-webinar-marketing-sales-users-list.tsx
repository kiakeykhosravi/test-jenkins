import endpoints from 'api/api-endpoints';
import { toast } from 'react-toastify';
import { IUserWebinarMarketingSalesUsersListResponse } from '../responses';
import { AxiosRequestConfig } from 'axios';
import apiRequest from 'api/api-request';

export default function apiUserWebinarMarketingSalesUsersList(
    isBackend = false,
    webinarID: number,
    config?: AxiosRequestConfig,
): Promise<IUserWebinarMarketingSalesUsersListResponse> {
    let url = isBackend
        ? endpoints.backend.user_marketing_webinars_sales_users_list
        : endpoints.frontend.user_marketing_webinars_sales_users_list;
    url = url.replace('{webinar}', webinarID.toString());

    return apiRequest<IUserWebinarMarketingSalesUsersListResponse>('get', url, undefined, undefined, undefined, config)
        .then((response) => {
            if (response) {
                if (response.status === 'data') {
                    return response.data;
                }
                toast.error(response.message || 'خطایی در دریافت لیست کاربران رخ داد');
            } else {
                toast.error('خطایی در دریافت لیست کاربران رخ داد');
            }

            return null;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در دریافت لیست کاربران رخ داد');
            return null;
        });
}
