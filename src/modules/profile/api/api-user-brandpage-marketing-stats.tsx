import endpoints from 'api/api-endpoints';
import { toast } from 'react-toastify';
import { IUserBrandPageMarketingStatsResponse } from '../responses';
import { AxiosRequestConfig } from 'axios';
import apiRequest from 'api/api-request';

export default function apiUserBrandPageMarketingStats(
    isBackend = false,
    config?: AxiosRequestConfig,
): Promise<IUserBrandPageMarketingStatsResponse> {
    const url = isBackend
        ? endpoints.backend.user_marketing_brandpage_stats
        : endpoints.frontend.user_marketing_brandpage_stats;

    return apiRequest<IUserBrandPageMarketingStatsResponse>('get', url, undefined, undefined, undefined, config)
        .then((response) => {
            if (response) {
                if (response.status === 'data') {
                    return response.data;
                }
                toast.error(response.message || 'خطایی در دریافت اطلاعات رخ داد');
            } else {
                toast.error('خطایی در دریافت اطلاعات رخ داد');
            }

            return null;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در دریافت اطلاعات رخ داد');
            return null;
        });
}
