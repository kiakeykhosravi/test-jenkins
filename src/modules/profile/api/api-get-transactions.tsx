import endpoints from 'api/api-endpoints';
import { ITransactionsResponse } from '../responses';
import { toast } from 'react-toastify';
import apiRequest from 'api/api-request';
import { AxiosRequestConfig } from 'axios';

export default async function apiGetTransactions(
    isBackend = false,
    page: number,
    config?: AxiosRequestConfig,
): Promise<{ data?: ITransactionsResponse; status: 'error' | 'data' }> {
    const url = (isBackend ? endpoints.backend.transactions : endpoints.frontend.transactions) + `?page=${page}`;
    const response = await apiRequest<ITransactionsResponse>('get', url, undefined, undefined, undefined, config);

    if (response.status === 'data') {
        return {
            data: response.data,
            status: response.status,
        };
    } else {
        toast.error(response.message || 'خطایی در دریافت تراکنش های مرتبط با شما رخ داد');
        return {
            status: 'error',
        };
    }
}
