import endpoints from 'api/api-endpoints';
import { toast } from 'react-toastify';
import apiRequest from 'api/api-request';
import { IAddCreditResponse } from '../responses';

export default async function apiAddCredit(
    amount: string,
): Promise<{ data?: IAddCreditResponse; status: 'error' | 'data' }> {
    const response = await apiRequest<IAddCreditResponse>('post', endpoints.frontend.credit_add, { value: amount });

    if (response.status === 'data') {
        return {
            data: response.data,
            status: response.status,
        };
    } else {
        toast.error(response.message || 'خطایی در درخواست افزایش اعتبار شما رخ داد');
        return {
            status: 'error',
        };
    }
}
