import endpoints from 'api/api-endpoints';
import { toast } from 'react-toastify';
import { IMeetingsResponse } from '../responses';
import { AxiosRequestConfig } from 'axios';
import apiRequest from 'api/api-request';

export default function apiMeetings(
    isBackend = false,
    page?: number,
    config?: AxiosRequestConfig,
): Promise<IMeetingsResponse> {
    let url = isBackend ? endpoints.backend.meetings : endpoints.frontend.meetings;
    url += page ? `?page=${page}` : '';

    return apiRequest<IMeetingsResponse>('get', url, undefined, undefined, undefined, config)
        .then((response) => {
            if (response) {
                if (response.status === 'data') {
                    return response.data;
                }
                toast.error(response.message || 'خطایی در دریافت جلسه های مرتبط با شما رخ داد');
            } else {
                toast.error('خطایی در دریافت جلسه های مرتبط با شما رخ داد');
            }

            return null;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در دریافت جلسه های مرتبط با شما رخ داد');
            return null;
        });
}
