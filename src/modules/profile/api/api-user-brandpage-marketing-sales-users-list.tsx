import endpoints from 'api/api-endpoints';
import { toast } from 'react-toastify';
import { IUserBrandPageMarketingSalesUsersListResponse } from '../responses';
import { AxiosRequestConfig } from 'axios';
import apiRequest from 'api/api-request';

export default function apiUserBrandPageMarketingSalesUsersList(
    isBackend = false,
    config?: AxiosRequestConfig,
): Promise<IUserBrandPageMarketingSalesUsersListResponse> {
    const url = isBackend
        ? endpoints.backend.user_marketing_brandpage_sales_users_list
        : endpoints.frontend.user_marketing_webinars_sales_users_list;

    return apiRequest<IUserBrandPageMarketingSalesUsersListResponse>(
        'get',
        url,
        undefined,
        undefined,
        undefined,
        config,
    )
        .then((response) => {
            if (response) {
                if (response.status === 'data') {
                    return response.data;
                }
                toast.error(response.message || 'خطایی در دریافت اطلاعات رخ داد');
            } else {
                toast.error('خطایی در دریافت اطلاعات رخ داد');
            }

            return null;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در دریافت اطلاعات رخ داد');
            return null;
        });
}
