import endpoints from 'api/api-endpoints';
import apiRequest from 'api/api-request';
import { IUserResponse } from '../responses';
import { toast } from 'react-toastify';
import { AxiosRequestConfig } from 'axios';
import { IUser } from 'models/user';

export default async function apiUser(isBackend = false, config?: AxiosRequestConfig): Promise<IUser | null> {
    const url = isBackend ? endpoints.backend.profile_user : endpoints.frontend.profile_user;

    const result = apiRequest<IUserResponse>('get', url, undefined, undefined, undefined, config);

    return result
        .then((response) => {
            if (response.status === 'data') {
                return response.data.user;
            }
            if (
                !response.errorMeta ||
                !response.errorMeta.response ||
                (response.errorMeta && response.errorMeta.response && response.errorMeta.response.status !== 401)
            ) {
                toast.error(response.message || 'خطایی در دریافت اطلاعات کاربری شما رخ داد');
            }
            return null;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در دریافت اطلاعات کاربری شما رخ داد');
            return null;
        });
}
