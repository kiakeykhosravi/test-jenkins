import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IAdminAuthenticationResponse } from '../responses';
import { toast } from 'react-toastify';
import { AxiosRequestConfig } from 'axios';

export default async function apiAdmin(
    isBackend = false,
    target?: string,
    config?: AxiosRequestConfig,
    isBacked?: boolean,
): Promise<string | null> {
    const url = isBackend ? endpoints.backend.admin_authentication : endpoints.frontend.admin_authentication;

    let address = process.env.NEXT_PUBLIC_BACKEND_BASE_ADDRESS;
    if ((!target && address[address.length - 1] !== '/') || (target && target[0] !== '/')) {
        address += '/';
    }

    address += target || 'admin';

    return apiRequest<IAdminAuthenticationResponse>(
        'post',
        url,
        {
            target: address,
        },
        undefined,
        undefined,
        config,
    )
        .then((response) => {
            if (response.status === 'data') {
                !isBacked && toast.success('شما در حال انتقال به پنل ادمین می باشید ...');
                return response.data.link;
            }

            !isBacked && toast.error(response.message || 'خطایی در ورود به پنل ادمین رخ داد');
            return null;
        })
        .catch((error) => {
            !isBacked && toast.error((error && error.message) || 'خطایی در ورود به پنل ادمین رخ داد');
            return null;
        });
}
