import endpoints from 'api/api-endpoints';
import { toast } from 'react-toastify';
import { IMarketingWebinarsListResponse } from '../responses';
import { AxiosRequestConfig } from 'axios';
import apiRequest from 'api/api-request';

export default function apiMarketingWebinarsList(
    isBackend = false,
    q: string,
    page: number,
    date: 'today' | 'day-1' | 'day-3' | 'week-1' | 'week-2' | 'month-1' | 'month-3' | 'month-6' | 'year-1',
    sortDate: 'asc' | 'desc',
    sortPrice: 'asc' | 'desc',
    sortMarketerPercent: 'asc' | 'desc',
    sortMarketingAmount: 'asc' | 'desc',
    config?: AxiosRequestConfig,
): Promise<IMarketingWebinarsListResponse> {
    let url = isBackend ? endpoints.backend.marketing_webinars : endpoints.frontend.marketing_webinars;
    url =
        url +
        `?q=${q}&page=${page}&date=${date}&sort-date=${sortDate}&sort-price=${sortPrice}&sort-marketer-percent=${sortMarketerPercent}&sort-marketing-amount=${sortMarketingAmount}`;

    return apiRequest<IMarketingWebinarsListResponse>('get', url, undefined, undefined, undefined, config)
        .then((response) => {
            if (response) {
                if (response.status === 'data') {
                    return response.data;
                }
                toast.error(response.message || 'خطایی در دریافت وبینارهای مرتبط با شما رخ داد');
            } else {
                toast.error('خطایی در دریافت وبینارهای مرتبط با شما رخ داد');
            }

            return null;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در دریافت وبینارهای مرتبط با شما رخ داد');
            return null;
        });
}
