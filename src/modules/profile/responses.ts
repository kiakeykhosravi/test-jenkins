import { IOrder } from 'models/orders';
import { IMeeting } from 'models/meeting';
import { IUser } from 'models/user';
import { IUserSafe } from 'models/user-safe';
import { ITransaction } from 'models/transaction';
import { IWebinarCompact } from 'models/webinar-compact';

export interface IOrdersResponse {
    lastpage: number;
    orders: IOrder[];
}

export interface IMeetingsResponse {
    meetings: IMeeting[];
    lastpage: number;
}

export interface IUserResponse {
    user: IUser;
}

export interface ITransactionsResponse {
    transactions: ITransaction[];
    lastpage: number;
}

export interface ICreditResponse {
    credit: number;
}

export interface IUserUpdateResponse {
    user: IUser;
}

export interface IAdminAuthenticationResponse {
    link: string;
}

export interface IMarketingWebinarsListResponse {
    webinars: IWebinarCompact[];
    lastpage: number;
}

export interface IUserWebinarMarketingStatsResponse {
    webinars: IMarketingStatsWebinar[];
}

export interface IMarketingStatsWebinar {
    webinar: IWebinarCompact;
    marketing_summary: IWebinarsMarketingSummary;
}

export interface IWebinarsMarketingSummary {
    count: number;
    share: number;
}

export interface IUserWebinarMarketingSalesUsersListResponse {
    users: IUserSafe[];
}

export interface IUserBrandPageMarketingStatsResponse {
    marketing_summary: IBrandPageMarketingSummary;
}

export interface IBrandPageMarketingSummary {
    count: number;
    amount: number;
}

export interface IUserBrandPageMarketingSalesUsersListResponse {
    candidates: ISalesCandidate[];
}

export interface ISalesCandidate {
    name: string;
    type: string;
    status: string;
}

export interface IUserCandidatesResponse {
    candidateSecret?: string;
}

export interface IUserBrandpageMarketingIsMarketer {
    is_marketer: boolean;
}

export interface IAddCreditResponse {
    link: string;
}
