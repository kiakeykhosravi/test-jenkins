import React from 'react';
import { Col } from 'react-bootstrap';
import { IWebinarCompact } from 'models/webinar-compact';
import MarketingWebinarCard from './marketing-webinar-card';
import { IUser } from 'models/user';
import { IMarketingStatsWebinar } from '../responses';

export default function MarketingWebinarsListSection(props: {
    webinar: IWebinarCompact[];
    userMarketingStats: IMarketingStatsWebinar[];
    user: IUser;
    basePath: string;
}) {
    const renderCourseCards = () => {
        return props.webinar && props.webinar.length > 0 ? (
            props.webinar.map((webinar: IWebinarCompact) => {
                return (
                    <Col sm={6} xl={4} key={webinar.id} className={'d-flex'}>
                        <MarketingWebinarCard
                            webinar={webinar}
                            userMarketingStats={props.userMarketingStats}
                            basePath={props.basePath}
                            user={props.user}
                        />
                    </Col>
                );
            })
        ) : (
            <div style={{ fontSize: '1.1rem', padding: '2rem 0', textAlign: 'center' }}>
                <img
                    src="/images/conference.png"
                    style={{ width: '100%', maxWidth: '200px', marginBottom: '1.5rem' }}
                    alt=""
                />
                <div>متاسفانه‌‌ هیچ وبیناری یافت نشد</div>
            </div>
        );
    };

    return <>{renderCourseCards()}</>;
}
