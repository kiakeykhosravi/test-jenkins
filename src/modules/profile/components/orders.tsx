import React, { useState } from 'react';
import apiOrders from '../api/api-orders';
import OrderItem from './order-item';
import Pagination from 'modules/shared/components/pagination';
import styles from '../styles/order.module.scss';
import Link from 'next/link';
import { IOrder } from 'models/orders';
import ProfileTableLoading from './profile-table-loading';
import { toast } from 'react-toastify';

export default function Orders(props: { orders: IOrder[]; lastPage: number }) {
    const [allOrders, setAllOrders] = useState<IOrder[][]>([props.orders]);
    const [orders, setOrders] = useState<IOrder[]>(props.orders);
    const [lastPage, setLastPage] = useState(props.lastPage);
    const [currentPage, setCurrentPage] = useState(1);
    const [loading, setLoading] = useState(false);

    const handlePageClick = (data) => {
        setCurrentPage(data.selected + 1);
        if (allOrders[data.selected]) {
            setOrders(allOrders[data.selected]);
        } else {
            setLoading(true);
            apiOrders(false, data.selected + 1)
                .then((response) => {
                    if (response) {
                        const ao = allOrders;
                        ao[data.selected] = response.orders;
                        setAllOrders(ao);
                        setOrders(response.orders);
                        setLastPage(response.lastpage);
                    }
                })
                .catch(() => {
                    toast.error('مشکلی در دریافت اطلاعات به وجود آمد!');
                })
                .finally(() => {
                    setLoading(false);
                });
        }
    };

    const renderOrders = () => {
        return orders.map((order) => {
            return <OrderItem data={order} key={order.id} />;
        });
    };

    if (orders.length) {
        return (
            <div style={{ position: 'relative' }}>
                <div className={styles.orders}>{renderOrders()}</div>
                {lastPage > 1 ? (
                    <Pagination pageCount={lastPage} page={String(currentPage)} handlePageClick={handlePageClick} />
                ) : null}
                {loading ? <ProfileTableLoading /> : null}
            </div>
        );
    } else {
        return (
            <div className={styles['not-found']}>
                <div className={styles['first-text']}>برای شما هیچ سمیناری ثبت نشده است</div>
                <div className={styles['second-text']}>
                    <span>از صفحه </span>
                    <Link href="/webinars">
                        <a title="وبینار ها" className={styles.link}>
                            جستجو{' '}
                        </a>
                    </Link>
                    <span>برای پیدا کردن سمینار مرتبط با نیاز خود اقدام کنید</span>
                </div>
            </div>
        );
    }
}
