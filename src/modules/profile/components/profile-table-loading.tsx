import React from 'react';
import styles from '../styles/profile-table-loading.module.scss';
import { Spinner } from 'react-bootstrap';

export default function ProfileTableLoading() {
    return (
        <div className={styles.loader}>
            <Spinner animation="grow" variant={'primary'} className={'mb-2'} />
            <div>درحال بارگزاری...</div>
        </div>
    );
}
