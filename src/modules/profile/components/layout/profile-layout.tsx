import React, { useEffect, useState } from 'react';
import styles from 'modules/profile/styles/layout/layout.module.scss';
import { Col, Container, Row } from 'react-bootstrap';
import Menu from 'modules/profile/components/layout/menu';
import Header from 'modules/profile/components/layout/header';
import { sessionStorageVars } from 'utils/storage-vars';
import CandidatesSubHeader from './candidates-sub-header';

export default function ProfileLayout(props: { header; main; footer; children?; toast }) {
    const [menuIsOpen, setMenuIsopen] = useState(false);

    useEffect(() => {
        const open = sessionStorage.getItem(sessionStorageVars.menuIsOpen);
        if (!open || open === 'false') {
            sessionStorage.setItem(sessionStorageVars.menuIsOpen, 'false');
            setMenuIsopen(false);
        } else {
            setMenuIsopen(open === 'true');
        }
    }, []);

    return (
        <>
            <div className={styles.profile}>
                <Container>
                    <Row>
                        <Col lg={3}>
                            <Menu menuIsOpen={menuIsOpen} setMenuIsOpen={setMenuIsopen} />
                        </Col>
                        <Col lg={9}>
                            <Header menuIsOpen={menuIsOpen} setMenuIsOpen={setMenuIsopen} />
                            <CandidatesSubHeader />
                            <div className={styles.main}>
                                {props.header}
                                {props.main}
                                {props.children}
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
            {props.footer}
            {props.toast}
        </>
    );
}
