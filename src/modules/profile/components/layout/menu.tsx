import React, { useContext, useState } from 'react';
import Link from 'next/link';
import styles from '../../styles/layout/menu.module.scss';
import {
    BoxArrowLeft,
    ChevronDown,
    ChevronUp,
    CreditCardFill,
    HouseDoorFill,
    LaptopFill,
    PersonFill,
    Gear,
    AwardFill,
} from 'react-bootstrap-icons';
import { Accordion, Image } from 'react-bootstrap';
import classNames from 'classnames';
import apiLogout from 'modules/auth/api/api-logout';
import { toast } from 'react-toastify';
import { useRouter } from 'next/router';
import { AuthContext } from 'contexts/auth-context';
import MenuLinkButton from './menu-link-button';
import { RoleEnum } from 'models/role';
import apiAdmin from 'modules/profile/api/api-admin';

export default function Menu(props: { menuIsOpen: boolean; setMenuIsOpen: any }) {
    const router = useRouter();
    const [finance, setFinance] = useState(() => {
        return router.pathname === '/profile/credit' || router.pathname === '/profile/transactions';
    });
    const [marketing, setMarketing] = useState(() => {
        return router.pathname === '/profile/webinar-marketing' || router.pathname === '/profile/brandpage-marketing';
    });
    const [requests, setRequests] = useState(false);

    const authContext = useContext(AuthContext);

    const logout = async () => {
        const result = await apiLogout();

        if (result.status === 'error') {
            toast.error(result.message || 'خطایی در هنگام خروج رخ داد');
        } else {
            authContext.changeIsLoggedIn(null);
            toast.success('شما با موفقیت از سیستم خارج شدید.');
            router.push('/');
        }
    };

    const onClickGoToAdmin = (event) => {
        const element = event.target;
        if (element.hasAttribute('disabled')) {
            return;
        }
        toast.info('لطفا کمی صبر کنید ...');
        element.setAttribute('disabled', true);

        apiAdmin()
            .then((response) => {
                if (response) {
                    router.push(response).catch(() => {
                        element.removeAttribute('disabled');
                    });
                } else {
                    element.removeAttribute('disabled');
                }
            })
            .catch((error) => {
                element.removeAttribute('disabled');
            });
    };

    const activeAccordion = (): string => {
        if (finance) return 'finance';
        else if (marketing) return 'marketing';
        return null;
    };

    return (
        <>
            <div
                className={classNames(styles.overlay, props.menuIsOpen ? styles['is-open'] : null)}
                onClick={() => {
                    props.setMenuIsOpen(false);
                }}
            />
            <div className={classNames(styles.container, props.menuIsOpen ? styles['is-open'] : null)}>
                <div className={styles.menu}>
                    <div className={styles['top-bg-circle']} />
                    <div className={styles.head}>
                        <Link href={'/'}>
                            <a title={'صفحه اصلی'} className={styles['logo-link']}>
                                <Image src={'/images/logos/asanseminar-logo.svg'} className={styles.logo} />
                            </a>
                        </Link>
                        <div className={styles.title}>حساب کاربری</div>
                    </div>
                    <ul className={styles.nav}>
                        <Accordion defaultActiveKey={activeAccordion()}>
                            <li className={styles.item}>
                                <MenuLinkButton path="/" Icon={HouseDoorFill} text="خانه" />
                            </li>
                            {authContext.user &&
                            (authContext.user.role === RoleEnum.ADMIN ||
                                authContext.user.role === RoleEnum.SUPPORT ||
                                authContext.user.role === RoleEnum.ORGANIZATION_MANAGER ||
                                authContext.user.role === RoleEnum.ORGANIZATION_SUPPORT ||
                                authContext.user.role === RoleEnum.MANAGER) ? (
                                <li className={styles.item}>
                                    <span className={styles.link}>
                                        <Gear className={styles['item-icon']} />
                                        <span className={classNames(styles['item-text'])} onClick={onClickGoToAdmin}>
                                            مدیریت
                                        </span>
                                    </span>
                                </li>
                            ) : null}
                            <li className={styles.item}>
                                <MenuLinkButton path="/profile/webinars" Icon={LaptopFill} text="کلاس ها و جلسات" />
                            </li>
                            <li className={styles.item}>
                                <MenuLinkButton path="/profile/settings" Icon={PersonFill} text="اطلاعات شخصی" />
                            </li>
                            <li className={styles['super-item']}>
                                <Accordion.Toggle
                                    eventKey="finance"
                                    className={classNames(
                                        styles['accordion-toggle'],
                                        finance ? styles['toggle-down'] : null,
                                    )}
                                    onClick={() => {
                                        setFinance((prevState) => !prevState);
                                        marketing ? setMarketing((prevState) => !prevState) : null;
                                        requests ? setRequests((prevState) => !prevState) : null;
                                    }}
                                >
                                    <CreditCardFill className={styles['item-icon']} />
                                    <span className={styles['item-text']}>بخش مالی</span>
                                    {finance ? (
                                        <ChevronUp className={styles['chevron']} />
                                    ) : (
                                        <ChevronDown className={styles['chevron']} />
                                    )}
                                </Accordion.Toggle>
                                <Accordion.Collapse eventKey="finance" className={styles['accordion-collapse']}>
                                    <ul className={styles['sub-items']}>
                                        <li className={styles.item}>
                                            <MenuLinkButton path="/profile/credit" text="اعتبار" />
                                        </li>
                                        <li className={styles.item}>
                                            <MenuLinkButton path="/profile/transactions" text="تراکنش ها" />
                                        </li>
                                    </ul>
                                </Accordion.Collapse>
                            </li>

                            <li className={styles['super-item']}>
                                <Accordion.Toggle
                                    eventKey="marketing"
                                    className={classNames(
                                        styles['accordion-toggle'],
                                        marketing ? styles['toggle-down'] : null,
                                    )}
                                    onClick={() => {
                                        setMarketing((prevState) => !prevState);
                                        finance ? setFinance((prevState) => !prevState) : null;
                                        requests ? setRequests((prevState) => !prevState) : null;
                                    }}
                                >
                                    <AwardFill className={styles['item-icon']} />
                                    <span className={styles['item-text']}>پنل سفیری</span>
                                    {marketing ? (
                                        <ChevronUp className={styles['chevron']} />
                                    ) : (
                                        <ChevronDown className={styles['chevron']} />
                                    )}
                                </Accordion.Toggle>
                                <Accordion.Collapse eventKey="marketing" className={styles['accordion-collapse']}>
                                    <ul className={styles['sub-items']}>
                                        <li className={styles.item}>
                                            <MenuLinkButton path="/profile/webinar-marketing" text="فروش وبینار" />
                                        </li>
                                        <li className={styles.item}>
                                            <MenuLinkButton path="/profile/brandpage-marketing" text="فروش برند پیج" />
                                        </li>
                                    </ul>
                                </Accordion.Collapse>
                            </li>
                            {/*<li className={styles['super-item']}>
                                <Accordion.Toggle
                                    eventKey="1"
                                    className={classNames(
                                        styles['accordion-toggle'],
                                        requests ? styles['toggle-down'] : null,
                                    )}
                                    onClick={() => {
                                        setRequests((prevState) => !prevState);
                                        finance ? setFinance((prevState) => !prevState) : null;
                                    }}
                                >
                                    <ChatDotsFill className={styles['item-icon']} />
                                    <span className={styles['item-text']}>درخواست ها</span>
                                    {requests ? (
                                        <ChevronUp className={styles['chevron']} />
                                    ) : (
                                        <ChevronDown className={styles['chevron']} />
                                    )}
                                </Accordion.Toggle>
                                <Accordion.Collapse eventKey="1" className={styles['accordion-collapse']}>
                                    <ul className={styles['sub-items']} />
                                </Accordion.Collapse>
                            </li>*/}
                            <li className={styles.item}>
                                <div className={styles.link} onClick={() => logout()}>
                                    <BoxArrowLeft className={styles['item-icon']} />
                                    <span className={styles['item-text']}>خروج</span>
                                </div>
                            </li>
                        </Accordion>
                    </ul>
                    <div className={styles['bottom-bg-circle']} />
                </div>
            </div>
        </>
    );
}
