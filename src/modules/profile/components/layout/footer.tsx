import React from 'react';
import styles from '../../styles/layout/footer.module.scss';
import { Envelope, Telephone } from 'react-bootstrap-icons';
import toPersianNum from 'utils/to-persian-num';

export default function Footer() {
    return (
        <div className={styles.footer}>
            <div className={styles.right}>
                <div className={styles['call-text']}>شماره تماس</div>
                <div className={styles['other-text']}>هنگام برگزاری رویدادها در کنار شما هستیم</div>
            </div>
            <div className={styles.left}>
                <div className={styles.email}>
                    <div className={styles['email-text']}>
                        <span>ایمیل: </span>
                        <a href="mailto:support@asanseminar.ir" className={styles['email-link']}>
                            support@asanseminar.ir
                        </a>
                    </div>
                    <Envelope className={styles.icon} />
                </div>
                <div className={styles.phone}>
                    <div className={styles['phone-text']}>{toPersianNum('991107030')}</div>
                    <Telephone className={styles.icon} />
                </div>
            </div>
        </div>
    );
}
