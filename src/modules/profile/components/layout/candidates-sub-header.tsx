import React, { useEffect, useState } from 'react';
import styles from '../../styles/layout/header.module.scss';
import apiUserCandidates from '../../api/api-user-candidates';
import { Button } from 'react-bootstrap';
import apiBrandpagePayRetry from 'modules/candidates/api/api-brandpage-pay-retry';
import { useRouter } from 'next/router';
import classNames from 'classnames';

export default function CandidatesSubHeader() {
    const [secret, setSecret] = useState('');
    const [message, setMessage] = useState('');

    const router = useRouter();

    useEffect(() => {
        getCandidates();
    }, []);

    const getCandidates = async () => {
        const result = await apiUserCandidates();
        if (result) {
            setSecret(result.data.candidateSecret);
            setMessage(result.message);
        }
    };

    const clickPayCandidate = async (event) => {
        const button: any = event.target;
        button.setAttribute('disabled', true);

        const result = await apiBrandpagePayRetry({ secret: secret });
        if (result.status === 'data') {
            router.push(result.data);
        } else {
            button.removeAttribute('disabled');
        }
    };

    return message ? (
        <div className={classNames(styles.header, styles['candidates-message-container'])}>
            <div className={styles['header-text']}>{message}</div>
            {secret ? (
                <Button variant="primary" onClick={clickPayCandidate}>
                    پرداخت
                </Button>
            ) : null}
        </div>
    ) : null;
}
