import React, { useEffect, useState } from 'react';
import styles from '../../styles/layout/header.module.scss';
import { Bell, List } from 'react-bootstrap-icons';
import useWindowDimensions from 'utils/use-window-dimensions';
import { sessionStorageVars } from 'utils/storage-vars';
import AddCredit from '../add-credit';
import apiGetCredit from 'modules/profile/api/api-get-credit';
import toPrice from 'utils/to-price';

export default function Header(props: { menuIsOpen: boolean; setMenuIsOpen: any }) {
    const { width } = useWindowDimensions();
    const [credit, setCredit] = useState(0);
    const [loading, setLoading] = useState(false);
    const [showModal, setShowModal] = useState(false);

    useEffect(() => {
        getCredit();
    }, []);

    const getCredit = async () => {
        setLoading(true);
        const result = await apiGetCredit(false);
        if (result.status === 'data') setCredit(result.data.credit);
        setLoading(false);
    };

    return (
        <div className={styles.header}>
            {width < 992 ? (
                <List
                    className={styles['menu-icon']}
                    onClick={() => {
                        props.setMenuIsOpen((prev) => !prev);
                        if (props.menuIsOpen) {
                            sessionStorage.setItem(sessionStorageVars.menuIsOpen, 'false');
                        } else {
                            sessionStorage.setItem(sessionStorageVars.menuIsOpen, 'true');
                        }
                    }}
                />
            ) : null}
            <div className={styles.credit}>
                <div className={styles['my-credit']}>
                    <div className={styles['my-credit-text']}>اعتبار حساب من:</div>
                    <div className={styles['my-credit-count']}>{loading ? '...' : `${toPrice(credit)} تومان`}</div>
                </div>
                <div onClick={() => setShowModal(true)} className={styles.increase}>
                    + افزایش اعتبار
                </div>
            </div>
            <Bell className={styles.notification} />
            <AddCredit showModal={showModal} setShowModal={setShowModal} />
        </div>
    );
}
