import React from 'react';
import styles from '../../styles/layout/menu.module.scss';
import classNames from 'classnames';
import Link from 'next/link';
import { useRouter } from 'next/router';

export default function MenuLinkButton(props: { path: string; Icon?; text: string }) {
    const router = useRouter();

    return (
        <Link href={props.path}>
            <a className={styles.link}>
                {props.Icon ? <props.Icon className={styles['item-icon']} /> : null}
                <span className={classNames(styles['item-text'], router.pathname === props.path ? styles.active : '')}>
                    {props.text}
                </span>
            </a>
        </Link>
    );
}
