import React from 'react';
import Link from 'next/link';
import { IOrder, OrderStatusEnum } from 'models/orders';
import styles from '../styles/order-item.module.scss';
import { Image, Row, Col } from 'react-bootstrap';
import { CheckCircleFill, XCircleFill } from 'react-bootstrap-icons';
import classNames from 'classnames';

export default function OrderItem(props: { data: IOrder }) {
    const cn = classNames;

    const renderStatus = () => {
        const statusTextElement = <div className={styles['status-text']}>{props.data.status_fa}</div>;
        if (props.data.status === OrderStatusEnum.PAID) {
            return (
                <div className={cn(styles.status, styles['status-success'])}>
                    <CheckCircleFill className={styles['status-icon']} />
                    {statusTextElement}
                </div>
            );
        } else if (props.data.status === OrderStatusEnum.FAILED) {
            return (
                <div className={cn(styles.status, styles['status-fail'])}>
                    <XCircleFill className={styles['status-icon']} />
                    {statusTextElement}
                </div>
            );
        } else {
            return <div className={cn(styles.status, styles['status-neutral'])}>{statusTextElement}</div>;
        }
    };

    return (
        <div className={styles['order-item']}>
            <Row className={styles.row}>
                <Col sm={4}>
                    <div className={styles.title}>
                        <Image
                            src={props.data.webinar.organization.logo && props.data.webinar.organization.logo.url}
                            alt={props.data.webinar.organization.title}
                            className={styles['order-logo']}
                        />
                        <div className={styles['order-title']}>
                            <Link href={`/webinars/${props.data.webinar.id}/${props.data.webinar.title}`}>
                                <a title={props.data.webinar.title}>{props.data.webinar.title}</a>
                            </Link>
                        </div>
                    </div>
                </Col>
                <Col sm={4}>{renderStatus()}</Col>
                <Col sm={4}>
                    <Link href={`/webinars/${props.data.webinar.id}/${props.data.webinar.title}`}>
                        <a title={props.data.webinar.title} className={styles.link}>
                            مشاهده کلاس
                        </a>
                    </Link>
                </Col>
            </Row>
        </div>
    );
}
