import React, { useEffect, useState } from 'react';
import styles from '../styles/change-password.module.scss';
import { Button, Form, Modal, Spinner } from 'react-bootstrap';
import apiUserPasswordUpdate from '../api/api-user-password-update';
import { useForm } from 'react-hook-form';
import { EyeFill, EyeSlashFill } from 'react-bootstrap-icons';

type TInputs = {
    password: string;
    password_confirmation: string;
};

export default function ChangePassword(props: { showModal: boolean; setShowModal: any }) {
    const [loading, setLoading] = useState(false);
    const [passShow, setPassShow] = useState(false);
    const [passRepeatShow, setPassRepeatShow] = useState(false);

    const { register, handleSubmit, errors, getValues } = useForm<TInputs>();

    // useEffect(() => {
    //     if (!props.showModal) {
    //         setNewPass('');
    //         setNewPassRepeat('');
    //     }
    // }, [props.showModal]);

    const submitPassword = async (data: { password: string; password_confirmation: string }) => {
        setLoading(true);
        apiUserPasswordUpdate(data)
            .then((response) => {
                if (response) {
                    props.setShowModal(false);
                }
            })
            .finally(() => {
                setLoading(false);
            });
    };
    return (
        <Modal
            centered
            show={props.showModal}
            onHide={() => {
                props.setShowModal(false);
                setLoading(false);
            }}
        >
            <Modal.Header className={styles['modal-header']}>
                <Modal.Title className={styles['modal-title']}>تغییر رمز عبور</Modal.Title>
            </Modal.Header>
            <Modal.Body className={styles['modal-body']}>
                <Form onSubmit={handleSubmit(submitPassword)} className={styles.form}>
                    <Form.Group className={styles['form-group']}>
                        <Form.Label className={styles.label}>رمز عبور جدید</Form.Label>
                        <div className={styles['pass-input']}>
                            <Form.Control
                                name="password"
                                type={passShow ? 'text' : 'password'}
                                className={styles.input}
                                ref={register({
                                    required: 'رمز عبور جدید را وارد کنید!',
                                    minLength: {
                                        value: 8,
                                        message: 'رمز عبور باید حداقل هشت کاراکتر باشد!',
                                    },
                                })}
                            />
                            <div className={styles.eye}>
                                {passShow ? (
                                    <EyeSlashFill onClick={() => setPassShow(!passShow)} />
                                ) : (
                                    <EyeFill onClick={() => setPassShow(!passShow)} />
                                )}
                            </div>
                        </div>
                        {errors.password && <div className={styles.error}>{errors.password.message}</div>}
                    </Form.Group>
                    <Form.Group className={styles['form-group']}>
                        <Form.Label className={styles.label}>تکرار رمز عبور جدید</Form.Label>
                        <div className={styles['pass-input']}>
                            <Form.Control
                                name="password_confirmation"
                                type={passRepeatShow ? 'text' : 'password'}
                                className={styles.input}
                                ref={register({
                                    required: 'تکرار رمز عبور جدید را وارد کنید!',
                                    minLength: {
                                        value: 8,
                                        message: 'تکرار رمز عبور باید حداقل هشت کاراکتر باشد!',
                                    },
                                    validate: (fieldValue) => {
                                        return (
                                            fieldValue === getValues().password ||
                                            'رمز عبور وارد شده و تکرار آن یکسان نمی باشند'
                                        );
                                    },
                                })}
                            />
                            <div className={styles.eye}>
                                {passRepeatShow ? (
                                    <EyeSlashFill onClick={() => setPassRepeatShow(!passRepeatShow)} />
                                ) : (
                                    <EyeFill onClick={() => setPassRepeatShow(!passRepeatShow)} />
                                )}
                            </div>
                        </div>
                        {errors.password_confirmation && (
                            <div className={styles.error}>{errors.password_confirmation.message}</div>
                        )}
                    </Form.Group>
                    <div className={styles.buttons}>
                        <Button
                            variant="secondary"
                            onClick={() => {
                                props.setShowModal(false);
                                setLoading(false);
                            }}
                            className={styles['cancel-button']}
                        >
                            انصراف
                        </Button>
                        {loading ? (
                            <Button className={styles['edit-button']}>
                                <Spinner animation={'border'} style={{ width: '1.3rem', height: '1.3rem' }} />
                            </Button>
                        ) : (
                            <Button type="submit" className={styles['edit-button']}>
                                تایید
                            </Button>
                        )}
                    </div>
                </Form>
            </Modal.Body>
        </Modal>
    );
}
