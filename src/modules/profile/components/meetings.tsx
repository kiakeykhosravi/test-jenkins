import React, { useState } from 'react';
import styles from '../styles/meetings.module.scss';
import { IMeeting } from 'models/meeting';
import MeetingItem from './meeting-item';
import Pagination from 'modules/shared/components/pagination';
import ProfileTableLoading from './profile-table-loading';
import apiMeetings from '../api/api-meetings';
import { toast } from 'react-toastify';

export default function Meetings(props: { meetings: IMeeting[]; lastPage: number }) {
    const [allMeetings, setAllMeetings] = useState<IMeeting[][]>([props.meetings]);
    const [meetings, setMeetings] = useState<IMeeting[]>(props.meetings);
    const [lastPage, setLastPage] = useState(props.lastPage);
    const [currentPage, setCurrentPage] = useState(1);
    const [loading, setLoading] = useState(false);

    const handlePageClick = (data) => {
        setCurrentPage(data.selected + 1);
        if (allMeetings[data.selected]) {
            setMeetings(allMeetings[data.selected]);
        } else {
            setLoading(true);
            apiMeetings(false, data.selected + 1)
                .then((response) => {
                    if (response) {
                        const am = allMeetings;
                        am[data.selected] = response.meetings;
                        setAllMeetings(am);
                        setMeetings(response.meetings);
                        setLastPage(response.lastpage);
                    }
                })
                .catch(() => {
                    toast.error('مشکلی در دریافت اطلاعات به وجود آمد!');
                })
                .finally(() => {
                    setLoading(false);
                });
        }
    };

    const renderMeetingsItem = () => {
        return meetings.map((meeting, index) => {
            return <MeetingItem data={meeting} key={index} />;
        });
    };

    return (
        <div className={meetings.length ? styles.meetings : null}>
            <div className={styles.point}>در این قسمت فقط جلسات چند ساعت اخیر و چند روز آینده نمایش داده می شوند</div>
            {meetings.length ? (
                <>
                    <div className={styles.items}>{renderMeetingsItem()}</div>
                    {lastPage > 1 ? (
                        <Pagination pageCount={lastPage} page={String(currentPage)} handlePageClick={handlePageClick} />
                    ) : null}
                    {loading ? <ProfileTableLoading /> : null}
                </>
            ) : (
                <div className={styles.title}>شما در چند ساعت آینده هیچ جلسه ای ندارید</div>
            )}
        </div>
    );
}
