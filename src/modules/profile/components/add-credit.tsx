import React, { useState } from 'react';
import styles from '../styles/add-credit.module.scss';
import { Button, Form, Modal, Spinner } from 'react-bootstrap';
import { toast } from 'react-toastify';
import toPersianNum from 'utils/to-persian-num';
import apiAddCredit from '../api/api-add-credit';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import toEnglishNum from 'utils/to-english-num';

type TInputs = {
    amount: string;
};

// TODO: To Spaceman: Review this more thoroughly
export default function AddCredit(props: { showModal: boolean; setShowModal: any }) {
    const [loading, setLoading] = useState(false);
    const { register, handleSubmit, errors } = useForm<TInputs>();

    async function gateWay(data: { amount: string }) {
        data.amount = toEnglishNum(data.amount);
        if (Number(data.amount) > 99) {
            setLoading(true);
            const addCreditResponse = await apiAddCredit(data.amount);
            if (addCreditResponse.status === 'data') {
                window.location.href = addCreditResponse.data.link;
            }
            setLoading(false);
        } else toast.error(toPersianNum('حداقل اعتبار باید 100 تومان باشد'));
    }
    return (
        <Modal
            centered
            show={props.showModal}
            onHide={() => {
                props.setShowModal(false);
                setLoading(false);
            }}
        >
            <Form onSubmit={handleSubmit(gateWay)}>
                <Modal.Header className={styles['modal-header']}>
                    <Modal.Title className={styles['modal-title']}>افزایش اعتبار</Modal.Title>
                </Modal.Header>
                <Modal.Body className={styles['modal-body']}>
                    <div className={styles.form}>
                        <Form.Label className={styles.label}>مقدار</Form.Label>
                        <Form.Control
                            type="text"
                            className={classNames(styles.input, errors.amount ? styles['border-red'] : '')}
                            name="amount"
                            ref={register({
                                required: 'مقدار افزایش اعتبار را وارد کنید!',
                                pattern: {
                                    value: /^([0-9]|([۰۱۲۳۴۵۶۷۸۹]))*$/,
                                    message: 'مقدار افزایش اعتبار باید به عدد باشد!',
                                },
                            })}
                        />
                        <div className={styles.tag}>تومان</div>
                    </div>
                    {errors.amount && <div className={styles.error}>{errors.amount.message}</div>}
                </Modal.Body>
                <Modal.Footer className={styles['modal-footer']}>
                    <Button
                        variant="secondary"
                        onClick={() => {
                            props.setShowModal(false);
                            setLoading(false);
                        }}
                        className={styles['cancel-button']}
                    >
                        انصراف
                    </Button>
                    {loading ? (
                        <Button className={styles['payment-button']}>
                            <Spinner animation={'border'} style={{ width: '1.5rem', height: '1.5rem' }} />
                        </Button>
                    ) : (
                        <Button type="submit" className={styles['payment-button']}>
                            پرداخت
                        </Button>
                    )}
                </Modal.Footer>
            </Form>
        </Modal>
    );
}
