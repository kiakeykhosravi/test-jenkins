import React from 'react';
import styles from '../styles/meeting-item.module.scss';
import { Image, Row, Col, Button } from 'react-bootstrap';
import { ChevronLeft } from 'react-bootstrap-icons';
import { IMeeting, MeetingStatusEnum } from 'models/meeting';
import apiEnterMeeting from 'modules/course/api/api-enter-meeting';
import { useRouter } from 'next/router';
import Link from 'next/link';

export default function MeetingItem(props: { data: IMeeting }) {
    const router = useRouter();

    const renderStatus = () => {
        if (props.data.can_enter) {
            return (
                <Button
                    variant="success"
                    className={styles.enter}
                    onClick={(event) => {
                        const element: any = event.target;
                        if (element.hasAttribute('disabled')) {
                            return;
                        }
                        element.setAttribute('disabled', true);

                        apiEnterMeeting(props.data.id)
                            .then((response) => {
                                if (response) {
                                    router.push(response);
                                } else {
                                    element.removeAttribute('disabled');
                                }
                            })
                            .catch((error) => {
                                element.removeAttribute('disabled');
                            });
                    }}
                >
                    ورود
                </Button>
            );
        } else if (props.data.status === MeetingStatusEnum.FINISHED) {
            return <div className={styles.finished}>{props.data.status_fa}</div>;
        } else if (props.data.status === MeetingStatusEnum.CANCELED) {
            return <div className={styles.canceled}>{props.data.status_fa}</div>;
        } else if (props.data.status === MeetingStatusEnum.PLANNED) {
            return <div className={styles.planned}>{props.data.status_fa}</div>;
        } else if (props.data.status === MeetingStatusEnum.RUNNING) {
            return <div className={styles.running}>{props.data.status_fa}</div>;
        }
    };

    return (
        <div className={styles['meeting-item']}>
            <Row className={styles.row}>
                <Col sm={4}>
                    <div className={styles.title}>
                        <Image
                            src={props.data.webinar.organization.logo && props.data.webinar.organization.logo.url}
                            alt={props.data.webinar.organization.title}
                            className={styles['meeting-logo']}
                        />
                        <Link href={`/webinars/${props.data.webinar.id}`}>
                            <a title={props.data.webinar.title}>
                                <div className={styles['meeting-title']}>{props.data.webinar.title}</div>
                            </a>
                        </Link>
                        <ChevronLeft className={styles['chevron-style']} />
                    </div>
                </Col>
                <Col sm={3}>
                    <div className={styles.subject}>{props.data.title}</div>
                </Col>
                <Col sm={3}>
                    <div className={styles.date}>
                        <div className={styles['date-text']}>
                            <span className={styles['date-text-date']}>{props.data.datetime_fadate}</span>
                            <span className={styles['date-text-time']}>{props.data.datetime_fatime}</span>
                        </div>
                    </div>
                </Col>
                <Col sm={2}>{renderStatus()}</Col>
            </Row>
        </div>
    );
}
