import React, { useRef, useState } from 'react';
import { IWebinarCompact } from 'models/webinar-compact';
import styles from '../styles/marketing-webinar-card.module.scss';
import Link from 'next/link';
import AsanImage from 'modules/shared/components/asan-image';
import { Card, Col, Row, Modal, Table } from 'react-bootstrap';
import classNames from 'classnames';
import { Cash, GraphUp, Link45deg, PeopleFill, X } from 'react-bootstrap-icons';
import toPersianNum from 'utils/to-persian-num';
import { toast } from 'react-toastify';
import { IUser } from 'models/user';
import toPrice from 'utils/to-price';
import { IMarketingStatsWebinar } from '../responses';
import { IUserSafe } from 'models/user-safe';
import apiUserWebinarMarketingSalesUsersList from '../api/api-user-webinar-marketing-sales-users-list';

export default function MarketingWebinarCard(props: {
    webinar: IWebinarCompact;
    userMarketingStats: IMarketingStatsWebinar[];
    user: IUser;
    basePath: string;
    cardClasses?: string;
    bodyClasses?: string;
    imgClasses?: string;
}) {
    const cn = classNames;
    const pageUrlElementRef = useRef(null);
    const [showModal, setShowModal] = useState(false);
    const [statsLoading, setStatsLoading] = useState(false);
    const [webinarSellShare, setWebinarSellShare] = useState(0);
    const [webinarSellCount, setWebinarSellCount] = useState(0);
    const [subSet, setSubSet] = useState<IUserSafe[]>([]);
    const webinarLink = `/webinars/${props.webinar.id}/${props.webinar.title.trim().replace(/\s+/g, '-')}`;

    const onclickCopyLink = () => {
        pageUrlElementRef.current.select();
        document.execCommand('copy');
        pageUrlElementRef.current.blur();
        toast.success('لینک معرف با موفقیت کپی شد');
    };

    const showStats = async (webinar_id) => {
        setStatsLoading(true);
        if (props.userMarketingStats.length) {
            props.userMarketingStats.map((item) => {
                if (item.webinar.id === webinar_id) {
                    setWebinarSellShare(item.marketing_summary.share);
                    setWebinarSellCount(item.marketing_summary.count);
                }
            });
        }
        const usersListResponse = await apiUserWebinarMarketingSalesUsersList(false, webinar_id);
        if (usersListResponse) {
            setSubSet(usersListResponse.users);
            setStatsLoading(false);
            setShowModal(true);
        } else setStatsLoading(false);
    };

    const renderSubset = () => {
        return subSet.map((s, index) => {
            return (
                <tr key={index}>
                    <td>{toPersianNum(s.id)}</td>
                    <td>{s.fullname}</td>
                </tr>
            );
        });
    };

    return (
        <Card className={cn(styles.card, props.cardClasses)}>
            <Link href={webinarLink}>
                <a title={props.webinar.title}>
                    <AsanImage
                        src={props.webinar.media && props.webinar.media.length && props.webinar.media[0].thumbnail_url}
                        className={cn(styles['card-img'], props.imgClasses)}
                        alt={props.webinar.title}
                    />
                </a>
            </Link>
            <Card.Body className={cn(styles['card-body'], props.bodyClasses)}>
                <div className={styles.main}>
                    <h3>
                        <Link href={webinarLink}>
                            <a className={styles.title} title={props.webinar.title}>
                                {props.webinar.title}
                            </a>
                        </Link>
                    </h3>
                    <div className={styles.price}>
                        <div className={styles['price-text']}>
                            <span>{`${toPrice(props.webinar.price_with_off)} `}</span>
                            <span style={{ fontSize: 'smaller' }}>تومان</span>
                        </div>
                        <div className={styles['price-percent']}>
                            {`${toPersianNum(props.webinar.marketer_percent || 0)}%`}
                            <span>سهم سفیر</span>
                        </div>
                    </div>
                </div>
                <Row className={styles.footer}>
                    <Col>
                        <div onClick={onclickCopyLink} className={styles['card-btn']}>
                            <Link45deg className={styles.icon} />
                            لینک معرف
                            <input
                                type="text"
                                value={`${props.basePath}/webinars/${props.webinar.id}?rf=${props.user.id}`}
                                className={styles['hidden-page-link']}
                                ref={pageUrlElementRef}
                            />
                        </div>
                    </Col>
                    <Col>
                        <button
                            className={styles['card-btn']}
                            disabled={statsLoading}
                            onClick={() => showStats(props.webinar.id)}
                        >
                            {statsLoading ? (
                                <span>لطفا صبر کنید...</span>
                            ) : (
                                <>
                                    <GraphUp className={styles.icon} />
                                    آمار فروش
                                </>
                            )}
                        </button>
                    </Col>
                </Row>
            </Card.Body>
            <Modal
                centered
                show={showModal}
                onHide={() => setShowModal(false)}
                dialogClassName={styles['modal-dialog']}
                contentClassName={styles.modal}
            >
                <Modal.Body className={styles['modal-body']}>
                    <div className={styles['modal-stats']}>
                        <GraphUp className={styles['modal-stats-icon']} />
                    </div>
                    <div className={styles['modal-close']} onClick={() => setShowModal(false)}>
                        <X className={styles['modal-close-icon']} />
                    </div>
                    <div className={styles['modal-webinar']}>
                        <AsanImage
                            src={
                                props.webinar.media &&
                                props.webinar.media.length &&
                                props.webinar.media[0].thumbnail_url
                            }
                            className={styles['modal-webinar-img']}
                            alt={props.webinar.title}
                        />
                        <Link href={`/webinars/${props.webinar.id}/${props.webinar.title.trim().replace(/\s+/g, '-')}`}>
                            <a className={styles['modal-webinar-title']} title={props.webinar.title}>
                                {props.webinar.title}
                            </a>
                        </Link>
                    </div>
                    <div className={styles['modal-stats-main']}>
                        <div className={styles['modal-stats-main-section']}>
                            <Cash className={styles['modal-icon']} />
                            <div className={styles['modal-stats-text']}>
                                <span>{`${toPrice(Number(webinarSellShare))} `}</span>
                                <span style={{ fontSize: 'smaller' }}>تومان</span>
                            </div>
                        </div>
                        <div className={styles['modal-stats-main-section']}>
                            <PeopleFill className={styles['modal-icon']} />
                            <div className={styles['modal-stats-text']}>
                                <span>{`${toPrice(webinarSellCount)} `}</span>
                                <span style={{ fontSize: 'smaller' }}>نفر</span>
                            </div>
                        </div>
                    </div>
                    {subSet.length ? (
                        <Table borderless striped className={styles.table}>
                            <thead className={styles['table-head']}>
                                <tr>
                                    <th>#</th>
                                    <th>نام و نام خانوادگی</th>
                                </tr>
                            </thead>
                            <tbody>{renderSubset()}</tbody>
                        </Table>
                    ) : null}
                </Modal.Body>
            </Modal>
        </Card>
    );
}
