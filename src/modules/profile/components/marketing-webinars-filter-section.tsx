import React from 'react';
import styles from '../styles/marketing-webinars-filter-section.module.scss';
import { Col, Form, Row } from 'react-bootstrap';
import classNames from 'classnames';

export default function MarketingWebinarsFilterSection(props: { filters; filterUpdate?; setLoading? }) {
    const cn = classNames;

    const onChangeSortHandler = (
        event: any,
        type: 'date' | 'sortDate' | 'sortPrice' | 'sortMarketerPercent' | 'sortMarketingAmount',
    ) => {
        if (props.filterUpdate) {
            const data = `{"${type}": "${event.target.selectedOptions[0].value}"}`;
            props.setLoading && props.setLoading(true);
            props.filterUpdate(JSON.parse(data));
        }
    };

    return (
        <Form className={styles.form}>
            <div className={styles.selects}>
                <div className={styles['filters-main-label']}>مرتب سازی و فیلتر ها:</div>
                <Row>
                    <Col sm={6} className={styles['filter-container']}>
                        <span className={styles['filter-label']}>زمان وبینار: </span>
                        <Form.Control
                            as="select"
                            id="date"
                            className={cn('form-select', styles['select'])}
                            onChange={(e) => onChangeSortHandler(e, 'date')}
                            value={props.filters.date}
                        >
                            <option value="">انتخاب کنید</option>
                            <option value="year-1">یک سال اخیر</option>
                            <option value="month-6">۶ ماه اخیر</option>
                            <option value="month-3">۳ ماه اخیر</option>
                            <option value="month-1">یک ماه اخیر</option>
                            <option value="week-2">دو هفته اخیر</option>
                            <option value="week-1">هفته اخیر</option>
                            <option value="day-3">۳ روز اخیر</option>
                            <option value="day-1">یک روز قبل</option>
                            <option value="today">امروز</option>
                        </Form.Control>
                    </Col>

                    <Col sm={6} className={styles['filter-container']}>
                        <span className={styles['filter-label']}>تاریخ: </span>
                        <Form.Control
                            as="select"
                            id="sortDate"
                            className={cn('form-select', styles['select'])}
                            onChange={(e) => onChangeSortHandler(e, 'sortDate')}
                            value={props.filters.sortDate}
                        >
                            <option value="">انتخاب کنید</option>
                            <option value="desc">تازه ترین</option>
                            <option value="asc">قدیمی ترین</option>
                        </Form.Control>
                    </Col>

                    <Col sm={6} className={styles['filter-container']}>
                        <span className={styles['filter-label']}>مبلغ: </span>
                        <Form.Control
                            as="select"
                            id="sortPrice"
                            className={cn('form-select', styles['select'])}
                            onChange={(e) => onChangeSortHandler(e, 'sortPrice')}
                            value={props.filters.sortPrice}
                        >
                            <option value="">انتخاب کنید</option>
                            <option value="desc">بیشترین قیمت</option>
                            <option value="asc">کمترین قیمت</option>
                        </Form.Control>
                    </Col>

                    <Col sm={6} className={styles['filter-container']}>
                        <span className={styles['filter-label']}>سهم سفیر: </span>
                        <Form.Control
                            as="select"
                            id="sortMarketerPercent"
                            className={cn('form-select', styles['select'])}
                            onChange={(e) => onChangeSortHandler(e, 'sortMarketerPercent')}
                            value={props.filters.sortMarketerPercent}
                        >
                            <option value="">انتخاب کنید</option>
                            <option value="desc">بیشترین درصد</option>
                            <option value="asc">کمترین درصد</option>
                        </Form.Control>
                    </Col>

                    {/*<Col sm={6} className={styles['filter-container']}>*/}
                    {/*    <span className={styles['filter-label']}>درآمد سفیر: </span>*/}
                    {/*    <Form.Control*/}
                    {/*        as="select"*/}
                    {/*        id="sortMarketingAmount"*/}
                    {/*        className={cn('form-select', styles['select'])}*/}
                    {/*        onChange={(e) => onChangeSortHandler(e, 'sortMarketingAmount')}*/}
                    {/*        value={props.filters.sortMarketingAmount}*/}
                    {/*    >*/}
                    {/*        <option value="">انتخاب کنید</option>*/}
                    {/*        <option value="desc">بیشترین مقدار</option>*/}
                    {/*        <option value="asc">کمترین مقدار</option>*/}
                    {/*    </Form.Control>*/}
                    {/*</Col>*/}
                </Row>
            </div>
        </Form>
    );
}
