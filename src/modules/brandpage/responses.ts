import { IPublicMarketer } from 'models/public-marketer';

export interface IPublicMarketerResponse {
    marketer: IPublicMarketer;
}
