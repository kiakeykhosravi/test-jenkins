import React from 'react';
import BrandpageSlider from './brandpage-slider';

export default function UserStoriesSection(props: { slides }) {
    return <BrandpageSlider type={'userStory'} slides={props.slides} title={'کسانی که به ما اعتماد کردند'} />;
}
