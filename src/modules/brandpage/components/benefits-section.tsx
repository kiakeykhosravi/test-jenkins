import React, { useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import styles from '../styles/benefits-section.module.scss';
import Link from 'next/link';
import classNames from 'classnames';

export default function BenefitsSection() {
    const [showTop, setShowTop] = useState(true);

    useEffect(() => {
        const interval = setInterval(() => {
            setShowTop(Math.random() < 0.5);
        }, 4000);
        return () => clearInterval(interval);
    }, []);

    const checkIcon = <img src="/images/brandpage/bp-check.png" alt="تیک" className={styles['item-icon']} />;

    return (
        <Container>
            <Row className={styles.row}>
                <h2 className={classNames(styles.title, styles['main-title'])}>مزایا و منافع برندپیج</h2>
                <Col lg={6} className={styles['right-col']}>
                    <h2 className={styles.title}>مزایا و منافع برندپیج</h2>
                    <div className={styles.item}>
                        {checkIcon}
                        <p className={styles['item-text']}>
                            به دلیل محدودیت تعداد تولید و عرضه برندپیج ها، رقبای شما دیده نمیشوند، فقط شما هستید.
                        </p>
                    </div>
                    <div className={styles.item}>
                        {checkIcon}
                        <p className={styles['item-text']}>قیمت های شگفت انگیز و استثنایی خدمات</p>
                    </div>
                    <div className={styles.item}>
                        {checkIcon}
                        <p className={styles['item-text']}>قدرت بسیار بالا در رقابت</p>
                    </div>
                    <div className={styles.item}>
                        {checkIcon}
                        <p className={styles['item-text']}>سریع، آسان و با هزینه بسیار مناسب</p>
                    </div>
                    <div className={styles.item}>
                        {checkIcon}
                        <p className={styles['item-text']}>آموزش کامل و مداوم برای نحوه استفاده</p>
                    </div>
                    <Link href="/brandpage/plans" passHref>
                        <a className={styles['plans-btn']} title="مشاهده پلن ها">
                            مشاهده پلن ها
                        </a>
                    </Link>
                </Col>
                <Col lg={6} className={styles['left-col']}>
                    <img
                        src="/images/brandpage/bp-benefits-section-2.png"
                        alt="صفحه برندپیج"
                        className={styles.image}
                    />
                    <img
                        src="/images/brandpage/bp-benefits-section-1.png"
                        alt="صفحه برندپیج"
                        className={classNames(styles.image, showTop ? '' : styles['transparent'])}
                    />
                </Col>
            </Row>
        </Container>
    );
}
