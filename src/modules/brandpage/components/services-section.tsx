import React from 'react';
import styles from '../styles/services-section.module.scss';
import { Col, Container, Row } from 'react-bootstrap';

export default function ServicesSection() {
    return (
        <Container>
            <Row className={styles.row}>
                <Col md={6} xl={3} className={styles.col}>
                    <div className={styles.item}>
                        <img src="/images/brandpage/bp-services-1.png" alt="سرویس" className={styles.image} />
                        <p className={styles.text} title="دیجیتال مارکتینگ">
                            تیم متخصص و حرفه ای برای ساخت کارت ویزیت دیجیتال و برند سازی کسب و کار شما
                        </p>
                    </div>
                </Col>
                <Col md={6} xl={3} className={styles.col}>
                    <div className={styles.item}>
                        <img src="/images/brandpage/bp-services-2.png" alt="سرویس" className={styles.image} />
                        <p className={styles.text} title="صفحه اول گوگل">
                            تضمین صفحه اول گوگل با عنوان برند شما در کمتر از یک ماه
                        </p>
                    </div>
                </Col>
                <Col md={6} xl={3} className={styles.col}>
                    <div className={styles.item}>
                        <img src="/images/brandpage/bp-services-3.png" alt="سرویس" className={styles.image} />
                        <p className={styles.text} title="پشتیبانی سایت">
                            پشتیبانی و مشاوره اختصاصی و بی نظیر
                        </p>
                    </div>
                </Col>
                <Col md={6} xl={3} className={styles.col}>
                    <div className={styles.item}>
                        <img src="/images/brandpage/bp-services-4.png" alt="سرویس" className={styles.image} />
                        <p className={styles.text} title="عودت وجه">
                            ضمانت صد در صد عودت وجه در صورت عدم رضایت
                        </p>
                    </div>
                </Col>
            </Row>
        </Container>
    );
}
