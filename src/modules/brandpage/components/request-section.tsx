import React, { useState } from 'react';
import styles from '../styles/request-section.module.scss';
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { useForm } from 'react-hook-form';
import toEnglishNum from 'utils/to-english-num';
import { CandidateTypeEnum } from 'models/candidate';
import apiRegisterCandidate from 'modules/candidates/api/api-register-candidate';
import { toast } from 'react-toastify';
import classNames from 'classnames';

export default function RequestSection() {
    const cn = classNames;
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [succeed, setSucceed] = useState(false);
    const [succeedMessage, setSucceedMessage] = useState('با موفقیت انجام شد!');
    const { handleSubmit, errors, register } = useForm();

    const onSubmitForm = async (data) => {
        setIsSubmitting(true);
        const formData = {
            owner_name: data.owner_name,
            mobile_number: [toEnglishNum(data.whatsapp)],
            email: data.email,
            instagram: data.instagram,
            custom_specialty: data.custom_specialty,
            name: data.name,
            type: CandidateTypeEnum.SUBSCRIPTION,
        };
        const response = await apiRegisterCandidate(formData);
        if (response.status === 'data') {
            setIsSubmitting(false);
            setSucceed(true);
            setSucceedMessage(response.message);
        } else {
            setIsSubmitting(false);
            toast.warning(response.message);
        }
    };

    const renderInput = (name: string, placeHolder: string, icon: string, required?: boolean) => {
        return (
            <div className={styles['form-input-container']}>
                <Form.Control
                    name={name}
                    type="text"
                    placeholder={placeHolder + ` ${required ? '*' : ''}`}
                    className={cn(styles['form-input'], errors[name] ? styles.error : '')}
                    ref={
                        required
                            ? register({
                                  required: `${placeHolder} الزامی است.`,
                              })
                            : register()
                    }
                />
                <img src={icon} alt="آیکون" className={styles['form-input-icon']} />
            </div>
        );
    };

    return (
        <Container className={styles.container}>
            <div className={styles.info}>
                <img src="/images/brandpage/info-circle.png" alt="آیکون اطلاعات" className={styles['info-icon']} />
                <p className={styles['info-text']}>
                    برای خرید برندپیج شما می توانید فرم مشاوره را پر کنید یا مستقیما با مشاورین ما تماس بگیرید.
                </p>
            </div>
            <div className={styles['form-section']}>
                <h3 className={styles['form-title']}>فرم مشاوره</h3>
                <Form onSubmit={handleSubmit(onSubmitForm)}>
                    <Row>
                        <Col md={6} className={styles['form-col']}>
                            {renderInput('owner_name', 'نام و نام خانوادگی', '/images/brandpage/user-icon.png', true)}
                            {renderInput(
                                'whatsapp',
                                'شماره تماس همراه یا واتساپ',
                                '/images/brandpage/calling-icon.png',
                                true,
                            )}
                            {renderInput('email', 'ایمیل', '/images/brandpage/email-icon.png')}
                        </Col>
                        <Col md={6} className={styles['form-col']}>
                            {renderInput(
                                'instagram',
                                'صفحه اینستاگرام کسب و کار شما',
                                '/images/brandpage/instagram-icon.png',
                            )}
                            {renderInput('custom_specialty', 'زمینه کسب و کار شما', '/images/brandpage/bag-icon.png')}
                            {renderInput('name', 'نام کسب و کار شما', '/images/brandpage/bag-icon.png')}
                        </Col>
                    </Row>
                    <Button
                        type="submit"
                        className={styles['submit-btn']}
                        {...(isSubmitting ? { disabled: true } : {})}
                    >
                        {isSubmitting ? 'در حال ارسال ...' : 'ارسال فرم'}
                    </Button>
                </Form>
                {succeed ? <p className={styles.succeed}>{succeedMessage}</p> : null}
            </div>
        </Container>
    );
}
