import React from 'react';
import styles from '../styles/brandpage-slider.module.scss';
import { Container } from 'react-bootstrap';
import classNames from 'classnames';
import Slider from 'modules/shared/components/slider';
import { SwiperSlide } from 'swiper/react';
import UserStoryCard from './user-story-card';
import BlogCard from './blog-card';
import useSwiperRef from 'utils/use-swiper-ref';

export default function BrandpageSlider(props: { type: 'blog' | 'userStory'; slides; title: string }) {
    const [paginationEl, paginationRef] = useSwiperRef();

    const renderSlides = (slides) => {
        if (props.type === 'userStory') {
            return slides.map((slide, index) => {
                return (
                    <SwiperSlide className={styles.slide} key={index}>
                        <UserStoryCard data={slide} />
                    </SwiperSlide>
                );
            });
        }
        return slides.map((slide, index) => {
            return (
                <SwiperSlide className={styles.slide} key={index}>
                    <BlogCard data={slide} />
                </SwiperSlide>
            );
        });
    };

    const breakpoints = {
        320: {
            slidesPerView: 1,
            spaceBetween: 30,
        },
        768: {
            slidesPerView: 2,
            spaceBetween: 30,
        },
        1200: {
            slidesPerView: 3,
            spaceBetween: 30,
        },
    };

    return (
        <Container className={styles.main}>
            <h3 className={styles.title}>{props.title}</h3>
            <div className={styles['card-container']} dir="ltr">
                <Slider
                    spaceBetween={30}
                    navigationOptions={{
                        navigation: {
                            nextEl: '.' + styles['slider-btn-next'],
                            prevEl: '.' + styles['slider-btn-prev'],
                        },
                    }}
                    paginationOptions={{
                        pagination: {
                            el: paginationEl,
                            clickable: true,
                        },
                    }}
                    breakpoints={breakpoints}
                    swiperClasses={styles['swiper-main']}
                    autoplayOptions={{
                        autoplay: {
                            delay: 20000,
                        },
                    }}
                >
                    {renderSlides(props.slides)}
                </Slider>
                <div className={classNames('slider-btn-next', styles['slider-btn-next'])} />
                <div className={classNames('slider-btn-prev', styles['slider-btn-prev'])} />
                <div className={styles.pagination} ref={paginationRef} />
            </div>
        </Container>
    );
}
