import React from 'react';
import styles from '../styles/user-story-card.module.scss';
import { Card } from 'react-bootstrap';
import toPersianNum from 'utils/to-persian-num';

export default function UserStoryCard(props: { data }) {
    return (
        <Card className={styles.card} dir="rtl">
            <div className={styles['card-header']}>
                <img src={props.data.src} alt={props.data.title} className={styles.logo} />
            </div>
            <Card.Body className={styles['card-body']}>
                <p className={styles.description}>{props.data.description}</p>
            </Card.Body>
            <div className={styles['card-footer']}>
                <div className={styles['user-info']}>
                    <img src={props.data.user_logo} alt={props.data.user_fullname} className={styles['user-img']} />
                    <div>
                        <div className={styles['user-info-name']}>{props.data.user_fullname}</div>
                        <div className={styles['user-info-role']}>{props.data.user_role}</div>
                    </div>
                </div>
                <div className={styles['date']}>
                    <img src="/images/brandpage/calendar.png" alt="تقویم" className={styles['date-icon']} />
                    <span className={styles['date-text']}>{toPersianNum(props.data.date)}</span>
                </div>
            </div>
        </Card>
    );
}
