import React from 'react';
import styles from '../styles/banner-section.module.scss';
import { Col, Row } from 'react-bootstrap';
import Link from 'next/link';
import AsanImage from 'modules/shared/components/asan-image';
import classNames from 'classnames';

export default function BannerSection() {
    const Icons = () => {
        const icons = [];
        for (let i = 1; i < 7; i++) {
            icons.push(
                <img
                    src={`images/brandpage/brandpage-icon-${i}.png`}
                    className={classNames(styles.icon, styles[`icon-${i}`])}
                    alt={'آیکون'}
                />,
            );
        }
        icons.push(
            <img
                src={`images/brandpage/brandpage-icon-3.png`}
                className={classNames(styles.icon, styles[`icon-7`])}
                alt={'آیکون'}
            />,
            <img
                src={`images/brandpage/brandpage-icon-4.png`}
                className={classNames(styles.icon, styles[`icon-8`])}
                alt={'آیکون'}
            />,
        );
        return icons;
    };
    return (
        <div className={styles.main}>
            <div className={styles.container}>
                <Row className={styles.row}>
                    <Col lg={6} className={styles['right-col']}>
                        <h1 className={styles.title}>
                            برندپیج، جدیدترین ابزار بازاریابی اینترنتی، دیجیتال مارکتینگ و کارت ویزیت الکترونیکی
                        </h1>
                        <p className={styles.description}>
                            یک جایگاه ویژه و حرفه ای با تکنولوژی روز در صفحه اول گوگل برای کسب و کارتان داشته باشید
                        </p>
                        <Link href="/brandpage/plans" passHref>
                            <a className={styles['plans-btn']} title="مشاهده پلن ها">
                                مشاهده پلن ها
                            </a>
                        </Link>
                    </Col>
                    <Col lg={6} className={styles['left-col']}>
                        <AsanImage
                            src={'images/brandpage/brandpage-top-banner-image.png'}
                            alt={'برندپیج'}
                            className={styles.image}
                        />
                        {Icons()}
                    </Col>
                </Row>
            </div>
        </div>
    );
}
