import React from 'react';
import styles from '../styles/build-steps-section.module.scss';
import { Col, Container, Row } from 'react-bootstrap';
import toPersianNum from 'utils/to-persian-num';

export default function BuildStepsSection() {
    const renderStep = (step: number, text: string) => {
        return (
            <div className={styles.item}>
                <img src={`/images/brandpage/bp-plans-step${step}.png`} alt={text} className={styles['item-icon']} />
                <div className={styles['item-step']}>{toPersianNum(step)}</div>
                <div className={styles['item-text']}>{text}</div>
            </div>
        );
    };
    return (
        <Container className={styles.container}>
            <h1 className={styles.title}>مراحل ساخت برندپیج</h1>
            <Row className={styles.row}>
                <Col md={6} lg={3}>
                    {renderStep(1, 'مشاهده پلن ها و انتخاب پلن مورد نظر')}
                </Col>
                <Col md={6} lg={3}>
                    {renderStep(2, 'تکمیل فرم مشاوره یا تماس با مشاورین')}
                </Col>
                <Col md={6} lg={3}>
                    {renderStep(3, 'بررسی از سمت مشاورین و ارسال فرم ثبت نام برندپیج برای شما')}
                </Col>
                <Col md={6} lg={3}>
                    {renderStep(4, 'تکمیل فرم اطلاعات و ساخت برندپیج')}
                </Col>
            </Row>
        </Container>
    );
}
