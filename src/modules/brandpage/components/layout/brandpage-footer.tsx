import React from 'react';
import styles from '../../styles/brandpage-footer.module.scss';

export default function BrandpageFooter() {
    return <div className={styles.footer}>قدرت گرفته از آسان سمینار</div>;
}
