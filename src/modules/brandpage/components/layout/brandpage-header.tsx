import React, { useState } from 'react';
import styles from '../../styles/brandpage-header.module.scss';
import { Container, Nav, Navbar } from 'react-bootstrap';
import Link from 'next/link';
import { useRouter } from 'next/router';
import classNames from 'classnames';
import { List, XCircle } from 'react-bootstrap-icons';

export default function BrandpageHeader() {
    return (
        <>
            <DesktopHeader />
            <MobileHeader />
        </>
    );
}

function DesktopHeader() {
    const router = useRouter();
    return (
        <Container fluid={true} className={styles['header-container']}>
            <Navbar className={styles.header}>
                <Link href="/brandpage" passHref>
                    <Navbar.Brand href="/brandpage">
                        <img
                            alt="لوگو برندپیج"
                            src="/images/logos/brandpage-logo.jpg"
                            className={styles['header-logo']}
                        />
                    </Navbar.Brand>
                </Link>
                <Nav className="align-items-center flex-grow-1 position-relative">
                    <Link href="/brandpage" passHref>
                        <a
                            className={classNames(
                                styles['header-item'],
                                router.pathname === '/brandpage' ? styles.selected : '',
                            )}
                            title="صفحه اصلی برندپیج"
                        >
                            خانه
                        </a>
                    </Link>
                    <Link href="/brandpage/plans" passHref>
                        <a
                            className={classNames(
                                styles['header-item'],
                                router.pathname === '/brandpage/plans' ? styles.selected : '',
                            )}
                            title="پلن های برندپیج"
                        >
                            پلن های برندپیج
                        </a>
                    </Link>
                    <Link href="/organizations/brandpage/blog" passHref>
                        <a
                            className={classNames(
                                styles['header-item'],
                                router.pathname === '/brandpage/blog' ? styles.selected : '',
                            )}
                            title="وبلاگ"
                        >
                            وبلاگ
                        </a>
                    </Link>
                    {/*<div className={styles['nav-left']}>*/}
                    {/*    <a href="#" className={styles['login-btn']}>*/}
                    {/*        ورود به برندپیج*/}
                    {/*    </a>*/}
                    {/*</div>*/}
                </Nav>
            </Navbar>
        </Container>
    );
}

function MobileHeader() {
    const router = useRouter();
    const [expand, setExpand] = useState(false);
    return (
        <div className={classNames(styles['mobile-header'], expand ? styles.expand : '')}>
            <div className={styles['mobile-nav-top']}>
                <Link href="/brandpage" passHref>
                    <Navbar.Brand href="/brandpage">
                        <img
                            alt="لوگو برندپیج"
                            src="/images/logos/brandpage-logo.jpg"
                            className={styles['mobile-header-logo']}
                        />
                    </Navbar.Brand>
                </Link>
                {expand ? (
                    <XCircle className={styles['mobile-nav-icon']} onClick={() => setExpand(!expand)} />
                ) : (
                    <List className={styles['mobile-nav-icon']} onClick={() => setExpand(!expand)} />
                )}
            </div>
            <div className={classNames(styles['mobile-nav-collapse'], expand ? styles.expand : '')}>
                <Link href="/brandpage" passHref>
                    <a
                        className={classNames(
                            styles['mobile-header-item'],
                            router.pathname === '/brandpage' ? styles.selected : '',
                        )}
                        title="صفحه اصلی برندپیج"
                    >
                        صفحه اصلی
                    </a>
                </Link>
                <Link href="/brandpage/plans" passHref>
                    <a
                        className={classNames(
                            styles['mobile-header-item'],
                            router.pathname === '/brandpage/plans' ? styles.selected : '',
                        )}
                        title="پلن های برندپیج"
                    >
                        پلن های برندپیج
                    </a>
                </Link>
                <Link href="/organizations/brandpage/blog" passHref>
                    <a
                        className={classNames(
                            styles['mobile-header-item'],
                            router.pathname === '/brandpage/blog' ? styles.selected : '',
                        )}
                        title="وبلاگ"
                    >
                        وبلاگ
                    </a>
                </Link>
                {/*<div className={styles['mobile-login']}>*/}
                {/*    <a href="#" className={styles['login-btn']}>*/}
                {/*        ورود به برندپیج*/}
                {/*    </a>*/}
                {/*</div>*/}
            </div>
        </div>
    );
}
