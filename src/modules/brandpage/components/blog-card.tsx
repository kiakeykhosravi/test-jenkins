import React from 'react';
import styles from '../styles/blog-card.module.scss';
import { Card } from 'react-bootstrap';
import toPersianNum from 'utils/to-persian-num';
import { IBlog } from 'models/blog';
import Link from 'next/link';

export default function BlogCard(props: { data: IBlog }) {
    return (
        <Card className={styles.card} dir="rtl">
            <div className={styles['card-header']}>
                <Link href={`/organizations/brandpage/blog/${props.data.id}`}>
                    <a title={props.data.title}>
                        <img src={props.data.media[0].thumbnail_url} alt={props.data.title} className={styles.banner} />
                    </a>
                </Link>
            </div>
            <Card.Body className={styles['card-body']}>
                <div className={styles.info}>
                    <div className={styles.author}>
                        <img src="/images/brandpage/profile.png" alt="نویسنده" className={styles['user-icon']} />
                        <span className={styles['user-text']}>تیم محتوا آسان سمینار</span>
                    </div>
                    <div className={styles['date']}>
                        <img src="/images/brandpage/calendar-2.png" alt="تقویم" className={styles['date-icon']} />
                        <span className={styles['date-text']}>{toPersianNum(props.data.created_at_fadate)}</span>
                    </div>
                </div>
                <h3 className={styles.title}>
                    <Link href={`/organizations/brandpage/blog/${props.data.id}`}>
                        <a title={props.data.title} className={styles.title}>
                            {props.data.title}
                        </a>
                    </Link>
                </h3>
                <p className={styles.description}>{props.data.description}</p>
                <div className={styles['card-footer']}>
                    <Link href={`/organizations/brandpage/blog/${props.data.id}`}>
                        <a title={props.data.title} className={styles.more}>
                            <span className={styles['more-text']}>مشاهده کامل</span>
                            <img
                                src="/images/brandpage/arrow-left.png"
                                alt="جهت به چپ"
                                className={styles['more-icon']}
                            />
                        </a>
                    </Link>
                </div>
            </Card.Body>
        </Card>
    );
}
