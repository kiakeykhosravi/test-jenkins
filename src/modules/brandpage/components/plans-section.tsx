import React from 'react';
import styles from '../styles/plans-section.module.scss';
import { Col, Row } from 'react-bootstrap';
import toPrice from 'utils/to-price';
import classNames from 'classnames';
import toPersianNum from 'utils/to-persian-num';

const plansPrices = {
    premium: 1800000,
    premiumToPlus: 4180000,
    plus: 5980000,
    plusToGold: 5120000,
    gold: 11000000,
    goldToPlatinum: 9000000,
    platinum: 20000000,
};

export default function PlansSection() {
    const renderPlanItems = (type: 'premium' | 'plus' | 'golden' | 'platinum') => {
        let items = [];
        const premiumItems = [
            'کارت ویزیت الکترونیک',
            'کاتالوگ ساده ولی حرفه ای دیجیتال',
            'لینک اختصاصی و دائمی',
            'قابلیت ویرایش نامحدود',
            'قابل جستجو در گوگل',
            'قیمت به صرفه و استثنایی',
            'پشتیبانی اختصاصی',
        ];
        const plusItems = [
            'قابلیت سئو کل صفحه',
            'تضمین صفحه اول گوگل با عنوان برند شما',
            '100 جایگاه برای متن و پوستر',
            'قابلیت مشاهده تعداد بازدیدها',
            'امکان رقابت هدفمند در کلمات کلیدی',
            'نوشتن سه متن تبلیغی ویژه برای محصولات و خدمات',
            'تحقیق کلمات کلیدی اولیه برای برند شما',
            'طراحی سه پوستر برای معرفی محصولات و خدمات',
            'آموزش مقدماتی بهینه سازی موتور جستجو (سئو)',
            'پشتیبانی اختصاصی تا یک سال',
            'تحلیل رقبا توسط تیم حرفه ای و ارائه گزارش',
        ];
        const goldenItems = [
            'تمامی امکانات برندپیچ پریمیوم و پلاس',
            'امکان ضبط و بازپخش ویدیو',
            'وبلاگ اختصاصی بسیار حرفه ای',
            'امکان بارگزاری ویدیو',
            'امکان تعامل با مشتریان با افزونه گفتینو',
            'فضای ذخیره سازی',
            'داشبورد مدیریت',
            'آموزش و مشاوره نحوه استفاده و کاربرد',
            'پشتیبانی اختصاصی',
        ];
        const platinumItems = [
            'فروش محصولات و خدمات از طریق وبینار مارکتینگ',
            'امکان مشارکت در فروش (افیلیت مارکتینگ)',
            'امکان استفاده از پکیج های تبلیغات و فروش',
            'امکان هم افزایی با سایر پنل ها',
            'امکان مدیریت آنلاین پرسنل و تیم',
            'یک لینک معتبر',
            'کال سنتر اختصاصی',
            'داشبورد حرفه ای برای نظارت و مدیریت کسب و کار',
            'آموزش های ویژه بروز مارکتینگ',
            'مشاوره با بهترین ها برای رشد کسب و کار',
        ];

        const renderPlanItem = (item, index) => {
            return (
                <div className={styles['plan-item']} key={index}>
                    <img
                        src={
                            type === 'plus' ? '/images/brandpage/bp-check.png' : '/images/brandpage/bp-check-black.png'
                        }
                        alt="آیکون تیک"
                        className={classNames(
                            styles['plan-item-icon'],
                            type === 'golden' || type === 'platinum' ? styles['plan-item-icon-gray'] : '',
                        )}
                    />
                    <p
                        className={classNames(
                            styles['plan-item-text'],
                            type === 'premium'
                                ? styles['premium-item-text']
                                : type === 'plus'
                                ? styles['plus-item-text']
                                : styles['golden-item-text'],
                        )}
                    >
                        {toPersianNum(item)}
                    </p>
                </div>
            );
        };
        switch (type) {
            case 'premium':
                items = premiumItems;
                break;
            case 'plus':
                items = plusItems;
                break;
            case 'golden':
                items = goldenItems;
                break;
            case 'platinum':
                items = platinumItems;
        }
        return items.map((item, index) => {
            return renderPlanItem(item, index);
        });
    };
    const renderPremiumCard = () => {
        return (
            <div className={classNames(styles['plan-card'], styles.premium)}>
                <h2 className={classNames(styles['plan-header'], styles['premium-header'])}>برندپیج پریمیوم</h2>
                <div className={styles['plan-body']}>
                    <h3 className={styles['plan-slogan']}>شناسنامه شما در دنیای دیجیتال</h3>
                    <div className={styles['plan-items']}>{renderPlanItems('premium')}</div>
                    <div className={styles['plan-bottom-section']}>
                        <div className={styles['plan-footer']}>
                            <div className={styles['plan-footer-br']} />
                            <div className={styles['plan-footer-price-label']}>
                                <span>قیمت خرید مستقیم: </span>
                                <span className={styles['plan-footer-price-label-tag']}>(یک سال)</span>
                            </div>
                            <div className={styles['plan-footer-price-text']}>
                                <span>{toPrice(plansPrices.premium)}</span>
                                <span className={styles['plan-footer-price-text-tag']}> تومان</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
    const renderPlusCard = () => {
        return (
            <div className={classNames(styles['plan-card'], styles.plus)}>
                <div className={classNames(styles['plan-header'], styles['plus-header'])}>برندپیج پلاس</div>
                <div className={styles['plan-body']}>
                    <h3 className={styles['plan-slogan']}>گذرنامه شما به دنیای دیجیتال</h3>
                    <div className={styles['plan-items']}>{renderPlanItems('plus')}</div>
                    <div className={styles['plan-bottom-section']}>
                        <div className={styles['plan-footer']} style={{ borderRadius: '0' }}>
                            <div className={styles['plan-footer-br']} />
                            <div className={styles['plan-footer-price-label']}>
                                <span>قیمت خرید مستقیم: </span>
                                <span className={styles['plan-footer-price-label-tag']}>(یک سال)</span>
                            </div>
                            <div className={styles['plan-footer-price-text']}>
                                <span>{toPrice(plansPrices.plus)}</span>
                                <span className={styles['plan-footer-price-text-tag']}> تومان</span>
                            </div>
                        </div>
                        <div className={styles['plan-footer']}>
                            <div className={styles['plan-footer-br']} />
                            <div className={styles['plan-footer-price-label']}>
                                <span>قیمت ارتقا از برندپیج پریمیوم: </span>
                                <span className={styles['plan-footer-price-label-tag']}>(یک سال)</span>
                            </div>
                            <div
                                className={classNames(
                                    styles['plan-footer-price-text'],
                                    styles['plan-footer-price-text-black'],
                                )}
                            >
                                <span>{toPrice(plansPrices.premiumToPlus)}</span>
                                <span className={styles['plan-footer-price-text-tag']}> تومان</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
    const renderGoldenCard = (type: 'golden' | 'platinum') => {
        return (
            <div className={classNames(styles['plan-card'], styles.golden)}>
                <div className={classNames(styles['plan-header'], styles['golden-header'])}>
                    {type === 'golden' ? 'برندپیج گلدن' : 'برندپیج پلاتینیوم'}
                </div>
                <div className={styles['plan-body']}>
                    <div className={styles['plan-items']}>{renderPlanItems(type)}</div>
                    <div className={styles['plan-bottom-section']}>
                        <div className={styles['golden-footer-br']} />
                        <div className={classNames(styles['plan-footer'], styles['golden-footer'])}>
                            <div className={styles['golden-footer-text']}>به زودی ...</div>
                        </div>
                    </div>
                </div>
            </div>
        );
    };
    return (
        <div className={styles.container}>
            <div className={styles.mobile}>
                <h2 className={styles.title}>پلن ها</h2>
                <Row>
                    <Col md={6}>{renderPremiumCard()}</Col>
                    <Col md={6}>{renderPlusCard()}</Col>
                    <Col md={6}>{renderGoldenCard('golden')}</Col>
                    <Col md={6}>{renderGoldenCard('platinum')}</Col>
                </Row>
            </div>
            <div className={styles.desktop}>
                <div className={styles.plans}>
                    <div className={styles['main-plans']}>
                        <div>{renderPremiumCard()}</div>
                        <div>{renderPlusCard()}</div>
                    </div>
                    <div className={styles['other-plans']}>
                        <div>{renderGoldenCard('golden')}</div>
                        <div>{renderGoldenCard('platinum')}</div>
                    </div>
                </div>
            </div>
        </div>
    );
}
