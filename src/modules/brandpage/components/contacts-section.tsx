import React from 'react';
import styles from '../styles/contact-section.module.scss';
import { Col, Container, Row } from 'react-bootstrap';
import toPersianNum from 'utils/to-persian-num';
import { BrandpageContactInfoType } from '../types';

export default function ContactsSection(props: { data: BrandpageContactInfoType }) {
    const addIranCodeToNumber = (number: string) => {
        if (number[0] !== '+') {
            number = number[0] === '0' ? number.replace('0', '+98') : '+98' + number;
        }
        return number;
    };

    const renderContactElement = (href: string, title: string, icon: string, text: string) => {
        return (
            <div className={styles['contact-item']}>
                <a href={href} title={title} target="_blank" rel="noreferrer" className={styles['contact-item-link']}>
                    <img src={icon} alt={text} className={styles['contact-item-icon']} />
                    <div className={styles['contact-item-text']}>{toPersianNum(text)}</div>
                </a>
            </div>
        );
    };

    const renderContacts = () => {
        if (!props.data) {
            return (
                <Row>
                    <Col md={12}>
                        {renderContactElement(
                            'tel:+989059907032',
                            'شماره همراه',
                            '/images/brandpage/contact-calling-icon.png',
                            '09059907032',
                        )}
                    </Col>
                </Row>
            );
        }

        const totalContactCount =
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            props.data.mobiles.length + !!props.data.whatsapp + !!props.data.instagram; // + !!props.data.name;

        return (
            <Row className={totalContactCount === 1 ? 'justify-content-center' : ''}>
                {props.data.mobiles.map((mobile, index) => {
                    return (
                        <Col md={6} key={index}>
                            {renderContactElement(
                                'tel:' + addIranCodeToNumber(mobile),
                                'شماره همراه',
                                '/images/brandpage/contact-calling-icon.png',
                                mobile,
                            )}
                        </Col>
                    );
                })}
                {props.data.whatsapp ? (
                    <Col md={6}>
                        {renderContactElement(
                            'whatsapp://send?phone=' + addIranCodeToNumber(props.data.whatsapp),
                            'واتسپ',
                            '/images/brandpage/contact-whatsapp-icon.png',
                            props.data.whatsapp,
                        )}
                    </Col>
                ) : null}
                {props.data.instagram ? (
                    <Col md={6}>
                        {renderContactElement(
                            'https://instagram.com/' + props.data.instagram,
                            'اینستاگرام',
                            '/images/brandpage/instagram-gold.svg',
                            'اینستاگرام',
                        )}
                    </Col>
                ) : null}
            </Row>
        );
    };

    return (
        <Container className={styles.container}>
            <div className={styles['contact-section']}>
                <h3 className={styles['contact-title']}>ارتباط با مشاورین آسان سمینار</h3>
                {renderContacts()}
            </div>
        </Container>
    );
}
