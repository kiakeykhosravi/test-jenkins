import React, { useState } from 'react';
import { Accordion, Container, useAccordionToggle } from 'react-bootstrap';
import styles from '../styles/faq-section.module.scss';
import { faqQuestionType } from '../types';

export default function FAQSection(props: { data: faqQuestionType[] }) {
    const [itemKey, setItemKey] = useState('item-0');

    const renderItems = () => {
        return props.data.map((item: faqQuestionType, index) => {
            return <FAQItem item={item} index={index} key={index} itemKey={itemKey} setItemKey={setItemKey} />;
        });
    };

    return (
        <Container>
            <h3 className={styles.title}>سوالات متداول</h3>
            <Accordion defaultActiveKey="item-0" className={styles.accordion}>
                {renderItems()}
            </Accordion>
        </Container>
    );
}

function FAQItem(props: { item: faqQuestionType; index; itemKey; setItemKey }) {
    function CustomToggle({ children, eventKey }) {
        const decoratedOnClick = useAccordionToggle(eventKey, () => {
            if (props.itemKey !== eventKey) props.setItemKey(eventKey);
            else props.setItemKey('none');
        });
        const isCurrentEventKey = props.itemKey === eventKey;
        return (
            <button type="button" className={styles.toggle} onClick={decoratedOnClick}>
                <div className={styles.question}>
                    {children}
                    {isCurrentEventKey ? (
                        <img src="/images/brandpage/arrow-up.png" alt="آیکون بالا" />
                    ) : (
                        <img src="/images/brandpage/arrow-down.png" alt="آیکون پایین" />
                    )}
                </div>
            </button>
        );
    }

    return (
        <div className={styles.item}>
            <CustomToggle eventKey={`item-${props.index}`}>{props.item.text}</CustomToggle>
            <Accordion.Collapse eventKey={`item-${props.index}`} className={styles.collapse}>
                <div className={styles.answer}>{props.item.answer}</div>
            </Accordion.Collapse>
        </div>
    );
}
