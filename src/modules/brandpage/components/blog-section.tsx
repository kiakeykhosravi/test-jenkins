import React from 'react';
import BrandpageSlider from './brandpage-slider';

export default function BlogSection(props: { slides }) {
    return <BrandpageSlider type={'blog'} slides={props.slides} title={'وبلاگ'} />;
}
