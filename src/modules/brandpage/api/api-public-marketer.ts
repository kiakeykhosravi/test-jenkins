import apiRequest from 'api/api-request';
import { IPublicMarketerResponse } from '../responses';
import endpoints from 'api/api-endpoints';
import { toast } from 'react-toastify';

export default async function ApiPublicMarketer(
    marketerId,
): Promise<{
    data?: IPublicMarketerResponse;
    status: 'error' | 'data';
    message?: string;
}> {
    const url = endpoints.frontend.public_marketer.replace('{public_marketer}', marketerId);
    const response = await apiRequest<IPublicMarketerResponse>('get', url);

    if (response.status === 'data') {
        return {
            data: response.data,
            status: 'data',
        };
    }

    toast.error(response.message || 'خطایی در دریافت اطلاعات معرف رخ داد. مطمئن شوید کد معرف معتیر می باشد');
    return {
        status: 'error',
        message: response.message,
    };
}
