import { faqQuestionType } from './types';

const faqQuestions: faqQuestionType[] = [
    {
        text: 'کارت ویزیت الکترونیک چیست؟',
        answer:
            'کارت های کسب و کار الکترونیکی در اصل "کارت" های دیجیتالی هستند که می توانید در تلفن های هوشمند خود ایجاد کنید و آن را از طریق کد QR یا برند پیج خود به تلفن مخاطب خود یا مستقیماً به ایمیل و پیامک او ارسال کنید. این بخشی از ایجاد یک پورتفولیو آنلاین و نمونه کارها برای نام تجاری و شخصی شما است.',
    },
    {
        text: 'چرا باید صفحه اول گوگل باشیم؟',
        answer:
            'با ۱۶۷ میلیارد جستجو در ماه ، قرار گرفتن در صفحه اول گوگل مانند این است که کسب و کار خود را مانند یک فروشگاه بزرگ در شلوغ ترین جاده شهر انجام دهید. هرچه تعداد بیشتری از افراد وب سایت شما را ببینند، افراد بیشتری با نام تجاری شما آشنا می شوند و در نتیجه فروش و کسب و کار شما رونق بیشتری خواهد داشت.',
    },
];

const schemas = {
    faqSchema: (() => {
        const $questions = faqQuestions.map((question: faqQuestionType) => {
            return {
                '@type': 'Question',
                name: question.text,
                acceptedAnswer: {
                    '@type': 'Answer',
                    text: question.answer,
                },
            };
        });

        return `
          {
            "@context": "https://schema.org",
            "@type": "FAQPage",
            "mainEntity": ${JSON.stringify($questions)}
          }`;
    })(),
    landingWebsiteSchema: `
    {
      "@context": "https://schema.org",
      "@type": "WebSite",
      "name": "آسان سمینار",
      "url": "https://asanseminar.ir"
    }`,
};

export { faqQuestions, schemas };
