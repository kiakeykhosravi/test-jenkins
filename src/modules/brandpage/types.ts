export type faqQuestionType = {
    text: string;
    answer: string;
};

export type BrandpageContactInfoType = {
    name?: string;
    mobiles?: string[];
    whatsapp?: string;
    instagram?: string;
    email?: string;
};
