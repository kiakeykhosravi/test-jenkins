import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IOrganizationResponse } from '../responses';

export default async function apiOrganization(
    slug: string,
): Promise<{
    data?: IOrganizationResponse;
    status: 'error' | 'data';
}> {
    const url = endpoints.backend.organization.replace('{organization_slug}', slug);
    const result = await apiRequest<IOrganizationResponse>('get', url);

    if (result.status === 'data') {
        return {
            data: result.data,
            status: result.status,
        };
    }

    return {
        status: 'error',
    };
}
