import React from 'react';
import styles from '../styles/organization-sidebar.module.scss';
import { Image } from 'react-bootstrap';
import toPersianNum from 'utils/to-persian-num';
import { IOrganization } from 'models/organization';
import Link from 'next/link';
import classNames from 'classnames';

export default function OrganizationSidebar(props: { organization: IOrganization }) {
    const contactSection = () => {
        let phoneNumbers = props.organization.phone_number;
        if (!Array.isArray(phoneNumbers)) {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            phoneNumbers = phoneNumbers ? [phoneNumbers] : [];
        }

        return phoneNumbers
            ? phoneNumbers.map((item: string, index: number) => {
                  return (
                      <div className={styles['contact-item']} key={index}>
                          <Image
                              src={'/images/logos/socialmedias/phone.svg'}
                              alt="تلفن"
                              className={styles['contact-item-img']}
                          />
                          <div className={styles['contact-item-phone']} dir="ltr">
                              {toPersianNum(item.trim())}
                          </div>
                      </div>
                  );
              })
            : null;
    };
    return (
        <div className={styles['sidebar']}>
            <div className={styles['sidebar-head']}>اطلاعات سازمان</div>
            <div className={styles['sidebar-body']}>
                <div className={styles['contacts']}>
                    <Image
                        src={
                            (props.organization.logo && props.organization.logo.url) ||
                            '/images/logos/asanseminar-logo.svg'
                        }
                        alt={props.organization.title}
                        className={styles['organ-img']}
                    />
                    <div className={styles['name']}>{props.organization.title}</div>
                    {/* TODO: Move this hard coded 'asanseminar' name into a config or something */}
                    <Link
                        href={
                            props.organization.slug === 'asanseminar'
                                ? `/blog`
                                : `/organizations/${props.organization.slug}/blog`
                        }
                    >
                        <a title={'وبلاگ سازمان'} className={classNames(styles['blog-link'], 'btn btn-primary')}>
                            وبلاگ
                        </a>
                    </Link>
                </div>
                <hr className={styles['hr']} />
                <div className={styles['social']}>
                    {contactSection()}
                    {props.organization.instagram ? (
                        <div className={styles['social-item']}>
                            <a
                                href={`https://instagram.com/${props.organization.instagram}`}
                                title={`اینستاگرام ${props.organization.title}`}
                                target="_blank"
                                rel="noreferrer"
                                className={styles['social-item-link']}
                            >
                                <Image
                                    src={'/images/logos/socialmedias/instagram-full.svg'}
                                    alt={'اینستاگرام'}
                                    className={styles['social-item-img']}
                                />
                                <div className={styles['social-item-text']}>اینستاگرام</div>
                            </a>
                        </div>
                    ) : null}
                    {/*{props.organization.telegram ? (*/}
                    {/*    <div className={styles['social-item']}>*/}
                    {/*        <Image*/}
                    {/*            src={'/images/logos/socialmedias/telegram-full-round.svg'}*/}
                    {/*            alt={'تلگرام'}*/}
                    {/*            className={styles['social-item-img']}*/}
                    {/*        />*/}
                    {/*        <div>*/}
                    {/*            <a*/}
                    {/*                href={`https://t.me/${props.organization.telegram}`}*/}
                    {/*                title={`تلگرام ${props.organization.title}`}*/}
                    {/*                target="_blank"*/}
                    {/*                rel="noreferrer"*/}
                    {/*                className={styles['social-item-link']}*/}
                    {/*            >*/}
                    {/*                تلگرام*/}
                    {/*            </a>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}
                    {/*) : null}*/}
                    {/*{props.organization.twitter ? (*/}
                    {/*    <div className={styles['social-item']}>*/}
                    {/*        <Image*/}
                    {/*            src={'/images/logos/socialmedias/twitter-full-round.svg'}*/}
                    {/*            alt={'توییتر'}*/}
                    {/*            className={styles['social-item-img']}*/}
                    {/*        />*/}
                    {/*        <div>*/}
                    {/*            <a*/}
                    {/*                href={`https://twitter.com/${props.organization.twitter}`}*/}
                    {/*                title={`توییتر ${props.organization.title}`}*/}
                    {/*                target="_blank"*/}
                    {/*                rel="noreferrer"*/}
                    {/*                className={styles['social-item-link']}*/}
                    {/*            >*/}
                    {/*                توییتر*/}
                    {/*            </a>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}
                    {/*) : null}*/}
                    {props.organization.aparat ? (
                        <div className={styles['social-item']}>
                            <a
                                href={`https://aparat.com/${props.organization.aparat}`}
                                title={`آپارات ${props.organization.title}`}
                                target="_blank"
                                rel="noreferrer"
                                className={styles['social-item-link']}
                            >
                                <Image
                                    src={'/images/logos/socialmedias/aparat.svg'}
                                    alt={'آپارات'}
                                    className={styles['social-item-img']}
                                />
                                <div className={styles['social-item-text']}>آپارات</div>
                            </a>
                        </div>
                    ) : null}
                    {/*{props.organization.youtube ? (*/}
                    {/*    <div className={styles['social-item']}>*/}
                    {/*        <Image*/}
                    {/*            src={'/images/logos/socialmedias/youtube-full.svg'}*/}
                    {/*            alt={'یوتیوب'}*/}
                    {/*            className={styles['social-item-img']}*/}
                    {/*        />*/}
                    {/*        <div>*/}
                    {/*            <a*/}
                    {/*                href={props.organization.youtube}*/}
                    {/*                title={`یوتیوب ${props.organization.title}`}*/}
                    {/*                target="_blank"*/}
                    {/*                rel="noreferrer"*/}
                    {/*                className={styles['social-item-link']}*/}
                    {/*            >*/}
                    {/*                یوتیوب*/}
                    {/*            </a>*/}
                    {/*        </div>*/}
                    {/*    </div>*/}
                    {/*) : null}*/}
                    {props.organization.whatsapp ? (
                        <div className={styles['social-item']}>
                            <a
                                href={`https://wa.me/${props.organization.whatsapp}`}
                                title={`واتساپ ${props.organization.title}`}
                                target="_blank"
                                rel="noreferrer"
                                className={styles['social-item-link']}
                            >
                                <Image
                                    src={'/images/logos/socialmedias/whatsapp.png'}
                                    alt={'واتساپ'}
                                    className={styles['social-item-img']}
                                />
                                <div className={styles['social-item-text']}>واتساپ</div>
                            </a>
                        </div>
                    ) : null}
                    {props.organization.website ? (
                        <div className={styles['social-item']}>
                            <a
                                href={props.organization.website}
                                title={`وبسایت ${props.organization.title}`}
                                target="_blank"
                                rel="noreferrer"
                                className={styles['social-item-link']}
                            >
                                <Image
                                    src={'/images/logos/socialmedias/address.svg'}
                                    alt={'وبسایت'}
                                    className={styles['social-item-img']}
                                />
                                <div className={styles['social-item-text']}>وبسایت</div>
                            </a>
                        </div>
                    ) : null}
                </div>
                <hr className={styles['hr']} />
                <div className={styles['footer']}>
                    <div className={styles['footer-title']}>لینک سازمان</div>
                    <div className={styles['footer-link']}>
                        <Link href={`/organizations/${props.organization.slug}`}>
                            <a title={`لینک سازمان ${props.organization.title}`} style={{ wordBreak: 'break-all' }}>
                                {`asanseminar.ir/organizations/${props.organization.slug}`}
                            </a>
                        </Link>
                    </div>
                    {/*<div className={styles['follow-btn']}>دنبال کردن</div>*/}
                </div>
            </div>
        </div>
    );
}
