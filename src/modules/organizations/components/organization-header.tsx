import React, { useState } from 'react';
import styles from '../styles/organization-header.module.scss';
import { Col, Container, Image, Row } from 'react-bootstrap';
import { Dash, Plus } from 'react-bootstrap-icons';
import StatsCard from './stats-card';
import classNames from 'classnames';
import { IOrganization } from 'models/organization';
import toPersianNum from 'utils/to-persian-num';

export default function OrganizationHeader(props: { organization: IOrganization }) {
    const [expandAboutCard, setExpandAboutCard] = useState(false);
    const organizationStats = (() => {
        const data = [];
        for (let i = 0; i < 3; i++) {
            data.push({
                img: '/images/5002.jpg',
                title: 'تعداد کاربران',
                count: 125,
            });
        }
        return data;
    })();

    const statsCards = () => {
        return organizationStats.map((item: any, index: number) => {
            return <StatsCard data={item} key={index} />;
        });
    };
    const moreBtn = () => {
        return expandAboutCard ? (
            <div>
                {' '}
                کمتر <Dash className={styles['plus']} />
            </div>
        ) : (
            <div>
                {' '}
                بیشتر <Plus className={styles['plus']} />
            </div>
        );
    };
    return (
        <div className={styles['header']}>
            <Container>
                <Row>
                    <Col md={12} lg={6} className={styles['header-about']}>
                        <div className={styles['header-about-img']}>
                            <Image
                                src={
                                    (props.organization.logo && props.organization.logo.url) ||
                                    '/images/logos/asanseminar-logo.svg'
                                }
                                alt={toPersianNum(props.organization.title)}
                            />
                        </div>
                        <div
                            className={classNames(styles['header-about-card'], expandAboutCard ? styles['expand'] : '')}
                        >
                            <div className={styles['name']}>{toPersianNum(props.organization.title)}</div>
                            <div className={classNames(styles['explain'], expandAboutCard ? styles['expand'] : '')}>
                                {toPersianNum(props.organization.description)}
                            </div>
                            <div
                                className={classNames(styles['more-btn'])}
                                onClick={() => setExpandAboutCard((prevState) => !prevState)}
                            >
                                {moreBtn()}
                            </div>
                        </div>
                    </Col>
                    {/*<Col md={12} lg={6} className={styles['header-info']}>
                        {statsCards()}
                    </Col>*/}
                </Row>
            </Container>
        </div>
    );
}
