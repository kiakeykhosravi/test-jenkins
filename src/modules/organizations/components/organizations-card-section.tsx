import React from 'react';
import Section from 'modules/shared/components/section';
import { Container, Row } from 'react-bootstrap';
import styles from '../styles/organization-section.module.scss';
import classNames from 'classnames';
import { IOrganizationCompact } from 'models/organization-compact';
import LogoCard from 'modules/shared/components/logo-card';

// TODO: !!IMPORTANT!! This component and InstructorSection share a lot in content. Merge them!
export default function OrganizationsCardSection(props: { data: IOrganizationCompact[]; classes?: string }) {
    const cn = classNames;
    const renderOrganizationCards = () => {
        if (props.data.length) {
            return props.data.map((item: IOrganizationCompact) => {
                return <LogoCard data={item} key={item.id} />;
            });
        }
        return (
            <div style={{ fontSize: '1.1rem', padding: '2rem 0', textAlign: 'center' }}>
                <img
                    src="/images/conference.png"
                    style={{ width: '100%', maxWidth: '250px', marginBottom: '1.5rem' }}
                    alt="متاسفانه‌‌ هیچ سازمانی یافت نشد"
                />
                <div>متاسفانه‌‌ هیچ سازمانی یافت نشد</div>
            </div>
        );
    };
    return (
        <Section classes={props.classes}>
            <Container>
                <Row
                    className={cn(
                        styles['gx-3'],
                        styles['gy-4'],
                        styles['gx-lg-4'],
                        styles['gy-lg-5'],
                        styles['row'],
                        'gx-3 gy-4 gx-lg-4 gy-lg-5 justify-content-center',
                    )}
                >
                    {renderOrganizationCards()}
                </Row>
            </Container>
        </Section>
    );
}
