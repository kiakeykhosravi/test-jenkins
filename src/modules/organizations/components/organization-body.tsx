import React, { useEffect, useRef, useState } from 'react';
import styles from '../styles/organization-body.module.scss';
import { Col, Container, Row, Spinner } from 'react-bootstrap';
import FiltersSection from 'modules/shared/components/filters-section';
import ListSection from 'modules/shared/components/list-section';
import classNames from 'classnames';
import OrganizationSidebar from './organization-sidebar';
import { IWebinarCompact } from 'models/webinar-compact';
import { IOrganization } from 'models/organization';
import Searchbar from 'modules/shared/components/searchbar';
import { useRouter } from 'next/router';

export default function OrganizationBody(props: {
    organization: IOrganization;
    webinars: IWebinarCompact[];
    initialValue: string;
    filters;
    updateFilters;
    searchHandler;
    loading: boolean;
    setLoading;
}) {
    const router = useRouter();
    const webinarsRef = useRef(null);

    const [firstRender, setFirstRender] = useState(true);

    useEffect(() => {
        if (!firstRender) webinarsRef.current.scrollIntoView();
        setFirstRender(false);
    }, [props.webinars]);

    return (
        <Container className={styles.body}>
            <Row className={styles['body-row']}>
                <Col lg={3} xl={3}>
                    <OrganizationSidebar organization={props.organization} />
                </Col>
                <Col lg={9} xl={9} className={styles['main']} ref={webinarsRef}>
                    <h6 className={styles['main-title']}>کلاس های برگزار شده توسط {props.organization.title}</h6>
                    <Searchbar
                        initialValue={props.initialValue}
                        containerClasses={styles.search}
                        fieldClasses={styles['search-field']}
                        placeHolder="جستجوی دوره"
                        customHandler={props.searchHandler}
                        setLoading={props.setLoading}
                        type={'organization_webinars'}
                        slug={router.query.slug as string}
                    />
                    <FiltersSection
                        filters={props.filters}
                        filterUpdate={props.updateFilters}
                        setLoading={props.setLoading}
                    />
                    <Row
                        className={classNames('gx-sm-4 gx-md-5 gy-5', styles['gy-5'], {
                            [styles['courses-not-found']]: !props.webinars || !props.webinars.length,
                        })}
                    >
                        {props.loading ? (
                            <div className={styles.loader}>
                                <Spinner animation="grow" variant={'primary'} />
                                <div style={{ marginRight: '1rem' }}>درحال بارگذاری...</div>
                            </div>
                        ) : (
                            <ListSection data={props.webinars} organizationType={props.organization.type} />
                        )}
                    </Row>
                </Col>
            </Row>
        </Container>
    );
}
