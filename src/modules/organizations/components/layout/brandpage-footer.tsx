import React from 'react';
import styles from '../../styles/organization-footer.module.scss';
import { Col, Row } from 'react-bootstrap';

export default function BrandPageFooter() {
    return (
        <div className={styles['main-container']}>
            <Row className={styles.contacts}>
                <Col className={styles['item']}>قدرت گرفته از آسان سمینار</Col>
            </Row>
        </div>
    );
}
