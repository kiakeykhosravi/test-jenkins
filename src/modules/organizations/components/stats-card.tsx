import React from 'react';
import styles from '../styles/stats-card.module.scss';
import { Image } from 'react-bootstrap';
import toPersianNum from 'utils/to-persian-num';

export default function StatsCard(props: { data: any }) {
    return (
        <div className={styles['stats-card']}>
            <Image src={props.data.img} alt={props.data.title} className={styles['stats-card-img']} />
            <div className={styles['stats-card-count']}>{toPersianNum(props.data.count)}</div>
            <div className={styles['stats-card-title']}>{props.data.title}</div>
        </div>
    );
}
