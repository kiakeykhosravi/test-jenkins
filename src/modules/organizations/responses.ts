import { IOrganization } from 'models/organization';
import { IWebinarCompact } from 'models/webinar-compact';

export interface IOrganizationsResponse {
    organizations: IOrganization[];
    lastpage: number;
}

export interface IOrganizationResponse {
    organization: IOrganization;
    lastpage: number;
    webinars: IWebinarCompact[];
}
