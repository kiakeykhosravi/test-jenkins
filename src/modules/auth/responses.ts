import { IMeeting } from 'models/meeting';
import { IOrganizationCompact } from 'models/organization-compact';
import { IWebinarCompact } from 'models/webinar-compact';

export interface ILoginResponse {
    access_token: string;
    token_type: number;
    expires_in: number;
    type: 'login' | 'verify';
}

export interface ILoginNoPassResponse {
    type: 'verify';
    secret: string;
    ask_fullname: boolean;
}

export interface IVerifyResponse {
    access_token: string;
    token_type: number;
    expires_in: number;
}

export interface IGuestInfoResponse {
    organization: IOrganizationCompact;
    webinar: IWebinarCompact;
    meetings: IMeeting[];
}

export interface IGuestEnterResponse {
    link: string;
}

export interface ILoginWithTokenResponse {
    access_token: string;
    token_type: string;
    expires_in: number;
    target: string;
}
