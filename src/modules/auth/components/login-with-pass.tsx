import React, { useContext, useEffect, useRef, useState } from 'react';
import styles from '../styles/modal-login.module.scss';
import { Button, Form } from 'react-bootstrap';
import { ExclamationTriangleFill, Key, Person } from 'react-bootstrap-icons';
import apiLogin from 'modules/auth/api/api-login';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { useRouter } from 'next/router';
import { ILoginNoPassResponse } from 'modules/auth/responses';
import { sessionStorageVars } from 'utils/storage-vars';
import { toast } from 'react-toastify';
import { AuthContext } from 'contexts/auth-context';
import apiUser from 'modules/profile/api/api-user';
import toEnglishNum from 'utils/to-english-num';

type TInputs = {
    identifier: string;
    password: string;
};

export default function LoginWithPass(props: { setShowModal; setLnpVerify; setLVerify; setLoginType }) {
    const router = useRouter();
    const inputRef = useRef(null);
    const authContext = useContext(AuthContext);
    const { register, handleSubmit, errors } = useForm<TInputs>();

    const [submitting, setSubmitting] = useState<boolean>(false);
    const [errorMessage, setErrorMessage] = useState({
        status: false,
        text: '',
    });

    useEffect(function () {
        inputRef.current.focus();
    }, []);

    async function onSubmit(data: { identifier: string; password: string }) {
        setSubmitting(true);
        setErrorMessage({
            status: false,
            text: '',
        });

        data.identifier = toEnglishNum(data.identifier);
        const response = await apiLogin(data.identifier, data.password);
        if (response.status === 'error') {
            setSubmitting(false);
            setErrorMessage({
                status: true,
                text: response.message,
            });
        } else if (response.status === 'data') {
            if (response.data.type === 'login') {
                let user;
                for (let i = 0; i < 3; i++) {
                    user = await apiUser();
                    if (user) {
                        break;
                    }
                }
                if (user) {
                    toast.success(response.message);
                    sessionStorage.removeItem(sessionStorageVars.user_mobile);
                    sessionStorage.removeItem(sessionStorageVars.ask_fullname);
                    sessionStorage.removeItem(sessionStorageVars.secret);
                    authContext.changeIsLoggedIn(user);

                    setTimeout(() => {
                        props.setShowModal(false);
                        router.reload();
                    }, 100);
                } else {
                    toast.error('خطایی در دریافت اطلاعات کاربری شما پس از ورود رخ داد. لطفا دوباره تلاش کنید', {
                        autoClose: 7500,
                    });
                }
                setSubmitting(false);
            } else {
                const secret = (response.data as ILoginNoPassResponse).secret;
                const askFullName = (response.data as ILoginNoPassResponse).ask_fullname;
                sessionStorage[sessionStorageVars.user_mobile] = data.identifier;
                sessionStorage[sessionStorageVars.ask_fullname] = askFullName ? 'true' : 'false';
                sessionStorage[sessionStorageVars.secret] = secret;
                props.setLVerify(true);
                props.setLnpVerify(false);
                setSubmitting(false);
            }
        }
    }

    return (
        <>
            <Form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
                <Form.Group controlId="formIdentifier">
                    <div className={styles['form-control-container']}>
                        <Form.Control
                            type="text"
                            placeholder="شماره همراه یا ایمیل"
                            className={classNames(
                                styles['form-control'],
                                errors.identifier ? styles['border-red'] : '',
                            )}
                            name={'identifier'}
                            ref={(e) => {
                                register(e, {
                                    required: 'شماره موبایل یا ایمیل را وارد کنید!',
                                    pattern: {
                                        value: /^(((?:98|\+98|0098|0)?9[0-9]{9})|((?:۹۸|\+۹۸|۰۰۹۸|۰)?۹[۰۹۸۷۶۵۴۳۲۱]{9})|(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+))$/,
                                        message: 'فرمت شماره موبایل یا ایمیل اشتباه است!',
                                    },
                                });
                                inputRef.current = e;
                            }}
                            onFocus={(e) => (e.target.placeholder = '')}
                            onBlur={(e) => (e.target.placeholder = 'شماره همراه یا ایمیل')}
                        />
                        <Person className={styles['form-control-icon']} />
                    </div>
                    {errors.identifier && <div className={styles.error}>{errors.identifier.message}</div>}
                </Form.Group>
                <Form.Group controlId="formPassword">
                    <div className={styles['form-control-container']}>
                        <Form.Control
                            type="password"
                            placeholder="رمز عبور"
                            className={classNames(styles['form-control'], errors.password ? styles['border-red'] : '')}
                            name={'password'}
                            ref={register({
                                required: 'رمز عبور را وارد کنید!',
                                minLength: { value: 8, message: 'رمز عبور باید حداقل هشت کاراکتر باشد!' },
                            })}
                        />
                        <Key className={styles['form-control-icon']} />
                    </div>
                    {errors.password && <div className={styles.error}>{errors.password.message}</div>}
                </Form.Group>
                <div className={styles['form-control-container']}>
                    <Button variant="success" type="submit" className={styles['login-btn']} disabled={submitting}>
                        {submitting ? 'در حال ارسال...' : 'ورود'}
                    </Button>
                </div>
                <span className={styles.back} onClick={() => props.setLoginType('loginNoPass')}>
                    ورود با کد تایید پیامکی
                </span>
            </Form>
            <div className={styles['error-message']}>
                {errorMessage.status ? (
                    <>
                        <ExclamationTriangleFill />
                        <span>{errorMessage.text}</span>
                    </>
                ) : null}
            </div>
        </>
    );
}
