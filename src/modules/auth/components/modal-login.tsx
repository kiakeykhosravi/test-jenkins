import React, { useEffect, useState } from 'react';
import styles from '../styles/modal-login.module.scss';
import { Modal, Tab, Tabs } from 'react-bootstrap';
import LoginNoPass from './login-no-pass';
import ModalVerify from './modal-verify';
import LoginWithPass from './login-with-pass';
import { Router } from 'next/router';
import Link from 'next/link';
import classNames from 'classnames';

export default function ModalLogin(props: { closable: boolean; showModal: boolean; setShowModal: any }) {
    const [lnpVerify, setLnpVerify] = useState(false);
    const [lVerify, setLVerify] = useState(false);
    const [loginType, setLoginType] = useState('loginNoPass');

    useEffect(() => {
        Router.events.on('routeChangeStart', () => props.setShowModal(false));
    }, []);

    function Icons() {
        const cn = classNames;
        const rand = Math.random;
        const animMultiplier = 9;
        const animBase = 25;
        const icons = [];
        for (let i = 0; i < 5; i++) {
            icons.push(
                <div
                    className={cn(styles.icon, styles[`anim-${i + 1}`])}
                    style={{ animationDuration: rand() * animMultiplier + animBase + 's' }}
                >
                    <img className={styles['icon-img']} src={`/images/icons/floaticon${(i % 3) + 1}.png`} />
                </div>,
            );
        }
        return icons;
    }

    return (
        <div>
            <Modal
                className="modal fade"
                centered
                show={props.showModal}
                onHide={() => {
                    if (props.closable) props.setShowModal(false);
                }}
            >
                <div className={styles.box}>
                    <div className={styles['img-container']}>
                        <img src="/images/logos/asanseminar-logo.svg" className={styles.logo} alt="آسان سمینار" />
                        {Icons()}
                    </div>
                    <h4 className={styles.title}>ورود به آسان سمینار</h4>
                    {loginType === 'loginNoPass' ? (
                        <div>
                            {lnpVerify ? (
                                <ModalVerify
                                    setVerify={setLnpVerify}
                                    setShowModal={props.setShowModal}
                                    loginType={loginType}
                                    setLoginType={setLoginType}
                                />
                            ) : (
                                <LoginNoPass
                                    setLnpVerify={setLnpVerify}
                                    setLVerify={setLVerify}
                                    setLoginType={setLoginType}
                                />
                            )}
                        </div>
                    ) : (
                        <div>
                            {lVerify ? (
                                <ModalVerify
                                    setVerify={setLVerify}
                                    setShowModal={props.setShowModal}
                                    loginType={loginType}
                                    setLoginType={setLoginType}
                                />
                            ) : (
                                <LoginWithPass
                                    setLnpVerify={setLnpVerify}
                                    setLVerify={setLVerify}
                                    setShowModal={props.setShowModal}
                                    setLoginType={setLoginType}
                                />
                            )}
                        </div>
                    )}
                </div>
            </Modal>
        </div>
    );
}
