import React from 'react';
import styles from '../styles/floating-icons.module.scss';
import classNames from 'classnames';
import Link from 'next/link';

export default function FloatingIcons() {
    return (
        <div className={styles['img-container']}>
            <Link href="/">
                <a title="خانه" className={styles.link}>
                    <img src="/images/logos/asanseminar-logo.svg" className={styles.logo} alt="آسان سمینار" />
                </a>
            </Link>
            {Icons()}
        </div>
    );
}

function Icons() {
    const cn = classNames;
    const rand = Math.random;
    const animMultiplier = 9;
    const animBase = 25;
    const icons = [];
    for (let i = 0; i < 5; i++) {
        icons.push(
            <div
                className={cn(styles.icon, styles[`anim-${i + 1}`])}
                style={{ animationDuration: rand() * animMultiplier + animBase + 's' }}
            >
                <img className={styles['icon-img']} src={`/images/icons/floaticon${(i % 3) + 1}.png`} />
            </div>,
        );
    }
    return icons;
}
