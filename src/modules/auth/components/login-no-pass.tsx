import React, { useEffect, useRef, useState } from 'react';
import styles from '../styles/modal-login.module.scss';
import { Button, Form } from 'react-bootstrap';
import { ExclamationTriangleFill, Phone } from 'react-bootstrap-icons';
import { useForm } from 'react-hook-form';
import toEnglishNum from '../../../utils/to-english-num';
import apiLoginNopass from '../api/api-login-nopass';
import { sessionStorageVars } from '../../../utils/storage-vars';

type TInputs = {
    mobile: string;
};

export default function LoginNoPass(props: { setLnpVerify; setLVerify; setLoginType }) {
    const inputRef = useRef(null);
    const { register, handleSubmit, errors } = useForm<TInputs>();

    const [submitting, setSubmitting] = useState<boolean>(false);
    const [errorMessage, setErrorMessage] = useState({
        status: false,
        text: '',
    });

    useEffect(function () {
        inputRef.current.focus();
    }, []);

    async function onSubmit(data: { mobile: string }) {
        setSubmitting(true);
        setErrorMessage({
            status: false,
            text: '',
        });

        data.mobile = toEnglishNum(data.mobile);
        const response = await apiLoginNopass(data.mobile);
        if (response.status === 'error') {
            setSubmitting(false);
            setErrorMessage({
                status: true,
                text: response.message,
            });
        } else if (response.status === 'data') {
            sessionStorage[sessionStorageVars.user_mobile] = data.mobile;
            sessionStorage[sessionStorageVars.ask_fullname] = response.data.ask_fullname ? 'true' : 'false';
            sessionStorage[sessionStorageVars.secret] = response.data.secret;
            setSubmitting(false);
            props.setLnpVerify(true);
            props.setLVerify(false);
        }
    }

    return (
        <>
            <Form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
                <Form.Group controlId="formMobile">
                    <div className={styles['form-control-container']}>
                        <Form.Control
                            type="text"
                            placeholder="شماره همراه"
                            className={styles['form-control']}
                            name={'mobile'}
                            ref={(e) => {
                                register(e, {
                                    required: 'شماره موبایل را وارد کنید!',
                                    pattern: {
                                        value: /^(((?:98|\+98|0098|0)?9[0-9]{9})|((?:۹۸|\+۹۸|۰۰۹۸|۰)?۹[۰۹۸۷۶۵۴۳۲۱]{9}))$/,
                                        message: 'فرمت شماره موبایل اشتباه است!',
                                    },
                                });
                                inputRef.current = e;
                            }}
                        />
                        <Phone className={styles['form-control-icon']} />
                    </div>
                    {errors.mobile && <div className={styles.error}>{errors.mobile.message}</div>}
                </Form.Group>
                <div className={styles['form-control-container']}>
                    <Button variant="success" type="submit" className={styles['login-btn']} disabled={submitting}>
                        {submitting ? 'در حال ارسال...' : 'دریافت کد تایید'}
                    </Button>
                </div>
                <span className={styles.back} onClick={() => props.setLoginType('login')}>
                    ورود با رمز عبور
                </span>
            </Form>
            <div className={styles['error-message']}>
                {errorMessage.status ? (
                    <div className={styles['error-message']}>
                        <ExclamationTriangleFill />
                        <span>{errorMessage.text}</span>
                    </div>
                ) : null}
            </div>
        </>
    );
}
