import React, { useContext, useEffect, useRef, useState } from 'react';
import styles from '../styles/modal-login.module.scss';
import { ExclamationTriangleFill, Person, Key } from 'react-bootstrap-icons';
import toPersianNum from 'utils/to-persian-num';
import { useForm } from 'react-hook-form';
import apiVerify from 'modules/auth/api/api-verify';
import { Button, Form } from 'react-bootstrap';
import { sessionStorageVars } from 'utils/storage-vars';
import { toast } from 'react-toastify';
import { AuthContext } from 'contexts/auth-context';
import apiUser from 'modules/profile/api/api-user';
import toEnglishNum from 'utils/to-english-num';
import { useRouter } from 'next/router';

type TInputs = {
    secret: string;
    code: string;
    fullName: string;
};

export default function ModalVerify(props: { setShowModal; setVerify; loginType; setLoginType }) {
    const router = useRouter();
    const authContext = useContext(AuthContext);
    const { register, handleSubmit, errors } = useForm<TInputs>();
    const inputRef = useRef(null);

    const [userMobile, setUserMobile] = useState('');
    const [askFullName, setAskFullName] = useState('');
    const [secret, setSecret] = useState('');
    const [submitting, setSubmitting] = useState<boolean>(false);
    const [errorMessage, setErrorMessage] = useState({
        status: false,
        text: '',
    });

    useEffect(function () {
        setUserMobile(sessionStorage[sessionStorageVars.user_mobile]);
        setAskFullName(sessionStorage[sessionStorageVars.ask_fullname]);
        setSecret(sessionStorage[sessionStorageVars.secret]);
        inputRef.current.focus();
    }, []);

    async function onSubmit(data: { secret: string; fullName?: string; code: string }) {
        setSubmitting(true);
        setErrorMessage({
            status: false,
            text: '',
        });

        data.code = toEnglishNum(data.code);
        const response = await apiVerify(data.secret, data.fullName, data.code);
        if (response.status === 'error') {
            setSubmitting(false);
            setErrorMessage({
                status: true,
                text: response.message,
            });
        } else if (response.status === 'data') {
            let user;
            for (let i = 0; i < 3; i++) {
                user = await apiUser();
                if (user) {
                    break;
                }
            }
            if (user) {
                toast.success(response.message);
                sessionStorage.removeItem(sessionStorageVars.user_mobile);
                sessionStorage.removeItem(sessionStorageVars.ask_fullname);
                sessionStorage.removeItem(sessionStorageVars.secret);
                authContext.changeIsLoggedIn(user);
                setTimeout(() => {
                    router.reload();
                }, 100);
            } else {
                toast.error('خطایی در دریافت اطلاعات کاربری شما پس از ورود رخ داد. لطفا دوباره تلاش کنید', {
                    autoClose: 7500,
                });
            }
            setSubmitting(false);
        }
    }

    return (
        <>
            <Form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
                <div className={styles.phone}>{toPersianNum(userMobile)}</div>
                <Form.Group controlId="formCode">
                    <div className={styles['form-control-container']}>
                        <Form.Control type="hidden" name={'secret'} value={secret} ref={register()} />
                        <Form.Control
                            type="text"
                            placeholder="کد دریافتی"
                            className={styles['form-control']}
                            name={'code'}
                            ref={(e) => {
                                register(e, {
                                    required: 'کد دریافتی را وارد کنید!',
                                });
                                inputRef.current = e;
                            }}
                        />
                        <Key className={styles['form-control-icon']} />
                    </div>
                    {errors.code && <div className={styles.error}>{errors.code.message}</div>}
                </Form.Group>
                <Form.Group controlId="formFullName">
                    {askFullName === 'true' && (
                        <div className={styles['form-control-container']}>
                            <Form.Control
                                type="text"
                                placeholder="نام و نام خانوادگی"
                                className={styles['form-control']}
                                name={'fullName'}
                                ref={register({
                                    required: 'نام و نام خانوادگی را وارد کنید!',
                                })}
                            />
                            <Person className={styles['form-control-icon']} />
                        </div>
                    )}

                    {errors.fullName && askFullName === 'true' && (
                        <div className={styles.error}>{errors.fullName.message}</div>
                    )}
                </Form.Group>
                <div className={styles['form-control-container']}>
                    <Button variant="success" type="submit" className={styles['login-btn']} disabled={submitting}>
                        {submitting ? 'در حال ارسال...' : 'ورود'}
                    </Button>
                </div>
                <span className={styles.back} onClick={() => props.setVerify(false)}>
                    بازگشت به مرحله قبل
                </span>
                <span
                    className={styles.back}
                    onClick={() => {
                        if (props.loginType === 'login') props.setLoginType('loginNoPass');
                        else props.setLoginType('login');
                    }}
                >
                    {props.loginType === 'login' ? 'ورود با کد تایید پیامکی' : 'ورود با رمز عبور'}
                </span>
            </Form>
            {errorMessage.status ? (
                <div className={styles['error-message']}>
                    <ExclamationTriangleFill />
                    <span>{errorMessage.text}</span>
                </div>
            ) : null}
        </>
    );
}
