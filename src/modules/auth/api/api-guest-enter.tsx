import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IGuestEnterResponse } from '../responses';
import { toast } from 'react-toastify';

export default async function apiGuestEnter(
    secret: string,
    params: { meeting_id: number; fullname?: string },
): Promise<string | null> {
    const url = endpoints.frontend.guest_enter.replace('{secret}', secret);

    return apiRequest<IGuestEnterResponse>('post', url, params)
        .then((response) => {
            if (response.status === 'data') {
                toast.success(response.message || 'ورود با موفقیت انجام شد');
                return response.data.link;
            }

            toast.error(response.message || 'خطایی در ورود به کلاس رخ داد', {
                autoClose: 7500,
            });
            return null;
        })
        .catch((error) => {
            toast.error((error && error.message) || 'خطایی در دریافت اطلاعات جهت ورود به کلاس رخ داد', {
                autoClose: 7500,
            });
            return null;
        });
}
