import { ILoginNoPassResponse, ILoginResponse } from '../responses';
import endpoints from 'api/api-endpoints';
import apiRequest from 'api/api-request';
import { setAuthCookie } from 'utils/client-cookie-utils';

export default async function apiLogin(
    identifier: string,
    password: string,
): Promise<{ status: 'data' | 'error'; data?: ILoginResponse | ILoginNoPassResponse; message?: string }> {
    const { data, status, message } = await apiRequest<ILoginResponse | ILoginNoPassResponse>(
        'post',
        endpoints.frontend.login,
        {
            identifier,
            password,
        },
    );

    if (status === 'data') {
        if (data.type === 'login') {
            setAuthCookie(data.access_token, data.expires_in);
        }
        return { status, data: data, message };
    } else {
        return { status: 'error', message };
    }
}
