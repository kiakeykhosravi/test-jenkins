import endpoints from 'api/api-endpoints';
import apiRequest from 'api/api-request';
import { IVerifyResponse } from '../responses';
import { setAuthCookie } from 'utils/client-cookie-utils';

export default async function apiVerify(
    secret: string,
    fullname: string,
    code: string,
): Promise<{ status: 'data' | 'error'; data?; message?; errors? }> {
    const { data, status, message } = await apiRequest<IVerifyResponse>('post', endpoints.frontend.verify, {
        secret: secret,
        fullname: fullname,
        code: code,
    });

    if (status === 'data') {
        setAuthCookie(data.access_token, data.expires_in);

        return { status, data: data.access_token };
    } else if (status === 'error') {
        return { status: 'error', message };
    }
}
