import endpoints from 'api/api-endpoints';
import apiRequest from 'api/api-request';
import { ILoginNoPassResponse } from '../responses';

export default async function apiLoginNopass(
    mobile: string,
): Promise<{ status: 'data' | 'error'; message; data?; errors? }> {
    const { data, status, message, errors } = await apiRequest<ILoginNoPassResponse>(
        'post',
        endpoints.frontend.login_nopass,
        {
            mobile: mobile,
        },
    );

    if (status === 'data') {
        return { status, data, message };
    } else {
        return { status: 'error', errors, message };
    }
}
