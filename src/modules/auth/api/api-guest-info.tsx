import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { IGuestInfoResponse } from '../responses';
import { AxiosRequestConfig } from 'axios';

export default async function apiGuestInfo(secret: string, config?: AxiosRequestConfig): Promise<any> {
    const url = endpoints.backend.guest_info.replace('{secret}', secret);

    return apiRequest<IGuestInfoResponse>('get', url, undefined, undefined, undefined, config)
        .then((response) => {
            if (response.status === 'data') {
                return { data: response.data };
            }

            return { message: response.message || 'کد کلاس ورودی شما صحیح نمی باشد' };
        })
        .catch((error) => {
            return { message: (error && error.message) || 'خطایی در دریافت اطلاعات وبینار رخ داد' };
        });
}
