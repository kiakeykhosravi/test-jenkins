import endpoints from 'api/api-endpoints';
import apiRequest from 'api/api-request';
import { deleteAuthCookie } from 'utils/client-cookie-utils';

export default async function apiLogout(): Promise<{ status: 'data' | 'error'; message; errors? }> {
    const { status, message, errors } = await apiRequest('post', endpoints.frontend.logout);

    if (status === 'data') {
        deleteAuthCookie();
        return { status, message };
    } else {
        return { status: 'error', message, errors };
    }
}
