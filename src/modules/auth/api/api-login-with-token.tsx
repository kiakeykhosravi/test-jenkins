import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';
import { ILoginWithTokenResponse } from '../responses';
import { setAuthCookie } from 'utils/client-cookie-utils';

export default async function apiLoginWithToken(
    token: string,
): Promise<{ data?: ILoginWithTokenResponse; status: 'error' | 'data'; message? }> {
    const sendData = {
        secret: token,
    };
    const { data, status, message } = await apiRequest<ILoginWithTokenResponse>(
        'post',
        endpoints.frontend.login_with_token,
        sendData,
    );

    if (status === 'data') {
        setAuthCookie(data.access_token, data.expires_in);

        return {
            data,
            status,
        };
    }
    return {
        status: 'error',
        message: message,
    };
}
