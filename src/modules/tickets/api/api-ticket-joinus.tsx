import apiRequest from 'api/api-request';
import endpoints from 'api/api-endpoints';

export default async function apiTicketJoinUs(data) {
    const response = await apiRequest<null>('post', endpoints.frontend.ticket_joinus, data);

    return {
        status: response.status,
        message: response.message,
    };
}
