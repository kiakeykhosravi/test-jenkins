import { IMedia } from 'models/media';

export interface IUploadResumeResponse {
    media: IMedia;
}
