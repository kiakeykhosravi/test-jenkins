import React from 'react';
import styles from '../styles/left-column.module.scss';
import { Button } from 'react-bootstrap';
//import SliderBanner from 'modules/shared/components/slider-banner';
import { ISlide } from 'models/slide';
import Link from 'next/link';
import classNames from 'classnames';
import SliderBanner from 'modules/shared/components/slider-banner';

export default function LeftColumn() {
    const cn = classNames;
    const renderItem = (label: string, slides: ISlide[]) => {
        return (
            <>
                <h3 className={styles.label}>{label}</h3>
                {/*<div className={cn(styles.slide, styles['slide-temporary-container'])}>*/}
                {/*    <img className={styles.img} src={slides[0].media.url} alt={slides[0].media.title} />*/}
                {/*</div>*/}
                <SliderBanner
                    slideClasses={styles.img}
                    bannerClasses={styles.slide}
                    bannerBodyClasses={styles['slide-body']}
                    swiperClasses={styles['swiper']}
                    autoplay={{ autoplay: true }}
                    slides={slides}
                    paginationOptions={{ pagination: false }}
                />
            </>
        );
    };

    const rand = Math.random;
    const floor = Math.floor;

    const fillSlide = (url, imgUrl, title, max) => {
        const slide = [];
        for (let i = 1; i <= max; i++) {
            slide.push({
                id: floor(rand() * 1000),
                title: title + i,
                url: url,
                media_id: 1,
                location: '',
                active: 1,
                media: {
                    id: floor(rand() * 1000),
                    storage: '',
                    title: title + i,
                    url: imgUrl + i + '.jpg',
                    thumbnail_url: '',
                    secret: '',
                    mime_type: '',
                    size: 1,
                },
            });
        }
        return slide;
    };

    const slides1: ISlide[] = fillSlide('', '/images/joinus/Fanni-', 'فنی و برنامه نویسی', 10);
    const slides2: ISlide[] = fillSlide('', '/images/joinus/TolidMohtava-', 'محتوا سازی', 3);
    const slides3: ISlide[] = fillSlide('', '/images/joinus/Teaching-', 'تدریس', 1);
    const slides4: ISlide[] = fillSlide('', '/images/joinus/Marketing-', 'بازاریابی و فروش', 8);

    return (
        <>
            <h2 className={styles.title}>فرصت های شغلی</h2>
            <div className={cn(styles.jobs, 'px-0 px-lg-5')}>
                {renderItem('فنی و برنامه نویسی', slides1)}
                {renderItem('محتوا سازی', slides2)}
                {renderItem('تدریس', slides3)}
                {renderItem('بازاریابی و فروش', slides4)}
            </div>
            <Link href="/team">
                <a title="تیم ما">
                    <Button variant="warning" type="button" className={styles.btn}>
                        آشنایی با تیم ما
                    </Button>
                </a>
            </Link>
        </>
    );
}
