import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import styles from '../styles/right-column.module.scss';
import classNames from 'classnames';
import { useForm } from 'react-hook-form';
import apiUploadResume from 'modules/tickets/api/api-upload-resume';
import { IMedia } from 'models/media';
import { toast } from 'react-toastify';
import apiTicketJoinUs from 'modules/tickets/api/api-ticket-joinus';
import CommonRegexUtil from 'utils/common-regex-util';
import AsanUpload from 'modules/shared/components/asan-upload';

export default function RightColumn(props: { setSuccess }) {
    const { errors, register, handleSubmit } = useForm();
    const [isSubmitting, setIsSubmitting] = useState(false);
    const [resumeFile, setResumeFile] = useState(null);

    const onSubmitForm = async (data) => {
        if (isSubmitting) {
            return;
        }
        setIsSubmitting(true);
        const formData = {
            fullname: data.fullname,
            mobile: data.mobile,
            email: data.email,
            job: data.job,
            description: data.description,
            resume_id: undefined,
        };
        const media: IMedia = resumeFile ? resumeFile : await uploadResume(data.fileupload && data.fileupload[0]);
        if (!media) {
            setIsSubmitting(false);
            return;
        }
        formData.resume_id = media.id;
        const uploadResult = await apiTicketJoinUs(formData);
        if (uploadResult.status !== 'data') {
            toast.error(uploadResult.message || 'خطایی در ارسال اطلاعات به سرور رخ داد');
        } else {
            toast.success('ارسال با موفقیت انجام شد');
            props.setSuccess(true);
        }
        setIsSubmitting(false);
    };

    const uploadResume = async (file) => {
        if (!file) {
            return null;
        }
        const formData = new FormData();
        formData.append('fileupload', file);
        const result = await apiUploadResume(formData);
        if (result.status !== 'data') {
            toast.error(result.message || 'خطایی در آپلود رزومه رخ داد');
            return null;
        }
        setResumeFile(result.data);
        return result.data;
    };

    return (
        <div className="px-0 pl-lg-5">
            <h2 className={styles.title}>اطلاعات فردی</h2>
            <Form onSubmit={handleSubmit(onSubmitForm)}>
                <Form.Group controlId="fullname" className={styles['form-group']}>
                    <Form.Label className={styles['form-label']}>نام و نام خانوادگی</Form.Label>
                    <Form.Control
                        name="fullname"
                        className={styles['form-input']}
                        ref={register({
                            required: 'نام و نام خانوادگی الزامی می باشد',
                            minLength: {
                                value: 3,
                                message: 'نام و نام خانوادگی باید حداقل ۳ حرفی باشد',
                            },
                        })}
                    />
                    {errors.fullname && <div className={styles['error-message']}>{errors.fullname.message}</div>}
                </Form.Group>
                <Form.Group controlId="mobile" className={styles['form-group']}>
                    <Form.Label className={styles['form-label']}>تلفن همراه</Form.Label>
                    <Form.Control
                        name="mobile"
                        className={styles['form-input']}
                        ref={register({
                            required: 'تلفن همراه الزامی می باشد',
                            pattern: {
                                value: CommonRegexUtil.mobileIran,
                                message: 'فرمت شماره موبایل نامعتبر است',
                            },
                        })}
                    />
                    {errors.mobile && <div className={styles['error-message']}>{errors.mobile.message}</div>}
                </Form.Group>
                <Form.Group controlId="email" className={styles['form-group']}>
                    <Form.Label className={styles['form-label']}>ایمیل</Form.Label>
                    <Form.Control
                        name="email"
                        className={styles['form-input']}
                        ref={register({
                            required: 'ایمیل الزامی می باشد',
                            pattern: {
                                value: CommonRegexUtil.email,
                                message: 'فرمت ایمیل نادرست است',
                            },
                        })}
                    />
                    {errors.email && <div className={styles['error-message']}>{errors.email.message}</div>}
                </Form.Group>
                <Form.Group controlId="job" className={styles['form-group']}>
                    <Form.Label className={styles['form-label']}>زمینه همکاری</Form.Label>
                    <Form.Control
                        name="job"
                        as="select"
                        className={classNames(styles['form-input'], 'form-select')}
                        ref={register({
                            required: 'زمینه همکاری الزامی می باشد',
                        })}
                    >
                        <option value="بازاریابی و فروش">بازاریابی و فروش</option>
                        <option value="سفیری">سفیری</option>
                        <option value="سیستم ادمین / دیتابیس ادمین">سیستم ادمین / دیتابیس ادمین</option>
                        <option value="تدریس">تدریس</option>
                        <option value="پشتیبانی">پشتیبانی</option>
                        <option value="پشتیبانی فنی">پشتیبانی فنی</option>
                        <option value="برنامه نویسی / توسعه دهنده رابط کاربری">
                            برنامه نویسی / توسعه دهنده رابط کاربری
                        </option>
                        <option value="برنامه نویسی / توسعه دهنده بک اند">برنامه نویسی / توسعه دهنده بک اند</option>
                        <option value="طراح گرافیک">طراح گرافیک</option>
                        <option value="تولید محتوا">تولید محتوا</option>
                        <option value="سئو">سئو</option>
                    </Form.Control>
                    {errors.job && <div className={styles['error-message']}>{errors.job.message}</div>}
                </Form.Group>
                <Form.Group controlId="description" className={styles['form-group']}>
                    <Form.Label className={styles['form-label']}>توضیحات تکمیلی</Form.Label>
                    <Form.Control
                        name="description"
                        className={styles['form-input']}
                        as="textarea"
                        rows={5}
                        ref={register()}
                    />
                </Form.Group>
                <Form.Group controlId="fileupload" className={styles['form-group']}>
                    <div>
                        ارسال فایل رزومه
                        <br />
                        <small>(فایل ارسالی باید پی دی اف باشد)</small>
                        <br />
                    </div>
                    <AsanUpload
                        name="fileupload"
                        register={register}
                        required="ارسال فایل رزومه الزامی می باشد"
                        iconType="pdf"
                    />
                    {errors.fileupload && <div className={styles['error-message']}>{errors.fileupload.message}</div>}
                </Form.Group>
                <Button
                    type="submit"
                    variant="success"
                    className={styles['submit-btn']}
                    {...(isSubmitting ? { disabled: true } : {})}
                >
                    {isSubmitting ? 'در حال ارسال ...' : 'ارسال رزومه'}
                </Button>
            </Form>
        </div>
    );
}
