import React from 'react';
import styles from '../styles/joinus-top-section.module.scss';
import { Container } from 'react-bootstrap';
import classNames from 'classnames';

export default function JoinusTopSection() {
    return (
        <div className={classNames('gradient-background', styles.section)}>
            <Container className="py-5">
                <h1 className="text-center mb-5 mb-lg-3">آسان سمیناری شوید</h1>
                <h3 className="mb-4">سلام دوست من</h3>
                <p className={styles.paragraph}>
                    آسان سمینار زمینه‌های همکاری و فرصت‌های شغلی متعددی دارد که شما برحسب توانمندی، علاقه مندی و همینطور
                    نیاز شرکت می توانید در آنها فعالیت کنید. آسان سمینار بستری برای ایده‌پردازی، کارآفرینی و ارتقا شخصی
                    برمبنای یادگیری مهارت و آموزش های علمی می‌باشد با تکمیل این فرم تیم منابع انسانی با شما تماس گرفته و
                    اطلاعات تکمیلی را به شما خواهد داد. شما می توانید در هر زمینه‌ای که علاقه و مهارت داشته باشید به
                    همکاری خود با آسان سمینار ادامه دهید همچنین اکثر فعالیت‌های آسان سمینار به صورت دورکاری و آنلاین
                    می‌باشد و نیازی به حضور فیزیکی یا رفت و آمد ندارد.
                </p>
            </Container>
        </div>
    );
}
