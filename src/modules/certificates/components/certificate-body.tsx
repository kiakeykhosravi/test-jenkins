import React from 'react';
import styles from '../styles/certificate-body.module.scss';
import { Col, Container, Row } from 'react-bootstrap';
import { ZoomIn } from 'react-bootstrap-icons';
import { CertificateType } from '../types';

export default function CertificateBody(props: {
    pageTitle: string;
    certificates: CertificateType[];
    certClickHandler;
}) {
    return (
        <Container>
            <h1 className={styles['page-title']}>{props.pageTitle}</h1>
            <Row className="justify-content-center">
                {props.certificates.map((cert, index) => (
                    <Col lg={6} className={styles.certificate} key={index}>
                        <div className={styles.card} onClick={() => props.certClickHandler(index)}>
                            <ZoomIn className={styles['zoom-in-icon']} />
                            {cert.title ? <h2 className={styles.title}>{cert.title}</h2> : null}
                            <img className={styles.image} src={cert.src} alt={cert.alt} />
                        </div>
                    </Col>
                ))}
            </Row>
        </Container>
    );
}
