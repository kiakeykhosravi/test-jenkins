import React from 'react';
import { Modal } from 'react-bootstrap';
import styles from '../styles/certificate-modal.module.scss';
import { CertificateType } from '../types';

export default function CertificateModal(props: {
    showModal: boolean;
    setShowModal;
    certificates: CertificateType[];
    imageId: number;
}) {
    return (
        <Modal
            size="xl"
            centered
            show={props.showModal}
            onHide={() => {
                props.setShowModal(false);
            }}
        >
            <Modal.Body>
                <img
                    className={styles['certificate-modal-img']}
                    src={props.imageId + 1 ? props.certificates[props.imageId].src : ''}
                    alt={props.imageId + 1 ? props.certificates[props.imageId].alt : ''}
                />
            </Modal.Body>
        </Modal>
    );
}
