export type CertificateType = {
    title?: string;
    src: string;
    alt: string;
};
