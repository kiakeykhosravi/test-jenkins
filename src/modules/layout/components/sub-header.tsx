import React from 'react';
import { Container } from 'react-bootstrap';
import classNames from 'classnames';
import styles from '../styles/sub-header.module.scss';

export default function SubHeader(props: {
    fullwidth?: boolean;
    containerClasses?: string;
    innerClasses?: string;
    children?: any;
}) {
    const cn = classNames;

    return props.fullwidth ? (
        <Container className={cn(styles.main, props.innerClasses)} fluid={true}>
            {props.children}
        </Container>
    ) : (
        <div className={cn(styles.abstract, props.containerClasses)}>
            <Container className={cn(styles.main, props.innerClasses)}>{props.children}</Container>
        </div>
    );
}
