import React, { useEffect, useState } from 'react';
import styles from '../styles/datetime.module.scss';
import endpoints from 'api/api-endpoints';
import { IDateTime } from 'models/date-time';
import apiRequest from 'api/api-request';

export default function DatetimeDisplay() {
    const [timer, setTimer] = useState(null);
    const [date, setDate] = useState<Date>(new Date());

    useEffect(() => {
        getTime();
    }, []);

    const getTime = async () => {
        const timeResult = await apiRequest<IDateTime>('get', endpoints.frontend.header_time);
        if (timeResult.status === 'data') {
            setDate(new Date(timeResult.data.currentTime));
            const interval = setInterval(() => {
                setDate((prevState) => {
                    const t = prevState.getTime() + 1000;
                    return new Date(t);
                });
            }, 1000);
            setTimer(interval);
            return clearInterval(timer);
        }
    };

    const renderTopDateRow = (date) => {
        const weekdayPart = new Intl.DateTimeFormat('fa-IR', {
            weekday: 'long',
            timeZone: 'Asia/Tehran',
        }).format(date);

        const timePart = new Intl.DateTimeFormat('fa-IR', {
            hour: '2-digit',
            minute: '2-digit',
            timeZone: 'Asia/Tehran',
        }).format(date);

        return `${weekdayPart} ${timePart}`;
    };

    const renderBottomDateRow = (date) => {
        const dayPart = new Intl.DateTimeFormat('fa-IR', {
            day: '2-digit',
            timeZone: 'Asia/Tehran',
        }).format(date);

        const monthPart = new Intl.DateTimeFormat('fa-IR', {
            month: 'long',
            timeZone: 'Asia/Tehran',
        }).format(date);

        return `${dayPart} ${monthPart}`;
    };

    return (
        <div className={styles['datetime-display']}>
            <div className={styles['top-row']}>{renderTopDateRow(date)}</div>
            <div className={styles['bottom-row']}>{renderBottomDateRow(date)}</div>
        </div>
    );
}
