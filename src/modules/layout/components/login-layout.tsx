import React from 'react';
import styles from 'styles/login.module.scss';
import { Col, Container, Row } from 'react-bootstrap';
import FloatingIcons from 'modules/auth/components/floating-icons';
import classNames from 'classnames';

export default function LoginLayout(props: { header; main; children?; toast }) {
    return (
        <>
            <div className={classNames(styles.layout, 'gradient-background')}>
                <Container>
                    <div className={styles['login-box']}>
                        <Row className={styles.row}>
                            <Col lg={6} className={styles['form-section']}>
                                {props.header}
                                {props.main}
                                {props.children}
                            </Col>
                            <Col lg={6} className={styles['logo-section']}>
                                <FloatingIcons />
                            </Col>
                        </Row>
                    </div>
                </Container>
            </div>
            {props.toast}
        </>
    );
}
