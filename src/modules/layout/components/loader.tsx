import React from 'react';
import styles from '../styles/loader.module.scss';
import { Spinner } from 'react-bootstrap';

export default function Loader() {
    return (
        <div className={styles.page}>
            <div className={styles.loader}>
                <Spinner animation="grow" variant={'primary'} />
                <div className={'mt-4'}>درحال بارگذاری...</div>
            </div>
        </div>
    );
}
