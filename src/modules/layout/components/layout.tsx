import React from 'react';

export default function Layout(props: { header; main; footer; children?; toast }) {
    return (
        <>
            {props.header}
            {props.main}
            {props.children}
            {props.footer}
            {props.toast}
        </>
    );
}
