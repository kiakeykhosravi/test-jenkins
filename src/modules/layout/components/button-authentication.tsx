import React, { useContext, useState } from 'react';
import ButtonRound from 'modules/shared/components/button-round';
import Link from 'next/link';
import apiLogout from 'modules/auth/api/api-logout';
import { toast } from 'react-toastify';
import styles from '../styles/button-authentication.module.scss';
import { AuthContext } from 'contexts/auth-context';
import { useRouter } from 'next/router';

export default function ButtonAuthentication() {
    const route = useRouter();
    const [exitBtnPressed, setExitBtnPressed] = useState(false);

    const authContext = useContext(AuthContext);

    const onClickExit = async (e) => {
        if (e.target.attributes.disabled) {
            return;
        }

        setExitBtnPressed(true);
        const result = await apiLogout();

        if (result.status === 'error') {
            toast.error(result.message || 'خطایی در هنگام خروج رخ داد');
        } else {
            authContext.changeIsLoggedIn(null);
            toast.success('شما با موفقیت از سیستم خارج شدید.');
        }
    };

    const otherProps: any = {
        onClick: onClickExit,
    };

    if (exitBtnPressed) {
        otherProps.disabled = true;
    }

    return authContext.user ? (
        <>
            {/*<ButtonRound*/}
            {/*    variant="outline-secondary"*/}
            {/*    classes={cn('button-no-outline', styles.exit)}*/}
            {/*    otherProps={otherProps}*/}
            {/*>*/}
            {/*    خروج*/}
            {/*</ButtonRound>*/}
            <Link href="/profile/webinars">
                <a title="پروفایل">
                    <ButtonRound classes={styles['user-profile-btn']}>{authContext.user.fullname}</ButtonRound>
                </a>
            </Link>
        </>
    ) : (
        <Link href={`/auth/login-nopass?return=${route.asPath}`}>
            <a title="ورود / ثبت نام">
                <ButtonRound classes={styles['user-profile-btn']}>ورود / ثبت نام</ButtonRound>
            </a>
        </Link>
    );
}
