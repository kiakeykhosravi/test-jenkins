import React from 'react';
import styles from '../styles/footer.module.scss';
import Link from 'next/link';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { ITag } from 'models/tag';
import TagsList from 'modules/shared/components/tags-list';
import AsanImage from 'modules/shared/components/asan-image';

export default function FooterLarge() {
    const renderSocialMediaIcons = () => {
        const iconsData = [
            {
                link: 'https://www.aparat.com/ASANSEMINAR',
                linkTitle: 'آپارات آسان سمینار',
                img: '/images/logos/socialmedias/aparat_black.png',
                imgAlt: 'آپارات آسان سمینار',
            },
            {
                link: 'https://instagram.com/asanseminar',
                linkTitle: 'اینستا گرام آسان سمینار',
                img: '/images/logos/socialmedias/instagram.svg',
                imgAlt: 'اینستا گرام آسان سمینار',
            },
            {
                link: 'https://www.linkedin.com/in/asanseminar-platform-7774061a6/',
                linkTitle: 'لینکدین آسان سمینار',
                img: '/images/logos/socialmedias/linkedin.svg',
                imgAlt: 'لینکدین آسان سمینار',
            },
        ];

        return iconsData.map((data) => {
            return (
                <Link href={data.link} key={data.linkTitle}>
                    <a className={styles.link} title={data.linkTitle} target="_blank">
                        <span className={'d-none'}>{data.linkTitle}</span>
                        <AsanImage src={data.img} className={styles['social-media-icon']} alt={data.imgAlt} />
                    </a>
                </Link>
            );
        });
    };

    // TODO: Separate this function into another component
    const renderTrustSeals = () => {
        const sealData = [
            {
                link: 'https://trustseal.enamad.ir/?id=175967&Code=WUrNMmPCYsJ0l2yZtgNN',
                linkTitle: 'مجوز نماد اعتماد الکترونیک',
                img: 'https://Trustseal.eNamad.ir/logo.aspx?id=175967&Code=WUrNMmPCYsJ0l2yZtgNN',
                imgAlt: 'مجوز نماد اعتماد الکترونیک',
                id: 'WUrNMmPCYsJ0l2yZtgNN',
            },
        ];

        return sealData.map((data) => (
            <div className={styles['trust-link']} key={data.id}>
                {/* eslint-disable-next-line react/jsx-no-target-blank */}
                <a
                    referrerPolicy="origin"
                    target="_blank"
                    title={data.linkTitle}
                    {...(data.link ? { href: data.link } : {})}
                >
                    <span className={'d-none'}>{data.linkTitle}</span>
                    <AsanImage
                        src={data.img}
                        alt={data.imgAlt}
                        className={styles['trust-logo']}
                        referrerPolicy="origin"
                        style={{ cursor: 'pointer' }}
                        id={data.id}
                    />
                </a>
            </div>
        ));
    };

    const renderCertificateLinks = () => {
        const links = [
            {
                href: '/certificates/sabt-alamat',
                linkTitle: 'گواهی نامه ثبت علامت',
                title: 'گواهی نامه ثبت علامت',
            },
            {
                href: '/certificates/nezam-senfi',
                linkTitle: 'مجوز نظام صنفی رایانه‌ای کشور',
                title: 'مجوز نظام صنفی رایانه‌ای کشور',
            },
            {
                href: '/certificates/amoozeshgah-type-b',
                linkTitle: 'مجوز تاسیس آموزشگاه نوع ب',
                title: 'مجوز تاسیس آموزشگاه نوع ب',
            },
            {
                href: '/certificates/kanoon-keshvari',
                linkTitle: 'تفاهم نامه کانون کشوری',
                title: 'تفاهم نامه کانون کشوری',
            },
            {
                href: '/certificates/shooraye-hamahanghi',
                linkTitle: 'تفاهم نامه شورای هماهنگی',
                title: 'تفاهم نامه شورای هماهنگی',
            },
        ];
        return links.map((link, index) => (
            <Link href={link.href} key={index}>
                <a className={styles['certificates-link']} title={link.linkTitle}>
                    {link.title}
                </a>
            </Link>
        ));
    };

    const tags: ITag[] = [
        {
            id: 2,
            title: 'صفحه اول گوگل',
        },
        {
            id: 3,
            title: 'برند پیج',
        },
        {
            id: 4,
            title: 'پلتفرم آموزش آنلاین',
        },
        {
            id: 5,
            title: 'کارت ویزیت الکترونیک',
        },
        {
            id: 6,
            title: 'بازاریابی اینترنتی',
        },
        {
            id: 7,
            title: 'دیجیتال مارکتینگ',
        },
    ];

    const renderAsanseminarLinks = () => {
        const asanseminarData = [
            {
                link: '/candidates',
                linkTitle: 'فرم پیش ثبت نام سازمان',
                linkName: 'پیش ثبت نام سازمان',
            },
            {
                link: 'https://asanseminar.ir/help',
                linkTitle: 'پایگاه دانش',
                linkName: 'پایگاه دانش',
            },
            {
                link: '/plans',
                linkTitle: 'تعرفه ها',
                linkName: 'تعرفه ها',
            },
            {
                link: '/joinus',
                linkTitle: 'همکاری با ما',
                linkName: 'همکاری با ما',
            },
            {
                link: '/contact-us',
                linkTitle: 'تماس با ما',
                linkName: 'تماس با ما',
            },
            {
                link: '/privacy-policy',
                linkTitle: 'سیاست های حریم خصوصی',
                linkName: 'حریم خصوصی',
            },
            {
                link: '/terms',
                linkTitle: 'قوانین استفاده',
                linkName: 'قوانین استفاده',
            },
        ];

        return renderLinks(asanseminarData);
    };

    const renderLinks = (linksData) => {
        return linksData.map((data) => {
            return (
                <Link href={data.link} key={data.linkName}>
                    <a className={styles.link} title={data.linkTitle}>
                        {data.linkName}
                    </a>
                </Link>
            );
        });
    };

    return (
        <footer className={styles['main-container']}>
            <Container className={styles['top-section']}>
                <Row>
                    <Col md={7} className={styles['col-right']}>
                        <h2 className={styles['main-title']}>آسان سمینار</h2>
                        <p className={styles.description}>
                            پلتفرم برگزاری وبینار و آموزش آنلاین آسان سمینار، یکی از قدرتمندترین پلتفرم های برگزاری کلاس
                            های آنلاین است. آسان سمینار همچنین در زمینه دیجیتال مارکتینگ و ابزارهای بازاریابی اینترنتی
                            نیز فعال است. شما می توانید با برند پیج اختصاصی خود در کوتاهترین زمان، رسیدن به صفحه اول
                            گوگل را با آسان سمینار تجربه کنید.
                        </p>
                        <div className={styles['links-container']}>
                            <div className={styles['tags-column']}>
                                <div className={styles['link-title']}>برچسب ها</div>
                                <TagsList
                                    data={tags}
                                    containerClasses={styles['tags-container']}
                                    textClasses={styles.tag}
                                    isH3={true}
                                    firstLinkedTag={{
                                        tag: {
                                            id: 1,
                                            title: 'برگزاری وبینار',
                                        },
                                        link: '/',
                                    }}
                                />
                            </div>
                            <div className={styles['links-column']}>
                                <div className={styles['link-title']}>دسترسی سریع</div>
                                {renderAsanseminarLinks()}
                            </div>
                        </div>
                    </Col>
                    <Col md={5} className={styles['col-left']}>
                        <div className={styles['subscribe-title']}>از جدیدترین‌های آسان سمینار باخبر شوید</div>
                        <div className={styles['subscribe-container']}>
                            <Form className="input-group">
                                <Form.Control
                                    type="text"
                                    id="subscribe"
                                    className={styles['subscribe-input']}
                                    placeholder="آدرس ایمیل خود را وارد کنید"
                                    disabled
                                />
                                <Button className={styles['subscribe-button']} disabled>
                                    ارسال
                                </Button>
                            </Form>
                        </div>
                        <div className={styles['social-media-container']}>
                            <div className={styles['social-media-title']}>
                                آسان سمینار را در شبکه‌های اجتماعی دنبال کنید
                            </div>
                            <div className={styles['social-media-links']}>{renderSocialMediaIcons()}</div>
                        </div>
                        <div className={styles['trust-seals-text']}>مجوز ها</div>
                        <div className={styles['trust-seals']}>{renderTrustSeals()}</div>
                        {renderCertificateLinks()}
                    </Col>
                </Row>
            </Container>
            <div className={styles['contacts-container']}>
                <Container>
                    <Row className={styles.contacts}>
                        <Col xl={4} className={styles['contact-item']}>
                            هنگام برگزاری وبینار و کلاس آنلاین کنار شما هستیم
                        </Col>
                        <Col className={styles['contact-item']} style={{ letterSpacing: '1px', direction: 'ltr' }}>
                            شماره تماس: ۹۱۰۹۰۷۰۳ (۰۲۱)
                        </Col>
                        <Col className={styles['contact-item']} style={{ letterSpacing: '1px' }}>
                            <span>ایمیل: </span>
                            <a href="mailto:support@asanseminar.ir" className={styles['contact-item-email']}>
                                support@asanseminar.ir
                            </a>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className={styles['copyright-container']}>
                <Container>
                    <Row>
                        <div className={styles['copyright-text']}>
                            تمامی حقوق این سایت برای آسان سمینار محفوظ است ۱۴۰۰ - ۱۳۹۸
                        </div>
                    </Row>
                </Container>
            </div>
        </footer>
    );
}
