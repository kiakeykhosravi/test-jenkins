import React from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import styles from '../styles/header.module.scss';
import Link from 'next/link';
import Searchbar from 'modules/shared/components/searchbar';
import DatetimeDisplay from './datetime-display';
import ButtonAuthentication from './button-authentication';
import classNames from 'classnames';
import apiSearchHeader from 'modules/search/api/api-search-header';
import YourMeetingsIndicator from './your-meetings-indicator';
import useWindowDimensions from 'utils/use-window-dimensions';
import AdminButton from './admin-button';

export default function Header() {
    const { width } = useWindowDimensions();

    return (
        <>
            <div className={styles.main}>
                <Container fluid={true} className={styles['header-container']}>
                    <Navbar className={styles.header}>
                        <Link href="/" passHref>
                            <Navbar.Brand href="/">
                                <img
                                    alt="لوگو آسان سمینار"
                                    src="/images/logos/asanseminar-logo.svg"
                                    className={styles['header-logo']}
                                />
                            </Navbar.Brand>
                        </Link>
                        <Nav className="align-items-center flex-grow-1">
                            {width > 576 ? (
                                <span>
                                    <YourMeetingsIndicator isHeader={true} />
                                </span>
                            ) : null}
                            <Link href="/webinars" passHref>
                                <a
                                    className={classNames(styles['header-item'], 'd-none d-lg-block')}
                                    title="وبینار های آسان سمینار"
                                >
                                    وبینار ها
                                </a>
                            </Link>
                            <Nav.Item className={classNames(styles['header-search'], 'flex-grow-1')}>
                                <Searchbar
                                    isHeader={true}
                                    action={apiSearchHeader}
                                    fieldClasses={styles['search-input']}
                                    iconClasses={styles['search-icon']}
                                    containerClasses={styles['search-container']}
                                />
                            </Nav.Item>
                            <Nav.Item className={classNames(styles['header-time'], 'd-none d-lg-inline')}>
                                <DatetimeDisplay />
                            </Nav.Item>
                            <Nav.Item>
                                <AdminButton />
                            </Nav.Item>
                            <Nav.Item className={styles['header-left-buttons']}>
                                <ButtonAuthentication />
                            </Nav.Item>
                        </Nav>
                    </Navbar>
                </Container>
            </div>
        </>
    );
}
