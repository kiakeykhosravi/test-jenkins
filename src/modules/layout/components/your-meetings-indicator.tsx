import React, { useContext, useEffect, useState } from 'react';
import styles from '../styles/your-meetings-indicator.module.scss';
import { ArrowLeft, BriefcaseFill } from 'react-bootstrap-icons';
import Link from 'next/link';
import { AuthContext, IAuthContextType } from 'contexts/auth-context';
import apiMeetings from 'modules/profile/api/api-meetings';
import { MeetingStatusEnum } from 'models/meeting';

export default function YourMeetingsIndicator(props: { isHeader }) {
    const [isRed, setIsRed] = useState(false);
    const authContext = useContext<IAuthContextType>(AuthContext);

    useEffect(() => {
        if (!authContext.user === isRed) {
            if (isRed) {
                setIsRed(false);
            } else {
                apiMeetings(false).then((response) => {
                    if (response && response.meetings.length) {
                        for (const meeting of response.meetings) {
                            if (meeting.status === MeetingStatusEnum.RUNNING) {
                                setIsRed(true);
                                break;
                            }
                        }
                    }
                });
            }
        }
    }, [authContext]);

    return props.isHeader || authContext.user ? (
        <Link href={authContext.user ? '/profile/webinars' : '/'}>
            {authContext.user ? (
                isRed ? (
                    <a className={styles['header-meetings-flashing']} title="جلسات شما">
                        <BriefcaseFill className={styles['briefcase-animation']} />
                        <div>جلسه در حال برگزاری</div>
                        <ArrowLeft className={styles['arrow-animation']} />
                    </a>
                ) : (
                    <a className={styles['header-meetings']} title="جلسات شما">
                        <BriefcaseFill />
                        <div>جلسات شما</div>
                        <ArrowLeft />
                    </a>
                )
            ) : (
                <a title="آسان سمینار" className={styles.asanseminar}>
                    آسان سمینار
                </a>
            )}
        </Link>
    ) : null;
}
