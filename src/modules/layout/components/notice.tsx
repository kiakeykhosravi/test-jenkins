import React from 'react';
import styles from '../styles/notice.module.scss';

type NoticeType = {
    text: string;
    type: 'danger' | 'warning' | 'info';
};

export default function Notice(props: NoticeType | { data: NoticeType[] }) {
    const result = [];

    if ((props as { data: NoticeType[] }).data) {
        const data = (props as { data: NoticeType[] }).data;
        for (const notice of data) {
            result.push(<div className={`${styles.main} ${styles[notice.type]}`}>{notice.text}</div>);
        }
    } else {
        result.push(
            <div className={`${styles.main} ${styles[(props as NoticeType).type]}`}>{(props as NoticeType).text}</div>,
        );
    }

    return <>{result}</>;
}
