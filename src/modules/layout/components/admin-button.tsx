import React, { useContext } from 'react';
import { AuthContext } from 'contexts/auth-context';
import { RoleEnum } from 'models/role';
import styles from '../styles/button-authentication.module.scss';
import apiAdmin from 'modules/profile/api/api-admin';
import { useRouter } from 'next/router';
import ButtonRound from 'modules/shared/components/button-round';
import classNames from 'classnames';

export default function AdminButton() {
    const authContext = useContext(AuthContext);
    const router = useRouter();

    const onClickGoToAdmin = (event) => {
        const element = event.target;
        element.setAttribute('disabled', true);

        apiAdmin()
            .then((response) => {
                if (response) {
                    router.push(response).catch(() => {
                        element.removeAttribute('disabled');
                    });
                }
                element.removeAttribute('disabled');
            })
            .catch((error) => {
                element.removeAttribute('disabled');
            });
    };

    return authContext.user &&
        (authContext.user.role === RoleEnum.ADMIN ||
            authContext.user.role === RoleEnum.SUPPORT ||
            authContext.user.role === RoleEnum.ORGANIZATION_MANAGER ||
            authContext.user.role === RoleEnum.ORGANIZATION_SUPPORT ||
            authContext.user.role === RoleEnum.MANAGER) ? (
        <ButtonRound
            type="button"
            variant="dark"
            otherProps={{ onClick: onClickGoToAdmin }}
            classes={classNames(styles['user-profile-btn'], styles['admin-btn'])}
        >
            مدیریت
        </ButtonRound>
    ) : null;
}
