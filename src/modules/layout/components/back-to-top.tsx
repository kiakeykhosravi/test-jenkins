import React, { useEffect, useState } from 'react';
import styles from '../styles/back-to-top.module.scss';
import { ArrowUp } from 'react-bootstrap-icons';
import Link from 'next/link';
import useWindowScroll from 'utils/use-window-scroll';
import useWindowDimensions from 'utils/use-window-dimensions';
import classNames from 'classnames';
import { useRouter } from 'next/router';

export default function BackToTop() {
    const cn = classNames;
    const router = useRouter();
    const [isFirstRender, setIsFirstRender] = useState(true);
    const [href, setHref] = useState('#app');

    const { scrollY } = useWindowScroll();
    const { height } = useWindowDimensions();

    useEffect(() => {
        setIsFirstRender(false);
    }, []);

    useEffect(() => {
        setHref(window.location.pathname + '#app');
    }, [router.query, router.pathname]);

    return (
        <Link href={href} shallow={true}>
            <a>
                <div className={cn(styles.main, { [styles.hide]: isFirstRender || scrollY < height - 150 })}>
                    <ArrowUp className={styles.icon} />
                </div>
            </a>
        </Link>
    );
}
