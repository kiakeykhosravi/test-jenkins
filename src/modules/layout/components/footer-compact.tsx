import React from 'react';
import styles from '../styles/footer-compact.module.scss';
import { Col, Row } from 'react-bootstrap';
import Link from 'next/link';
import AsanImage from 'modules/shared/components/asan-image';

export default function FooterCompact() {
    const renderSocialMediaIcons = () => {
        const iconsData = [
            {
                link: 'https://www.aparat.com/ASANSEMINAR',
                linkTitle: 'aparat icon',
                img: '/images/logos/socialmedias/aparat_black.png',
                imgAlt: 'aparat icon',
            },
            {
                link: 'https://instagram.com/asanseminar',
                linkTitle: 'instagram icon',
                img: '/images/logos/socialmedias/instagram.svg',
                imgAlt: 'instagram icon',
            },
            {
                link: 'https://www.linkedin.com/in/asanseminar-platform-7774061a6/',
                linkTitle: 'linkedin icon',
                img: '/images/logos/socialmedias/linkedin.svg',
                imgAlt: 'linkedin icon',
            },
            /*
            {
                link: 'https://www.youtube.com/channel/UCstvZznZfOi32Zxoxfv9aXw',
                linkTitle: 'youtube icon',
                img: '/images/logos/socialmedias/youtube.svg',
                imgAlt: 'youtube icon',
            },
            */
        ];

        return iconsData.map((data) => {
            return (
                <Link href={data.link} key={data.linkTitle}>
                    <a className={styles.link} title={data.linkTitle}>
                        <AsanImage src={data.img} className={styles['social-media-icon']} alt={data.imgAlt} />
                    </a>
                </Link>
            );
        });
    };

    return (
        <div className={styles['main-container']}>
            <Row className={styles.contacts}>
                <Col sm={3} lg className={styles['contact-item']}>
                    هنگام برگزاری وبینار و کلاس آنلاین کنار شما هستیم
                </Col>
                <Col sm md lg className={styles['contact-item']} style={{ letterSpacing: '1px', direction: 'ltr' }}>
                    شماره تماس: ۹۱۰۹۰۷۰۳ (۰۲۱)
                </Col>
                <Col sm={5} lg className={styles['social-media-container']}>
                    <span className={styles['social-media-title']}>با ما در ارتباط باشید</span>
                    <span className={styles['social-media-links']}>{renderSocialMediaIcons()}</span>
                </Col>
            </Row>
        </div>
    );
}
