export default async function sitemapServerSideProps(apiFunction, res) {
    const result = await apiFunction();

    if (res) {
        res.setHeader('Content-Type', 'text/xml');
        res.write(result);
        res.end();
        res.finished = true;
    }

    return {
        props: {},
    };
}
