import parseCookies from './parse-cookies';
import { AxiosRequestConfig } from 'axios';
import { cookieNames } from './storage-vars';
import apiAdmin from '../modules/profile/api/api-admin';

export default async function redirectOldAdmin(req) {
    const cookies = parseCookies(req);
    const config: AxiosRequestConfig = {};

    if (cookies && cookies[cookieNames.apiToken]) {
        config.headers = {
            Authorization: `Bearer ${cookies[cookieNames.apiToken]}`,
        };

        const adminRedirect = await apiAdmin(true, req.url, config, true);
        if (adminRedirect) {
            return {
                redirect: {
                    destination: adminRedirect,
                    permanent: false,
                },
            };
        }
    }

    return {
        redirect: {
            destination: `/auth/login-nopass?return=${req.url}`,
            permanent: false,
        },
    };
}
