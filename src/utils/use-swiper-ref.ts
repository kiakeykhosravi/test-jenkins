import { useEffect, useRef, useState } from 'react';

export default function useSwiperRef() {
    const [wrapper, setWrapper] = useState(null);
    const ref = useRef(null);

    useEffect(() => {
        setWrapper(ref.current);
    }, []);

    return [wrapper, ref];
}
