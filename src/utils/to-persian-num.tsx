import persianJs from 'persianjs/persian.min';

export default function toPersianNum(text: string | number) {
    if (typeof text === 'number') {
        text = String(text);
    }
    if (!text || (typeof text !== 'string' && typeof text !== 'number')) {
        return '';
    }

    return persianJs(text).englishNumber().toString();
}
