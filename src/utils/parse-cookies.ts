import cookie from 'cookie';

export default function parseCookies(request) {
    return cookie.parse((request && request.headers && request.headers.cookie) || '');
}
