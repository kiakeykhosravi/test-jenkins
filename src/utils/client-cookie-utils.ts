import { cookieNames } from './storage-vars';
import cookieCutter from 'cookie-cutter';

export function getAuthCookie() {
    return cookieCutter.get(cookieNames.apiToken);
}

export function setAuthCookie(token: string, expires_in: number) {
    const options = {
        expires: expires_in && new Date(Date.now() + expires_in * 1000),
        path: '/',
    };
    cookieCutter.set(cookieNames.apiToken, token, options);
}

export function deleteAuthCookie() {
    const cookie = cookieCutter.get(cookieNames.apiToken);
    if (cookie) {
        cookieCutter.set(cookieNames.apiToken, '', { path: '/', expires: new Date(0), maxAge: -1 });
    }
}
