export class sessionStorageVars {
    public static secret = 'secret';
    public static ask_fullname = 'ask_fullname';
    public static user_mobile = 'user_mobile';
    public static menuIsOpen = 'menuIsOpen';
}

export class cookieNames {
    public static apiToken = 'api-token';
}
