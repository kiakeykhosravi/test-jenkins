import { ITeamMember } from 'models/team-member';
import { IJob } from '../models/job';

export default class schemas {
    public static websiteSchema = `
    {
        "@context": "https://schema.org/",
        "@type": "WebSite",
        "name": "آسان سمینار",
        "url": "https://asanseminar.ir/"
    }`;

    public static plansSchema = `
    {
        "@context": "https://schema.org/", 
        "@type": "Product", 
        "name": "وبینار آنلاین",
        "image": "",
        "description": "به اندازه میزان برگزاری کلاس هایتان پرداخت کنید هر نفرساعت فقط 350 تومان",
        "brand": "آسان سمینار",
        "offers": {
            "@type": "Offer",
            "url": "https://asanseminar.ir/plans",
            "priceCurrency": "IRR",
            "price": "350"
        }
    }`;

    public static teamSchema(teamMember: ITeamMember) {
        return `
            {
              "@context": "https://schema.org",
              "@type": "Person",
              "name": "${teamMember.name}",
              "image": "https://asanseminar.ir${teamMember.img}",
              "jobTitle": "${teamMember.job}"
            }`;
    }

    public static authSchema = `
    {
        "@context": "https://schema.org",
        "@type": "RegisterAction",
        "agent": {
            "@type": "Person",
            "name": "کاربر"
        },
        "object": {
            "@type": "Organization",
            "name": "آسان سمینار"
        }
    }`;

    public static termsSchema = `
    {
        "@context": "https://schema.org/",
        "@type": "WebAPI",
        "name": "قوانین و شرایط استفاده از آسان سمینار",
        "description": "حفظ حریم شخصی کاربران، قوانین و شرایط استفاده از آسان سمینار",
        "termsOfService": "https://asanseminar.ir/terms",
        "provider": {
        "@type": "Organization",
        "name": "آسان سمینار"
        }
    }`;

    public static privacySchema = `
    {
        "@context": "https://schema.org/",
        "@type": "WebAPI",
        "name": "قوانین حفظ حریم خصوصی",
        "description": "حفظ حریم شخصی کاربران، قوانین و شرایط استفاده از آسان سمینار",
        "termsOfService": "https://asanseminar.ir/privacy-policy",
        "provider": {
        "@type": "Organization",
        "name": "آسان سمینار"
        }
    }`;

    public static contactusSchema = `
    {
        "@context":"https://schema.org",
        "@type":"LocalBusiness",
        "name":"Asanseminar - آسان سمینار",
        "image":"https://asanseminar.ir/images/logos/asanseminar-logo.svg",
        "@id":"https://asanseminar.ir",
        "url":"https://asanseminar.ir",
        "telephone":"+982191090703",
        "priceRange":"IRR",
        "address":
            {
                "@type":"PostalAddress",
                "streetAddress":"ارومیه، خیابان سرداران، میدان امام حسین (گول اوستی) ساختمان خیری، طبقه ۳ واحد ۵ و ۶",
                "addressLocality":"ارومیه",
                "postalCode":"5713734415",
                "addressCountry":"IR"
            },
        "geo":
            {
                "@type":"GeoCoordinates",
                "latitude":37.548364,
                "longitude":45.066954
            },
        "openingHoursSpecification":
            {
                "@type":"OpeningHoursSpecification",
                "dayOfWeek":["Monday","Tuesday","Wednesday","Thursday","Saturday","Sunday"],
                "opens":"8:00",
                "closes":"18:00"
            },
        "sameAs":["https://www.instagram.com/asanseminar/","https://www.aparat.com/asanseminar","https://wa.me/989059907032","https://www.linkedin.com/in/asanseminar-platform-7774061a6/"]
    }`;

    public static joinusSchema(job: IJob) {
        return `
            {
                "@context" : "https://schema.org/",
                "@type" : "JobPosting",
                "title" : "${job.title}",
                "description" : "${job.description}",
                "datePosted" : "2021/11/27",
                "validThrough" : "2025/12/12",
                "employmentType" : "CONTRACTOR",
                "hiringOrganization" : 
                    {
                        "@type" : "Organization",
                        "name" : "آسان سمینار",
                        "sameAs" : "https://asanseminar.ir",
                        "logo" : "https://asanseminar.ir/images/logos/asanseminar-logo.svg"
                    },
                "jobLocation": 
                    {
                        "@type": "Place",
                        "address": 
                            {
                                "@type": "PostalAddress",
                                "streetAddress": "ارومیه، خیابان سرداران، میدان امام حسین (گول اوستی) ساختمان خیری، طبقه ۳ واحد ۵ و ۶",
                                "addressLocality":"ارومیه",
                                "postalCode":"5713734415",
                                "addressRegion": "IR",
                                "addressCountry":"IR"
                            }
                    },
                "baseSalary": 
                    {
                        "@type": "MonetaryAmount",
                        "currency": "IRR",
                        "value": 
                            {
                                "@type": "QuantitativeValue",
                                "value": "30000000",
                                "unitText": "MONTH"
                            }
                    }
            }`;
    }
}
