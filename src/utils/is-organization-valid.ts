import apiOrganization from 'modules/organizations/api/api-organization';

export async function isOrganizationValid(slug: string) {
    if (slug) {
        const organizationResult = await apiOrganization(slug);
        if (organizationResult.status === 'data') {
            return organizationResult.data;
        }
    }
    return false;
}
