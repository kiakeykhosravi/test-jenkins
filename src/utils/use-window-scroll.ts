import { useState, useEffect } from 'react';

function getWindowScroll(functionsOnly) {
    if (typeof window !== 'undefined') {
        if (functionsOnly) {
            const { scrollBy, scrollTo } = window;
            return {
                scrollBy,
                scrollTo,
            };
        }
        const { scrollX, scrollY, scrollBy, scrollTo } = window;
        return {
            scrollX,
            scrollY,
            scrollBy,
            scrollTo,
        };
    }
    return {
        scrollX: 0,
        scrollY: 0,
        scrollBy: null,
        scrollTo: null,
    };
}

export default function useWindowScroll(functionsOnly = false) {
    const [windowScroll, setWindowScroll] = useState(getWindowScroll(functionsOnly));

    useEffect(() => {
        function handleScroll() {
            setWindowScroll(getWindowScroll(functionsOnly));
        }

        window.addEventListener('scroll', handleScroll);

        return () => window.removeEventListener('scroll', handleScroll);
    }, []);

    return windowScroll;
}
