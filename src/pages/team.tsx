import React from 'react';
import styles from 'styles/team.module.scss';
import TeamMemberSection from 'modules/team/components/team-member-section';
import { Image } from 'react-bootstrap';
import TeamStorySection from 'modules/team/components/team-story-section';
import Head from 'next/head';
import schemas from 'utils/schemas';
import { ITeamMember } from 'models/team-member';

export default function Index() {
    const teamStoryData = (() => {
        return [
            {
                tab: '1397',
                title: 'تولد آسان سمینار',
                content: `
                    <p>
                        پاییز سال ۹۷ با دوستی در یک کافی شاپ نشستیم و در مورد ایده <b>برگزاری وبینار و آموزش های آنلاین</b> گوهر
                        شناسی به صورت وبیناری صحبت کردیم این دوست بسیار باهوش و ایده پرداز کمک کرد تا این ایده را تا جای ممکن به
                        صورت ذهنی توسعه دهیم بعد روی کاغذ پیاده سازیش کردیم بعد خیلی سریع دست به کار شدیم اول سراغ برنامه زوم
                        رفتیم که متاسفانه موفقیت آمیز نبود بعد به دنبال یک نرم افزار داخلی گشتیم و اسکای روم رو پیدا کردیم و با
                        یک سایت بسیار ساده که فقط سه دکمه در صفحه اول داشت کار را شروع کردیم اولین آموزش گوهر شناسی را با استاد
                        ماسوری در ۱۱ بهمن ۹۷ برگزار کردیم کم کم متوجه نیازمندی ها و در عین حال مشکلات کار شدیم یکی از نیازمندی
                        ها ضبط و بازپخش کلاس های انلاین بود که اسکای روم نداشت یکی از سخت ترین و پیچیده ترین مشکلات این کار
                        مسئله پشتیبانی کاربران و عدم اشنایی کاربران با محیط های آنلاین بود که تا امروز هم این موضوع ادامه دارد
                        ما متوجه شدیم کاربران حتی با اون سه دکمه هم مشکل دارند اویل سعی می کردیم آموزش هایی رو که فکر می کردیم
                        براشون مخاطب داریم رو برگزار کنیم تجربه های خوب و بدی رو پشت سر گذاشتیم.
                    </p>`,
            },
            {
                tab: '1398',
                title: 'آغاز پیشرفت آسان سمینار',
                content: `
                    <p>
                        اوایل سال ۹۸ شروع به رسمی سازی کار کردیم متوجه شدیم راه اندازی بستر آموزش آنلاین نیاز به مجوز و یک سری
                        کارهای حقوقی داره تصمیم به ثبت شرکت گرفتیم. موضوع فعالیت شرکت ارائه و راه اندازی بستر سمینار و همایش و
                        آموزش های آنلاین بود و چون ما اولین شرکتی بودیم که با این موضوع درخواست ثبت داشتیم به شدت کارمشکلی بود و
                        علی رغم این که مشاور حقوقی خبره ای داشتیم ثبت شرکت چند ماه طول کشید یکی دیگه از مراحل، دریافت نماد
                        اعتماد الکترونیک بود همینطور درگاه پرداخت. برای این که بتونیم به مخاطب دسترسی پیدا کنیم با سازمان نظام
                        پزشکی وارد مذاکره شدیم همینطور با جامعه دندانپزشکان ایران ، با اساتید زیادی صحبت کردیم و این ایده رو
                        مطرح کردیم. سال ۹۸ را بیشتر به ساخت و توسعه نرم افزار و زیر ساخت های آسان سمینار مشغول بودیم و زیاد در
                        فکر جذب مخاطب نبودیم. تا این که کرونا شیوع پیدا کرد.
                    </p>`,
            },
            {
                tab: '1399',
                title: 'رویارویی با چالش ها و حل مسئله ها',
                content: `
                    <p>
                        اسفند سال ۹۸ یک جهش بزرگ برای ما بود . کرونا باعث شد یک دفعه بازارآموزش آنلاین و برگزاری وبینار داغ بشه
                        ما به لحاظی کاملا آماده بودیم و به لحاظی غافلگیر شدیم با این حال وبینارهای متعددی را با اساتید
                        دندانپزشکی کشور برگزار کردیم همینطور همکاریمون رو با سازمان محیط زیست و طبکس و زونکن شروع کردیم و آموزش
                        های یک مدرسه رو پوشش می دادیم با توجه به نیازمندی های مدارس و آموزشگاه ها تصمیم گرفیتم که سیستم رو برای
                        ارائه آموزش های آنلاین مدارس و اموزشگاه ها کامل تر و به روزتر کنیم. از شهریور ماه نودو نه با مدارس و
                        آموزشگاه ها شروع به همکاری کردیم. ما دائما در حال ارتقا و بهینه سازی فرایندهای پشتیبانی اسان سمینار
                        بودیم که برخلاف اسم آن بسیار کار پیچیده ای است. یکی از مسائل بسیار مهم در سایت ، رابط کاربری و تجربه
                        کاربری بود که تصمیم گرفتیم رابط کاربری جدیدی طراحی کنیم و تجربه کاربری بهتری رو برای کاربران ایجاد کنیم
                        امیدواریم اوایل سال ۱۴۰۰ بتونیم از این رابط کاربری رو نمایی کنیم و همینطور یکی از دستاوردهای موفق آسان
                        سمینار در سال ۹۹ اخذ مجوز آموزشگاه نوع ب و عقد تفاهم نامه با کانون کشوری آموزشگاه های آزاد فنی حرفه ای و
                        همینطور تفاهم نامه همکاری با شورای هماهنگی موسسان مراکز آموزشی غیر دولتی بود.
                    </p>`,
            },
        ];
    })();

    const teamData: ITeamMember[] = (() => {
        return [
            {
                img: '/images/team/Dr_edited.jpg',
                name: 'هرمز هارطون',
                job: 'بنیان گذار',
                description: '',
            },
            {
                img: '/images/team/DrSarhadian_edited.jpg',
                name: 'ریما سرحدیان',
                job: 'هم بنیان گذار و سهام دار',
                description: '',
            },
            {
                img: '/images/team/GhoshadehDel_edited.jpg',
                name: 'الهام گشاده دل',
                job: 'مدیر فروش',
                description: '',
            },
            {
                img: '/images/team/Akram_edited.jpg',
                name: 'لادن اکرم',
                job: 'مدیر پشتیبانی',
                description: '',
            },
            {
                img: '/images/team/Khosroabadi.jpeg',
                name: 'مونیکا خسروآبادی',
                job: 'مدیر مالی',
                description: '',
            },
            {
                img: '/images/team/farzin_edited.jpg',
                name: 'فرزین خلیل زاده',
                job: 'مدیر فنی',
                description: '',
            },
            {
                img: '/images/team/Soleimannezhad.jpeg',
                name: 'مهدی سلیمان نژاد',
                job: 'مدیر بازاریابی',
                description: '',
            },
            {
                img: '/images/team/Mamad_edited.jpg',
                name: 'محمد افتخاری',
                job: 'توسعه دهنده ارشد بک اند',
                description: '',
            },
            {
                img: '/images/team/Vahid_edited.jpg',
                name: 'وحید ایمانی',
                job: 'توسعه دهنده فرانت اند',
                description: '',
            },
            {
                img: '/images/team/Kianoosh_edited.jpg',
                name: 'کیانوش کیخسروی',
                job: 'دِوآپس',
                description: '',
            },
            {
                img: '/images/team/Naeim.jpg',
                name: 'نعیم شیری',
                job: 'دِوآپس',
                description: '',
            },
            {
                img: '/images/team/Khashayar.jpg',
                name: 'خشایار شاهی',
                job: 'توسعه دهنده بک اند',
                description: '',
            },
            {
                img: '/images/team/Rastgou.jpg',
                name: 'نیما راستگو',
                job: 'توسعه دهنده بک اند',
                description: '',
            },
            {
                img: '/images/team/Rahmati_edited.jpg',
                name: 'فاطمه رحمتی',
                job: 'پشتیبان سایت',
                description: '',
            },
            {
                img: '/images/team/Jaafari_edited.jpg',
                name: 'مریم جعفری',
                job: 'پشتیبان فنی و تولید محتوا',
                description: '',
            },
            {
                img: '/images/team/Salmani.jpeg',
                name: 'نگار سلمانی',
                job: 'فروش و بازاریابی',
                description: '',
            },
            {
                img: '/images/team/Mahmoodzadeh.jpg',
                name: 'صبا محمودزاده',
                job: 'تولید محتوا',
                description: '',
            },
        ];
    })();
    return (
        <>
            <Head>
                <meta name="robots" content="index, follow" />
                <title>تیم آسان سمینار</title>
                <meta name="description" content="تولد و اهداف آسان سمینار" />
                <meta property="og:url" content="https://asanseminar.ir" />
                <meta property="og:title" content="تیم آسان سمینار" />
                <meta property="og:description" content="تولد و اهداف آسان سمینار" />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="تیم آسان سمینار" />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
                {teamData.map((member, index) => {
                    return (
                        <script
                            type="application/ld+json"
                            dangerouslySetInnerHTML={{
                                __html: schemas.teamSchema(member).toString(),
                            }}
                            key={index}
                        />
                    );
                })}
            </Head>
            <div className={styles['team-page']}>
                <div className={styles['header']}>
                    <Image src="/images/team-banner.png" className={styles.img} />
                </div>
                <div className={styles['team-story-section']}>
                    <TeamStorySection data={teamStoryData} />
                </div>
                <div id="team" className={styles['team-member-section']}>
                    <h1 className={styles['title']}>اعضای گروه آسان سمینار</h1>
                    <TeamMemberSection data={teamData} />
                    {/*<h1 className={styles['title']}>دوستان قدیمی تر ما</h1>*/}
                    {/*<h6 className={styles['subtitle']}>*/}
                    {/*    دوستانی که زمانی در آسان سمینار همکار ما بوده اند ولی مدتی است در کنار ما نیستند*/}
                    {/*</h6>*/}
                </div>
            </div>
        </>
    );
}
