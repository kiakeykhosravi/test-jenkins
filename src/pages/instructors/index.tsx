import React, { useEffect, useRef, useState } from 'react';
import styles from 'styles/instructors.module.scss';
import apiSearchInstructors from 'modules/search/api/api-search-instructors';
import { IUserSafe } from 'models/user-safe';
import { useRouter } from 'next/router';
import Pagination from 'modules/shared/components/pagination';
import InstructorSection from 'modules/instructors/components/instructor-section';
import Head from 'next/head';
import { Container, Spinner } from 'react-bootstrap';
import Searchbar from 'modules/shared/components/searchbar';
import { toast } from 'react-toastify';

export async function getServerSideProps({ query }) {
    const q = query.q || '';
    const page = query.page || '';

    const instructorsResult = await apiSearchInstructors(true, q, page);
    const instructors = instructorsResult.status === 'data' ? instructorsResult.data.instructors : [];
    const lastPage = instructorsResult.status === 'data' ? instructorsResult.data.lastpage : 1;

    return {
        props: {
            instructors,
            lastPage,
        },
    };
}

export default function Instructors(props: { instructors: IUserSafe[]; lastPage: number }) {
    const router = useRouter();
    const instructorsRef = useRef(null);

    const [firstRender, setFirstRender] = useState(true);
    const [loading, setLoading] = useState(false);
    const [instructorsData, setInstructorsData] = useState(props.instructors);
    const [lastPage, setLastPage] = useState(props.lastPage);
    const [currentPage, setCurrentPage] = useState((router.query.page as string) || '1');

    useEffect(() => {
        if (!firstRender) instructorsRef.current.scrollIntoView();
        setFirstRender(false);
    }, [instructorsData]);

    const handleSearch = (query: string) => {
        let queryParams = '';
        if (query !== '') queryParams = `?q=${query}`;
        window.history.replaceState(
            { ...window.history.state, url: `/instructors` + queryParams, as: `/instructors` + queryParams },
            '',
            window.location.origin + `/instructors` + queryParams,
        );
        setCurrentPage('1');
        sendRequest({ phrase: query, page: 1 });
    };

    const handlePageClick = (data) => {
        const urlSearchParams = new URLSearchParams(window.location.search);
        const params = Object.fromEntries(urlSearchParams.entries());
        const queryParamList: string[] = [];
        params.q ? queryParamList.push(`q=${params.q}`) : '';
        let queryParams = '?';
        for (const param of queryParamList) {
            queryParams += param + '&';
        }
        queryParams += `page=${data.selected + 1}`;
        window.history.replaceState(
            { ...window.history.state, url: `/instructors` + queryParams, as: `/instructors` + queryParams },
            '',
            window.location.origin + `/instructors` + queryParams,
        );
        setCurrentPage(data.selected + 1);
        sendRequest({ phrase: params.q, page: data.selected + 1 });
    };

    const sendRequest = (params: { phrase?; page? }) => {
        setLoading(true);
        const data = {
            phrase: params.phrase || '',
            page: params.page || 1,
        };
        const result = apiSearchInstructors(false, data.phrase, data.page);
        result
            .then((response) => {
                if (response.status === 'data') {
                    setInstructorsData(response.data.instructors);
                    setLastPage(response.data.lastpage);
                }
                setLoading(false);
            })
            .catch((error) => {
                setLoading(false);
                console.log('An error occurred while getting webinars');
                toast.error('مشکلی در دریافت اطلاعات به وجود آمد!');
            });
    };

    return (
        <div className={styles['body']}>
            <Head>
                <title>اساتید آسان سمینار</title>
                <meta name="description" content="اساتید همکار با پلتفرم برگزاری وبینار و آموزش آنلاین آسان سمینار" />
                <meta property="og:url" content="https://asanseminar.ir" />
                <meta property="og:title" content="اساتید آسان سمینار" />
                <meta
                    property="og:description"
                    content="اساتید همکار با پلتفرم برگزاری وبینار و آموزش آنلاین آسان سمینار"
                />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="اساتید آسان سمینار" />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
            </Head>
            <h1 className={styles['title']} ref={instructorsRef}>
                اساتید آسان سمینار
            </h1>
            <Container>
                <Searchbar
                    initialValue={(router.query.q as string) || ''}
                    placeHolder="بهترین اساتید آنلاین کشور را پیدا کنید ..."
                    containerClasses={styles['search-container']}
                    fieldClasses={styles['search-input']}
                    iconClasses={styles['search-icon']}
                    nonEmptyClasses={styles['search-text-non-empty']}
                    customHandler={handleSearch}
                    setLoading={setLoading}
                />
            </Container>
            {loading ? (
                <div className={styles.loader}>
                    <Spinner animation="grow" variant={'primary'} />
                    <div style={{ marginRight: '1rem' }}>درحال بارگذاری...</div>
                </div>
            ) : (
                <InstructorSection data={instructorsData} />
            )}
            {!loading && lastPage > 1 ? (
                <Pagination page={currentPage} handlePageClick={handlePageClick} pageCount={lastPage} />
            ) : null}
        </div>
    );
}
