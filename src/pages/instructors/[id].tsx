import React, { useEffect } from 'react';
import styles from 'styles/instructor.module.scss';
import { Col, Container, Row } from 'react-bootstrap';
import InstructorSidebar from 'modules/instructors/components/instructor-sidebar';
import { apiInstructor } from 'modules/instructors/api/api-instructor';
import { IUser } from 'models/user';
import { useRouter } from 'next/router';
import Head from 'next/head';
import Custom404 from '../404';

export const getStaticPaths = async () => {
    return {
        paths: [],
        fallback: 'blocking',
    };
};

export const getStaticProps = async ({ params }) => {
    const id = params.id ? params.id : '';

    const instructorResult = await apiInstructor(id);
    const instructor = instructorResult.status === 'data' ? instructorResult.data : null;

    const origin = process.env.NODE_ENV === 'production' ? process.env.NEXT_PUBLIC_APP_URL : 'http://localhost:3000';

    return {
        props: {
            instructor,
            origin,
        },
        revalidate: 120,
    };
};

export default function Instructor(props: { instructor: IUser; origin: string }) {
    const router = useRouter();
    const { id } = router.query as { id: string };
    useEffect(() => {
        if (props.instructor) {
            let fixedUrl = router.asPath;
            const indexOfRandomSlug = fixedUrl.indexOf(id) + id.length;
            if (indexOfRandomSlug !== -1) {
                fixedUrl = fixedUrl.slice(0, indexOfRandomSlug).concat(`/${props.instructor.fullname}`);
                window.history.replaceState(window.history.state, '', window.location.origin + fixedUrl);
            }
        }
    }, []);

    if (!props.instructor) return <Custom404 />;
    else {
        return (
            <div className={styles['master-page']}>
                <Head>
                    <title>{props.instructor.fullname} - اساتید آسان سمینار</title>
                    <meta name="description" content={props.instructor.description} />
                    <meta property="og:url" content={props.origin + router.asPath} />
                    <meta property="og:title" content={`صفحه ${props.instructor.fullname} در آسان سمینار`} />
                    <meta property="og:description" content={props.instructor.description} />
                    <meta property="og:type" content={'website'} />
                    <meta property="og:image" content="/images/avatar-placeholder.png" />
                    <meta property="og:image:type" content={'image/png'} />
                    <meta property="og:image:alt" content={props.instructor.fullname} />
                    <meta property="og:site_name" content="آسان سمینار - پلتفرم آموزش آنلاین" />
                    <meta property="og:locale" content="fa_IR" />
                    <meta name="twitter:card" content="summary_large_image" />
                    <meta name="twitter:title" content={`صفحه ${props.instructor.fullname} در آسان سمینار`} />
                    <meta name="twitter:image:alt" content={props.instructor.fullname} />
                </Head>
                <Container>
                    <Row>
                        <Col lg={3} />
                        <Col lg={9}>
                            <h1 className={styles['master-page-title']}>صفحه استاد {props.instructor.fullname}</h1>
                        </Col>
                        <Col lg={3} xl={3}>
                            <InstructorSidebar data={props.instructor} />
                        </Col>
                        <Col lg={9} xl={7}>
                            <div className={styles['main']}>
                                <h2 className={styles.title}>در مورد استاد</h2>
                                <h5 className="mb-3">شغل استاد</h5>
                                <div className={styles.description}>{props.instructor.description}</div>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
