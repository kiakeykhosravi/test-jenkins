import React, { useState } from 'react';
import styles from 'styles/certificates.module.scss';
import CertificateBody from 'modules/certificates/components/certificate-body';
import CertificateModal from 'modules/certificates/components/certificate-modal';
import Head from 'next/head';

export default function SabtAlamat() {
    const [showCertificateModal, setShowCertificateModal] = useState(false);

    const certificate = [
        {
            alt: 'گواهی نامه ثبت علامت',
            src: '/images/certificates/alamat.jpg',
        },
    ];

    return (
        <>
            <Head>
                <title>گواهی نامه ثبت علامت</title>
            </Head>
            <div className={styles.main}>
                <CertificateBody
                    pageTitle="گواهی نامه ثبت علامت"
                    certificates={certificate}
                    certClickHandler={() => {
                        setShowCertificateModal(true);
                    }}
                />
            </div>
            <CertificateModal
                showModal={showCertificateModal}
                setShowModal={setShowCertificateModal}
                certificates={certificate}
                imageId={0}
            />
        </>
    );
}
