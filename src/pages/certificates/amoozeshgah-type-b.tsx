import React, { useState } from 'react';
import styles from 'styles/certificates.module.scss';
import CertificateBody from 'modules/certificates/components/certificate-body';
import CertificateModal from 'modules/certificates/components/certificate-modal';
import Head from 'next/head';

export default function AmoozeshgahTypeB() {
    const [showCertificateModal, setShowCertificateModal] = useState(false);

    const certificate = [
        {
            alt: 'مجوز تاسیس آموزشگاه نوع ب',
            src: '/images/certificates/taasis.jpeg',
        },
    ];

    return (
        <>
            <Head>
                <title>مجوز تاسیس آموزشگاه نوع ب</title>
            </Head>
            <div className={styles.main}>
                <CertificateBody
                    pageTitle="مجوز تاسیس آموزشگاه نوع ب"
                    certificates={certificate}
                    certClickHandler={() => {
                        setShowCertificateModal(true);
                    }}
                />
            </div>
            <CertificateModal
                showModal={showCertificateModal}
                setShowModal={setShowCertificateModal}
                certificates={certificate}
                imageId={0}
            />
        </>
    );
}
