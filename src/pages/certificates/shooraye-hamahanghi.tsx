import React, { useState } from 'react';
import styles from 'styles/certificates.module.scss';
import { CertificateType } from 'modules/certificates/types';
import CertificateBody from 'modules/certificates/components/certificate-body';
import CertificateModal from 'modules/certificates/components/certificate-modal';
import Head from 'next/head';

export default function ShoorayeHamahanghi() {
    const [showCertificateModal, setShowCertificateModal] = useState(false);
    const [modalImageId, setModalImageId] = useState(-1);

    const certificates: CertificateType[] = [
        {
            title: 'صفحه ۱',
            alt: 'تفاهم نامه شورای هماهنگی صفحه یک',
            src: '/images/certificates/shooraye-hamahanghi-1.png',
        },
        {
            title: 'صفحه ۲',
            alt: 'تفاهم نامه شورای هماهنگی صفحه دو',
            src: '/images/certificates/shooraye-hamahanghi-2.png',
        },
        {
            title: 'صفحه ۳',
            alt: 'تفاهم نامه شورای هماهنگی صفحه سه',
            src: '/images/certificates/shooraye-hamahanghi-3.png',
        },
    ];

    return (
        <>
            <Head>
                <title>تفاهم نامه شورای هماهنگی</title>
            </Head>
            <div className={styles.main}>
                <CertificateBody
                    pageTitle="تفاهم نامه شورای هماهنگی"
                    certificates={certificates}
                    certClickHandler={(index) => {
                        setModalImageId(index);
                        setShowCertificateModal(true);
                    }}
                />
            </div>
            <CertificateModal
                showModal={showCertificateModal}
                setShowModal={setShowCertificateModal}
                certificates={certificates}
                imageId={modalImageId}
            />
        </>
    );
}
