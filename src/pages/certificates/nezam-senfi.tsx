import React, { useState } from 'react';
import styles from 'styles/certificates.module.scss';
import CertificateBody from 'modules/certificates/components/certificate-body';
import CertificateModal from 'modules/certificates/components/certificate-modal';
import Head from 'next/head';

export default function NezamSenfi() {
    const [showCertificateModal, setShowCertificateModal] = useState(false);

    const certificate = [
        {
            alt: 'مجوز فعالیت آسان‌سمینار از سازمان نظام صنفی رایانه‌ای کشور',
            src: '/images/certificates/faaliyat.jpeg',
        },
    ];

    return (
        <>
            <Head>
                <title>مجوز نظام صنفی رایانه‌ای کشور</title>
            </Head>
            <div className={styles.main}>
                <CertificateBody
                    pageTitle="مجوز نظام صنفی رایانه‌ای کشور"
                    certificates={certificate}
                    certClickHandler={() => {
                        setShowCertificateModal(true);
                    }}
                />
            </div>
            <CertificateModal
                showModal={showCertificateModal}
                setShowModal={setShowCertificateModal}
                certificates={certificate}
                imageId={0}
            />
        </>
    );
}
