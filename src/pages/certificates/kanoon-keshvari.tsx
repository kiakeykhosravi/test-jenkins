import React, { useState } from 'react';
import styles from 'styles/certificates.module.scss';
import { CertificateType } from 'modules/certificates/types';
import CertificateBody from 'modules/certificates/components/certificate-body';
import CertificateModal from 'modules/certificates/components/certificate-modal';
import Head from 'next/head';

export default function KanoonKeshvari() {
    const [showCertificateModal, setShowCertificateModal] = useState(false);
    const [modalImageId, setModalImageId] = useState(-1);

    const certificates: CertificateType[] = [
        {
            title: 'صفحه ۱',
            alt: 'تفاهم نامه کانون کشوری صفحه یک',
            src: '/images/certificates/tafahom-nameh-1.png',
        },
        {
            title: 'صفحه ۲',
            alt: 'تفاهم نامه کانون کشوری صفحه دو',
            src: '/images/certificates/tafahom-nameh-2.png',
        },
        {
            title: 'صفحه ۳',
            alt: 'تفاهم نامه کانون کشوری صفحه سه',
            src: '/images/certificates/tafahom-nameh-3.png',
        },
    ];

    return (
        <>
            <Head>
                <title>تفاهم نامه کانون کشوری</title>
            </Head>
            <div className={styles.main}>
                <CertificateBody
                    pageTitle="تفاهم نامه کانون کشوری"
                    certificates={certificates}
                    certClickHandler={(index) => {
                        setModalImageId(index);
                        setShowCertificateModal(true);
                    }}
                />
            </div>
            <CertificateModal
                showModal={showCertificateModal}
                setShowModal={setShowCertificateModal}
                certificates={certificates}
                imageId={modalImageId}
            />
        </>
    );
}
