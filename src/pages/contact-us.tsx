import React from 'react';
import styles from 'styles/contact-us.module.scss';
import { Col, Container, Row } from 'react-bootstrap';
import Head from 'next/head';
import { CheckCircleFill, EnvelopeFill, GeoAltFill, TelephoneFill } from 'react-bootstrap-icons';
import classNames from 'classnames';
import schemas from 'utils/schemas';

export default function ContactUs() {
    const renderInnerPhones = () => {
        const items = [
            'داخلی ۱: واحد کاربران و دانش آموزان',
            'داخلی ۲: واحد پشتیبانی سازمان ها',
            'داخلی ۳: واحد فروش',
            'داخلی ۴: واحد مالی',
        ];
        return items.map((text, index) => {
            return (
                <Col md={6} key={index}>
                    <div className={styles['inner-item']}>
                        <CheckCircleFill className={classNames(styles.icon, styles['inner-icon'])} />
                        <h3 className={styles['inner-text']}>{text}</h3>
                    </div>
                </Col>
            );
        });
    };
    return (
        <>
            <Head>
                <meta name="robots" content="index, follow" />
                <title>با آسان سمینار تماس بگیرید</title>
                <meta name="description" content="راه های ارتباطی با آسان سمینار 02191090103" />
                <meta property="og:url" content="https://asanseminar.ir" />
                <meta property="og:title" content="با آسان سمینار تماس بگیرید" />
                <meta property="og:description" content="راه های ارتباطی با آسان سمینار 02191090103" />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="با آسان سمینار تماس بگیرید" />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={{
                        __html: schemas.contactusSchema,
                    }}
                />
            </Head>
            <Container className={styles.main}>
                <div className={styles['top-section']}>
                    <hr className={styles.hr} />
                    <img src="/images/contact.png" alt="تماس با ما" className={styles['top-logo']} />
                    <hr className={styles.hr} />
                </div>
                <h1 className={styles.title}>با آسان سمینار تماس بگیرید</h1>
                <p>برای برگزاری وبینار، کلاس آنلاین و برند پیج در کنار شما هستیم.</p>
                <div className={styles['contact-section']}>
                    <TelephoneFill className={styles.icon} />
                    <h2 className={styles['contact-section-text']}>
                        تلفن ثابت: <a href="tel:+982191090703">۹۱۰۹۰۷۰۳ (۰۲۱)</a>
                    </h2>
                </div>
                <div className={styles.inner}>
                    <h2 className={styles['contact-section-text']}>داخلی ها:</h2>
                    <Row>{renderInnerPhones()}</Row>
                </div>
                <h2 className={styles.h2}>پاسخگویی : تمام روز های هفته از ساعت ۸ صبح تا ۱۸ عصر</h2>
                <div className={styles['contact-section']}>
                    <EnvelopeFill className={styles.icon} />
                    <h2 className={styles['contact-section-text']}>
                        ایمیل:{' '}
                        <a href="mailto:info@asanseminar.ir?subject=%D8%AA%D9%85%D8%A7%D8%B3%20%D8%A8%D8%A7%20%D8%A2%D8%B3%D8%A7%D9%86%20%D8%B3%D9%85%DB%8C%D9%86%D8%A7%D8%B1">
                            info@asanseminar.ir
                        </a>
                    </h2>
                </div>
                <div className={styles['contact-section']}>
                    <GeoAltFill className={classNames(styles.icon, styles['address-icon'])} />
                    <h2 className={classNames(styles['contact-section-text'], styles['address-text'])}>
                        نشانی : ارومیه، خیابان سرداران، میدان امام حسین (گول اوستی) ساختمان خیری، طبقه ۳ واحد ۵ و ۶
                    </h2>
                </div>
                <div>
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d470.2247268365821!2d45.066637349075265!3d37.54841230292807!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4005536c2d503e4d%3A0xb1ecdaae6e79b306!2zMzfCsDMyJzU0LjEiTiA0NcKwMDQnMDEuMCJF!5e0!3m2!1sen!2s!4v1636280196332!5m2!1sen!2s"
                        width="100%"
                        height="450"
                        loading="lazy"
                    />
                    <br />
                </div>
            </Container>
        </>
    );
}
