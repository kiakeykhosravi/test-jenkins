import React, { useEffect, useState } from 'react';
import classNames from 'classnames';
import styles from 'modules/candidates/styles/candidates.module.scss';
import { Button, Container, Form } from 'react-bootstrap';
import Link from 'next/link';
import { OrganizationType } from 'models/organization';
import { useRouter } from 'next/router';
import Head from 'next/head';

export default function Candidate() {
    const cn = classNames;

    const [referrer, setReferrer] = useState<number | null>(null);
    const [organizationType, setOrganizationType] = useState(OrganizationType.SCHOOL);

    const router = useRouter();

    useEffect(() => {
        let ref;
        if (typeof router.query.referrer !== 'string' || !parseInt(router.query.referrer as string, 10)) {
            ref = null;
        } else {
            ref = parseInt(router.query.referrer as string);
        }

        setReferrer(ref);
    }, [router.query.referrer]);

    const goToSelectedType = () => {
        if (organizationType) {
            const url = `/candidates/${organizationType.replace('_', '-')}${referrer ? `?referrer=${referrer}` : ''}`;
            router.push(url);
        }
    };

    return (
        <>
            <Head>
                <meta name="robots" content="index, follow" />
                <title>ثبت نام در آسان سمینار</title>
                <meta name="description" content="ثبت نام مدارس، آموزشگاه ها، سازمان، شرکت و اداره در آسان سمینار" />
                <meta property="og:url" content="https://asanseminar.ir" />
                <meta property="og:title" content="ثبت نام در آسان سمینار" />
                <meta
                    property="og:description"
                    content="ثبت نام مدارس، آموزشگاه ها، سازمان، شرکت و اداره در آسان سمینار"
                />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="ثبت نام در آسان سمینار" />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
            </Head>
            <div className={styles.body}>
                <Container className={styles.container}>
                    <div className={cn(styles.main, 'col-12 col-lg-10 col-xl-9')}>
                        <Link href="/">
                            <a title="آسان سمینار" className={styles['asanseminar-logo-link']}>
                                <img
                                    src="/images/asanseminar_full.png"
                                    alt="آسان سمینار"
                                    className={styles['asanseminar-logo']}
                                />
                            </a>
                        </Link>
                        <h1 className={styles['title-bar']}>به خانواده آسان سمینار بپیوندید</h1>
                        <div className={styles.box}>
                            <h2 className={styles['box-title']}>
                                در کدام یک از بخش های زیر تمایل به پیوستن به آسان سمینار را دارید؟
                            </h2>
                            <Form>
                                <Form.Group controlId="organization_type" className={cn('row', styles['field-row'])}>
                                    <Form.Label className={cn('col-12 col-md-4', styles['field-title'])}>
                                        نوع سازمان
                                    </Form.Label>
                                    <div className="col-12 col-md-7 col-lg-8">
                                        <Form.Control
                                            as="select"
                                            name="organization_type"
                                            onChange={(e) => {
                                                // eslint-disable-next-line @typescript-eslint/ban-ts-comment
                                                // @ts-ignore
                                                setOrganizationType(e.target.selectedOptions[0].value);
                                            }}
                                        >
                                            <option value={OrganizationType.SCHOOL}>مدرسه</option>
                                            <option value={OrganizationType.EDUCATIONAL_INSTITUTION}>آموزشگاه</option>
                                            <option value={OrganizationType.OTHER}>سازمان / شرکت / اداره</option>
                                        </Form.Control>
                                    </div>
                                </Form.Group>

                                <div className="row">
                                    <div className="col-12 col-md-4" />
                                    <div className="col-12 col-md-8" />
                                </div>

                                <div style={{ textAlign: 'center' }}>
                                    <Button
                                        type="button"
                                        variant="dark"
                                        style={{
                                            width: '100%',
                                            marginTop: '1rem',
                                            padding: '0.5rem 0',
                                            fontSize: '1.1rem',
                                        }}
                                        onClick={goToSelectedType}
                                    >
                                        پیوستن به آسان سمینار
                                    </Button>
                                </div>
                            </Form>
                        </div>
                    </div>
                </Container>
            </div>
        </>
    );
}
