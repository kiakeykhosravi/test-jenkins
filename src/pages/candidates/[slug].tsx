import React, { useEffect, useState } from 'react';
import apiShowCandidate from 'modules/candidates/api/api-show-candidate';
import { CandidateTypeEnToFaEnum, CandidateTypeEnum, ICandidate } from 'models/candidate';
import FinalCandidates from 'modules/candidates/components/final-candidates';
import apiProvinces from 'modules/candidates/api/api-provinces';
import { IProvince } from 'models/province';
import InitialCandidates from 'modules/candidates/components/initial-candidates';
import BrandInitialCandidates from 'modules/candidates/components/brand-initial-candidates';
import { ISpecialty } from 'models/specialty';
import apiSpecialties from 'modules/candidates/api/api-specialties';
import SubscriptionPage from 'modules/candidates/components/subscription-page';
import FinalBrandpageCandidates from 'modules/candidates/components/final-brandpage-candidates';

export const getServerSideProps = async ({ params }) => {
    params.slug = params.slug.replace('-', '_');
    const type = CandidateTypeEnToFaEnum[params.slug] ? params.slug : 'id';

    if (type === 'id') {
        const candidateResponse = await apiShowCandidate(params.slug);
        const candidate = candidateResponse.status === 'data' ? candidateResponse.data : null;
        return {
            props: {
                type,
                candidate,
                paramsID: params.slug,
                provinces: null,
            },
        };
    } else if (type === CandidateTypeEnum.SUBSCRIPTION) {
        return {
            props: {
                type,
                candidate: null,
                paramsID: null,
                provinces: null,
            },
        };
    } else {
        const provincesResult = await apiProvinces();
        const provinces = provincesResult.status === 'data' ? provincesResult.data : [];
        return {
            props: {
                type,
                candidate: null,
                paramsID: null,
                provinces,
            },
        };
    }
};

export default function Candidate(props: {
    type: CandidateTypeEnum | 'id';
    candidate?: ICandidate;
    paramsID?: string;
    provinces?: IProvince[];
}) {
    const [specialties, setSpecialties] = useState<ISpecialty[]>([]);

    useEffect(() => {
        apiSpecialties().then((response: { data?: ISpecialty[]; status: 'data' | 'error' }) => {
            if (response.status === 'data') {
                setSpecialties(response.data);
            }
        });
    }, []);

    const isBrandpage = (type) => {
        return [
            CandidateTypeEnum.BRANDPAGE_PREMIUM,
            CandidateTypeEnum.BRANDPAGE_PLUS,
            CandidateTypeEnum.BRANDPAGE_GOLDEN,
            CandidateTypeEnum.BRANDPAGE_PLATINUM,
        ].includes(type);
    };

    if (props.type === 'id') {
        if (isBrandpage(props.candidate && props.candidate.type)) {
            return (
                <FinalBrandpageCandidates
                    candidate={props.candidate}
                    paramsID={props.paramsID}
                    specialties={specialties}
                />
            );
        }
        return <FinalCandidates candidate={props.candidate} paramsID={props.paramsID} />;
    } else if (props.type === CandidateTypeEnum.SUBSCRIPTION) {
        return <SubscriptionPage />;
    } else if (!isBrandpage(props.type)) {
        return <InitialCandidates provinces={props.provinces} type={props.type} />;
    }
    return <BrandInitialCandidates provinces={props.provinces} specialties={specialties} type={props.type} />;
}
