import React, { useState } from 'react';
import styles from 'styles/enter.module.scss';
import { Button, Col, Container, Form, Image, Row } from 'react-bootstrap';
import Link from 'next/link';
import { ArrowLeft, ArrowRight, Calendar, Clock } from 'react-bootstrap-icons';
import Head from 'next/head';
import parseCookies from 'utils/parse-cookies';
import { cookieNames } from 'utils/storage-vars';
import apiUser from 'modules/profile/api/api-user';
import { AxiosRequestConfig } from 'axios';
import { IUser } from 'models/user';
import apiGuestInfo from 'modules/auth/api/api-guest-info';
import { IOrganizationCompact } from 'models/organization-compact';
import { IWebinarCompact } from 'models/webinar-compact';
import { IMeeting, MeetingStatusEnum } from 'models/meeting';
import { useRouter } from 'next/router';
import apiGuestEnter from 'modules/auth/api/api-guest-enter';
import { toast } from 'react-toastify';
import toEnglishNum from 'utils/to-english-num';
import classNames from 'classnames';

export async function getServerSideProps({ params, req, query }) {
    const lang = query.lang || '';
    let user: IUser | null = null;
    let organization: IOrganizationCompact = null;
    let webinar: IWebinarCompact = null;
    let meetings: IMeeting[] = [];
    let errorMessage: string = null;

    if (!params.secret || !params.secret.trim().length) {
        errorMessage = lang === 'en' ? 'Your class code is incorrect' : 'کد کلاس ورودی شما صحیح نمی باشد';
    } else {
        const cookies = parseCookies(req);
        const config: AxiosRequestConfig = {};

        if (cookies && cookies[cookieNames.apiToken]) {
            config.headers = {
                Authorization: `Bearer ${cookies[cookieNames.apiToken]}`,
            };

            user = await apiUser(true, config);
        }
        const guestInfo = await apiGuestInfo(params.secret, config);

        if (guestInfo.data) {
            organization = guestInfo.data.organization;
            webinar = guestInfo.data.webinar;
            meetings = guestInfo.data.meetings;
        } else {
            errorMessage = guestInfo.message;
        }
    }

    return {
        props: {
            meetings,
            secret: params.secret,
            errorMessage,
            organization,
            webinar,
            user,
            lang,
        },
    };
}

export default function Guest(props: {
    meetings: IMeeting[];
    secret: string;
    errorMessage?: string;
    organization?: IOrganizationCompact;
    webinar?: IWebinarCompact;
    user?: IUser;
    lang: string;
}) {
    const enGuest = props.lang === 'en';
    const router = useRouter();
    const [fullnameField, setFullnameField] = useState('');
    const [fieldError, setFieldError] = useState('');

    const onClickEnterMeeting = (event) => {
        if (!props.user && (!fullnameField || fullnameField.length < 3 || fullnameField.length > 300)) {
            toast.error(enGuest ? 'Please enter your full name' : 'برای ورود به کلاس لطفا نام خود را وارد کنید', {
                autoClose: 5000,
            });
            return;
        }

        const element: any = event.target;
        const fullname = (props.user && props.user.fullname) || fullnameField;

        element.setAttribute('disabled', true);
        apiGuestEnter(props.secret, { fullname, meeting_id: element.id })
            .then((response) => {
                if (response) {
                    toast.success(
                        enGuest
                            ? 'You are being transferred to the meeting...'
                            : 'شما در حال انتقال به جلسه می باشید ...',
                    );
                    router.push(response);
                } else {
                    element.removeAttribute('disabled');
                }
            })
            .catch((error) => {
                element.removeAttribute('disabled');
            });
    };

    const renderMeetings = () => {
        return props.meetings.length ? (
            props.meetings.map((meeting) => {
                return (
                    <Row key={meeting.id} className={styles.meeting} style={{ direction: enGuest ? 'ltr' : 'rtl' }}>
                        <Col md={enGuest ? 5 : 6} className={enGuest ? 'me-md-4' : ''}>
                            <div className={styles.title}>{meeting.title}</div>
                        </Col>
                        <Col md={2}>
                            <div className={styles.date}>
                                <div className={classNames(styles.time, enGuest ? styles.english : '')}>
                                    <Calendar />
                                    <span
                                        style={{
                                            marginTop: enGuest ? '3px' : '0px',
                                            marginLeft: enGuest ? '5px' : '0',
                                            marginRight: enGuest ? '0' : '5px',
                                        }}
                                    >
                                        {enGuest
                                            ? toEnglishNum(meeting.datetime_fadate.replace(/-/g, '/'))
                                            : meeting.datetime_fadate.replace(/-/g, '/')}
                                    </span>
                                </div>
                                <div className={classNames(styles.time, enGuest ? styles.english : '')}>
                                    <Clock />
                                    <span
                                        style={{
                                            marginTop: enGuest ? '3px' : '0px',
                                            marginLeft: enGuest ? '5px' : '0',
                                            marginRight: enGuest ? '0' : '5px',
                                        }}
                                    >
                                        {enGuest ? toEnglishNum(meeting.datetime_fatime) : meeting.datetime_fatime}
                                    </span>
                                </div>
                                {enGuest ? (
                                    <span className={classNames(styles.time, enGuest ? styles.english : '')}>
                                        (Tehran time)
                                    </span>
                                ) : null}
                            </div>
                        </Col>
                        <Col md={4} className={'d-flex justify-content-center'}>
                            {meeting.status === MeetingStatusEnum.RUNNING ? (
                                <Button
                                    variant="success"
                                    className={styles['btn-success']}
                                    onClick={onClickEnterMeeting}
                                    id={String(meeting.id)}
                                >
                                    {enGuest ? null : <ArrowLeft className={styles['btn-enter-icon']} />}
                                    {enGuest ? 'Enter Meeting' : 'ورود به جلسه'}
                                    {enGuest ? <ArrowRight className={styles['btn-enter-icon']} /> : null}
                                </Button>
                            ) : (
                                <span className={styles['status-general']}>
                                    {enGuest
                                        ? meeting.status === MeetingStatusEnum.PLANNED
                                            ? 'Planned'
                                            : meeting.status
                                        : meeting.status_fa}
                                </span>
                            )}
                        </Col>
                    </Row>
                );
            })
        ) : (
            <div className={styles['no-meetings']}>
                {enGuest
                    ? 'There are no running sessions for this webinar yet'
                    : 'هنوز هیچ جلسه‌ای در این وبینار در حال برگزاری نمی‌باشد'}
            </div>
        );
    };

    if (props.errorMessage) {
        return (
            <div className={styles.guest}>
                <Head>
                    <title>{enGuest ? 'Guests Entry - Asanseminar' : 'ورود مهمان - آسان سمینار'}</title>
                </Head>
                <Container>
                    <Row className="justify-content-center">
                        <Col lg={10} xl={8}>
                            <div className={styles.box}>
                                <div className={styles.brand}>
                                    <Link href="/">
                                        <a className={styles.link} title={enGuest ? 'Home page' : 'صفحه اصلی'}>
                                            <Image
                                                src="images/logos/asanseminar-logo.svg"
                                                alt="آسان سمینار"
                                                className={styles['brand-img']}
                                            />
                                        </a>
                                    </Link>
                                    <br />
                                    <div className={styles.error}>
                                        {enGuest ? 'Your class code is incorrect' : props.errorMessage}
                                    </div>
                                    <Link href="/">
                                        <a
                                            className={styles['brand-title']}
                                            title={enGuest ? 'Return to Asanseminar' : 'بازگشت به آسان سمینار'}
                                        >
                                            {enGuest ? 'Return to Asanseminar' : 'بازگشت به آسان سمینار'}
                                        </a>
                                    </Link>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }

    return (
        <>
            <div className={styles.guest}>
                <Head>
                    <title>{enGuest ? 'Guests Entry - Asanseminar' : 'ورود مهمان - آسان سمینار'}</title>
                </Head>
                <Container>
                    <Row className="justify-content-center">
                        <Col lg={10} xl={8}>
                            <div className={styles.box}>
                                <div className={styles.brand}>
                                    <Link href="/">
                                        <a className={styles.link} title={enGuest ? 'Home page' : 'صفحه اصلی'}>
                                            <Image
                                                src={props.organization.logo.url}
                                                alt={props.organization.title}
                                                className={styles['brand-img']}
                                            />
                                        </a>
                                    </Link>
                                    <div className={styles['brand-title']}>{props.organization.title}</div>
                                </div>
                                <div className={styles.login}>
                                    <div className={styles.title}>
                                        {enGuest ? 'Guests Entry' : 'ورود مهمان به کلاس'}
                                    </div>
                                    <Link href={`/webinars/${props.webinar.id}`}>
                                        <a className={styles.link} title={enGuest ? 'Class page link' : 'صفحه کلاس'}>
                                            {props.webinar.title}
                                        </a>
                                    </Link>
                                </div>
                            </div>
                            <div className={styles.box}>
                                {props.user ? (
                                    <div className={styles['form-label']}>{props.user.fullname}</div>
                                ) : (
                                    <Form className={styles.form}>
                                        <Form.Group className={styles['form-group']}>
                                            <Form.Label className={styles['form-label']}>
                                                {enGuest ? 'Fullname' : 'نام و نام خانوادگی'}
                                            </Form.Label>
                                            <Form.Control
                                                type="text"
                                                placeholder={
                                                    enGuest
                                                        ? 'Please enter your full name'
                                                        : 'نام و نام خانوادگی خود را وارد کنید.'
                                                }
                                                className={styles['form-control']}
                                                onChange={(e) => {
                                                    setFullnameField(e.target.value);
                                                    if (e.target.value && e.target.value.length < 3) {
                                                        setFieldError(
                                                            enGuest
                                                                ? 'Name field must be at least 3 characters'
                                                                : 'طول نام باید حداقل سه کاراکتر باشد',
                                                        );
                                                    } else if (e.target.value && e.target.value.length > 300) {
                                                        setFieldError(
                                                            enGuest
                                                                ? 'Name field should not exceed 300 characters'
                                                                : 'طول نام نباید از ۳۰۰ کاراکتر بیشتر باشد',
                                                        );
                                                    } else {
                                                        setFieldError('');
                                                    }
                                                }}
                                                value={fullnameField}
                                            />
                                        </Form.Group>
                                        <div className={styles['field-error']}>{fieldError}</div>
                                    </Form>
                                )}
                                {renderMeetings()}
                            </div>
                        </Col>
                        <Col sm="12">
                            <Link href="/">
                                <a title={enGuest ? 'Home page' : 'صفحه اصلی'}>
                                    <Image
                                        src="/images/asanseminar_full.png"
                                        alt="آسان سمینار"
                                        className={styles['footer-img']}
                                    />
                                </a>
                            </Link>
                        </Col>
                    </Row>
                </Container>
            </div>
        </>
    );
}
