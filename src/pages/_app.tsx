import React, { useEffect, useState } from 'react';
import '../../public/vendor/bootstrap/css/bootstrap.rtl.min.css';
import '../../public/fonts/style.css';
import 'jalali-react-big-calendar/lib/css/react-big-calendar.css';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import 'styles/global.scss';
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import 'styles/buttons.scss';
import 'react-toastify/dist/ReactToastify.css';
import Layout from 'modules/layout/components/layout';
import Header from 'modules/layout/components/header';
import FooterLarge from 'modules/layout/components/footer-large';
import { Router, useRouter } from 'next/router';
import FooterCompact from 'modules/layout/components/footer-compact';
import { ToastContainer, Flip } from 'react-toastify';
import LoginLayout from 'modules/layout/components/login-layout';
import Loader from 'modules/layout/components/loader';
import ProfileLayout from 'modules/profile/components/layout/profile-layout';
import Custom404 from './404';
import Head from 'next/head';
import MainHeader from 'modules/blog/components/layout/main-header';
import ArticleHeader from 'modules/blog/components/layout/article-header';
import BlogFooter from 'modules/blog/components/layout/blog-footer';
import OrganizationBlogMainHeader from 'modules/blog/components/layout/organization-blog-main-header';
import OrganizationBlogArticleHeader from 'modules/blog/components/layout/organization-blog-article-header';
import { AuthContext, IAuthContextType } from 'contexts/auth-context';
import apiUser from 'modules/profile/api/api-user';
import { TagsFilterContext } from 'contexts/tags-filter-context';
import { ITag } from 'models/tag';
import { IUser } from 'models/user';
import BackToTop from 'modules/layout/components/back-to-top';
import { getAuthCookie } from 'utils/client-cookie-utils';
import BrandpageHeader from 'modules/brandpage/components/layout/brandpage-header';
import BrandpageFooter from 'modules/brandpage/components/layout/brandpage-footer';
import schemas from 'utils/schemas';

NProgress.configure({ showSpinner: false });
Router.events.on('routeChangeStart', () => NProgress.start());
Router.events.on('routeChangeComplete', () => NProgress.done());
Router.events.on('routeChangeError', () => NProgress.done());

// TODO: Clean up the contexts related code
export default function Asanseminar({ Component, pageProps }) {
    const [preloading, setPreloading] = useState(true);
    const [goftinoObject, setGoftinoObject] = useState(null);

    const changeAuthState = (newValue: IUser | null) => {
        setAuthState((prevState: IAuthContextType) => {
            const shouldUpdate =
                typeof newValue !== typeof prevState.user ||
                (prevState.user && prevState.user.id) !== (newValue && newValue.id);
            if (shouldUpdate) {
                return {
                    user: newValue || null,
                    changeIsLoggedIn: changeAuthState,
                };
            }
            return prevState;
        });
    };

    const [authState, setAuthState] = useState<IAuthContextType>({
        user: null,
        changeIsLoggedIn: changeAuthState,
    });

    const changeTagsFilter = (newValue: ITag[]) => {
        setTagsFilterState((prevState) => {
            return {
                tags: newValue,
                changeTags: changeTagsFilter,
            };
        });
    };

    const [tagsFilterState, setTagsFilterState] = useState({
        tags: [],
        changeTags: changeTagsFilter,
    });

    const route = useRouter();

    const isPageAuth =
        route.pathname === '/auth/verify' ||
        route.pathname === '/auth/login' ||
        route.pathname === '/auth/login-nopass';

    useEffect(() => {
        window.addEventListener('load', () => {
            if (preloading) {
                setPreloading(false);
            }
        });

        document.addEventListener('DOMContentLoaded', () => {
            if (preloading) {
                setPreloading(false);
            }
        });

        setTimeout(function () {
            if (preloading) {
                setPreloading(false);
            }
        }, 1500);
    }, []);

    useEffect(() => {
        if (!route.pathname.startsWith('/profile') && !isPageAuth && route.pathname !== '/guest' && getAuthCookie()) {
            apiUser()
                .then((response) => {
                    setAuthState({
                        user: response ? response : null,
                        changeIsLoggedIn: changeAuthState,
                    });
                })
                .catch(() => {
                    setAuthState({
                        user: null,
                        changeIsLoggedIn: changeAuthState,
                    });
                });
        }
    }, []);

    useEffect(() => {
        window.addEventListener('goftino_ready', () => {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            setGoftinoObject(Goftino);
            updateGoftinoUser(authState.user);
        });
    }, []);

    useEffect(() => {
        if (goftinoObject) {
            updateGoftinoUser(authState.user);
        }
    }, [authState]);

    const updateGoftinoUser = (user: IUser) => {
        if (user) {
            goftinoObject.setUser({
                email: user.email || '',
                name: user.fullname,
                phone: user.mobile,
                about: 'https://m.asanseminar.ir/asanseminar/admin/supporting/users?id=' + user.id,
                forceUpdate: true,
            });
        }
    };

    let LayoutElement = Layout;
    let HeaderElement = Header;
    let FooterElement = FooterLarge;

    // TODO: Separate layouts to prevent this mess
    if (isPageAuth) {
        LayoutElement = LoginLayout;
        HeaderElement = null;
        FooterElement = null;
    }
    if (route.pathname.startsWith('/blog/')) {
        HeaderElement = ArticleHeader;
        FooterElement = BlogFooter;
    }
    if (
        route.pathname.startsWith('/blog/page') ||
        route.pathname.startsWith('/blog/search') ||
        route.pathname.startsWith('/blog/tag') ||
        route.pathname.startsWith('/blog/category')
    ) {
        HeaderElement = MainHeader;
        FooterElement = BlogFooter;
    } else if (route.pathname.startsWith('/joinus')) {
        FooterElement = FooterCompact;
    } else if (route.pathname.startsWith('/enter') || route.pathname.startsWith('/candidates')) {
        HeaderElement = null;
        FooterElement = null;
    } else if (route.pathname.startsWith('/profile')) {
        LayoutElement = ProfileLayout;
        HeaderElement = null;
        FooterElement = FooterCompact;
    } else if (route.pathname.startsWith('/organizations/[slug]/blog/[id]')) {
        HeaderElement = OrganizationBlogArticleHeader;
        FooterElement = BlogFooter;
    } else if (route.pathname.startsWith('/organizations/[slug]/blog')) {
        HeaderElement = OrganizationBlogMainHeader;
        FooterElement = BlogFooter;
    } else if (route.pathname.startsWith('/organizations/[slug]')) {
        HeaderElement = null;
        FooterElement = null;
    } else if (route.pathname.startsWith('/brandpage')) {
        HeaderElement = BrandpageHeader;
        FooterElement = BrandpageFooter;
    } else if (Component === Custom404) {
        HeaderElement = null;
        FooterElement = null;
    }

    return (
        <TagsFilterContext.Provider value={tagsFilterState}>
            <AuthContext.Provider value={authState}>
                {preloading ? <Loader /> : null}
                <div id="app" style={preloading ? { opacity: 0 } : { position: 'relative' }}>
                    <Head>
                        <meta charSet="utf-8" />
                        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=3.0" />

                        {route.pathname.startsWith('/organizations/[slug]') ? null : (
                            <>
                                <link rel="icon" type="image/ico" href="/favicon.ico" />
                                {/* Goftino Script */}
                                <script
                                    type="text/javascript"
                                    dangerouslySetInnerHTML={{
                                        __html: `!function(){var i="Gl8TWj",a=window,d=document;function g(){var g=d.createElement("script"),s="https://www.goftino.com/widget/"+i,l=localStorage.getItem("goftino_"+i);g.async=!0,g.src=l?s+"?o="+l:s;d.getElementsByTagName("head")[0].appendChild(g);}"complete"===d.readyState?g():a.attachEvent?a.attachEvent("onload",g):a.addEventListener("load",g,!1);}();`,
                                    }}
                                />

                                {/* Website Schema */}
                                <script
                                    type="application/ld+json"
                                    dangerouslySetInnerHTML={{
                                        __html: schemas.websiteSchema,
                                    }}
                                />
                            </>
                        )}

                        {/* Yektanet Script */}
                        <script
                            type="text/javascript"
                            dangerouslySetInnerHTML={{
                                __html: `!function (t, e, n) { t.yektanetAnalyticsObject = n, t[n] = t[n] || function () { t[n].q.push(arguments) }, t[n].q = t[n].q || []; var a = new Date, r = a.getFullYear().toString() + "0" + a.getMonth() + "0" + a.getDate() + "0" + a.getHours(), c = e.getElementsByTagName("script")[0], s = e.createElement("script"); s.id = "ua-script-67mUO8dx"; s.dataset.analyticsobject = n; s.async = 1; s.type = "text/javascript"; s.src = "https://cdn.yektanet.com/rg_woebegone/scripts_v3/67mUO8dx/rg.complete.js?v=" + r, c.parentNode.insertBefore(s, c) }(window, document, "yektanet");`,
                            }}
                        />

                        {/* Google Analytics */}
                        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-151941750-1" />
                        <script
                            dangerouslySetInnerHTML={{
                                __html:
                                    "window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-151941750-1');",
                            }}
                        />
                    </Head>
                    <LayoutElement
                        header={HeaderElement ? <HeaderElement /> : null}
                        main={<Component {...pageProps} />}
                        footer={FooterElement ? <FooterElement /> : null}
                        toast={
                            <ToastContainer
                                position="bottom-right"
                                autoClose={3500}
                                hideProgressBar={true}
                                newestOnTop={false}
                                closeOnClick
                                rtl
                                pauseOnFocusLoss
                                draggable
                                pauseOnHover={false}
                                transition={Flip}
                                style={{ zIndex: 9999999999 }}
                            />
                        }
                    />
                    <BackToTop />
                </div>
            </AuthContext.Provider>
        </TagsFilterContext.Provider>
    );
}
