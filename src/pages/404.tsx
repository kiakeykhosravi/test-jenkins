import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import styles from 'styles/error.module.scss';
import Head from 'next/head';
import AsanImage from 'modules/shared/components/asan-image';

export default function Custom404() {
    return (
        <>
            <Head>
                <title>صفحه یافت نشد!</title>
                <meta name="description" content="جای نگرانی نیست! خطای 404 رخ داده است" />
                <meta property="og:url" content="https://asanseminar.ir" />
                <meta property="og:title" content="صفحه یافت نشد!" />
                <meta property="og:description" content="جای نگرانی نیست! خطای 404 رخ داده است" />
                <meta property="og:type" content={'website'} />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content={'image/png'} />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="صفحه یافت نشد!" />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
            </Head>
            <div className={styles.page}>
                <Container className={styles.container}>
                    <Row className={styles.row}>
                        <Col lg={6} className={styles['col-right']}>
                            <div className={styles['main-text']}>
                                <div className={styles['main-text-code']}>۴۰۴</div>
                                <div>صفحه یافت نشد!</div>
                            </div>
                            <div className={styles.description}>
                                <div className="my-1">جای نگرانی نیست! خطای ۴۰۴ رخ داده</div>
                                <div className="my-1">آدرس وارد شده رو بررسی کنید ممکنه اشتباه باشه</div>
                            </div>
                            <div className={styles.poem}>
                                <div style={{ fontWeight: 'bold' }}>شاعر میگه:</div>
                                <div className="my-1 me-sm-4 d-sm-inline-block">
                                    از کشت‌زار چرخ و زمین کاین دو گاو راست
                                </div>
                                <div className="my-1 text-sm-end d-sm-block d-xl-inline-block">
                                    یک جو نیافتم که به خرمن درآورم
                                </div>
                                <div className={styles.poet}>خاقانی</div>
                            </div>
                        </Col>
                        <Col lg={6} className={styles['col-left']}>
                            <AsanImage src="/images/404-img.png" alt="صفحه 404" className={styles.img} />
                        </Col>
                    </Row>
                </Container>
            </div>
        </>
    );
}
