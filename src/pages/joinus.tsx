import React, { useState } from 'react';
import Link from 'next/link';
import { Button } from 'react-bootstrap';
import JoinusTopSection from 'modules/joinus/components/joinus-top-section';
import { Container, Row, Col } from 'react-bootstrap';
import styles from 'styles/joinus.module.scss';
import RightColumn from 'modules/joinus/components/right-column';
import LeftColumn from 'modules/joinus/components/left-column';
import Head from 'next/head';
import schemas from 'utils/schemas';
import { IJob } from 'models/job';

export default function Joinus() {
    const [success, setSuccess] = useState(false);
    const jobs: IJob[] = (() => {
        return [
            {
                title: 'فنی و برنامه نویسی',
                description:
                    'استخدام برنامه نویس و مهندس کامپیوتر در حوزه توسعه دهنده بک اند و رابط کاربری و سئو و دواپس',
            },
            {
                title: 'محتوا سازی',
                description: 'استخدام طراح گرافیک و تولیدکننده محتوا',
            },
            {
                title: 'تدریس',
                description: 'همکاری با اساتید برای برگزاری دوره های آموزشی مختلف',
            },
            {
                title: 'بازاریابی و فروش',
                description: 'استخدام افراد با موقعیت های مدیر فروش و بازاریاب و سفیر',
            },
        ];
    })();
    return (
        <>
            <Head>
                <meta name="robots" content="index, follow" />
                <title>همکاری با آسان سمینار</title>
                <meta name="description" content="همکاری با آسان سمینار - فرصت های شغلی" />
                <meta property="og:url" content="https://asanseminar.ir" />
                <meta property="og:title" content="همکاری با آسان سمینار" />
                <meta property="og:description" content="همکاری با آسان سمینار - فرصت های شغلی" />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="همکاری با آسان سمینار" />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
                {jobs.map((job, index) => {
                    return (
                        <script
                            type="application/ld+json"
                            dangerouslySetInnerHTML={{
                                __html: schemas.joinusSchema(job).toString(),
                            }}
                            key={index}
                        />
                    );
                })}
            </Head>
            {success ? (
                <div className={styles.middle}>
                    <div>
                        <p className={styles['success-text']}>
                            درخواست شما با موفقیت ثبت شد!
                            <br />
                            تیم آسان سمینار در اسرع وقت با شما تماس خواهند گرفت.
                        </p>
                        <div className="text-center">
                            <Link href="/" passHref>
                                <Button type="button" variant="info">
                                    بازگشت به صفحه اصلی
                                </Button>
                            </Link>
                        </div>
                    </div>
                </div>
            ) : (
                <>
                    <JoinusTopSection />
                    <div className={styles['main-section']}>
                        <Container>
                            <Row>
                                <Col md={6} className={styles['right-column']}>
                                    <RightColumn setSuccess={setSuccess} />
                                </Col>
                                <Col md={6} className={styles['left-column']}>
                                    <LeftColumn />
                                </Col>
                            </Row>
                        </Container>
                    </div>
                </>
            )}
        </>
    );
}
