import apiSitemapIndex from 'modules/sitemap/api/api-sitemap-index';
import sitemapServerSideProps from 'utils/sitemap-server-side-props';

export async function getServerSideProps({ res }) {
    return await sitemapServerSideProps(apiSitemapIndex, res);
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
export default function IndexSitemap() {}
