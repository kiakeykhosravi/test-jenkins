import React, { useContext, useEffect, useState } from 'react';
import styles from 'styles/profile-transactions.module.scss';
import { Form, Table } from 'react-bootstrap';
import { Search } from 'react-bootstrap-icons';
import toPersianNum from 'utils/to-persian-num';
import Pagination from 'modules/shared/components/pagination';
import apiGetTransactions from 'modules/profile/api/api-get-transactions';
import { ITransaction } from 'models/transaction';
import parseCookies from 'utils/parse-cookies';
import { AxiosRequestConfig } from 'axios';
import { IUser } from 'models/user';
import { cookieNames } from 'utils/storage-vars';
import apiUser from 'modules/profile/api/api-user';
import { AuthContext } from 'contexts/auth-context';
import toPrice from 'utils/to-price';
import ProfileTableLoading from 'modules/profile/components/profile-table-loading';
import Head from 'next/head';
import { toast } from 'react-toastify';

export async function getServerSideProps({ req }) {
    const cookies = parseCookies(req);
    const config: AxiosRequestConfig = {};

    let transactions: ITransaction[] = [];
    let lastPage = 1;
    let user: IUser | null = null;

    if (cookies && cookies[cookieNames.apiToken]) {
        config.headers = {
            Authorization: `Bearer ${cookies[cookieNames.apiToken]}`,
        };

        const transactionsResult = await apiGetTransactions(true, 1, config);
        if (transactionsResult.status === 'data') {
            transactions = transactionsResult.data.transactions;
            lastPage = transactionsResult.data.lastpage;
        }

        user = await apiUser(true, config);
        if (!user) {
            return {
                redirect: {
                    destination: '/auth/login-nopass?return=/profile/transactions',
                    permanent: false,
                },
            };
        }
    } else {
        return {
            redirect: {
                destination: '/auth/login-nopass?return=/profile/transactions',
                permanent: false,
            },
        };
    }

    return {
        props: {
            user,
            transactions,
            lastPage,
        },
    };
}

export default function Transactions(props: { user: IUser; transactions: ITransaction[]; lastPage: number }) {
    const [allTransactions, setAllTransactions] = useState<ITransaction[][]>([props.transactions]);
    const [transactions, setTransactions] = useState<ITransaction[]>(props.transactions);
    const [lastPage, setLastPage] = useState(props.lastPage);
    const [currentPage, setCurrentPage] = useState(1);
    const [loading, setLoading] = useState(false);

    const authContext = useContext(AuthContext);

    useEffect(() => {
        authContext.changeIsLoggedIn(props.user);
    }, []);

    const getTransactions = async (page: number) => {
        setLoading(true);
        const result = await apiGetTransactions(false, page);
        if (result.status === 'data') {
            const at = allTransactions;
            at[page - 1] = result.data.transactions;
            setAllTransactions(at);
            setTransactions(result.data.transactions);
            setLastPage(result.data.lastpage);
        } else {
            toast.error('مشکلی در دریافت اطلاعات به وجود آمد!');
        }
        setLoading(false);
    };

    const tableData = () => {
        return transactions.map((transaction) => {
            return (
                <tr key={transaction.id} className={styles['table-body-row']}>
                    <td>{toPersianNum(transaction.id)}</td>
                    <td>{toPersianNum(transaction.via_fa)}</td>
                    <td>{toPersianNum(transaction.type_fa)}</td>
                    <td>{toPrice(transaction.amount)}</td>
                    <td className={transaction.success ? styles['payment-success'] : styles['payment-fail']}>
                        {transaction.success ? 'موفق' : 'ناموفق'}
                    </td>
                    <td>{toPersianNum(transaction.description)}</td>
                    <td>
                        <div>{toPersianNum(transaction.created_at_fadate)}</div>
                        <div>{toPersianNum(transaction.created_at_fatime)}</div>
                    </td>
                </tr>
            );
        });
    };

    const pageHandler = async (data) => {
        setCurrentPage(data.selected + 1);
        if (allTransactions[data.selected]) {
            setTransactions(allTransactions[data.selected]);
        } else {
            await getTransactions(data.selected + 1);
        }
    };

    return (
        <>
            <Head>
                <title>پروفایل - تراکنش ها</title>
            </Head>
            <div className={styles.transactions}>
                <div className={styles.header}>
                    <div className={styles.search}>
                        <Form.Control placeholder={'جستجو...'} className={styles['search-text']} />
                        <Search className={styles['search-icon']} />
                    </div>
                    <div className={styles['show-row-count']}>
                        <span>نمایش</span>
                        <Form.Control as="select" className={styles.select}>
                            <option>{toPersianNum('10')}</option>
                            <option>{toPersianNum('25')}</option>
                            <option>{toPersianNum('50')}</option>
                        </Form.Control>
                        <span>رکورد</span>
                    </div>
                </div>
                <>
                    <div className="table-responsive">
                        <Table borderless hover className={styles.table}>
                            <thead className={styles['table-head']}>
                                <tr>
                                    <th>شناسه</th>
                                    <th>از طریق</th>
                                    <th>نوع</th>
                                    <th>مبلغ</th>
                                    <th>پرداخت</th>
                                    <th>توضیحات</th>
                                    <th>زمان ایجاد</th>
                                </tr>
                            </thead>
                            {transactions.length ? (
                                <>
                                    <tbody className={styles['table-body']}>{tableData()}</tbody>
                                </>
                            ) : null}
                        </Table>
                    </div>
                    {!transactions.length ? (
                        <div className={styles['not-found']}>هیچ داده ای در جدول وجود ندارد</div>
                    ) : loading ? (
                        <div style={{ height: '5rem' }} />
                    ) : (
                        <div className={styles['table-data-counter']}>
                            <span className={styles.span}>نمایش</span>
                            <span className={styles.span}>{toPersianNum((currentPage - 1) * 10 + 1)}</span>
                            <span className={styles.span}>تا</span>
                            <span className={styles.span}>
                                {toPersianNum((currentPage - 1) * 10 + transactions.length)}
                            </span>
                            <span className={styles.span}>از</span>
                            <span className={styles.span}>{toPersianNum(lastPage * 10)}</span>
                            <span>رکورد</span>
                        </div>
                    )}
                </>
                {lastPage > 1 ? (
                    <div className={styles.pagination}>
                        <Pagination pageCount={lastPage} page={String(currentPage)} handlePageClick={pageHandler} />
                    </div>
                ) : null}
                {loading ? <ProfileTableLoading /> : null}
            </div>
        </>
    );
}
