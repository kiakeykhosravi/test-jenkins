import React, { useContext, useEffect, useRef } from 'react';
import Link from 'next/link';
import styles from 'styles/profile-webinar-marketing.module.scss';
import Head from 'next/head';
import parseCookies from 'utils/parse-cookies';
import { AxiosRequestConfig } from 'axios';
import { IUser } from 'models/user';
import { cookieNames } from 'utils/storage-vars';
import apiUser from 'modules/profile/api/api-user';
import { AuthContext } from 'contexts/auth-context';
import { Image, Table } from 'react-bootstrap';
import { ArrowReturnLeft, EmojiFrown, GraphUp, Link45deg } from 'react-bootstrap-icons';
import apiUserWebinarMarketingStats from 'modules/profile/api/api-user-webinar-marketing-stats';
import { IMarketingStatsWebinar } from 'modules/profile/responses';
import { toast } from 'react-toastify';
import toPrice from 'utils/to-price';
import toPersianNum from 'utils/to-persian-num';

export async function getServerSideProps({ req }) {
    const cookies = parseCookies(req);
    const config: AxiosRequestConfig = {};

    let marketing_webinars_stats = [];
    let user: IUser | null = null;
    const origin = process.env.NODE_ENV === 'production' ? process.env.NEXT_PUBLIC_APP_URL : 'http://localhost:3000';

    if (cookies && cookies[cookieNames.apiToken]) {
        config.headers = {
            Authorization: `Bearer ${cookies[cookieNames.apiToken]}`,
        };

        user = await apiUser(true, config);
        if (!user) {
            return {
                redirect: {
                    destination: '/auth/login-nopass?return=/profile/webinar-marketing-sell',
                    permanent: false,
                },
            };
        }

        const marketingWebinarsStatsResult = await apiUserWebinarMarketingStats(true, config);
        marketing_webinars_stats = (marketingWebinarsStatsResult && marketingWebinarsStatsResult.webinars) || [];
    } else {
        return {
            redirect: {
                destination: '/auth/login-nopass?return=/profile/webinar-marketing-sell',
                permanent: false,
            },
        };
    }

    return {
        props: {
            marketing_webinars_stats,
            user,
            origin,
        },
    };
}

export default function WebinarMarketingSell(props: {
    marketing_webinars_stats: IMarketingStatsWebinar[];
    user: IUser;
    origin: string;
}) {
    const authContext = useContext(AuthContext);
    const pageUrlElementRef = useRef(null);
    useEffect(() => {
        authContext.changeIsLoggedIn(props.user);
    }, []);

    const onclickCopyLink = () => {
        pageUrlElementRef.current.select();
        document.execCommand('copy');
        pageUrlElementRef.current.blur();
        toast.success('لینک معرف با موفقیت کپی شد');
    };

    const totalSellShare = () => {
        let total = 0;
        if (props.marketing_webinars_stats.length) {
            props.marketing_webinars_stats.map((item) => (total += item.marketing_summary.share));
        }
        return toPrice(total);
    };

    const totalSellCount = () => {
        let total = 0;
        if (props.marketing_webinars_stats.length) {
            props.marketing_webinars_stats.map((item) => (total += item.marketing_summary.count));
        }
        return toPersianNum(total);
    };

    const renderStats = () => {
        return props.marketing_webinars_stats.map((s, index) => {
            return (
                <tr key={index}>
                    <td>
                        <div className={styles['table-webinar-title']}>
                            <Image
                                src={s.webinar.organization.logo && s.webinar.organization.logo.url}
                                alt={s.webinar.title}
                                className={styles['table-webinar-logo']}
                            />
                            <div className={styles['table-webinar-title-text']}>
                                <Link href={`/webinars/${s.webinar.id}/${s.webinar.title.trim().replace(/\s+/g, '-')}`}>
                                    <a title={s.webinar.title}>{s.webinar.title}</a>
                                </Link>
                            </div>
                        </div>
                    </td>
                    <td>{toPersianNum(s.marketing_summary.count)}</td>
                    <td>{toPrice(Number(s.marketing_summary.share))}</td>
                    <td>
                        <div onClick={onclickCopyLink} className={styles['copy-link']}>
                            <Link45deg className={styles.icon} />
                            کپی
                            <input
                                type="text"
                                value={`${props.origin}/webinars/${s.webinar.id}?rf=${props.user.id}`}
                                className={styles['hidden-page-link']}
                                ref={pageUrlElementRef}
                            />
                        </div>
                    </td>
                </tr>
            );
        });
    };

    return (
        <>
            <Head>
                <title>پروفایل - آمار فروش وبینار</title>
            </Head>
            <div className={styles.main}>
                <div className={styles.top}>
                    <div className={styles['title-text']}>
                        <GraphUp className={styles['top-right-icon']} />
                        <h4 className={styles.title}>آمار فروش</h4>
                    </div>
                    <Link href={'/profile/webinar-marketing'}>
                        <a title={'آمار فروش شما'} className={styles['top-stats-text']}>
                            <ArrowReturnLeft className={styles['top-left-icon']} />
                            بازگشت به لیست وبینارها
                        </a>
                    </Link>
                </div>
                {props.marketing_webinars_stats.length ? (
                    <>
                        <Table borderless striped className={styles.table}>
                            <thead className={styles['table-head']}>
                                <tr>
                                    <th>وبینار</th>
                                    <th>تعداد فروش</th>
                                    <th>درآمد (تومان)</th>
                                    <th>لینک معرف شما</th>
                                </tr>
                            </thead>
                            <tbody>
                                {renderStats()}
                                <tr className={styles['table-last-row']}>
                                    <td>مجموع</td>
                                    <td>{totalSellCount()}</td>
                                    <td>{totalSellShare()}</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </Table>
                    </>
                ) : (
                    <div className={styles['no-sell-container']}>
                        <div className={styles['no-sell']}>
                            <EmojiFrown className={styles['no-sell-icon']} />
                            شما تا کنون هیچ فروشی نداشته اید!
                        </div>
                    </div>
                )}
            </div>
        </>
    );
}
