import React, { useContext, useEffect, useState } from 'react';
import styles from 'styles/profile-credit.module.scss';
import apiGetCredit from 'modules/profile/api/api-get-credit';
import Loader from 'modules/layout/components/loader';
import AddCredit from 'modules/profile/components/add-credit';
import parseCookies from 'utils/parse-cookies';
import { AxiosRequestConfig } from 'axios';
import { IUser } from 'models/user';
import { cookieNames } from 'utils/storage-vars';
import apiUser from 'modules/profile/api/api-user';
import { AuthContext } from 'contexts/auth-context';
import toPrice from 'utils/to-price';
import Head from 'next/head';

export async function getServerSideProps({ req }) {
    const cookies = parseCookies(req);
    const config: AxiosRequestConfig = {};

    let credit = 0;
    let user: IUser | null = null;

    if (cookies && cookies[cookieNames.apiToken]) {
        config.headers = {
            Authorization: `Bearer ${cookies[cookieNames.apiToken]}`,
        };

        const creditResult = await apiGetCredit(true, config);
        if (creditResult.status === 'data') credit = creditResult.data.credit;

        user = await apiUser(true, config);
        if (!user) {
            return {
                redirect: {
                    destination: '/auth/login-nopass?return=/profile/credit',
                    permanent: false,
                },
            };
        }
    } else {
        return {
            redirect: {
                destination: '/auth/login-nopass?return=/profile/credit',
                permanent: false,
            },
        };
    }

    return {
        props: {
            user,
            credit,
        },
    };
}

// TODO: To Spaceman: Review this more thoroughly
export default function Credit(props: { user: IUser; credit: number }) {
    const [showModal, setShowModal] = useState(false);
    const authContext = useContext(AuthContext);

    useEffect(() => {
        authContext.changeIsLoggedIn(props.user);
    }, []);

    return (
        <>
            <Head>
                <title>پروفایل - اعتبار</title>
            </Head>
            <div className={styles.credit}>
                <div className={styles['credit-card']}>
                    <div className={styles.title}>اعتبار حساب</div>
                    <div className={styles['credit-count']}>
                        <span className={styles['credit-count-num']}>{toPrice(props.credit)}</span>
                        <span>تومان</span>
                    </div>
                    <div onClick={() => setShowModal(true)} className={styles['credit-increase']}>
                        + افزایش اعتبار
                    </div>
                </div>
                <AddCredit showModal={showModal} setShowModal={setShowModal} />
            </div>
        </>
    );
}
