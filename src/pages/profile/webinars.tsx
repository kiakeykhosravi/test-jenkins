import React, { useContext, useEffect } from 'react';
import styles from 'styles/profile-courses.module.scss';
import Orders from 'modules/profile/components/orders';
import Meetings from 'modules/profile/components/meetings';
import Head from 'next/head';
import apiOrders from 'modules/profile/api/api-orders';
import parseCookies from 'utils/parse-cookies';
import { AxiosRequestConfig } from 'axios';
import { IUser } from 'models/user';
import { cookieNames } from 'utils/storage-vars';
import apiMeetings from 'modules/profile/api/api-meetings';
import { IOrder } from 'models/orders';
import { IMeeting } from 'models/meeting';
import apiUser from 'modules/profile/api/api-user';
import { AuthContext } from 'contexts/auth-context';

export async function getServerSideProps({ req }) {
    const cookies = parseCookies(req);
    const config: AxiosRequestConfig = {};

    let orders = [];
    let meetings = [];
    let ordersLastPage = 1;
    let meetingsLastPage = 1;
    let user: IUser | null = null;

    if (cookies && cookies[cookieNames.apiToken]) {
        config.headers = {
            Authorization: `Bearer ${cookies[cookieNames.apiToken]}`,
        };

        user = await apiUser(true, config);
        if (!user) {
            return {
                redirect: {
                    destination: '/auth/login-nopass?return=/profile/webinars',
                    permanent: false,
                },
            };
        }

        const ordersResult = await apiOrders(true, 1, config);
        const meetingsResult = await apiMeetings(true, 1, config);

        orders = (ordersResult && ordersResult.orders) || [];
        ordersLastPage = (ordersResult && ordersResult.lastpage) || 1;
        meetings = (meetingsResult && meetingsResult.meetings) || [];
        meetingsLastPage = (meetingsResult && meetingsResult.lastpage) || 1;
    } else {
        return {
            redirect: {
                destination: '/auth/login-nopass?return=/profile/webinars',
                permanent: false,
            },
        };
    }

    return {
        props: {
            orders,
            meetings,
            ordersLastPage,
            meetingsLastPage,
            user,
        },
    };
}

export default function Webinars(props: {
    orders: IOrder[];
    meetings: IMeeting[];
    ordersLastPage: number;
    meetingsLastPage: number;
    user: IUser;
}) {
    const authContext = useContext(AuthContext);
    useEffect(() => {
        authContext.changeIsLoggedIn(props.user);
    }, []);

    return (
        <div className={styles.courses}>
            <Head>
                <title>پروفایل - دوره ها</title>
            </Head>
            <div className={styles.webinars}>
                <Meetings meetings={props.meetings} lastPage={props.meetingsLastPage} />
            </div>
            <hr />
            <div className={styles.webinars}>
                <Orders orders={props.orders} lastPage={props.ordersLastPage} />
            </div>
        </div>
    );
}
