import React, { useContext, useEffect, useRef } from 'react';
import styles from 'styles/profile-brandpage-marketing.module.scss';
import Head from 'next/head';
import parseCookies from 'utils/parse-cookies';
import { AxiosRequestConfig } from 'axios';
import { IUser } from 'models/user';
import { cookieNames } from 'utils/storage-vars';
import apiUser from 'modules/profile/api/api-user';
import { AuthContext } from 'contexts/auth-context';
import toPrice from 'utils/to-price';
import AsanImage from 'modules/shared/components/asan-image';
import { Cash, Link45deg, PeopleFill } from 'react-bootstrap-icons';
import { toast } from 'react-toastify';
import { Table } from 'react-bootstrap';
import toPersianNum from 'utils/to-persian-num';
import apiUserBrandPageMarketingStats from 'modules/profile/api/api-user-brandpage-marketing-stats';
import apiUserBrandPageMarketingSalesUsersList from 'modules/profile/api/api-user-brandpage-marketing-sales-users-list';
import { IBrandPageMarketingSummary, ISalesCandidate } from 'modules/profile/responses';
import apiUserBrandpageMarketingIsMarketer from 'modules/profile/api/api-user-brandpage-marketing-is-marketer';

export async function getServerSideProps({ req }) {
    const cookies = parseCookies(req);
    const config: AxiosRequestConfig = {};

    let user: IUser | null = null;
    const origin = process.env.NODE_ENV === 'production' ? process.env.NEXT_PUBLIC_APP_URL : 'http://localhost:3000';

    if (cookies && cookies[cookieNames.apiToken]) {
        config.headers = {
            Authorization: `Bearer ${cookies[cookieNames.apiToken]}`,
        };

        user = await apiUser(true, config);
        if (!user) {
            return {
                redirect: {
                    destination: '/auth/login-nopass?return=/profile/brandpage-marketing',
                    permanent: false,
                },
            };
        } else {
            // TODO: Use Promise.all() or something like that to handle these async functions together
            const marketingBrandPageStatsResult = await apiUserBrandPageMarketingStats(true, config);
            const marketingBrandPageStats = marketingBrandPageStatsResult
                ? marketingBrandPageStatsResult.marketing_summary
                : null;

            const marketingBrandPageSaleUsersListResult = await apiUserBrandPageMarketingSalesUsersList(true, config);
            const marketingBrandPageSaleUsersList = marketingBrandPageSaleUsersListResult
                ? marketingBrandPageSaleUsersListResult.candidates
                : [];

            const marketingBrandpageIsUserMarketer = await apiUserBrandpageMarketingIsMarketer(true, config);
            const isUserPublicMarketer =
                marketingBrandpageIsUserMarketer && marketingBrandpageIsUserMarketer.is_marketer;

            return {
                props: {
                    marketingBrandPageStats,
                    marketingBrandPageSaleUsersList,
                    isUserPublicMarketer,
                    user,
                    origin,
                },
            };
        }
    } else {
        return {
            redirect: {
                destination: '/auth/login-nopass?return=/profile/brandpage-marketing',
                permanent: false,
            },
        };
    }
}

export default function BrandpageMarketing(props: {
    marketingBrandPageStats: IBrandPageMarketingSummary;
    marketingBrandPageSaleUsersList: ISalesCandidate[];
    isUserPublicMarketer: boolean;
    user: IUser;
    origin: string;
}) {
    const authContext = useContext(AuthContext);
    const brandpageBuyUrlElementRef = useRef(null);
    const brandpageLandingUrlElementRef = useRef(null);

    useEffect(() => {
        authContext.changeIsLoggedIn(props.user);
    }, []);

    const onclickCopyBuyLink = () => {
        brandpageBuyUrlElementRef.current.select();
        document.execCommand('copy');
        brandpageBuyUrlElementRef.current.blur();
        toast.success('لینک معرف خرید برندپیج با موفقیت کپی شد', {
            autoClose: 7500,
        });
    };

    const onclickCopyLandingLink = () => {
        brandpageLandingUrlElementRef.current.select();
        document.execCommand('copy');
        brandpageLandingUrlElementRef.current.blur();
        toast.success('لینک معرف صفحه تعرفه های برندپیج با موفقیت کپی شد', {
            autoClose: 7500,
        });
    };

    const renderSubset = () => {
        return props.marketingBrandPageSaleUsersList.map((s, index) => {
            return (
                <tr key={index}>
                    <td>{s.name}</td>
                    <td>{s.type}</td>
                    <td>{s.status}</td>
                </tr>
            );
        });
    };

    return (
        <>
            <Head>
                <title>پروفایل - فروش برندپیج</title>
            </Head>
            <div className={styles.brand}>
                <div className={styles['brand-card']}>
                    <div className={styles.logo}>
                        <AsanImage
                            src={'/images/logos/brandpage-logo.jpg'}
                            alt={'برندپیج'}
                            className={styles['logo-img']}
                        />
                    </div>
                    <div className={styles.title}>بازاریابی برندپیج</div>
                    <div className={styles.subtitle}>
                        شما می توانید با استفاده از لینک معرف خودتان آشنایان خود را با برندپیج آشنا کنید و سهم بازاریابی
                        خود را دریافت کنید.
                    </div>
                    <div onClick={onclickCopyBuyLink} className={styles['copy-link']}>
                        <Link45deg className={styles.icon} />
                        کپی لینک معرف شما
                        <input
                            type="text"
                            value={`${props.origin}/candidates/brandpage-premium?referrer=${props.user.id}`}
                            className={styles['hidden-page-link']}
                            ref={brandpageBuyUrlElementRef}
                        />
                    </div>
                    {props.isUserPublicMarketer ? (
                        <div onClick={onclickCopyLandingLink} className={styles['copy-link']}>
                            <Link45deg className={styles.icon} />
                            کپی لینک صفحه تعرف ها با کد شما
                            <input
                                type="text"
                                value={`${props.origin}/brandpage/plans?ref=${props.user.id}`}
                                className={styles['hidden-page-link']}
                                ref={brandpageLandingUrlElementRef}
                            />
                        </div>
                    ) : null}
                </div>
                <h2 className={styles['stats-title']}>آمار شما</h2>
                <div className={styles['stats-main']}>
                    <div className={styles['stats-main-section']}>
                        <Cash className={styles['stats-icon']} />
                        <div className={styles['stats-text']}>
                            <span>{`${toPrice(
                                props.marketingBrandPageStats ? Number(props.marketingBrandPageStats.amount) || 0 : 0,
                            )} `}</span>
                            <span style={{ fontSize: 'smaller' }}>تومان</span>
                        </div>
                    </div>
                    <div className={styles['stats-main-section']}>
                        <PeopleFill className={styles['stats-icon']} />
                        <div className={styles['stats-text']}>
                            <span>{`${toPersianNum(
                                props.marketingBrandPageStats ? props.marketingBrandPageStats.count || 0 : 0,
                            )} `}</span>
                            <span style={{ fontSize: 'smaller' }}>نفر</span>
                        </div>
                    </div>
                </div>
                {props.marketingBrandPageSaleUsersList.length ? (
                    <Table borderless striped className={styles.table}>
                        <thead className={styles['table-head']}>
                            <tr>
                                <th>نام</th>
                                <th>نوع</th>
                                <th>وضعیت</th>
                            </tr>
                        </thead>
                        <tbody>{renderSubset()}</tbody>
                    </Table>
                ) : null}
            </div>
        </>
    );
}
