import React, { useContext, useEffect, useState } from 'react';
import styles from 'styles/profile-settings.module.scss';
import { Button, Col, Form, Row } from 'react-bootstrap';
import classNames from 'classnames';
import { PencilSquare } from 'react-bootstrap-icons';
import { useForm } from 'react-hook-form';
import ChangePassword from 'modules/profile/components/change-password';
import parseCookies from 'utils/parse-cookies';
import { AxiosRequestConfig } from 'axios';
import { IUser } from 'models/user';
import { cookieNames } from 'utils/storage-vars';
import apiUser from 'modules/profile/api/api-user';
import apiUserUpdate from 'modules/profile/api/api-user-update';
import toPersianNum from 'utils/to-persian-num';
import { AuthContext } from 'contexts/auth-context';
import toEnglishNum from 'utils/to-english-num';
import Head from 'next/head';
import CommonRegexUtil from 'utils/common-regex-util';

type TInputs = {
    fullName: string;
    mobile: string;
    email: string;
    nationalCode: string;
    whatsapp: string;
    cityId: string;
};

export async function getServerSideProps({ req }) {
    const cookies = parseCookies(req);
    const config: AxiosRequestConfig = {};

    let user: IUser | null = null;

    if (cookies && cookies[cookieNames.apiToken]) {
        config.headers = {
            Authorization: `Bearer ${cookies[cookieNames.apiToken]}`,
        };

        user = await apiUser(true, config);
        if (!user) {
            return {
                redirect: {
                    destination: '/auth/login-nopass?return=/profile/settings',
                    permanent: false,
                },
            };
        }
    } else {
        return {
            redirect: {
                destination: '/auth/login-nopass?return=/profile/settings',
                permanent: false,
            },
        };
    }

    return {
        props: {
            user,
        },
    };
}

export default function Settings(props: { user: IUser }) {
    const authContext = useContext(AuthContext);
    const [showModal, setShowModal] = useState(false);
    const [submitting, setSubmitting] = useState<boolean>(false);
    const [fullName, setFullName] = useState(props.user.fullname);
    const { register, handleSubmit, errors } = useForm<TInputs>();

    useEffect(() => {
        authContext.changeIsLoggedIn(props.user);
    }, []);

    async function onSubmit(data: { email: string; fullName: string; nationalCode?: string; whatsapp?: string }) {
        setSubmitting(true);
        const englishNationalCode = (data.nationalCode && toEnglishNum(data.nationalCode)) || null;
        const englishWhatsapp = (data.whatsapp && toEnglishNum(data.whatsapp)) || null;

        apiUserUpdate({
            fullname: data.fullName,
            email: data.email,
            nationalCode: englishNationalCode,
            whatsapp: englishWhatsapp,
        }).then((response) => {
            setFullName(data.fullName);
            setSubmitting(false);
        });
    }

    return (
        <>
            <Head>
                <title>پروفایل - تنظیمات</title>
            </Head>
            <div className={styles.main}>
                <Row>
                    <Col lg={8} className="offset-lg-2">
                        <Form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
                            <div className={styles.header}>
                                <h5 className={styles.title}>{fullName}</h5>
                                <div onClick={() => setShowModal(true)} className={styles['change-pass-btn']}>
                                    <div>تغییر گذرواژه</div>
                                    <PencilSquare className={styles.icon} />
                                </div>
                            </div>
                            <Form.Group controlId="formName">
                                <div className={styles['form-control-container']}>
                                    <Form.Label className={styles['form-label']}>نام و نام خانوادگی</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="fullName"
                                        className={classNames(
                                            styles['form-control'],
                                            errors.fullName ? styles['border-red'] : '',
                                        )}
                                        ref={register({
                                            required: 'نام و نام خانوادگی را وارد کنید!',
                                            minLength: {
                                                value: 3,
                                                message: 'نام و نام خانوادگی باید حداقل سه کاراکتر باشد!',
                                            },
                                        })}
                                        defaultValue={props.user.fullname}
                                    />
                                </div>
                                {errors.fullName && <div className={styles.error}>{errors.fullName.message}</div>}
                            </Form.Group>
                            <Form.Group>
                                <div className={styles['form-control-container']}>
                                    <Form.Label className={styles['form-label']}>شماره موبایل</Form.Label>
                                    <Form.Control
                                        className={styles['form-control']}
                                        style={{ textAlign: 'left' }}
                                        value={toPersianNum(props.user.mobile)}
                                        disabled
                                    />
                                </div>
                            </Form.Group>
                            <Form.Group controlId="formEmail">
                                <div className={styles['form-control-container']}>
                                    <Form.Label className={styles['form-label']}>ایمیل</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="email"
                                        className={classNames(
                                            styles['form-control'],
                                            errors.email ? styles['border-red'] : '',
                                        )}
                                        style={{ textAlign: 'left' }}
                                        ref={register({
                                            pattern: {
                                                value: CommonRegexUtil.email,
                                                message: 'فرمت ایمیل اشتباه است!',
                                            },
                                        })}
                                        defaultValue={props.user.email}
                                    />
                                </div>
                                {errors.email && <div className={styles.error}>{errors.email.message}</div>}
                            </Form.Group>
                            <Form.Group controlId="formNationalCode">
                                <div className={styles['form-control-container']}>
                                    <Form.Label className={styles['form-label']}>کد ملی</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="nationalCode"
                                        className={classNames(
                                            styles['form-control'],
                                            errors.nationalCode ? styles['border-red'] : '',
                                        )}
                                        style={{ textAlign: 'left' }}
                                        ref={register({
                                            pattern: {
                                                value: /^[0-9۰۱۲۳۴۵۶۷۸۹]{10}$/,
                                                message: 'کد ملی باید ۱۰ رقم و فقط شامل عدد باشد',
                                            },
                                        })}
                                        defaultValue={props.user.national_code}
                                    />
                                </div>
                                {errors.nationalCode && (
                                    <div className={styles.error}>{errors.nationalCode.message}</div>
                                )}
                            </Form.Group>
                            <Form.Group controlId="formWhatsapp">
                                <div className={styles['form-control-container']}>
                                    <Form.Label className={styles['form-label']}>شماره واتساپ</Form.Label>
                                    <Form.Control
                                        type="text"
                                        name="whatsapp"
                                        className={classNames(
                                            styles['form-control'],
                                            errors.whatsapp ? styles['border-red'] : '',
                                        )}
                                        style={{ textAlign: 'left' }}
                                        ref={register({
                                            pattern: {
                                                value: /^\+?[0-9۰۱۲۳۴۵۶۷۸۹]+$/,
                                                message: 'شماره واتساپ نامعتبر است',
                                            },
                                        })}
                                        defaultValue={props.user.whatsapp}
                                    />
                                </div>
                                {errors.whatsapp && <div className={styles.error}>{errors.whatsapp.message}</div>}
                            </Form.Group>
                            {/*<Form.Group controlId="formCity">
                            <div className={styles['form-control-container']}>
                                <Form.Label className={styles['form-label']}>شهر</Form.Label>
                                <Form.Control
                                    type="text"
                                    className={classNames(
                                        styles['form-control'],
                                        errors.cityId ? styles['border-red'] : '',
                                    )}
                                    name={'cityId'}
                                    ref={register({
                                        required: 'شهر را وارد کنید!',
                                    })}
                                    value={cityId}
                                    onChange={(e) => {
                                        setCityId(e.target.value);
                                    }}
                                    {...(isEditing ? {} : { disabled: true })}
                                />
                            </div>
                            {errors.cityId && <div className={styles.error}>{errors.cityId.message}</div>}
                        </Form.Group>*/}
                            <div className={styles['form-control-container']}>
                                <Button
                                    variant="success"
                                    type="submit"
                                    className={styles['edit-btn']}
                                    disabled={submitting}
                                >
                                    {submitting ? 'در حال ارسال...' : 'ویرایش مشخصات'}
                                </Button>
                            </div>
                        </Form>
                    </Col>
                </Row>
                <ChangePassword showModal={showModal} setShowModal={setShowModal} />
            </div>
        </>
    );
}
