import React, { useContext, useEffect, useRef, useState } from 'react';
import Link from 'next/link';
import styles from 'styles/profile-webinar-marketing.module.scss';
import Head from 'next/head';
import parseCookies from 'utils/parse-cookies';
import { AxiosRequestConfig } from 'axios';
import { IUser } from 'models/user';
import { cookieNames } from 'utils/storage-vars';
import apiUser from 'modules/profile/api/api-user';
import { AuthContext } from 'contexts/auth-context';
import apiMarketingWebinarsList from 'modules/profile/api/api-marketing-webinars-list';
import { IWebinarCompact } from 'models/webinar-compact';
import { Row, Spinner } from 'react-bootstrap';
import Pagination from 'modules/shared/components/pagination';
import classNames from 'classnames';
import { useRouter } from 'next/router';
import MarketingWebinarsListSection from 'modules/profile/components/marketing-webinars-list-section';
import { GraphUp } from 'react-bootstrap-icons';
import apiUserWebinarMarketingStats from 'modules/profile/api/api-user-webinar-marketing-stats';
import { IMarketingStatsWebinar } from 'modules/profile/responses';
import Searchbar from 'modules/shared/components/searchbar';
import MarketingWebinarsFilterSection from 'modules/profile/components/marketing-webinars-filter-section';

export async function getServerSideProps({ req, query }) {
    const q = query.q || '';
    const page = query.page || 1;
    const date = query.date || '';
    const sortDate = query.sortDate || '';
    const sortPrice = query.sortPrice || '';
    const sortMarketerPercent = query.sortMarketerPercent || '';
    const sortMarketingAmount = query.sortMarketingAmount || '';

    const cookies = parseCookies(req);
    const config: AxiosRequestConfig = {};

    let marketing_webinars = [];
    let marketing_webinars_stats = [];
    let lastPage = 1;
    let user: IUser | null = null;
    const origin = process.env.NODE_ENV === 'production' ? process.env.NEXT_PUBLIC_APP_URL : 'http://localhost:3000';

    if (cookies && cookies[cookieNames.apiToken]) {
        config.headers = {
            Authorization: `Bearer ${cookies[cookieNames.apiToken]}`,
        };

        user = await apiUser(true, config);
        if (!user) {
            return {
                redirect: {
                    destination: '/auth/login-nopass?return=/profile/webinar-marketing',
                    permanent: false,
                },
            };
        }

        const marketingWebinarsResult = await apiMarketingWebinarsList(
            true,
            q,
            page,
            date,
            sortDate,
            sortPrice,
            sortMarketerPercent,
            sortMarketingAmount,
            config,
        );
        const marketingWebinarsStatsResult = await apiUserWebinarMarketingStats(true, config);

        marketing_webinars = (marketingWebinarsResult && marketingWebinarsResult.webinars) || [];
        lastPage = (marketingWebinarsResult && marketingWebinarsResult.lastpage) || 1;
        marketing_webinars_stats = (marketingWebinarsStatsResult && marketingWebinarsStatsResult.webinars) || [];
    } else {
        return {
            redirect: {
                destination: '/auth/login-nopass?return=/profile/webinar-marketing',
                permanent: false,
            },
        };
    }

    return {
        props: {
            marketing_webinars,
            lastPage,
            marketing_webinars_stats,
            user,
            origin,
        },
    };
}

export default function WebinarMarketing(props: {
    marketing_webinars: IWebinarCompact[];
    lastPage: number;
    marketing_webinars_stats: IMarketingStatsWebinar[];
    user: IUser;
    origin: string;
}) {
    const webinarsRef = useRef(null);
    const authContext = useContext(AuthContext);
    const router = useRouter();
    const [webinars, setWebinars] = useState(props.marketing_webinars);
    const [lastPage, setLastPage] = useState(props.lastPage);
    const [firstRender, setFirstRender] = useState(true);
    const [loading, setLoading] = useState(false);
    const [currentPage, setCurrentPage] = useState((router.query.page as string) || '1');
    const [filters, setFilters] = useState({
        date: (router.query.date as string) || '',
        sortDate: (router.query.sortDate as string) || '',
        sortPrice: (router.query.sortPrice as string) || '',
        sortMarketerPercent: (router.query.sortMarketerPercent as string) || '',
        sortMarketingAmount: (router.query.sortMarketingAmount as string) || '',
    });

    useEffect(() => {
        authContext.changeIsLoggedIn(props.user);
        if (!firstRender) {
            webinarsRef.current.scrollIntoView();
        }
        setFirstRender(false);
    }, [webinars]);

    const updateFilters = (data) => {
        const urlSearchParams = new URLSearchParams(window.location.search);
        const params = Object.fromEntries(urlSearchParams.entries());
        const queryParamList: string[] = [];

        const pushToParams = (param) => {
            if (data[`${param}`] !== undefined && data[`${param}`] !== '') {
                queryParamList.push(`${param}=${data[`${param}`]}`);
            } else if (data[`${param}`] !== '' && params[`${param}`]) {
                queryParamList.push(`${param}=${params[`${param}`]}`);
            }
        };

        params.q ? queryParamList.push(`q=${params.q}`) : '';
        pushToParams('date');
        pushToParams('sortDate');
        pushToParams('sortPrice');
        pushToParams('sortMarketerPercent');
        pushToParams('sortMarketingAmount');

        let queryParams = queryParamList.length ? '?' : '';
        for (const param of queryParamList) {
            queryParams += param + '&';
        }
        queryParams = queryParams.substring(0, queryParams.length - 1);
        window.history.replaceState(
            {
                ...window.history.state,
                url: `/profile/webinar-marketing` + queryParams,
                as: `/profile/webinar-marketing` + queryParams,
            },
            '',
            window.location.origin + `/profile/webinar-marketing` + queryParams,
        );
        setFilters((prev) => {
            return { ...Object.assign(prev, data) };
        });
        setCurrentPage('1');
        sendRequest({
            ...params,
            ...data,
            page: 1,
        });
    };

    const searchHandler = (q) => {
        const urlSearchParams = new URLSearchParams(window.location.search);
        const params = Object.fromEntries(urlSearchParams.entries());
        const queryParamList: string[] = [];
        const pushToParams = (param) => {
            params[`${param}`] ? queryParamList.push(`${param}=${params[`${param}`]}`) : '';
        };
        if (q !== '') {
            queryParamList.push(`q=${q}`);
        }
        pushToParams('date');
        pushToParams('sortDate');
        pushToParams('sortPrice');
        pushToParams('sortMarketerPercent');
        pushToParams('sortMarketingAmount');

        let queryParams = queryParamList.length ? '?' : '';
        for (const param of queryParamList) {
            queryParams += param + '&';
        }
        queryParams = queryParams.substring(0, queryParams.length - 1);
        window.history.replaceState(
            {
                ...window.history.state,
                url: `/profile/webinar-marketing` + queryParams,
                as: `/profile/webinar-marketing` + queryParams,
            },
            '',
            window.location.origin + `/profile/webinar-marketing` + queryParams,
        );
        setCurrentPage('1');
        sendRequest({
            ...params,
            q,
            page: 1,
        });
    };

    const handlePageClick = async (data) => {
        const urlSearchParams = new URLSearchParams(window.location.search);
        const params = Object.fromEntries(urlSearchParams.entries());
        const queryParamList: string[] = [];

        const pushToParams = (param) => {
            params[`${param}`] ? queryParamList.push(`${param}=${params[`${param}`]}`) : '';
        };
        pushToParams('q');
        pushToParams('date');
        pushToParams('sortDate');
        pushToParams('sortPrice');
        pushToParams('sortMarketerPercent');
        pushToParams('sortMarketingAmount');

        let queryParams = queryParamList.length ? '?' : '';
        for (const param of queryParamList) {
            queryParams += param + '&';
        }
        queryParams += queryParamList.length ? '' : '?';
        queryParams += `page=${data.selected + 1}`;
        window.history.replaceState(
            {
                ...window.history.state,
                url: `/profile/webinar-marketing` + queryParams,
                as: `/profile/webinar-marketing` + queryParams,
            },
            '',
            window.location.origin + `/profile/webinar-marketing` + queryParams,
        );
        setCurrentPage(data.selected + 1);
        sendRequest({
            ...params,
            page: data.selected + 1,
        });
    };

    const sendRequest = async (params: {
        q?;
        page?;
        date?;
        sortDate?;
        sortPrice?;
        sortMarketerPercent?;
        sortMarketingAmount?;
    }) => {
        setLoading(true);
        const marketingWebinarsResult = await apiMarketingWebinarsList(
            false,
            params.q || '',
            params.page || 1,
            params.date || '',
            params.sortDate || '',
            params.sortPrice || '',
            params.sortMarketerPercent || '',
            params.sortMarketingAmount || '',
        );
        if (marketingWebinarsResult) {
            setWebinars(marketingWebinarsResult.webinars);
            setLastPage(marketingWebinarsResult.lastpage);
        }
        setLoading(false);
    };

    return (
        <div className={styles.courses}>
            <Head>
                <title>پروفایل - فروش وبینار</title>
            </Head>
            <div className={styles.main}>
                <div className={styles.top}>
                    <h4 className={styles.title}>لیست وبینارها</h4>
                    <Link href={'/profile/webinar-marketing-sell'}>
                        <a title={'آمار فروش شما'} className={styles['top-stats-text']}>
                            <GraphUp className={styles['top-left-icon']} />
                            آمار فروش شما
                        </a>
                    </Link>
                </div>
                <Searchbar
                    initialValue={(router.query.q as string) || ''}
                    containerClasses={styles.search}
                    fieldClasses={styles['search-field']}
                    placeHolder="جستجو در وبینارها ..."
                    customHandler={searchHandler}
                    setLoading={setLoading}
                />
                <MarketingWebinarsFilterSection filterUpdate={updateFilters} filters={filters} />
                <Row className={classNames('gx-sm-4 gx-md-4 gy-5', styles['gy-5'], styles.webinars)} ref={webinarsRef}>
                    {loading ? (
                        <div className={styles.loader}>
                            <div className={styles['loader-content']}>
                                <Spinner animation="grow" variant={'primary'} />
                                <div style={{ marginRight: '1rem' }}>درحال بارگذاری...</div>
                            </div>
                        </div>
                    ) : null}
                    <MarketingWebinarsListSection
                        webinar={webinars}
                        userMarketingStats={props.marketing_webinars_stats}
                        user={props.user}
                        basePath={props.origin}
                    />
                </Row>
                {lastPage > 1 ? (
                    <Pagination page={currentPage} handlePageClick={handlePageClick} pageCount={lastPage} />
                ) : null}
            </div>
        </div>
    );
}
