import React from 'react';
import styles from 'styles/plans.module.scss';
import { Col, Row } from 'react-bootstrap';
import classNames from 'classnames';
import toPersianNum from 'utils/to-persian-num';
import Link from 'next/link';
import Head from 'next/head';
import AsanImage from 'modules/shared/components/asan-image';
import schemas from 'utils/schemas';

export default function Plans() {
    return (
        <>
            <Head>
                <meta name="robots" content="index, follow" />
                <title>شرایط و هزینه های برگزاری وبینار و کلاس آنلاین</title>
                <meta name="description" content="هزینه برگزاری وبینار و آموزش آنلاین در آسان سمینار" />
                <meta property="og:url" content="https://asanseminar.ir" />
                <meta property="og:title" content="شرایط و هزینه های برگزاری وبینار و کلاس آنلاین" />
                <meta property="og:description" content="هزینه برگزاری وبینار و آموزش آنلاین در آسان سمینار" />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="شرایط و هزینه های برگزاری وبینار و کلاس آنلاین" />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={{
                        __html: schemas.plansSchema,
                    }}
                />
            </Head>
            <div className={styles['plans-page']}>
                <SectionOne />
                <SectionTwo />
                <SectionThree />
                <SectionFour />
                <SectionFive />
                <BannerSection />
                <PlanSection />
            </div>
        </>
    );
}

function SectionOne() {
    return (
        <Row className={styles.row}>
            <Col md={12} lg={6} className={styles['not-banner']}>
                <div className={classNames(styles.content, styles.right)}>
                    <h3 className={styles.title}>سلام و درود بزرگوار</h3>
                    <div className={styles.text}>
                        سپاسگزاریم که همراه ما هستید ، حتما می خواهید هزینه آموزش آنلاین در آسان سمینار را بدانید. خواهش
                        داریم پیش از برآورد هزینه ها مطالبی را که برای شما آماده کرده ایم مطالعه بفرمایید. این کار فقط
                        سه دقیقه از وقت شما را میگیرد مطمئن باشید ارزشش را دارد.
                    </div>
                </div>
            </Col>
            <Col md={12} lg={6} className={styles.banner}>
                <img src="/images/plans/AS01-plans-(3).jpg" alt="مقدمه صفحه تعرفه ها" className={styles.img} />
            </Col>
        </Row>
    );
}

function SectionTwo() {
    const renderDots = () => {
        return (
            <div className={styles.pre}>
                <div className={styles['pre-border-right']} />
                <div className={styles['pre-red-point']} />
                <div className={styles['pre-border-right']} />
            </div>
        );
    };

    const renderItem = (text: string) => {
        return (
            <div className={styles.item}>
                {renderDots()}
                <div className={styles.text}>{text}</div>
            </div>
        );
    };

    return (
        <Row className={styles.row}>
            <Col md={12} lg={6} className={styles.banner}>
                <img src="/images/plans/AS01-plans-(5).jpg" alt="سه نکته صفحه تعرفه ها" className={styles.img} />
            </Col>
            <Col md={12} lg={6} className={styles['not-banner']}>
                <div className={classNames(styles.points, styles.left)}>
                    <h3 className={styles.title}>سه نکته مهم</h3>
                    {renderItem(
                        'ما تمام دغدغه های شما را می دانیم ، درک می کنیم و برای آن راه حل داریم آسان سمینار فقط یک نرم افزار آموزش آنلاین نیست بلکه مجموعه ای است که هدفش کمک به کسب و کار و پیشرفت مجموعه شماست',
                    )}
                    {renderItem(
                        'تیم فنی آسان سمینار شبانه روز در تحقیق و تلاش است تا تمام استانداردهای روز دنیا و ایده ها و نیازمندی های جدید شما را به انجام برساند',
                    )}
                    <div className={styles.item}>
                        {renderDots()}
                        <div className={styles.text}>
                            برای تست نرم افزار آموزش آنلاین ، راه اندازی و دریافت همه خدمات نیاز به پرداخت هیچ هزینه‌ای
                            نیست <b>فقط کافیست اراده کنید</b>
                        </div>
                    </div>
                </div>
            </Col>
        </Row>
    );
}

function SectionThree() {
    const renderRowWithRightArrow = (text: string) => {
        return (
            <Row className={classNames(styles.items, styles.row)}>
                <Col sm={6} />
                <Col sm={6} className={styles['item-col-left']}>
                    <div className={styles['right-arrow']} />
                    <div className={styles.text}>{text}</div>
                </Col>
            </Row>
        );
    };

    const renderRowWithLeftArrow = (text: string) => {
        return (
            <Row className={classNames(styles.items, styles.row)}>
                <Col sm={6} className={styles['item-col-right']}>
                    <div className={classNames(styles.text)}>{text}</div>
                    <div className={styles['left-arrow']} />
                </Col>
                <Col sm={6} />
            </Row>
        );
    };

    return (
        <Row className={styles.row}>
            <Col md={12} lg={6} className={styles['not-banner']}>
                <div className={classNames(styles.dimensions, styles.right)}>
                    <h3 className={styles.title}>
                        پشتیبانی آنلاین <span className={styles.red}>{toPersianNum(8)}</span> بعدی
                    </h3>
                    {renderRowWithLeftArrow('پشتیبان های آسان سمینار در گروه های واتس اپی شما را همراهی میکنند')}
                    {renderRowWithRightArrow('هر پیامی را که به سایت بفرستید میخوانیم و پاسخ میدهیم')}
                    {renderRowWithLeftArrow(
                        'پشتیبانی از طریق پنل پیامکی شما را در جریان تغییرات و اخبار مهم قرار میدهد',
                    )}
                    {renderRowWithRightArrow('از طریق تماس تلفنی با مرکز تماس میتوانید با پشتیبانی در ارتباط باشید')}
                    {renderRowWithLeftArrow('پشتیبانی اختصاصی از طریق شماره اختصاصی کاربرهای شما را پشتیبانی میکند')}
                    {renderRowWithRightArrow(
                        'پشتیبانان آسان سمینار از طریق جلسات آسان سمیناری پرسنل، اساتید و کاربران شما را آموزش می‌دهد',
                    )}
                    {renderRowWithLeftArrow('پشتیبانی از طریق تیکت کاربران')}
                </div>
            </Col>
            <Col md={12} lg={6} className={styles.banner}>
                <AsanImage src="/images/plans/AS01-plans-(1).jpg" alt="مقدمه صفحه تعرفه ها" className={styles.img} />
            </Col>
        </Row>
    );
}

function SectionFour() {
    const renderItem = (text: string) => {
        return (
            <div className={styles.item}>
                <div className={styles.pre}>
                    <AsanImage src="/images/icons/checkmark.png" alt="آیکون" className={styles.img} />
                </div>
                <div className={styles.text}>{text}</div>
            </div>
        );
    };

    return (
        <Row className={styles.row}>
            <Col md={12} lg={6} className={styles.banner}>
                <AsanImage src="/images/plans/AS01-plans-(8).jpg" alt="امکانات آسان سمینار" className={styles.img} />
            </Col>
            <Col md={12} lg={6} className={styles['not-banner']}>
                <div className={classNames(styles.facility, styles.left)}>
                    {renderItem('کلاس های شما با برنامه بیگ بلو باتن اجرا می شوند')}
                    {renderItem('محیط بسیار کاربرپسند و راحت برای آموزش آنلاین با همه امکانات مورد نیاز شما')}
                    {renderItem('سرورهای اختصاصی برای مدارس و آموزشگاه ها که کیفیت کلاس های شما را تضمین می کند')}
                    {renderItem('دارای امکان ضبط داخل کلاس')}
                    {renderItem('دارای امکان نظرسنجی و پنل گفتگوی عمومی و خصوصی')}
                    {renderItem('شما نیازی به اجاره سرور و استخدام تیم فنی ندارید')}
                </div>
            </Col>
        </Row>
    );
}

function SectionFive() {
    const renderItem = (text: string) => {
        return (
            <div className={styles.item}>
                <div className={styles.pre}>
                    <img src="/images/icons/success.png" alt="آیکون" className={styles.img} />
                </div>
                <div className={styles.text}>{text}</div>
            </div>
        );
    };

    return (
        <Row className={styles.row}>
            <Col md={12} lg={6} className={styles['not-banner']}>
                <div className={classNames(styles.facility, styles['facility-two'], styles.right)}>
                    <h3 className={styles.title}>
                        سیستم مدیریت کاربردی ، کامل و راحت برای مدیریت کلاس ها و آموزشگاه آنلاین
                    </h3>
                    {renderItem('مدیریت کلاس ها ( تقویم آموزشی، کلاس بندی و ایجاد جلسات و ...)')}
                    {renderItem('حضور و غیاب خودکار جلسات')}
                    {renderItem('ضبط و بازپخش کلاس و وبینار و امکان دانلود فایل کلاس ها')}
                    {renderItem('بولتن اطلاعیه های آموزشی و وبلاگ')}
                    {renderItem('سامانه اطلاع رسانی ( پنل پیامکی ) و گزارش گیری')}
                    {renderItem('آزمون آنلاین ، نمره دهی و تمرینات بدون امکان تقلب')}
                    <div className={styles.item}>
                        <div className={styles.pre}>
                            <img src="/images/icons/megaphone.png" alt="آیکون" className={styles.img} />
                        </div>
                        <div className={styles.text}>
                            <div>
                                امکان بازاریابی دوره ها و وبینارهای شما با سیستم همکاری در فروش (Affiliate Marketing )
                            </div>
                            <div>و امکان بلیط فروشی</div>
                        </div>
                    </div>
                </div>
            </Col>
            <Col md={12} lg={6} className={styles.banner}>
                <AsanImage
                    src="/images/plans/AS01-plans-(9).jpg"
                    alt="امکانات آسان سمینار"
                    className={styles.img}
                    style={{ height: '100%' }}
                />
            </Col>
        </Row>
    );
}

function BannerSection() {
    return (
        <Row className={styles.row}>
            <div className={styles['banner-section']}>
                <h2 className={styles.title}>
                    ما مجموعه شما را تا راه اندازی کامل همراهی می کنیم ، آموزش می دهیم ، پشتیبانی می کنیم و کمک می کنیم
                    آن را مدیریت کنید
                </h2>
                <AsanImage src="/images/plans/progress.png" alt="پیشرفت" className={styles.img} />
            </div>
        </Row>
    );
}

function PlanSection() {
    return (
        <Row className={styles.row}>
            <div className={styles['plan-section']}>
                <h5 className={styles.title}>تمامی این خدمات با مناسب ترین هزینه ممکن</h5>
                <div className={styles.card}>
                    <h1 className={styles.head}>تعرفه برگزاری وبینار ها و کلاس های آنلاین</h1>
                    <div className={classNames(styles.text, styles.price)}>
                        قیمت به ازای هر نفر در یک ساعت فقط
                        <span className={styles.red}>{toPersianNum('350  تومان')}</span>
                    </div>
                    <p className={styles.text}>به اندازه میزان برگزاری کلاس هایتان پرداخت کنید</p>
                    <p className={styles.text}>{toPersianNum('ظرفیت کلاس ها حداقل 5 و حداکثر 150 نفر می باشد')}</p>
                    <p className={styles.text}>
                        <b>
                            <span className={styles.red}>{toPersianNum(500)}</span> نفر / ساعت هم مهمان ما هستید
                        </b>
                    </p>
                    <Link href="/candidates">
                        <a className={styles.link} title="ثبت نام در آسان سمینار">
                            ثبت نام
                        </a>
                    </Link>
                    <div className={styles['business-contact']}>
                        <img
                            className={styles['contact-icon']}
                            src="/images/logos/socialmedias/whatsapp.png"
                            alt="آیکون واتساپ"
                        />
                        <img
                            className={styles['contact-icon']}
                            src="/images/logos/socialmedias/phone.svg"
                            alt="آیکون تلفن"
                        />
                        {toPersianNum('7032 990 0905')}
                    </div>
                </div>
            </div>
        </Row>
    );
}
