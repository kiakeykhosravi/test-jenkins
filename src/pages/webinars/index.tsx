import React, { useEffect, useRef, useState } from 'react';
import { Container, Row, Col, Spinner } from 'react-bootstrap';
import styles from 'styles/courses.module.scss';
import classNames from 'classnames';
import TagsList from 'modules/shared/components/tags-list';
import { ITag } from 'models/tag';
import Searchbar from 'modules/shared/components/searchbar';
import ListSection from 'modules/shared/components/list-section';
import FiltersSection from 'modules/shared/components/filters-section';
import Pagination from 'modules/shared/components/pagination';
//import RecommendedSection from 'modules/courses/components/recommended-section';
import { IWebinarCompact } from 'models/webinar-compact';
import apiCoursesTags from 'modules/courses/api/api-courses-tags';
import apiSearchWebinars from 'modules/search/api/api-search-webinars';
import { useRouter } from 'next/router';
import Head from 'next/head';
import { toast } from 'react-toastify';

export async function getServerSideProps({ query }) {
    const q = query.q || '';
    const page = query.page || '';
    const date = query.date || '';
    const price = query.price || '';
    const tags = query.tags ? query.tags.toString().trim().replace(/-/g, ' ') : '';
    const sortDate = query.sortDate || '';

    const webinarsResult = await apiSearchWebinars({ phrase: q, page, date, price, tags, sortDate });
    const webinars = webinarsResult.status === 'data' ? webinarsResult.data.webinars : [];
    const lastPage = webinarsResult.status === 'data' ? webinarsResult.data.lastpage : 1;

    const tagResults = await apiCoursesTags(true, true, -1);
    const tagsData = tagResults.status === 'data' ? tagResults.data : [];

    return {
        props: {
            tags: tagsData,
            webinars: webinars,
            lastPage: lastPage,
        },
    };
}

export default function Webinars(props: { tags: ITag[]; webinars: IWebinarCompact[]; lastPage: number }) {
    const cn = classNames;
    const router = useRouter();
    const webinarsRef = useRef(null);

    const [firstRender, setFirstRender] = useState(true);
    const [loading, setLoading] = useState(false);
    const [lastPage, setLastPage] = useState(props.lastPage);
    const [webinars, setWebinars] = useState(props.webinars);
    const [limitedTagsStatus, setLimitedTagsStatus] = useState(true);
    const [tags, setTags] = useState<ITag[]>([]);
    const [currentPage, setCurrentPage] = useState((router.query.page as string) || '1');
    const filterTags = () => {
        if (router.query.tags !== undefined) {
            return (router.query.tags as string).split(',');
        }
        return [];
    };
    const [filters, setFilters] = useState({
        tags: filterTags(),
        date: (router.query.date as string) || '',
        price: (router.query.price as string) || '',
        sortDate: (router.query.sortDate as string) || '',
    });

    useEffect(() => {
        setTagsLimit(true);
    }, []);

    useEffect(() => {
        if (!firstRender) {
            webinarsRef.current.scrollIntoView();
        }
        setFirstRender(false);
    }, [webinars]);

    const updateTagsFilter = (event) => {
        const urlSearchParams = new URLSearchParams(window.location.search);
        const params = Object.fromEntries(urlSearchParams.entries());
        const tags = () => {
            const tagsArray = params.tags ? params.tags.split(',') : [];
            return tagsArray.map((tag) => tag.trim().replace(/\s+/g, '-'));
        };
        const queryParamList: string[] = [];
        const pushToParams = (param) => {
            params[`${param}`] ? queryParamList.push(`${param}=${params[`${param}`]}`) : '';
        };
        pushToParams('q');
        pushToParams('date');
        pushToParams('price');
        pushToParams('sortDate');

        let queryParams = queryParamList.length ? '?' : '';
        for (const param of queryParamList) {
            queryParams += param + '&';
        }
        const tagElement = event.target;
        if (tagElement.checked) {
            queryParams += queryParamList.length ? '' : '?';
            queryParams += `tags=${
                tags().length ? tags().toString() + ',' : ''
            }${tagElement.id.toString().trim().replace(/\s+/g, '-')}`;
            window.history.replaceState(
                { ...window.history.state, url: `/webinars` + queryParams, as: `/webinars` + queryParams },
                '',
                window.location.origin + `/webinars` + queryParams,
            );
            setFilters((prev) => {
                const newFilters = prev;
                newFilters.tags.push(tagElement.id);
                return { ...newFilters };
            });
            setCurrentPage('1');
            const forSendToAPITags = params.tags
                ? params.tags
                      .split(',')
                      .map((tag) => tag.trim().replace(/-/g, ' '))
                      .toString() +
                  ',' +
                  tagElement.id
                : tagElement.id;
            sendRequest({
                ...params,
                page: 1,
                tags: forSendToAPITags,
            });
        } else {
            const newTags = tags().filter((tag) => tag !== tagElement.id.toString().trim().replace(/\s+/g, '-'));
            if (newTags.length) {
                queryParams += queryParamList.length ? '' : '?';
                queryParams += `tags=${newTags.toString()}`;
            } else if (queryParamList.length) {
                queryParams = queryParams.substring(0, queryParams.length - 1);
            }
            window.history.replaceState(
                { ...window.history.state, url: `/webinars` + queryParams, as: `/webinars` + queryParams },
                '',
                window.location.origin + `/webinars` + queryParams,
            );
            setFilters((prev) => {
                const newFilter = prev;
                const index = newFilter.tags.indexOf(tagElement.id);
                if (index !== -1) {
                    newFilter.tags = newFilter.tags.slice(0, index).concat(newFilter.tags.slice(index + 1));
                }
                return { ...newFilter };
            });
            setCurrentPage('1');
            const forSendToAPITags = newTags.map((tag) => tag.trim().replace(/-/g, ' ')).toString();
            sendRequest({
                ...params,
                page: 1,
                tags: forSendToAPITags,
            });
        }
    };

    const updateFilters = (data) => {
        const urlSearchParams = new URLSearchParams(window.location.search);
        const params = Object.fromEntries(urlSearchParams.entries());
        const queryParamList: string[] = [];
        const pushToParams = (param) => {
            if (data[`${param}`] !== undefined && data[`${param}`] !== '') {
                queryParamList.push(`${param}=${data[`${param}`]}`);
            } else if (data[`${param}`] !== '' && params[`${param}`]) {
                queryParamList.push(`${param}=${params[`${param}`]}`);
            }
        };
        if (params.q) {
            queryParamList.push(`q=${params.q}`);
        }
        pushToParams('date');
        pushToParams('price');
        pushToParams('sortDate');
        if (params.tags) {
            queryParamList.push(`tags=${params.tags}`);
        }

        let queryParams = queryParamList.length ? '?' : '';
        for (const param of queryParamList) {
            queryParams += param + '&';
        }
        queryParams = queryParams.substring(0, queryParams.length - 1);
        window.history.replaceState(
            { ...window.history.state, url: `/webinars` + queryParams, as: `/webinars` + queryParams },
            '',
            window.location.origin + `/webinars` + queryParams,
        );
        setFilters((prev) => {
            return { ...Object.assign(prev, data) };
        });
        setCurrentPage('1');
        const forSendToAPITags = params.tags
            ? params.tags
                  .split(',')
                  .map((tag) => tag.trim().replace(/-/g, ' '))
                  .toString()
            : '';
        sendRequest({
            ...params,
            ...data,
            tags: forSendToAPITags,
            page: 1,
        });
    };

    const searchHandler = (phrase) => {
        const urlSearchParams = new URLSearchParams(window.location.search);
        const params = Object.fromEntries(urlSearchParams.entries());
        const queryParamList: string[] = [];
        const pushToParams = (param) => {
            params[`${param}`] ? queryParamList.push(`${param}=${params[`${param}`]}`) : '';
        };
        if (phrase !== '') {
            queryParamList.push(`q=${phrase}`);
        }
        pushToParams('date');
        pushToParams('price');
        pushToParams('sortDate');
        pushToParams('tags');

        let queryParams = queryParamList.length ? '?' : '';
        for (const param of queryParamList) {
            queryParams += param + '&';
        }
        queryParams = queryParams.substring(0, queryParams.length - 1);
        window.history.replaceState(
            { ...window.history.state, url: `/webinars` + queryParams, as: `/webinars` + queryParams },
            '',
            window.location.origin + `/webinars` + queryParams,
        );
        setCurrentPage('1');
        const forSendToAPITags = params.tags
            ? params.tags
                  .split(',')
                  .map((tag) => tag.trim().replace(/-/g, ' '))
                  .toString()
            : '';
        sendRequest({
            ...params,
            tags: forSendToAPITags,
            q: phrase,
            page: 1,
        });
    };

    const handlePageClick = (data) => {
        const urlSearchParams = new URLSearchParams(window.location.search);
        const params = Object.fromEntries(urlSearchParams.entries());
        const queryParamList: string[] = [];
        const pushToParams = (param) => {
            params[`${param}`] ? queryParamList.push(`${param}=${params[`${param}`]}`) : '';
        };
        pushToParams('q');
        pushToParams('date');
        pushToParams('price');
        pushToParams('sortDate');
        pushToParams('tags');

        let queryParams = queryParamList.length ? '?' : '';
        for (const param of queryParamList) {
            queryParams += param + '&';
        }
        queryParams += queryParamList.length ? '' : '?';
        queryParams += `page=${data.selected + 1}`;
        window.history.replaceState(
            {
                ...window.history.state,
                url: `/webinars` + queryParams,
                as: `/webinars` + queryParams,
            },
            '',
            window.location.origin + `/webinars` + queryParams,
        );
        setCurrentPage(data.selected + 1);
        const forSendToAPITags = params.tags
            ? params.tags
                  .split(',')
                  .map((tag) => tag.trim().replace(/-/g, ' '))
                  .toString()
            : '';
        sendRequest({
            ...params,
            tags: forSendToAPITags,
            page: data.selected + 1,
        });
    };

    const sendRequest = (params: { q?; page?; date?; price?; sortDate?; tags? }) => {
        setLoading(true);
        const data = {
            phrase: params.q || '',
            page: params.page || 1,
            date: params.date || '',
            price: params.price || '',
            sortDate: params.sortDate || '',
            tags: params.tags || '',
        };

        const result = apiSearchWebinars(data);
        result
            .then((response) => {
                if (response.status === 'data') {
                    setWebinars(response.data.webinars);
                    setLastPage(response.data.lastpage);
                }
            })
            .catch(() => {
                toast.error('مشکلی در دریافت اطلاعات به وجود آمد!');
            })
            .finally(() => setLoading(false));
    };

    const setTagsLimit = (status: boolean) => {
        if (status) {
            setTags(props.tags.slice(0, 5));
        } else {
            setTags(props.tags);
        }
    };

    const toggleLimitAction = () => {
        setTagsLimit(!limitedTagsStatus);
        setLimitedTagsStatus(!limitedTagsStatus);
    };

    return (
        <div className={styles['background-container']}>
            <Head>
                <title>همه وبینار ها - آسان سمینار</title>
                <meta name="description" content="وبینارهای آنلاین برگزار شده توسط آسان سمینار" />
                <meta property="og:url" content="https://asanseminar.ir" />
                <meta property="og:title" content="همه وبینار ها - آسان سمینار" />
                <meta property="og:description" content="وبینارهای آنلاین برگزار شده توسط آسان سمینار" />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="همه وبینار ها - آسان سمینار" />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
            </Head>
            <Container className={styles.page}>
                <Row>
                    <Col>
                        <h1 className={styles.title}>همه کلاس های آنلاین</h1>
                        <Searchbar
                            initialValue={(router.query.q as string) || ''}
                            containerClasses={styles.search}
                            fieldClasses={styles['search-field']}
                            placeHolder="هر دوره ای رو که دوست داری جستجو کن ..."
                            customHandler={searchHandler}
                            setLoading={setLoading}
                        />
                        <TagsList
                            data={tags}
                            arePersistent={true}
                            tagUpdate={updateTagsFilter}
                            limitAction={props.tags.length > 5 ? limitedTagsStatus : undefined}
                            toggleLimitAction={toggleLimitAction}
                        />
                        <hr className={styles.hr2} ref={webinarsRef} />
                    </Col>
                    <FiltersSection filterUpdate={updateFilters} filters={filters} />
                </Row>
                <Row className={cn('gx-sm-4 gx-md-5 gy-5', styles['gy-5'], styles.courses)}>
                    {loading ? (
                        <div className={styles.loader}>
                            <Spinner animation="grow" variant={'primary'} />
                            <div style={{ marginRight: '1rem' }}>درحال بارگذاری...</div>
                        </div>
                    ) : (
                        <ListSection data={webinars} />
                    )}
                </Row>
                {lastPage > 1 ? (
                    <Pagination page={currentPage} handlePageClick={handlePageClick} pageCount={lastPage} />
                ) : null}
                {/*<Row className={cn('gx-sm-4 gx-md-5 gy-5', styles['gy-5'], styles['recommended-courses'])}>
                    <RecommendedSection data={webinars.slice(0, 3)} />
                </Row>*/}
                {/*<Row>*/}
                {/*    <Col>*/}
                {/*        <h2 className={styles['bottom-text-title']}>دوره هایی رو که می خواهید به راحتی پیدا کنید</h2>*/}
                {/*        <p className={styles['bottom-text']}>*/}
                {/*            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است.*/}
                {/*            چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی*/}
                {/*            تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در*/}
                {/*            شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها*/}
                {/*            شناخت بیشتری را برای طراحان رایانه ای علی الخصوص طراحان خلاقی و فرهنگ پیشرو در زبان فارسی*/}
                {/*            ایجاد کرد. در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط*/}
                {/*            سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته*/}
                {/*            اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.{' '}*/}
                {/*        </p>*/}
                {/*    </Col>*/}
                {/*</Row>*/}
            </Container>
        </div>
    );
}
