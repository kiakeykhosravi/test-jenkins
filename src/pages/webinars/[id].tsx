import React, { useContext, useEffect, useState } from 'react';
import styles from 'styles/course.module.scss';
import CourseTopSection from 'modules/course/components/course-top-section';
import { useRouter } from 'next/router';
import CourseMainSection from 'modules/course/components/course-main-section';
//import RecommendedSection from 'modules/course/components/recommended-section';
import apiWebinar from 'modules/course/api/api-webinar';
import { IWebinarResponse } from 'modules/course/responses';
import { OrganizationType } from 'models/organization';
import Custom404 from '../404';
import parseCookies from 'utils/parse-cookies';
import { AxiosRequestConfig } from 'axios';
import Head from 'next/head';
import { IWebinar } from 'models/webinar';
import { cookieNames } from 'utils/storage-vars';
import apiUser from 'modules/profile/api/api-user';
import { IUser } from 'models/user';
import { AuthContext, IAuthContextType } from 'contexts/auth-context';

export async function getServerSideProps({ params, req, query }) {
    const id = params ? params.id : '';
    const marketer = query && query.marketer ? query.marketer : null;
    const rf = query && query.rf ? query.rf : null;
    const cookies = parseCookies(req);
    const config: AxiosRequestConfig = {};

    let user: IUser | null = null;
    let shouldLogin = false;

    if (cookies && cookies[cookieNames.apiToken]) {
        config.headers = {
            Authorization: `Bearer ${cookies[cookieNames.apiToken]}`,
        };

        user = await apiUser(true, config);
    }

    const webinarResult = await apiWebinar(true, id, config);
    const webinar = webinarResult.status === 'data' ? webinarResult.data : null;

    if (
        webinarResult.status === 'error' &&
        webinarResult.errors &&
        webinarResult.errors.organization_active === false
    ) {
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        };
    }

    if (
        webinar &&
        webinar.webinar.organization.type === OrganizationType.SCHOOL &&
        (!cookies || !cookies[cookieNames.apiToken] || !user)
    ) {
        shouldLogin = true;
    }

    let fullUrl = '';
    if (req) {
        fullUrl =
            (process.env.NODE_ENV === 'production' ? process.env.NEXT_PUBLIC_APP_URL : 'http://localhost:3000') +
            req.url;
    }

    return {
        props: { webinarData: webinar, userData: user, fullUrl, shouldLogin, marketer, rf },
    };
}

export default function Webinar(props: {
    webinarData: IWebinarResponse | null;
    userData?;
    fullUrl;
    shouldLogin;
    marketer;
    rf;
}) {
    const route = useRouter();
    const { id } = route.query as { id: string };
    const outerAuthContext = useContext(AuthContext);

    const changeAuthState = (newValue: IUser | null) => {
        setAuthState((prevState: IAuthContextType) => {
            const shouldUpdate =
                typeof newValue !== typeof prevState.user ||
                (prevState.user && prevState.user.id) !== (newValue && newValue.id);
            if (shouldUpdate) {
                return {
                    user: newValue ? newValue : null,
                    changeIsLoggedIn: changeAuthState,
                };
            }
            return prevState;
        });
    };

    const [authState, setAuthState] = useState<IAuthContextType>({
        user: props.userData,
        changeIsLoggedIn: changeAuthState,
    });
    const [renderCount, setRenderCount] = useState(0);
    const [showLoginModal, setShowLoginModal] = useState(props.shouldLogin);
    const [webinarData, setWebinarData] = useState<IWebinarResponse | null>(props.webinarData);
    const [firstRender, setFirstRender] = useState(true);

    useEffect(() => {
        if (webinarData) {
            const webinarTitle = webinarData.webinar.title.trim().replace(/\s+/g, '-');
            let fixedUrl = route.asPath;
            const indexOfRandomSlug = fixedUrl.indexOf(id) + id.length;
            if (indexOfRandomSlug !== -1) {
                fixedUrl = fixedUrl.slice(0, indexOfRandomSlug).concat(`/${webinarTitle}`);
                if (props.rf) fixedUrl = fixedUrl + `?rf=${props.rf}`;
                else if (props.marketer) fixedUrl = fixedUrl + `?marketer=${props.marketer}`;
                window.history.replaceState(window.history.state, '', window.location.origin + fixedUrl);
            }
        }
    }, []);

    useEffect(() => {
        if (renderCount) {
            if (outerAuthContext.user) {
                changeAuthState(props.userData);
            } else {
                changeAuthState(null);
            }
        }
        setRenderCount((prevVal) => ++prevVal);
    }, [outerAuthContext]);

    useEffect(() => {
        setShowLoginModal(props.shouldLogin);
        if (!firstRender) setWebinarData(props.webinarData);
        setFirstRender(false);
    }, [route.query.id]);

    const updateWebinar = async () => {
        const webinarResult = await apiWebinar(false, id);
        if (webinarResult.status === 'data') {
            setWebinarData(webinarResult.data);
        } else if (webinarResult.errors && webinarResult.errors.organization_active === false) {
            route.push('/');
        } else {
            route.reload();
        }
    };

    const shouldIndex = [
        '1007607945',
        '1033591001',
        '1046718028',
        '105473864',
        '1095432918',
        '1122976062',
        '1142066376',
        '116895866',
        '1198445446',
        '1207766601',
        '1257922250',
        '1276208731',
        '1334788338',
        '1390419242',
        '1392652435',
        '1435644725',
        '146716062',
        '148465642',
        '1486723216',
        '1548434061',
        '1577389684',
        '1692812908',
        '1713288298',
        '1735441326',
        '1752774157',
        '1758029464',
        '1814127789',
        '1816345110',
        '186885988',
        '188026426',
        '1934550001',
        '2039219200',
        '210759028',
        '2117751090',
        '232911016',
        '237041637',
        '24945600',
        '252069776',
        '271504222',
        '302224913',
        '314580950',
        '356291049',
        '373384401',
        '404004798',
        '418001894',
        '448850265',
        '449646467',
        '501614875',
        '53916855',
        '565069755',
        '570961092',
        '603017816',
        '604631891',
        '615224892',
        '644665200',
        '666468600',
        '668706721',
        '688602021',
        '70421036',
        '770830778',
        '779944345',
        '781024684',
        '808500038',
        '820153074',
        '820641359',
        '853456130',
        '881863919',
        '895959255',
        '943081382',
        '955567899',
        '97969052',
        '985360684',
        '990599559',
        '997362964',
        '220190983',
        '653645750',
        '1702750092',
    ];

    if (webinarData) {
        const webinar: IWebinar = webinarData.webinar;
        const webinarType: OrganizationType = webinar.organization.type;
        return (
            <div className={styles.page}>
                <Head>
                    {(webinarType === OrganizationType.EDUCATIONAL_INSTITUTION ||
                        webinarType === OrganizationType.SCHOOL) &&
                    !shouldIndex.includes(props.webinarData.webinar.id.toString()) ? (
                        <meta name="robots" content="noindex" />
                    ) : (
                        <meta name="robots" content="index, nofollow" />
                    )}
                    <title>{webinar.title} - آسان سمینار</title>
                    <meta name="description" content={webinar.seo_description || ''} />
                    <meta
                        name="keywords"
                        content={(webinarData.tags && webinarData.tags.map((tag) => tag.title).join()) || ''}
                    />
                    <meta property="og:url" content={props.fullUrl} />
                    <meta property="og:title" content={`${webinar.title} - آسان سمینار`} />
                    <meta property="og:description" content={webinar.seo_description || ''} />
                    <meta property="og:type" content={'website'} />
                    <meta property="og:image" content={webinar.media && webinar.media.length && webinar.media[0].url} />
                    <meta property="og:image:type" content="image/png" />
                    <meta property="og:image:alt" content={webinar.title} />
                    <meta property="og:site_name" content="آسان سمینار - پلتفرم آموزش آنلاین" />
                    <meta property="og:locale" content="fa_IR" />
                    <meta property="og:price" content={String(webinar.price_with_off)} />
                    <meta property="og:price:currency" content="IRR" />
                    <meta name="twitter:card" content="summary_large_image" />
                    <meta name="twitter:title" content={webinar.title} />
                    <meta name="twitter:image:alt" content={webinar.title} />
                    <script
                        type="application/ld+json"
                        dangerouslySetInnerHTML={{ __html: JSON.stringify(webinarData.webinar.schema || {}) }}
                    />
                </Head>
                <AuthContext.Provider value={authState}>
                    <CourseTopSection
                        data={webinarData}
                        showLoginModal={showLoginModal}
                        setShowLoginModal={setShowLoginModal}
                        shouldLogin={props.shouldLogin}
                        marketer={props.rf || props.marketer || null}
                        updateWebinar={updateWebinar}
                        key={webinar.id}
                    />
                    <CourseMainSection data={webinarData} key={webinar.id} />
                    {/*TODO: Add recommended webinar when api is completed*/}
                    {/*{webinarType === OrganizationType.ORGANIZATION || webinarType === OrganizationType.INSTITUTION ? (*/}
                    {/*    <RecommendedSection data={webinarData.} />*/}
                    {/*) : null}*/}
                </AuthContext.Provider>
            </div>
        );
    } else return <Custom404 />;
}
