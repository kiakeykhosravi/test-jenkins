import React, { useEffect, useState } from 'react';
import OrganizationHeader from 'modules/organizations/components/organization-header';
import OrganizationBody from 'modules/organizations/components/organization-body';
import apiOrganization from 'modules/organizations/api/api-organization';
import { useRouter } from 'next/router';
import Pagination from 'modules/shared/components/pagination';
import { IOrganization, OrganizationType } from 'models/organization';
import { IWebinarCompact } from 'models/webinar-compact';
import Head from 'next/head';
import apiSearchOrganizationWebinars from 'modules/search/api/api-search-organization-webinars';
import Custom404 from 'pages/404';
import FooterLarge from 'modules/layout/components/footer-large';
import Header from 'modules/layout/components/header';
import BrandpagePlus from 'modules/brand/components/brandpage-plus';
import BrandpagePremium from 'modules/brand/components/brandpage-premium';
import schemas from 'utils/schemas';
import { toast } from 'react-toastify';

const brandPages = [
    OrganizationType.BRANDPAGE_PREMIUM,
    OrganizationType.BRANDPAGE_PLUS,
    OrganizationType.BRANDPAGE_GOLDEN,
    OrganizationType.BRANDPAGE_PLATINUM,
];
export async function getServerSideProps({ params, query }) {
    const organizationResult = await apiOrganization(params.slug);
    const organizationData = organizationResult.status === 'data' ? organizationResult.data.organization : null;
    const origin = process.env.NODE_ENV === 'production' ? process.env.NEXT_PUBLIC_APP_URL : 'http://localhost:3000';
    const sliders = organizationData ? organizationData.sliders : [];

    if (organizationResult.status === 'data') {
        if (!brandPages.includes(organizationResult.data.organization.type)) {
            const data = {
                organizationID: organizationResult.data.organization.id,
                organizationSlug: params.slug,
                phrase: query.q || '',
                page: query.page || 1,
                date: query.date || '',
                price: query.price || '',
                tags: query.tags || '',
                sortDate: query.sortDate || '',
            };
            const webinarsResult = await apiSearchOrganizationWebinars(true, data);
            const webinarsData = webinarsResult.status === 'data' ? webinarsResult.data.webinars : [];
            const lastPage = webinarsResult.status === 'data' ? webinarsResult.data.lastpage : 1;
            return {
                props: {
                    organizationData: organizationResult.data.organization,
                    webinarsData,
                    lastPage,
                    origin,
                    sliders,
                },
            };
        }
    } else {
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        };
    }
    return {
        props: {
            organizationData,
            webinarsData: [],
            lastPage: 1,
            origin,
            sliders,
        },
    };
}

export default function Organization(props: {
    organizationData: IOrganization;
    webinarsData: IWebinarCompact[];
    lastPage: number;
    origin: string;
    sliders;
}) {
    const router = useRouter();

    const [firstRender, setFirstRender] = useState(true);
    const [loading, setLoading] = useState(false);
    const [lastPage, setLastPage] = useState(props.lastPage);
    const [webinars, setWebinars] = useState(props.webinarsData);
    const [search, setSearch] = useState((router.query.q as string) || '');
    const [currentPage, setCurrentPage] = useState((router.query.page as string) || 1);
    const [filters, setFilters] = useState({
        date: (router.query.date as string) || '',
        price: (router.query.price as string) || '',
        sortDate: (router.query.sortDate as string) || '',
    });

    useEffect(() => {
        if (!firstRender) {
            setLastPage(props.lastPage);
            setWebinars(props.webinarsData);
            setSearch((router.query.q as string) || '');
            setCurrentPage((router.query.page as string) || 1);
            setFilters({
                date: (router.query.date as string) || '',
                price: (router.query.price as string) || '',
                sortDate: (router.query.sortDate as string) || '',
            });
        }
        setFirstRender(false);
    }, [router.query.slug]);

    const updateFilters = (data) => {
        const urlSearchParams = new URLSearchParams(window.location.search);
        const params = Object.fromEntries(urlSearchParams.entries());
        const queryParamList: string[] = [];

        const pushToParams = (param) => {
            if (data[`${param}`] !== undefined && data[`${param}`] !== '') {
                queryParamList.push(`${param}=${data[`${param}`]}`);
            } else if (data[`${param}`] !== '' && params[`${param}`]) {
                queryParamList.push(`${param}=${params[`${param}`]}`);
            }
        };
        if (params.q) {
            queryParamList.push(`q=${params.q}`);
        }
        pushToParams('date');
        pushToParams('price');
        pushToParams('sortDate');

        let queryParams = queryParamList.length ? '?' : '';
        for (const param of queryParamList) {
            queryParams += param + '&';
        }
        queryParams = queryParams.substring(0, queryParams.length - 1);
        window.history.replaceState(
            {
                ...window.history.state,
                url: `/organizations/${router.query.slug}` + queryParams,
                as: `/organizations/${router.query.slug}` + queryParams,
            },
            '',
            window.location.origin + `/organizations/${router.query.slug}` + queryParams,
        );

        setFilters((prev) => {
            return { ...Object.assign(prev, data) };
        });
        setCurrentPage('1');
        sendRequest({
            ...params,
            ...data,
            page: 1,
        });
    };

    const searchHandler = (phrase) => {
        const urlSearchParams = new URLSearchParams(window.location.search);
        const params = Object.fromEntries(urlSearchParams.entries());
        const queryParamList: string[] = [];

        const pushToParams = (param) => {
            params[`${param}`] ? queryParamList.push(`${param}=${params[`${param}`]}`) : '';
        };
        if (phrase !== '') {
            queryParamList.push(`q=${phrase}`);
        }
        pushToParams('date');
        pushToParams('price');
        pushToParams('sortDate');

        let queryParams = queryParamList.length ? '?' : '';
        for (const param of queryParamList) {
            queryParams += param + '&';
        }
        queryParams = queryParams.substring(0, queryParams.length - 1);
        window.history.replaceState(
            {
                ...window.history.state,
                url: `/organizations/${router.query.slug}` + queryParams,
                as: `/organizations/${router.query.slug}` + queryParams,
            },
            '',
            window.location.origin + `/organizations/${router.query.slug}` + queryParams,
        );
        setSearch(phrase);
        setCurrentPage('1');
        sendRequest({
            ...params,
            q: phrase,
            page: 1,
        });
    };

    const handlePageClick = (data) => {
        const urlSearchParams = new URLSearchParams(window.location.search);
        const params = Object.fromEntries(urlSearchParams.entries());
        const queryParamList: string[] = [];

        const pushToParams = (param) => {
            params[`${param}`] ? queryParamList.push(`${param}=${params[`${param}`]}`) : '';
        };
        pushToParams('q');
        pushToParams('date');
        pushToParams('price');
        pushToParams('sortDate');

        let queryParams = queryParamList.length ? '?' : '';
        for (const param of queryParamList) {
            queryParams += param + '&';
        }
        queryParams += queryParamList.length ? '' : '?';
        queryParams += `page=${data.selected + 1}`;
        window.history.replaceState(
            {
                ...window.history.state,
                url: `/organizations/${router.query.slug}` + queryParams,
                as: `/organizations/${router.query.slug}` + queryParams,
            },
            '',
            window.location.origin + `/organizations/${router.query.slug}` + queryParams,
        );

        setCurrentPage(data.selected + 1);
        sendRequest({
            ...params,
            page: data.selected + 1,
        });
    };

    const sendRequest = (params: { q?; page?; date?; price?; sortDate? }) => {
        const data = {
            organizationID: props.organizationData.id,
            organizationSlug: props.organizationData.slug,
            phrase: params.q || '',
            page: params.page || 1,
            date: params.date || '',
            price: params.price || '',
            sortDate: params.sortDate || '',
        };
        const result = apiSearchOrganizationWebinars(false, data);
        result
            .then((response) => {
                if (response.status === 'data') {
                    setWebinars(response.data.webinars);
                    setLastPage(response.data.lastpage);
                }
                setLoading(false);
            })
            .catch((error) => {
                setLoading(false);
                console.log('An error occured while getting webinars');
                toast.error('مشکلی در دریافت اطلاعات به وجود آمد!');
            });
    };

    const renderBrandpage = () => {
        if (props.organizationData.type === OrganizationType.BRANDPAGE_PREMIUM) {
            return <BrandpagePremium organizationData={props.organizationData} />;
        }
        if (props.organizationData.type === OrganizationType.BRANDPAGE_PLUS) {
            return <BrandpagePlus organizationData={props.organizationData} sliders={props.sliders} />;
        }
    };

    if (!props.organizationData) {
        return <Custom404 />;
    }
    return (
        <>
            <Head>
                {brandPages.includes(props.organizationData.type) ? (
                    <link rel="icon" type="image/ico" href={props.organizationData.logo.thumbnail_url} />
                ) : (
                    <>
                        <link rel="icon" type="image/ico" href="/favicon.ico" />
                        {/* Goftino Script */}
                        <script
                            type="text/javascript"
                            dangerouslySetInnerHTML={{
                                __html: `!function(){var i="Gl8TWj",a=window,d=document;function g(){var g=d.createElement("script"),s="https://www.goftino.com/widget/"+i,l=localStorage.getItem("goftino_"+i);g.async=!0,g.src=l?s+"?o="+l:s;d.getElementsByTagName("head")[0].appendChild(g);}"complete"===d.readyState?g():a.attachEvent?a.attachEvent("onload",g):a.addEventListener("load",g,!1);}();`,
                            }}
                        />

                        {/* Website Schema */}
                        <script
                            type="application/ld+json"
                            dangerouslySetInnerHTML={{
                                __html: schemas.websiteSchema,
                            }}
                        />
                    </>
                )}
                <meta name="robots" content="index, nofollow" />
                <title>{props.organizationData.title}</title>
                <meta name="description" content={props.organizationData.description} />
                <link rel="canonical" href={props.origin + '/organizations/' + props.organizationData.slug} />
                <meta property="og:url" content={props.origin + router.asPath} />
                <meta property="og:title" content={`${props.organizationData.title}`} />
                <meta property="og:description" content={props.organizationData.description} />
                <meta property="og:type" content={'website'} />
                <meta
                    property="og:image"
                    content={`${props.organizationData.logo && props.organizationData.logo.url}`}
                />
                <meta property="og:image:type" content={'image/jpeg'} />
                <meta property="og:image:alt" content={props.organizationData.title} />
                <meta property="og:site_name" content="آسان سمینار - پلتفرم آموزش آنلاین" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content={props.organizationData.title} />
                <meta name="twitter:image:alt" content={props.organizationData.title} />
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={{ __html: JSON.stringify(props.organizationData.schema) }}
                />
            </Head>
            {brandPages.includes(props.organizationData.type) ? (
                renderBrandpage()
            ) : (
                <>
                    <Header />
                    <OrganizationHeader organization={props.organizationData} />
                    <OrganizationBody
                        organization={props.organizationData}
                        webinars={webinars}
                        initialValue={search}
                        filters={filters}
                        updateFilters={updateFilters}
                        searchHandler={searchHandler}
                        loading={loading}
                        setLoading={setLoading}
                    />
                    {!loading && lastPage > 1 ? (
                        <div className="container">
                            <div className="col-lg-9 offset-lg-3">
                                <Pagination
                                    page={String(currentPage)}
                                    handlePageClick={handlePageClick}
                                    pageCount={lastPage}
                                />
                            </div>
                        </div>
                    ) : null}
                    <FooterLarge />
                </>
            )}
        </>
    );
}
