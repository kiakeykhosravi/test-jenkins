import React from 'react';
import BrandpageHeader from 'modules/brandpage/components/layout/brandpage-header';
import BrandpageProduct from 'modules/brand/components/brandpage-product';
import apiProduct from 'modules/brand/api/api-product';
import { IOrganizationProduct } from 'models/organization-product';
import { IOrganizationSliderProduct } from 'models/organization-slider-product';
import BrandpageFooter from 'modules/brandpage/components/layout/brandpage-footer';
import Head from 'next/head';
import { useRouter } from 'next/router';
import apiOrganization from 'modules/organizations/api/api-organization';
import { IOrganization } from 'models/organization';

export const getServerSideProps = async ({ params }) => {
    const organizationResult = await apiOrganization(params.slug);
    const organization = organizationResult.status === 'data' ? organizationResult.data.organization : null;
    if (!organization) {
        return {
            redirect: {
                destination: '/organizations',
                permanent: false,
            },
        };
    }

    const productResult = await apiProduct(params.id);
    if (productResult.status !== 'data') {
        return {
            redirect: {
                destination: '/organizations/' + organization.slug,
                permanent: false,
            },
        };
    }

    const product = productResult.data.product;
    const relatedProducts = productResult.data.related_products;

    const origin = process.env.NODE_ENV === 'production' ? process.env.NEXT_PUBLIC_APP_URL : 'http://localhost:3000';

    return {
        props: {
            product,
            organization,
            relatedProducts,
            origin,
            organizationSlug: params.slug,
        },
    };
};

export default function Product(props: {
    product: IOrganizationProduct;
    organization: IOrganization;
    relatedProducts: IOrganizationSliderProduct[];
    origin: string;
    organizationSlug: string;
}) {
    const router = useRouter();
    return (
        <>
            <Head>
                <link rel="icon" type="image/ico" href={props.organization.logo.thumbnail_url} />
                <meta name="robots" content="index, nofollow" />
                <title>{`${props.organization.title} | ${props.product.title}`}</title>
                <meta name="description" content={props.product.short_description} />
                <meta property="og:url" content={props.origin + router.asPath} />
                <meta property="og:title" content={`${props.organization.title} | ${props.product.title}`} />
                <meta property="og:description" content={props.product.short_description} />
                <meta property="og:type" content={'website'} />
                <meta
                    property="og:image"
                    content={`${
                        (props.product.media && props.product.media.length && props.product.media[0]) ||
                        '/images/logos/brandpage-logo.jpg'
                    }`}
                />
                <meta property="og:image:type" content={'image/jpeg'} />
                <meta property="og:image:alt" content={props.product.title} />
                <meta property="og:site_name" content="آسان سمینار - پلتفرم آموزش آنلاین" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content={props.product.title} />
                <meta name="twitter:image:alt" content={props.product.title} />
            </Head>
            <BrandpageHeader />
            <BrandpageProduct
                product={props.product}
                relatedProducts={props.relatedProducts}
                slug={props.organizationSlug}
            />
            <BrandpageFooter />
        </>
    );
}
