import React from 'react';
import styles from 'styles/blog-search.module.scss';
import { IBlog } from 'models/blog';
import { Col, Container, Row } from 'react-bootstrap';
import classNames from 'classnames';
import Pagination from 'modules/shared/components/pagination';
import router from 'next/router';
import Head from 'next/head';
import apiOrganizationBlog from 'modules/blog/api/api-organization-blog';
import OrganizationBlogArticleCard from 'modules/blog/components/organization-blog-article-card';
import { isOrganizationValid } from 'utils/is-organization-valid';
import { OrganizationType } from 'models/organization';

export async function getServerSideProps({ req, query, params }) {
    const isValidOrganization = await isOrganizationValid(params.slug);
    if (!isValidOrganization) {
        return {
            redirect: {
                destination: '/organizations',
                permanent: false,
            },
        };
    } else {
        const brandPages = [
            OrganizationType.BRANDPAGE_PREMIUM,
            OrganizationType.BRANDPAGE_PLUS,
            OrganizationType.BRANDPAGE_GOLDEN,
            OrganizationType.BRANDPAGE_PLATINUM,
        ];
        if (brandPages.includes(isValidOrganization.organization.type)) {
            return {
                redirect: {
                    destination: '/organizations/' + isValidOrganization.organization.slug,
                    permanent: false,
                },
            };
        }
    }

    const blogsResult = await apiOrganizationBlog(true, params.slug, query.page || 1, query.q || '');
    const [blogs, lastPage, organizationTitle] =
        blogsResult.status === 'data'
            ? [blogsResult.data.posts, blogsResult.data.lastpage, blogsResult.data.organization_title]
            : [[], 1, ''];

    const fullUrl = req
        ? (process.env.NODE_ENV === 'production' ? process.env.NEXT_PUBLIC_APP_URL : 'http://localhost:3000') + req.url
        : '';

    return {
        props: {
            blogs,
            lastPage,
            q: query.q || '',
            page: query.page || 1,
            fullUrl,
            organization_slug: params.slug,
            organizationTitle,
        },
    };
}

export default function BlogSearch(props: {
    blogs: IBlog[];
    lastPage: number;
    q: string;
    page: number;
    fullUrl;
    organization_slug;
    organizationTitle;
}) {
    const renderArticles = () => {
        return props.blogs.length ? (
            props.blogs.map((article) => {
                return (
                    <Col sm={6} lg={4} key={article.title}>
                        <OrganizationBlogArticleCard data={article} description={true} />
                    </Col>
                );
            })
        ) : (
            <div className={styles['not-found']}>
                <img src="/images/notes.png" className={styles['not-found-image']} />
                <div>مقاله ای برای جستجوی شما یافت نشد</div>
            </div>
        );
    };

    return (
        <>
            <Head>
                <title>{`جستجو در وبلاگ ${props.organizationTitle}`}</title>
                <meta name="description" content="بلاگ مورد نظر خود را جستجو کنید" />
                <meta property="og:url" content={props.fullUrl} />
                <meta property="og:title" content={`جستجو در وبلاگ ${props.organizationTitle}`} />
                <meta property="og:description" content="بلاگ مورد نظر خود را جستجو کنید" />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار - پلتفرم آموزش آنلاین" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content={`وبلاگ ${props.organizationTitle}`} />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
            </Head>
            <Container className={styles.main}>
                <Row className={classNames('gx-sm-4 gx-md-5 gy-4', styles.articles)}>{renderArticles()}</Row>
                {props.lastPage > 1 ? (
                    <Pagination
                        pageCount={props.lastPage}
                        page={String(props.page)}
                        handlePageClick={(data) =>
                            router
                                .push(
                                    `/organizations/${props.organization_slug}/blog/search?q=${props.q}&page=${
                                        data.selected + 1
                                    }`,
                                )
                                .then(() => window.scrollTo(0, 0))
                        }
                    />
                ) : null}
            </Container>
        </>
    );
}
