import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import styles from 'styles/blog.module.scss';
import { Breadcrumb, Container } from 'react-bootstrap';
import ArticleHeader from 'modules/blog/components/article-header';
import Link from 'next/link';
import DOMPurify from 'isomorphic-dompurify';
import CommunicateSection from 'modules/blog/components/communicate-section';
import TagsSection from 'modules/blog/components/tags-section';
import classNames from 'classnames';
import apiBlog from 'modules/blog/api/api-blog';
import { IBlog } from 'models/blog';
import Custom404 from 'pages/404';
import Head from 'next/head';
import CommentSection from 'modules/blog/components/comment-section';
import AsanImage from 'modules/shared/components/asan-image';
import parseCookies from 'utils/parse-cookies';
import { cookieNames } from 'utils/storage-vars';
import { AxiosRequestConfig } from 'axios';
import { isOrganizationValid } from 'utils/is-organization-valid';
import { OrganizationType } from 'models/organization';

export async function getServerSideProps({ req, params }) {
    const isValidOrganization = await isOrganizationValid(params.slug);
    if (!isValidOrganization) {
        return {
            redirect: {
                destination: '/organizations',
                permanent: false,
            },
        };
    } else {
        const brandPages = [
            OrganizationType.BRANDPAGE_PREMIUM,
            OrganizationType.BRANDPAGE_PLUS,
            OrganizationType.BRANDPAGE_GOLDEN,
            OrganizationType.BRANDPAGE_PLATINUM,
        ];
        if (brandPages.includes(isValidOrganization.organization.type)) {
            return {
                redirect: {
                    destination: '/organizations/' + isValidOrganization.organization.slug,
                    permanent: false,
                },
            };
        }
    }

    const cookies = parseCookies(req);
    const config: AxiosRequestConfig = {};

    if (cookies && cookies[cookieNames.apiToken]) {
        config.headers = {
            Authorization: `Bearer ${cookies[cookieNames.apiToken]}`,
        };
    }

    const blogResult = await apiBlog(params.id, config);
    const blog = blogResult.status === 'data' ? blogResult.data.post : null;

    const origin = process.env.NODE_ENV === 'production' ? process.env.NEXT_PUBLIC_APP_URL : 'http://localhost:3000';

    return {
        props: {
            blog,
            organization_slug: params.slug,
            origin,
        },
    };
}

export default function Article(props: { blog: IBlog | null; organization_slug: string; origin: string }) {
    const route = useRouter();
    const { id } = route.query as { id: string };
    const [isLiked, setIsLiked] = useState(props.blog ? props.blog.is_liked : false);
    const [likesCount, setLikesCount] = useState(props.blog ? props.blog.likes_count || 0 : 0);
    const [pageRandomKey, setPageRandomKey] = useState((props.blog && props.blog.id) || 0);

    const changeIsLiked = (value: boolean) => {
        setIsLiked(value);
        setLikesCount((prevVal) => (value ? prevVal + 1 : prevVal - 1));
    };

    useEffect(() => {
        if (props.blog) {
            setIsLiked(props.blog.is_liked);
            setLikesCount(props.blog.likes_count || 0);
        }

        setPageRandomKey((props.blog && props.blog.id) || 0);
    }, [props.blog]);

    useEffect(() => {
        if (props.blog) {
            const blogTitle = props.blog.title.trim().replace(/\s+/g, '-');
            let fixedUrl = route.asPath;
            const indexOfRandomSlug = fixedUrl.indexOf(id) + id.length;
            if (indexOfRandomSlug !== -1) {
                fixedUrl = fixedUrl.slice(0, indexOfRandomSlug).concat(`/${blogTitle}`);
                setTimeout(() => {
                    window.history.replaceState(window.history.state, '', window.location.origin + fixedUrl);
                }, 200);
            }
        }
    }, []);

    if (props.blog) {
        return (
            <>
                <Head>
                    <script
                        type="application/ld+json"
                        dangerouslySetInnerHTML={{ __html: JSON.stringify(props.blog.schema || {}) }}
                    />
                    <script
                        type="application/ld+json"
                        dangerouslySetInnerHTML={{
                            __html: JSON.stringify((props.blog.category && props.blog.category.schema) || {}),
                        }}
                    />
                    <title>
                        {props.blog.title} - {props.organization_slug}
                    </title>
                    <meta name="description" content={props.blog.description} />
                    <meta property="og:url" content={props.origin + route.asPath} />
                    <meta property="og:title" content={props.blog.title} />
                    <meta property="og:description" content={props.blog.description} />
                    <meta property="og:type" content="aritcle" />
                    <meta property="article:author" content={props.blog.author.fullname} />
                    <meta property="article:published_time" content={props.blog.created_at} />
                    {props.blog.tags.map((tag) => {
                        return <meta key={tag.id} property="article:tag" content={tag.title} />;
                    })}
                    <meta
                        property="og:image"
                        content={
                            (props.blog.media && props.blog.media.length && props.blog.media[0].url) ||
                            '/images/avatar-placeholder.png'
                        }
                    />
                    <meta property="og:image:type" content="image/jpeg" />
                    <meta property="og:image:alt" content={props.blog.title} />
                    <meta property="og:site_name" content="آسان سمینار - پلتفرم آموزش آنلاین" />
                    <meta property="og:locale" content="fa_IR" />
                    <meta name="twitter:card" content="summary_large_image" />
                    <meta name="twitter:title" content={props.blog.title} />
                    <meta name="twitter:image:alt" content={props.blog.title} />
                </Head>
                <Container className={styles.container}>
                    <div className={classNames(styles.blog, styles['org-blog'])}>
                        <Breadcrumb className={styles.breadcrumb}>
                            <Link href="/" passHref>
                                <Breadcrumb.Item href="/" className={styles['breadcrumb-item']}>
                                    خانه
                                </Breadcrumb.Item>
                            </Link>
                            <Link href={`/organizations/${props.organization_slug}`} passHref>
                                <Breadcrumb.Item
                                    href={`/organizations/${props.organization_slug}`}
                                    className={styles['breadcrumb-item']}
                                >
                                    سازمان
                                </Breadcrumb.Item>
                            </Link>
                            <Link href={`/organizations/${props.organization_slug}/blog`} passHref>
                                <Breadcrumb.Item
                                    href={`/organizations/${props.organization_slug}/blog`}
                                    className={styles['breadcrumb-item']}
                                >
                                    وبلاگ
                                </Breadcrumb.Item>
                            </Link>
                            <Breadcrumb.Item active className={styles['breadcrumb-active']}>
                                {props.blog.title}
                            </Breadcrumb.Item>
                        </Breadcrumb>
                        <ArticleHeader
                            article={props.blog}
                            isLiked={isLiked}
                            changeIsLiked={changeIsLiked}
                            likesCountState={likesCount}
                            key={pageRandomKey + 1}
                        />
                        <AsanImage
                            src={
                                props.blog.media && props.blog.media.length
                                    ? props.blog.media[0].url
                                    : '/images/avatar-placeholder.png'
                            }
                            alt={props.blog.title}
                            className={styles.banner}
                            key={pageRandomKey + 2}
                        />
                        <div
                            className={styles['blog-body']}
                            dangerouslySetInnerHTML={{
                                __html: DOMPurify.sanitize(props.blog.body, {
                                    ADD_TAGS: ['iframe'],
                                }),
                            }}
                        />
                        <CommunicateSection
                            article={props.blog}
                            isLiked={isLiked}
                            changeIsLiked={changeIsLiked}
                            likesCountState={likesCount}
                            basePath={props.origin}
                            key={pageRandomKey + 3}
                        />
                        <TagsSection tags={props.blog.tags} key={pageRandomKey + 4} />
                        <CommentSection article={props.blog} key={pageRandomKey + 5} />
                        {/*<CategoriesSection />*/}
                    </div>
                </Container>
            </>
        );
    } else return <Custom404 />;
}
