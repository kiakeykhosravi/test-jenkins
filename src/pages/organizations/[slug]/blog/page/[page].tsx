import React from 'react';
import styles from 'styles/blog.module.scss';
import { Container } from 'react-bootstrap';
import { IBlog } from 'models/blog';
import Head from 'next/head';
import { useRouter } from 'next/router';
import apiOrganizationBlog from 'modules/blog/api/api-organization-blog';
import OrganizationBlog from 'modules/blog/components/organization-blog';
import { isOrganizationValid } from 'utils/is-organization-valid';
import { OrganizationType } from 'models/organization';

export async function getStaticPaths() {
    return {
        paths: [],
        fallback: 'blocking',
    };
}

export const getStaticProps = async ({ params }) => {
    const isValidOrganization = await isOrganizationValid(params.slug);
    if (!isValidOrganization) {
        return {
            redirect: {
                destination: '/organizations',
                permanent: false,
            },
        };
    } else {
        const brandPages = [
            OrganizationType.BRANDPAGE_PREMIUM,
            OrganizationType.BRANDPAGE_PLUS,
            OrganizationType.BRANDPAGE_GOLDEN,
            OrganizationType.BRANDPAGE_PLATINUM,
        ];
        if (brandPages.includes(isValidOrganization.organization.type)) {
            return {
                redirect: {
                    destination: '/organizations/' + isValidOrganization.organization.slug,
                    permanent: false,
                },
            };
        }
    }

    const blogsResult = await apiOrganizationBlog(true, params.slug, params.page);
    const [blogs, blogsLastPage, organizationTitle] =
        blogsResult.status === 'data'
            ? [blogsResult.data.posts, blogsResult.data.lastpage, blogsResult.data.organization_title]
            : [[], 1, ''];

    const origin = process.env.NODE_ENV === 'production' ? process.env.NEXT_PUBLIC_APP_URL : 'http://localhost:3000';

    // const schema = (await apiBlogMainSchema()) || {};

    return {
        props: {
            blogs,
            blogsLastPage,
            blogsCurrentPage: params.page,
            organization_slug: params.slug,
            origin,
            organizationTitle,
            // schema,
        },
        revalidate: 300,
    };
};

export default function Blog(props: {
    blogs: IBlog[];
    blogsLastPage: number;
    blogsCurrentPage: number;
    organization_slug: string;
    origin: string;
    organizationTitle: string;
    // schema: Record<string, any>;
}) {
    const router = useRouter();
    return (
        <div className={styles['main-blog']}>
            <Head>
                <title>{`وبلاگ ${props.organizationTitle}`}</title>
                {/*<script type="application/ld+json" dangerouslySetInnerHTML={{ __html: JSON.stringify(props.schema) }} />*/}
                <meta property="og:url" content={props.origin + router.asPath} />
                <meta property="og:title" content={`وبلاگ ${props.organizationTitle}`} />
                <meta property="og:description" content="" />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content={`لوگو ${props.organizationTitle}`} />
                <meta property="og:site_name" content="آسان سمینار - پلتفرم آموزش آنلاین" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content={`وبلاگ ${props.organizationTitle}`} />
                <meta name="twitter:image:alt" content={`لوگو ${props.organizationTitle}`} />
            </Head>
            <Container className={styles['main-container']}>
                <OrganizationBlog
                    articles={props.blogs}
                    lastPage={props.blogsLastPage}
                    currentPage={props.blogsCurrentPage}
                    organization_slug={props.organization_slug}
                    organization_title={props.organizationTitle}
                />
            </Container>
        </div>
    );
}
