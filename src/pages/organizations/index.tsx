import React, { useEffect, useRef, useState } from 'react';
import OrganizationsCardSection from 'modules/organizations/components/organizations-card-section';
import styles from 'styles/organizations.module.scss';
import { IOrganizationCompact } from 'models/organization-compact';
import Pagination from 'modules/shared/components/pagination';
import { useRouter } from 'next/router';
import apiSearchOrganizations from 'modules/search/api/api-search-organizations';
import Head from 'next/head';
import { Container, Spinner } from 'react-bootstrap';
import Searchbar from 'modules/shared/components/searchbar';
import { toast } from 'react-toastify';

export async function getServerSideProps({ query }) {
    const q = query.q || '';
    const page = query.page || '';

    const organizationsResult = await apiSearchOrganizations(true, q, page);
    const organizations = organizationsResult.status === 'data' ? organizationsResult.data.organizations : [];
    const lastPage = organizationsResult.status === 'data' ? organizationsResult.data.lastpage : 1;
    const origin = process.env.NODE_ENV === 'production' ? process.env.NEXT_PUBLIC_APP_URL : 'http://localhost:3000';

    return {
        props: {
            organizations,
            lastPage,
            origin,
        },
    };
}

export default function Index(props: { organizations: IOrganizationCompact[]; lastPage: number; origin: string }) {
    const router = useRouter();
    const organizationsRef = useRef(null);

    const [firstRender, setFirstRender] = useState(true);
    const [organizationsData, setOrganizationsData] = useState(props.organizations);
    const [lastPage, setLastPage] = useState(props.lastPage);
    const [currentPage, setCurrentPage] = useState((router.query.page as string) || '1');
    const [loading, setLoading] = useState(false);

    useEffect(() => {
        if (!firstRender) organizationsRef.current.scrollIntoView();
        setFirstRender(false);
    }, [organizationsData]);

    const handleSearch = (query: string) => {
        let queryParams = '';
        if (query !== '') queryParams = `?q=${query}`;
        window.history.replaceState(
            { ...window.history.state, url: `/organizations` + queryParams, as: `/organizations` + queryParams },
            '',
            window.location.origin + `/organizations` + queryParams,
        );

        setCurrentPage('1');
        sendRequest({ phrase: query, page: 1 });
    };

    const handlePageClick = (data) => {
        const urlSearchParams = new URLSearchParams(window.location.search);
        const params = Object.fromEntries(urlSearchParams.entries());
        const queryParamList: string[] = [];
        params.q ? queryParamList.push(`q=${params.q}`) : '';
        let queryParams = '?';
        for (const param of queryParamList) {
            queryParams += param + '&';
        }
        queryParams += `page=${data.selected + 1}`;
        window.history.replaceState(
            { ...window.history.state, url: `/organizations` + queryParams, as: `/organizations` + queryParams },
            '',
            window.location.origin + `/organizations` + queryParams,
        );
        setCurrentPage(data.selected + 1);
        sendRequest({ phrase: params.q, page: data.selected + 1 });
    };

    const sendRequest = (params: { phrase?; page? }) => {
        setLoading(true);
        const data = {
            phrase: params.phrase || '',
            page: params.page || 1,
        };
        const result = apiSearchOrganizations(false, data.phrase, data.page);
        result
            .then((response) => {
                if (response.status === 'data') {
                    setOrganizationsData(response.data.organizations);
                    setLastPage(response.data.lastpage);
                }
                setLoading(false);
            })
            .catch((error) => {
                setLoading(false);
                console.log('An error occured while getting webinars');
                toast.error('مشکلی در دریافت اطلاعات به وجود آمد!');
            });
    };

    return (
        <div className={styles.body}>
            <Head>
                <title>سازمان های آسان سمینار</title>
                <meta
                    name="description"
                    content="سازمان ها و ارگان های همکار با پلتفرم برگزاری وبینار و آموزش آنلاین آسان سمینار"
                />
                <link rel="canonical" href={props.origin + '/organizations'} />
                <meta property="og:url" content="https://asanseminar.ir" />
                <meta property="og:title" content="سازمان های آسان سمینار" />
                <meta
                    property="og:description"
                    content="سازمان ها و ارگان های همکار با پلتفرم برگزاری وبینار و آموزش آنلاین آسان سمینار"
                />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="سازمان های آسان سمینار" />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
            </Head>
            <h1 className={styles.title} ref={organizationsRef}>
                سازمان ها
            </h1>
            <Container>
                <Searchbar
                    initialValue={(router.query.q as string) || ''}
                    placeHolder="سازمان مورد نظر خود را جستجو کنید"
                    containerClasses={styles['search-container']}
                    fieldClasses={styles['search-input']}
                    iconClasses={styles['search-icon']}
                    nonEmptyClasses={styles['search-text-non-empty']}
                    customHandler={handleSearch}
                    setLoading={setLoading}
                />
            </Container>
            {loading ? (
                <div className={styles.loader}>
                    <Spinner animation="grow" variant={'primary'} />
                    <div style={{ marginRight: '1rem' }}>درحال بارگذاری...</div>
                </div>
            ) : (
                <OrganizationsCardSection data={organizationsData} />
            )}
            {!loading && lastPage > 1 ? (
                <Pagination page={currentPage} handlePageClick={handlePageClick} pageCount={lastPage} />
            ) : null}
        </div>
    );
}
