import React from 'react';
import BannerSection from 'modules/brandpage/components/banner-section';
import ServicesSection from 'modules/brandpage/components/services-section';
import BenefitsSection from 'modules/brandpage/components/benefits-section';
import UserStoriesSection from 'modules/brandpage/components/user-stories-section';
import BlogSection from 'modules/brandpage/components/blog-section';
import FAQSection from 'modules/brandpage/components/faq-section';
import apiOrganizationBlog from 'modules/blog/api/api-organization-blog';
import { IBlog } from 'models/blog';
import Head from 'next/head';
import { faqQuestions, schemas } from 'modules/brandpage/data';

export const getStaticProps = async () => {
    const blogsResult = await apiOrganizationBlog(true, 'brandpage', 1);
    const blogs = blogsResult.status === 'data' ? blogsResult.data.posts : [];
    return {
        props: {
            blogs,
        },
        revalidate: 300,
    };
};

export default function Brandpage(props: { blogs: IBlog[] }) {
    const userStorySlides = () => {
        const slides = [];
        for (let i = 0; i < 5; i++) {
            slides.push({
                src: '/images/brandpage/logo-rastak.png',
                title: 'رستاک',
                description:
                    'سلام امیدوارم حال دلتون خوب باشه ، تشکر میکنم از بچه های تیم خوب شرکت که اینقدر باحوصله به ما کمک کردند و زحمت کشیدند که برندپیج رو راه اندازی کنیم. حتما در گروه هایی که عضو هستم این رو به دوستان هم معرفی خواهم کرد.',
                user_logo: '/images/brandpage/user-logo.png',
                user_fullname: 'مهندس ساعتچی',
                user_role: 'مدیرعامل رستاک',
                date: '1400/07/05',
            });
        }
        return slides;
    };

    return (
        <>
            <Head>
                <title>برند پیج | دیجیتال مارکتینگ، کارت ویزیت دیجیتال، بازاریابی اینترنتی</title>
                <meta
                    name="description"
                    content="دیجیتال مارکتینگ، کارت ویزیت دیجیتال، بازاریابی اینترنتی، صفحه اول گوگل و جدیدترین ابزار برندسازی و کسب جایگاه ویژه برند را با برند پیج تجربه کنید."
                />
                <meta property="og:url" content="https://asanseminar.ir/brandpage" />
                <meta property="og:title" content="برند پیج | ابزار بازاریابی اینترنتی" />
                <meta
                    property="og:description"
                    content="دیجیتال مارکتینگ، کارت ویزیت دیجیتال، بازاریابی اینترنتی، صفحه اول گوگل و جدیدترین ابزار برندسازی و کسب جایگاه ویژه برند را با برند پیج تجربه کنید."
                />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/logos/brandpage-logo.jpg" />
                <meta property="og:image:type" content="image/jpeg" />
                <meta property="og:image:alt" content="لوگو برندپیج" />
                <meta property="og:site_name" content="برند پیج | ابزار بازاریابی اینترنتی" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="برند پیج | ابزار بازاریابی اینترنتی" />
                <meta name="twitter:image:alt" content="لوگو برندپیج" />
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={{
                        __html: schemas.faqSchema,
                    }}
                />
            </Head>
            <BannerSection />
            <ServicesSection />
            <BenefitsSection />
            {/*<UserStoriesSection slides={userStorySlides()} />*/}
            <BlogSection slides={props.blogs} />
            <FAQSection data={faqQuestions} />
        </>
    );
}
