import React, { useEffect, useState } from 'react';
import PlansSection from 'modules/brandpage/components/plans-section';
import BuildStepsSection from 'modules/brandpage/components/build-steps-section';
import RequestSection from 'modules/brandpage/components/request-section';
import ContactsSection from 'modules/brandpage/components/contacts-section';
import Head from 'next/head';
import { BrandpageContactInfoType } from 'modules/brandpage/types';
import ApiPublicMarketer from 'modules/brandpage/api/api-public-marketer';
import { useRouter } from 'next/router';

export default function Plans() {
    const router = useRouter();
    const [contactInfo, setContactInfo] = useState<BrandpageContactInfoType>({
        mobiles: ['09022959193'],
    });

    const extractReferalFromQueryString = () => {
        let queryindex = router.asPath.indexOf('?');
        if (queryindex !== -1) {
            let query = router.asPath.substr(queryindex + 1);
            while (query) {
                queryindex = query.indexOf('=');
                if (queryindex === -1) {
                    return null;
                }
                const paramName = query.substring(0, queryindex);
                if (paramName === 'ref') {
                    query = query.substring(queryindex + 1);
                    return parseInt(query);
                } else {
                    queryindex = query.indexOf('&');
                    if (queryindex === -1) {
                        return null;
                    }
                    query = query.substring(queryindex + 1);
                }
            }
        }

        return null;
    };

    useEffect(() => {
        const referralId = extractReferalFromQueryString();
        if (referralId) {
            const response = ApiPublicMarketer(referralId);
            response
                .then((result) => {
                    if (result.status === 'data') {
                        setContactInfo({
                            mobiles: [result.data.marketer.mobile],
                            name: result.data.marketer.fullname,
                            email: result.data.marketer.email,
                            whatsapp: result.data.marketer.whatsapp,
                            instagram: result.data.marketer.instagram,
                        });
                    }
                })
                .catch((error) => null);
        }
    }, []);

    return (
        <>
            <Head>
                <title>تعرفه های برند پیج</title>
                <meta name="description" content="پلن ها و قیمت های خرید برند پیج پلاس و پریمیوم" />
                <meta property="og:url" content="https://asanseminar.ir/brandpage/plans" />
                <meta property="og:title" content="تعرفه های برند پیج" />
                <meta property="og:description" content="پلن ها و قیمت های خرید برند پیج پلاس و پریمیوم" />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/logos/brandpage-logo.jpg" />
                <meta property="og:image:type" content="image/jpeg" />
                <meta property="og:image:alt" content="لوگو برندپیج" />
                <meta property="og:site_name" content="تعرفه های برند پیج" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="تعرفه های برند پیج" />
                <meta name="twitter:image:alt" content="لوگو برندپیج" />
            </Head>
            <div style={{ paddingTop: '4rem' }}>
                <PlansSection />
                <BuildStepsSection />
                {/*<RequestSection />*/}
                <ContactsSection data={contactInfo} />
            </div>
        </>
    );
}
