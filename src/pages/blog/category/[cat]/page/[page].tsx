import React from 'react';
import styles from 'styles/blog-search.module.scss';
import { IBlog } from 'models/blog';
import { Col, Container, Row } from 'react-bootstrap';
import ArticleCard from 'modules/blog/components/article-card';
import classNames from 'classnames';
import Pagination from 'modules/shared/components/pagination';
import router, { useRouter } from 'next/router';
import Head from 'next/head';
import apiBlogSearch from 'modules/blog/api/api-blog-search';

export const getStaticPaths = async () => {
    return {
        paths: [],
        fallback: 'blocking',
    };
};

export const getStaticProps = async ({ params }) => {
    const blogsResult = await apiBlogSearch('', params.page, undefined, params.cat.toString().replace(/-/g, ' '));
    const blogs = blogsResult.status === 'data' ? blogsResult.data.posts : [];
    const lastPage = blogsResult.status === 'data' ? blogsResult.data.lastpage : 1;
    const origin = process.env.NODE_ENV === 'production' ? process.env.NEXT_PUBLIC_APP_URL : 'http://localhost:3000';

    return {
        props: { blogs, lastPage, category: params.cat.toString().replace(/-/g, ' '), page: params.page, origin },
        revalidate: 60,
    };
};

export default function BlogCategory(props: {
    blogs: IBlog[];
    lastPage: number;
    category: string;
    page: number;
    origin;
}) {
    const renderArticles = () => {
        return props.blogs.length ? (
            props.blogs.map((article) => {
                return (
                    <Col sm={6} lg={4} key={article.title}>
                        <ArticleCard data={article} description={true} />
                    </Col>
                );
            })
        ) : (
            <div className={styles['not-found']}>
                <img src="/images/notes.png" className={styles['not-found-image']} alt="مقاله" />
                <div>مقاله ای برای جستجوی شما یافت نشد</div>
            </div>
        );
    };

    return (
        <>
            <Head>
                <title>{`وبلاگ آسان سمینار - دسته ${props.category}`}</title>
                <meta name="description" content={`وبلاگ آسان سمینار - مقالات با دسته ${props.category}`} />
                <meta property="og:url" content={props.origin + useRouter().asPath} />
                <meta property="og:title" content={`وبلاگ آسان سمینار - دسته ${props.category}`} />
                <meta property="og:description" content={`وبلاگ آسان سمینار - مقالات با دسته ${props.category}`} />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار - پلتفرم آموزش آنلاین" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="وبلاگ آسان سمینار" />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
            </Head>
            <Container className={styles.main}>
                <Row className={classNames('gx-sm-4 gx-md-5 gy-4', styles.articles)}>{renderArticles()}</Row>
                {props.lastPage > 1 ? (
                    <Pagination
                        pageCount={props.lastPage}
                        page={String(props.page)}
                        handlePageClick={(data) =>
                            router
                                .push(`/blog/category/${props.category}/page/${data.selected + 1}`)
                                .then(() => window.scrollTo(0, 0))
                        }
                    />
                ) : null}
            </Container>
        </>
    );
}
