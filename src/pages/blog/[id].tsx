import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import styles from 'styles/blog.module.scss';
import { Breadcrumb, Col, Container, Row } from 'react-bootstrap';
import ArticleHeader from 'modules/blog/components/article-header';
import Link from 'next/link';
import DOMPurify from 'isomorphic-dompurify';
import CommunicateSection from 'modules/blog/components/communicate-section';
import TagsSection from 'modules/blog/components/tags-section';
import SidebarBox from 'modules/blog/components/sidebar-box';
import classNames from 'classnames';
import apiLatestBlogs from 'modules/blog/api/api-latest-blogs';
import apiBlog from 'modules/blog/api/api-blog';
import { IBlog } from 'models/blog';
import Custom404 from 'pages/404';
import Head from 'next/head';
import CommentSection from 'modules/blog/components/comment-section';
import AsanImage from 'modules/shared/components/asan-image';
import RelatedPosts from 'modules/blog/components/related-posts';
import apiMostLikedBlogs from 'modules/blog/api/api-most-liked-blogs';
import apiLatestNews from 'modules/blog/api/api-latest-news';
import { IBlogNews } from 'models/blog-news';
import parseCookies from 'utils/parse-cookies';
import { cookieNames } from 'utils/storage-vars';
import { AxiosRequestConfig } from 'axios';
import { IBlogCompact } from 'models/blog-compact';
import { IBlogSimple } from 'models/blog-simple';

export async function getServerSideProps({ req, params }) {
    const cookies = parseCookies(req);
    const config: AxiosRequestConfig = {};

    if (cookies && cookies[cookieNames.apiToken]) {
        config.headers = {
            Authorization: `Bearer ${cookies[cookieNames.apiToken]}`,
        };
    }

    const latestBlogsResponse = await apiLatestBlogs();
    const mostLikedBlogsResponse = await apiMostLikedBlogs();
    const blogResult = await apiBlog(params.id, config);
    const latestNewsResponse = await apiLatestNews();

    const origin = process.env.NODE_ENV === 'production' ? process.env.NEXT_PUBLIC_APP_URL : 'http://localhost:3000';

    const latestBlogs = latestBlogsResponse.status === 'data' ? latestBlogsResponse.data : [];
    const mostLikedBlogs = mostLikedBlogsResponse.status === 'data' ? mostLikedBlogsResponse.data : [];
    const blog = blogResult.status === 'data' ? blogResult.data.post : null;
    const relatedPosts = blogResult.status === 'data' ? blogResult.data.related_posts : [];
    const latestNews = latestNewsResponse.status === 'data' ? latestNewsResponse.data.blog_news : [];
    return {
        props: {
            blog,
            relatedPosts,
            latestBlogs,
            mostLikedBlogs,
            latestNews,
            origin,
        },
    };
}

export default function Article(props: {
    blog: IBlog | null;
    relatedPosts: IBlogSimple[];
    latestBlogs: IBlogCompact[];
    mostLikedBlogs: IBlogSimple[];
    latestNews: IBlogNews[];
    origin: string;
}) {
    const route = useRouter();
    const { id } = route.query as { id: string };
    const [isLiked, setIsLiked] = useState(props.blog ? props.blog.is_liked : false);
    const [likesCount, setLikesCount] = useState(props.blog ? props.blog.likes_count || 0 : 0);
    const [pageRandomKey, setPageRandomKey] = useState((props.blog && props.blog.id) || 0);

    const changeIsLiked = (value: boolean) => {
        setIsLiked(value);
        setLikesCount((prevVal) => (value ? prevVal + 1 : prevVal - 1));
    };

    useEffect(() => {
        if (props.blog) {
            setIsLiked(props.blog.is_liked);
            setLikesCount(props.blog.likes_count || 0);
        }

        setPageRandomKey((props.blog && props.blog.id) || 0);
    }, [props.blog]);

    useEffect(() => {
        if (props.blog) {
            const blogTitle = props.blog.title.trim().replace(/\s+/g, '-');
            let fixedUrl = route.asPath;
            const indexOfRandomSlug = fixedUrl.indexOf(id) + id.length;
            if (indexOfRandomSlug !== -1) {
                fixedUrl = fixedUrl.slice(0, indexOfRandomSlug).concat(`/${blogTitle}`);
                setTimeout(() => {
                    window.history.replaceState(window.history.state, '', window.location.origin + fixedUrl);
                }, 200);
            }
        }
    }, []);

    if (props.blog) {
        return (
            <>
                <Head>
                    <title>{props.blog.title} - آسان سمینار</title>
                    <meta name="description" content={props.blog.description || ''} />
                    <meta
                        name="keywords"
                        content={(props.blog.tags && props.blog.tags.map((tag) => tag.title).join()) || ''}
                    />
                    <meta property="og:url" content={props.origin + route.asPath} />
                    <meta property="og:title" content={props.blog.title} />
                    <meta property="og:description" content={props.blog.description || ''} />
                    <meta property="og:type" content="aritcle" />
                    <meta property="article:author" content="تیم محتوا آسان سمینار" />
                    <meta property="article:published_time" content={props.blog.created_at} />
                    {props.blog.tags &&
                        props.blog.tags.map((tag) => {
                            return <meta key={tag.id} property="article:tag" content={tag.title} />;
                        })}
                    <meta
                        property="og:image"
                        content={
                            (props.blog.media && props.blog.media.length && props.blog.media[0].url) ||
                            '/images/avatar-placeholder.png'
                        }
                    />
                    <meta property="og:image:type" content="image/jpeg" />
                    <meta property="og:image:alt" content={props.blog.title} />
                    <meta property="og:site_name" content="آسان سمینار - پلتفرم آموزش آنلاین" />
                    <meta property="og:locale" content="fa_IR" />
                    <meta name="twitter:card" content="summary_large_image" />
                    <meta name="twitter:title" content={props.blog.title} />
                    <meta name="twitter:image:alt" content={props.blog.title} />
                    <script
                        type="application/ld+json"
                        dangerouslySetInnerHTML={{ __html: JSON.stringify(props.blog.schema || {}) }}
                    />
                    <script
                        type="application/ld+json"
                        dangerouslySetInnerHTML={{
                            __html: JSON.stringify((props.blog.category && props.blog.category.schema) || {}),
                        }}
                    />
                </Head>
                <Container className={styles.container}>
                    <Row>
                        <Col xl={8}>
                            <div className={styles.blog}>
                                <Breadcrumb className={styles.breadcrumb}>
                                    <Link href="/blog" passHref>
                                        <Breadcrumb.Item href="/blog" className={styles['breadcrumb-item']}>
                                            وبلاگ
                                        </Breadcrumb.Item>
                                    </Link>
                                    {props.blog.category && props.blog.category.parent_category ? (
                                        <Link
                                            href={`/blog/category/${props.blog.category.parent_category.title.replace(
                                                /\s+/g,
                                                '-',
                                            )}`}
                                            passHref
                                        >
                                            <Breadcrumb.Item
                                                href={`/blog/category/${props.blog.category.parent_category.title.replace(
                                                    /\s+/g,
                                                    '-',
                                                )}`}
                                                className={styles['breadcrumb-item']}
                                            >
                                                {props.blog.category.parent_category.title}
                                            </Breadcrumb.Item>
                                        </Link>
                                    ) : null}
                                    <Link
                                        href={
                                            (props.blog.category &&
                                                `/blog/category/${props.blog.category.title.replace(/\s+/g, '-')}`) ||
                                            '/blog'
                                        }
                                        passHref
                                    >
                                        <Breadcrumb.Item className={styles['breadcrumb-item']}>
                                            {(props.blog.category && props.blog.category.title) || ''}
                                        </Breadcrumb.Item>
                                    </Link>
                                    <Breadcrumb.Item active className={styles['breadcrumb-active']}>
                                        {props.blog.title}
                                    </Breadcrumb.Item>
                                </Breadcrumb>
                                <ArticleHeader
                                    article={props.blog}
                                    isLiked={isLiked}
                                    changeIsLiked={changeIsLiked}
                                    likesCountState={likesCount}
                                    key={pageRandomKey + 1}
                                />
                                <AsanImage
                                    src={
                                        props.blog.media && props.blog.media.length
                                            ? props.blog.media[0].url
                                            : '/images/avatar-placeholder.png'
                                    }
                                    alt={props.blog.title}
                                    className={styles.banner}
                                    key={pageRandomKey + 2}
                                />
                                <div
                                    className={styles['blog-body']}
                                    dangerouslySetInnerHTML={{
                                        __html: DOMPurify.sanitize(props.blog.body || '', {
                                            ADD_TAGS: ['iframe'],
                                        }),
                                    }}
                                />
                                <CommunicateSection
                                    article={props.blog}
                                    isLiked={isLiked}
                                    changeIsLiked={changeIsLiked}
                                    likesCountState={likesCount}
                                    basePath={props.origin}
                                    key={pageRandomKey + 3}
                                />
                                <TagsSection tags={props.blog.tags || []} key={pageRandomKey + 4} />
                                <RelatedPosts posts={props.relatedPosts} key={pageRandomKey + 5} />
                                <CommentSection article={props.blog} key={pageRandomKey + 6} />
                                {/*<CategoriesSection />*/}
                            </div>
                        </Col>
                        <Col xl={4}>
                            <Row className={classNames(styles.sidebar, 'gx-md-3')}>
                                <Col sm={6} md={4} xl={12}>
                                    <SidebarBox
                                        articles={props.mostLikedBlogs}
                                        title="محبوب ترین ها"
                                        link="/blog/search"
                                    />
                                </Col>
                                <Col sm={6} md={4} xl={12}>
                                    <SidebarBox
                                        articles={props.latestBlogs}
                                        title="پربازدید ترین ها"
                                        link="/blog/search"
                                    />
                                </Col>
                                <Col sm={6} md={4} xl={12}>
                                    <SidebarBox articles={props.latestNews} title="اخبار" isNews={true} />
                                </Col>
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </>
        );
    } else return <Custom404 />;
}
