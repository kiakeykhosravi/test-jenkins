import React from 'react';
import styles from 'styles/blog.module.scss';
import { Container } from 'react-bootstrap';
import ChoosenArticles from 'modules/blog/components/choosen-articles';
import PopularArticle from 'modules/blog/components/popular-article';
import MainSection from 'modules/blog/components/main-section';
import apiLatestBlogs from 'modules/blog/api/api-latest-blogs';
import { IBlog } from 'models/blog';
import apiBlogs from 'modules/blog/api/api-blogs';
import Head from 'next/head';
import { useRouter } from 'next/router';
import apiBlogMainSchema from 'modules/blog/api/api-blog-main-schema';
import apiMostLikedBlogs from 'modules/blog/api/api-most-liked-blogs';
import apiLatestNews from 'modules/blog/api/api-latest-news';
import { IBlogNews } from 'models/blog-news';
import { IBlogCompact } from 'models/blog-compact';
import { IBlogSimple } from 'models/blog-simple';

export async function getStaticPaths() {
    const blogsResult = await apiBlogs(1);
    const pages = blogsResult.status === 'data' ? Math.min(blogsResult.data.lastpage, 10) : 1;

    const paths = [];
    for (let i = 1; i <= pages; i++) {
        paths.push({ params: { page: i.toString() } });
    }
    return {
        paths,
        fallback: 'blocking',
    };
}

export const getStaticProps = async ({ params }) => {
    const latestBlogsResponse = await apiLatestBlogs();
    const mostLikedBlogsResponse = await apiMostLikedBlogs();
    const blogsResult = await apiBlogs(params.page);
    const schema = (await apiBlogMainSchema()) || {};
    const latestNewsResponse = await apiLatestNews();

    const origin = process.env.NODE_ENV === 'production' ? process.env.NEXT_PUBLIC_APP_URL : 'http://localhost:3000';

    const latestBlogs = latestBlogsResponse.status === 'data' ? latestBlogsResponse.data : [];
    const mostLikedBlogs = mostLikedBlogsResponse.status === 'data' ? mostLikedBlogsResponse.data : [];
    const blogs = blogsResult.status === 'data' ? blogsResult.data.posts : [];
    const blogsLastPage = blogsResult.status === 'data' ? blogsResult.data.lastpage : 1;
    const latestNews = latestNewsResponse.status === 'data' ? latestNewsResponse.data.blog_news : [];
    return {
        props: {
            blogs,
            blogsLastPage,
            blogsCurrentPage: params.page,
            latestBlogs,
            mostLikedBlogs,
            latestNews,
            origin,
            schema,
        },
        revalidate: 300,
    };
};

export default function Blog(props: {
    blogs: IBlog[];
    blogsLastPage: number;
    blogsCurrentPage: number;
    latestBlogs: IBlogCompact[];
    mostLikedBlogs: IBlogSimple[];
    latestNews: IBlogNews[];
    origin: string;
    schema: Record<string, any>;
}) {
    const router = useRouter();
    return (
        <div className={styles['main-blog']}>
            <Head>
                <title>وبلاگ آسان سمینار</title>
                <meta name="description" content="مقالات آسان سمینار" />
                <meta property="og:url" content={props.origin + router.asPath} />
                <meta property="og:title" content="وبلاگ آسان سمینار" />
                <meta property="og:description" content="مقالات آسان سمینار" />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار - پلتفرم آموزش آنلاین" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="وبلاگ آسان سمینار" />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
                <script type="application/ld+json" dangerouslySetInnerHTML={{ __html: JSON.stringify(props.schema) }} />
            </Head>
            <Container className={styles['main-container']}>
                <ChoosenArticles articles={props.latestBlogs} />
                <PopularArticle article={props.latestBlogs && props.latestBlogs.length && props.latestBlogs[0]} />
                <MainSection
                    articles={props.blogs}
                    lastPage={props.blogsLastPage}
                    currentPage={props.blogsCurrentPage}
                    mostLikedBlogs={props.mostLikedBlogs}
                    mostViewedBlogs={props.latestBlogs}
                    latestNews={props.latestNews}
                />
            </Container>
        </div>
    );
}
