import React, { useEffect, useRef } from 'react';
import styles from 'styles/blog-search.module.scss';
import apiBlogSearch from 'modules/blog/api/api-blog-search';
import { IBlog } from 'models/blog';
import { Col, Container, Row } from 'react-bootstrap';
import ArticleCard from 'modules/blog/components/article-card';
import classNames from 'classnames';
import Pagination from 'modules/shared/components/pagination';
import router from 'next/router';
import Head from 'next/head';
import { AxiosRequestConfig } from 'axios';
import { cookieNames } from 'utils/storage-vars';
import parseCookies from 'utils/parse-cookies';

export async function getServerSideProps({ req, query }) {
    const config: AxiosRequestConfig = {};
    const cookies = parseCookies(req);

    if (cookies && cookies[cookieNames.apiToken]) {
        config.headers = {
            Authorization: `Bearer ${cookies[cookieNames.apiToken]}`,
        };
    }

    const q = query.q ? (query.q as string) : '';
    const page = (): number => {
        if (query.page) {
            return !isNaN(Number(query.page)) ? Number(query.page) : 1;
        } else return 1;
    };
    const blogsResult = await apiBlogSearch(q, page(), undefined, undefined, config);
    const blogs = blogsResult.status === 'data' ? blogsResult.data.posts : [];
    const lastPage = blogsResult.status === 'data' ? blogsResult.data.lastpage : 1;

    let fullUrl = '';
    if (req) {
        fullUrl =
            (process.env.NODE_ENV === 'production' ? process.env.NEXT_PUBLIC_APP_URL : 'http://localhost:3000') +
            req.url;
    }

    return {
        props: { blogs, lastPage, q, page: page(), fullUrl },
    };
}

export default function BlogSearch(props: { blogs: IBlog[]; lastPage: number; q: string; page: number; fullUrl }) {
    const renderArticles = () => {
        return props.blogs.length ? (
            props.blogs.map((article) => {
                return (
                    <Col sm={6} lg={4} key={article.title}>
                        <ArticleCard data={article} description={true} />
                    </Col>
                );
            })
        ) : (
            <div className={styles['not-found']}>
                <img src="/images/notes.png" className={styles['not-found-image']} />
                <div>مقاله ای برای جستجوی شما یافت نشد</div>
            </div>
        );
    };

    return (
        <>
            <Head>
                <title>جستجو در وبلاگ آسان سمینار</title>
                <meta name="description" content="بلاگ مورد نظر خود را جستجو کنید" />
                <meta property="og:url" content={props.fullUrl} />
                <meta property="og:title" content="جستجو در وبلاگ آسان سمینار" />
                <meta property="og:description" content="بلاگ مورد نظر خود را جستجو کنید" />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار - پلتفرم آموزش آنلاین" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="وبلاگ آسان سمینار" />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
            </Head>
            <Container className={styles.main}>
                <Row className={classNames('gx-sm-4 gx-md-5 gy-4', styles.articles)}>{renderArticles()}</Row>
                {props.lastPage > 1 ? (
                    <Pagination
                        pageCount={props.lastPage}
                        page={String(props.page)}
                        handlePageClick={(data) =>
                            router
                                .push(`/blog/search?q=${props.q}&page=${data.selected + 1}`)
                                .then(() => window.scrollTo(0, 0))
                        }
                    />
                ) : null}
            </Container>
        </>
    );
}
