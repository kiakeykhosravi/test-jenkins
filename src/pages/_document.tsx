import React from 'react';
import Document, { Head, Html, Main, NextScript } from 'next/document';

class AsanseminarDocument extends Document {
    render() {
        return (
            <Html lang="fa" dir="rtl">
                <Head />
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}
export default AsanseminarDocument;
