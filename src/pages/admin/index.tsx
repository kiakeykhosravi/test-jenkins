import React from 'react';
import redirectOldAdmin from 'utils/redirect-old-admin';
import AdminEnterError from 'modules/shared/components/admin-enter-error';

export async function getServerSideProps({ req }) {
    return await redirectOldAdmin(req);
}

export default function Admin() {
    return <AdminEnterError />;
}
