import React, { useEffect, useRef, useState } from 'react';
import styles from 'styles/login.module.scss';
import Link from 'next/link';
import { Button, Form } from 'react-bootstrap';
import { ExclamationTriangleFill, Phone } from 'react-bootstrap-icons';
import { useForm } from 'react-hook-form';
import apiLoginNopass from 'modules/auth/api/api-login-nopass';
import { useRouter } from 'next/router';
import { cookieNames, sessionStorageVars } from 'utils/storage-vars';
import Head from 'next/head';
import toEnglishNum from 'utils/to-english-num';
import { toast } from 'react-toastify';
import parseCookies from 'utils/parse-cookies';
import apiUser from 'modules/profile/api/api-user';
import { AxiosRequestConfig } from 'axios';
import schemas from 'utils/schemas';

type TInputs = {
    mobile: string;
};

export async function getServerSideProps({ req }) {
    const cookies = parseCookies(req);
    const config: AxiosRequestConfig = {};
    if (cookies && cookies[cookieNames.apiToken]) {
        config.headers = {
            Authorization: `Bearer ${cookies[cookieNames.apiToken]}`,
        };
        const userResponse = await apiUser(true, config);
        if (userResponse) {
            return {
                redirect: {
                    destination: '/',
                    permanent: false,
                },
            };
        }
    }
    return {
        props: {},
    };
}

export default function LoginNoPass() {
    const inputRef = useRef(null);
    const route = useRouter();
    const [submitting, setSubmitting] = useState<boolean>(false);
    const [errorMessage, setErrorMessage] = useState({
        status: false,
        text: '',
    });
    const { register, handleSubmit, errors } = useForm<TInputs>();

    useEffect(function () {
        inputRef.current.focus();
    }, []);

    async function onSubmit(data: { mobile: string }) {
        setSubmitting(true);
        setErrorMessage({
            status: false,
            text: '',
        });

        data.mobile = toEnglishNum(data.mobile);
        const response = await apiLoginNopass(data.mobile);
        if (response.status === 'error') {
            setSubmitting(false);
            setErrorMessage({
                status: true,
                text: response.message,
            });
        } else if (response.status === 'data') {
            sessionStorage[sessionStorageVars.user_mobile] = data.mobile;
            sessionStorage[sessionStorageVars.ask_fullname] = response.data.ask_fullname ? 'true' : 'false';
            sessionStorage[sessionStorageVars.secret] = response.data.secret;
            if (route.query.return)
                route.push(`/auth/verify?return=${route.query.return as string}`).catch((e) => {
                    toast.warning(e);
                    setSubmitting(false);
                });
            else
                route.push('/auth/verify').catch((e) => {
                    toast.warning(e);
                    setSubmitting(false);
                });
        }
    }

    return (
        <>
            <Head>
                <meta name="robots" content="index, follow" />
                <title>ورود به آسان سمینار</title>
                <meta
                    name="description"
                    content="ورود و ثبت نام در آسان سمینار | هنگام برگزاری وبینار و کلاس آنلاین کنار شما هستیم"
                />
                <link rel="canonical" href="https://asanseminar.ir/auth/login-nopass" />
                <meta property="og:url" content="https://asanseminar.ir" />
                <meta property="og:title" content="ورود به آسان سمینار" />
                <meta
                    property="og:description"
                    content="ورود و ثبت نام در آسان سمینار | هنگام برگزاری وبینار و کلاس آنلاین کنار شما هستیم"
                />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="ورود به آسان سمینار" />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
                {/*<script*/}
                {/*    type="application/ld+json"*/}
                {/*    dangerouslySetInnerHTML={{*/}
                {/*        __html: schemas.authSchema,*/}
                {/*    }}*/}
                {/*/>*/}
            </Head>
            <Form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
                <h1 className={styles.title}>ورود به آسان سمینار</h1>
                <Form.Group controlId="formMobile">
                    <div className={styles['form-control-container']}>
                        <Form.Control
                            type="text"
                            placeholder="شماره همراه"
                            className={styles['form-control']}
                            name="mobile"
                            ref={(e) => {
                                register(e, {
                                    required: 'شماره موبایل را وارد کنید!',
                                    pattern: {
                                        value: /^(((?:98|\+98|0098|0)?9[0-9]{9})|((?:۹۸|\+۹۸|۰۰۹۸|۰)?۹[۰۹۸۷۶۵۴۳۲۱]{9}))$/,
                                        message: 'فرمت شماره موبایل اشتباه است!',
                                    },
                                });
                                inputRef.current = e;
                            }}
                        />
                        <Phone className={styles['form-control-icon']} />
                    </div>
                    {errors.mobile && <div className={styles.error}>{errors.mobile.message}</div>}
                </Form.Group>
                <div className={styles['form-control-container']}>
                    <Button variant="success" type="submit" className={styles['login-btn']} disabled={submitting}>
                        {submitting ? 'در حال ارسال...' : 'دریافت کد تایید'}
                    </Button>
                </div>
                <Link href={route.query.return ? `/auth/login?return=${route.query.return}` : `/auth/login`}>
                    <a title={'ورود با رمز عبور'} className={styles.link}>
                        ورود با رمز عبور
                    </a>
                </Link>
            </Form>
            {errorMessage.status ? (
                <div className={styles['error-message']}>
                    <ExclamationTriangleFill />
                    <span>{errorMessage.text}</span>
                </div>
            ) : null}
        </>
    );
}
