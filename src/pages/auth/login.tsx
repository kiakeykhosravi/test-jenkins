import React, { useContext, useEffect, useRef, useState } from 'react';
import styles from 'styles/login.module.scss';
import { Button, Form } from 'react-bootstrap';
import { ExclamationTriangleFill, Key, Person } from 'react-bootstrap-icons';
import apiLogin from 'modules/auth/api/api-login';
import { useForm } from 'react-hook-form';
import classNames from 'classnames';
import { useRouter } from 'next/router';
import { ILoginNoPassResponse } from 'modules/auth/responses';
import { cookieNames, sessionStorageVars } from 'utils/storage-vars';
import { toast } from 'react-toastify';
import Link from 'next/link';
import Head from 'next/head';
import { AuthContext } from 'contexts/auth-context';
import apiUser from 'modules/profile/api/api-user';
import toEnglishNum from 'utils/to-english-num';
import parseCookies from 'utils/parse-cookies';
import { AxiosRequestConfig } from 'axios';
import schemas from 'utils/schemas';

type TInputs = {
    identifier: string;
    password: string;
};

export async function getServerSideProps({ req }) {
    const cookies = parseCookies(req);
    const config: AxiosRequestConfig = {};
    if (cookies && cookies[cookieNames.apiToken]) {
        config.headers = {
            Authorization: `Bearer ${cookies[cookieNames.apiToken]}`,
        };
        const userResponse = await apiUser(true, config);
        if (userResponse) {
            return {
                redirect: {
                    destination: '/',
                    permanent: false,
                },
            };
        }
    }
    return {
        props: {},
    };
}

export default function Login() {
    const inputRef = useRef(null);
    const route = useRouter();
    const [submitting, setSubmitting] = useState<boolean>(false);
    const [errorMessage, setErrorMessage] = useState({
        status: false,
        text: '',
    });
    const { register, handleSubmit, errors } = useForm<TInputs>();

    const authContext = useContext(AuthContext);

    useEffect(function () {
        inputRef.current.focus();
    }, []);

    async function onSubmit(data: { identifier: string; password: string }) {
        setSubmitting(true);
        setErrorMessage({
            status: false,
            text: '',
        });

        data.identifier = toEnglishNum(data.identifier);
        const response = await apiLogin(data.identifier, data.password);
        if (response.status === 'error') {
            setSubmitting(false);
            setErrorMessage({
                status: true,
                text: response.message,
            });
        } else if (response.status === 'data') {
            if (response.data.type === 'login') {
                toast.success(response.message);
                let user;
                for (let i = 0; i < 3; i++) {
                    user = await apiUser();
                    if (user) {
                        break;
                    }
                }
                if (user) {
                    toast.success('در حال انتقال به صفحه اصلی ...');
                    sessionStorage.removeItem(sessionStorageVars.user_mobile);
                    sessionStorage.removeItem(sessionStorageVars.ask_fullname);
                    sessionStorage.removeItem(sessionStorageVars.secret);
                    authContext.changeIsLoggedIn(user);

                    setTimeout(() => {
                        if (route.query.return)
                            route.push(route.query.return as string).catch((e) => {
                                toast.warning(e);
                                setSubmitting(false);
                            });
                        else
                            route.push('/').catch((e) => {
                                toast.warning(e);
                                setSubmitting(false);
                            });
                    }, 100);
                } else {
                    toast.error('خطایی در دریافت اطلاعات کاربری شما پس از ورود رخ داد. لطفا دوباره تلاش کنید', {
                        autoClose: 7500,
                    });
                }
            } else {
                const secret = (response.data as ILoginNoPassResponse).secret;
                const askFullName = (response.data as ILoginNoPassResponse).ask_fullname;
                sessionStorage[sessionStorageVars.user_mobile] = data.identifier;
                sessionStorage[sessionStorageVars.ask_fullname] = askFullName ? 'true' : 'false';
                sessionStorage[sessionStorageVars.secret] = secret;
                route.push('/auth/verify').catch((e) => {
                    toast.warning(e);
                    setSubmitting(false);
                });
            }
        }
    }
    return (
        <>
            <Head>
                <meta name="robots" content="index, follow" />
                <title>ورود به آسان سمینار</title>
                <meta
                    name="description"
                    content="ورود و ثبت نام در آسان سمینار | هنگام برگزاری وبینار و کلاس آنلاین کنار شما هستیم"
                />
                <link rel="canonical" href="https://asanseminar.ir/auth/login-nopass" />
                <meta property="og:url" content="https://asanseminar.ir" />
                <meta property="og:title" content="ورود به آسان سمینار" />
                <meta
                    property="og:description"
                    content="ورود و ثبت نام در آسان سمینار | هنگام برگزاری وبینار و کلاس آنلاین کنار شما هستیم"
                />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="ورود به آسان سمینار" />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
                {/*<script*/}
                {/*    type="application/ld+json"*/}
                {/*    dangerouslySetInnerHTML={{*/}
                {/*        __html: schemas.authSchema,*/}
                {/*    }}*/}
                {/*/>*/}
            </Head>
            <Form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
                <h1 className={styles.title}>ورود به آسان سمینار</h1>
                <Form.Group controlId="formIdentifier">
                    <div className={styles['form-control-container']}>
                        <Form.Control
                            type="text"
                            placeholder="شماره همراه یا ایمیل"
                            className={classNames(
                                styles['form-control'],
                                errors.identifier ? styles['border-red'] : '',
                            )}
                            name={'identifier'}
                            ref={(e) => {
                                register(e, {
                                    required: 'شماره موبایل یا ایمیل را وارد کنید!',
                                    pattern: {
                                        value: /^(((?:98|\+98|0098|0)?9[0-9]{9})|((?:۹۸|\+۹۸|۰۰۹۸|۰)?۹[۰۹۸۷۶۵۴۳۲۱]{9})|(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+))$/,
                                        message: 'فرمت شماره موبایل یا ایمیل اشتباه است!',
                                    },
                                });
                                inputRef.current = e;
                            }}
                            onFocus={(e) => (e.target.placeholder = '')}
                            onBlur={(e) => (e.target.placeholder = 'شماره همراه یا ایمیل')}
                        />
                        <Person className={styles['form-control-icon']} />
                    </div>
                    {errors.identifier && <div className={styles.error}>{errors.identifier.message}</div>}
                </Form.Group>
                <Form.Group controlId="formPassword">
                    <div className={styles['form-control-container']}>
                        <Form.Control
                            type="password"
                            placeholder="رمز عبور"
                            className={classNames(styles['form-control'], errors.password ? styles['border-red'] : '')}
                            name={'password'}
                            ref={register({
                                required: 'رمز عبور را وارد کنید!',
                                minLength: { value: 8, message: 'رمز عبور باید حداقل هشت کاراکتر باشد!' },
                            })}
                        />
                        <Key className={styles['form-control-icon']} />
                    </div>
                    {errors.password && <div className={styles.error}>{errors.password.message}</div>}
                </Form.Group>
                <div className={styles['form-control-container']}>
                    <Button variant="success" type="submit" className={styles['login-btn']} disabled={submitting}>
                        {submitting ? 'در حال ارسال...' : 'ورود'}
                    </Button>
                </div>
                <Link
                    href={route.query.return ? `/auth/login-nopass?return=${route.query.return}` : `/auth/login-nopass`}
                >
                    <a title={'ورود با کد تایید'} className={styles.link}>
                        ورود با کد تایید
                    </a>
                </Link>
            </Form>
            <div className={styles['error-message']}>
                {errorMessage.status ? (
                    <>
                        <ExclamationTriangleFill />
                        <span>{errorMessage.text}</span>
                    </>
                ) : null}
            </div>
        </>
    );
}
