import React, { useContext, useEffect, useState } from 'react';
import apiLoginWithToken from 'modules/auth/api/api-login-with-token';
import Custom404 from 'pages/404';
import apiUser from 'modules/profile/api/api-user';
import { toast } from 'react-toastify';
import router, { useRouter } from 'next/router';
import { AuthContext } from 'contexts/auth-context';
import Loader from '../../../modules/layout/components/loader';

export default function Token() {
    const authContext = useContext(AuthContext);
    const route = useRouter();
    const [error, setError] = useState(false);

    useEffect(() => {
        if (route.query.token !== undefined) checkToken();
    }, [route.query]);

    const checkToken = async () => {
        const loginResult = await apiLoginWithToken(route.query.token as string);
        if (loginResult.status === 'data') {
            checkAuth(loginResult.data.target);
        } else {
            setError(true);
        }
    };

    const checkAuth = async (target) => {
        let user;
        for (let i = 0; i < 3; i++) {
            user = await apiUser();
            if (user) {
                break;
            }
        }
        if (user) {
            toast.success('در حال انتقال به صفحه اصلی ...', { autoClose: 5000 });
            authContext.changeIsLoggedIn(user);
            if (target) route.push(target);
            else route.push('/');
        } else {
            toast.error('خطایی در دریافت اطلاعات کاربری شما پس از ورود رخ داد. لطفا دوباره تلاش کنید', {
                autoClose: 7500,
            });
            setError(true);
        }
    };

    if (error) return <Custom404 />;
    return <Loader />;
}
