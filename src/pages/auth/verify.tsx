import React, { useContext, useEffect, useRef, useState } from 'react';
import styles from 'styles/login.module.scss';
import { ExclamationTriangleFill, Person, Key } from 'react-bootstrap-icons';
import toPersianNum from 'utils/to-persian-num';
import { useForm } from 'react-hook-form';
import apiVerify from 'modules/auth/api/api-verify';
import { Button, Form } from 'react-bootstrap';
import { useRouter } from 'next/router';
import { cookieNames, sessionStorageVars } from 'utils/storage-vars';
import { toast } from 'react-toastify';
import Head from 'next/head';
import { AuthContext } from 'contexts/auth-context';
import apiUser from 'modules/profile/api/api-user';
import toEnglishNum from 'utils/to-english-num';
import parseCookies from 'utils/parse-cookies';
import { AxiosRequestConfig } from 'axios';
import schemas from 'utils/schemas';
import Link from 'next/link';

type TInputs = {
    secret: string;
    code: string;
    fullName: string;
    terms: boolean;
};

export async function getServerSideProps({ req }) {
    const cookies = parseCookies(req);
    const config: AxiosRequestConfig = {};
    if (cookies && cookies[cookieNames.apiToken]) {
        config.headers = {
            Authorization: `Bearer ${cookies[cookieNames.apiToken]}`,
        };
        const userResponse = await apiUser(true, config);
        if (userResponse) {
            return {
                redirect: {
                    destination: '/',
                    permanent: false,
                },
            };
        }
    }
    return {
        props: {},
    };
}

export default function Verify() {
    const inputRef = useRef(null);
    const route = useRouter();
    const [submitting, setSubmitting] = useState<boolean>(false);
    const [userMobile, setUserMobile] = useState('');
    const [askFullName, setAskFullName] = useState('');
    const [secret, setSecret] = useState('');
    const [errorMessage, setErrorMessage] = useState({
        status: false,
        text: '',
    });
    const { register, handleSubmit, errors } = useForm<TInputs>();

    const authContext = useContext(AuthContext);

    useEffect(function () {
        if (!sessionStorage[sessionStorageVars.user_mobile] || !sessionStorage[sessionStorageVars.secret]) {
            route.push('/auth/login-nopass');
            return;
        }
        setUserMobile(sessionStorage[sessionStorageVars.user_mobile]);
        setAskFullName(sessionStorage[sessionStorageVars.ask_fullname]);
        setSecret(sessionStorage[sessionStorageVars.secret]);
        inputRef.current.focus();
    }, []);

    async function onSubmit(data: { secret: string; fullName?: string; code: string }) {
        setSubmitting(true);
        setErrorMessage({
            status: false,
            text: '',
        });

        data.code = toEnglishNum(data.code);
        const response = await apiVerify(data.secret, data.fullName, data.code);
        if (response.status === 'error') {
            setSubmitting(false);
            setErrorMessage({
                status: true,
                text: response.message,
            });
        } else if (response.status === 'data') {
            toast.success(response.message);
            let user;
            for (let i = 0; i < 3; i++) {
                user = await apiUser();
                if (user) {
                    break;
                }
            }
            if (user) {
                toast.success('در حال انتقال به صفحه اصلی ...');
                sessionStorage.removeItem(sessionStorageVars.user_mobile);
                sessionStorage.removeItem(sessionStorageVars.ask_fullname);
                sessionStorage.removeItem(sessionStorageVars.secret);
                authContext.changeIsLoggedIn(user);
                setTimeout(() => {
                    if (route.query.return)
                        route.push(route.query.return as string).catch((e) => {
                            toast.warning(e);
                            setSubmitting(false);
                        });
                    else
                        route.push('/').catch((e) => {
                            toast.warning(e);
                            setSubmitting(false);
                        });
                }, 100);
            } else {
                toast.error('خطایی در دریافت اطلاعات کاربری شما پس از ورود رخ داد. لطفا دوباره تلاش کنید', {
                    autoClose: 7500,
                });
            }
        }
    }

    return (
        <>
            <Head>
                <meta name="robots" content="index, follow" />
                <title>تایید کد دریافتی - آسان سمینار</title>
                <meta
                    name="description"
                    content="ورود و ثبت نام در آسان سمینار | هنگام برگزاری وبینار و کلاس آنلاین کنار شما هستیم"
                />
                <link rel="canonical" href="https://asanseminar.ir/auth/login-nopass" />
                <meta property="og:url" content="https://asanseminar.ir" />
                <meta property="og:title" content="تایید کد دریافتی - آسان سمینار" />
                <meta
                    property="og:description"
                    content="ورود و ثبت نام در آسان سمینار | هنگام برگزاری وبینار و کلاس آنلاین کنار شما هستیم"
                />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="تایید کد دریافتی - آسان سمینار" />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
                {/*<script*/}
                {/*    type="application/ld+json"*/}
                {/*    dangerouslySetInnerHTML={{*/}
                {/*        __html: schemas.authSchema,*/}
                {/*    }}*/}
                {/*/>*/}
            </Head>
            <Form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
                <h1 className={styles.title}>ورود به آسان سمینار</h1>
                <div className={styles.phone}>{toPersianNum(userMobile)}</div>
                <Form.Group controlId="formCode">
                    <div className={styles['form-control-container']}>
                        <Form.Control type="hidden" name={'secret'} value={secret} ref={register()} />
                        <Form.Control
                            type="text"
                            placeholder="کد دریافتی"
                            className={styles['form-control']}
                            name={'code'}
                            ref={(e) => {
                                register(e, {
                                    required: 'کد دریافتی را وارد کنید!',
                                });
                                inputRef.current = e;
                            }}
                        />
                        <Key className={styles['form-control-icon']} />
                    </div>
                    {errors.code && <div className={styles.error}>{errors.code.message}</div>}
                </Form.Group>
                {askFullName === 'true' && (
                    <>
                        <Form.Group controlId="formFullName">
                            <div className={styles['form-control-container']}>
                                <Form.Control
                                    type="text"
                                    placeholder="نام و نام خانوادگی"
                                    className={styles['form-control']}
                                    name={'fullName'}
                                    ref={register({
                                        required: 'نام و نام خانوادگی را وارد کنید!',
                                    })}
                                />
                                <Person className={styles['form-control-icon']} />
                            </div>
                            {errors.fullName && <div className={styles.error}>{errors.fullName.message}</div>}
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicCheckbox">
                            <div className={styles['form-control-container']}>
                                <Form.Check type="checkbox" className={styles['form-check']}>
                                    <Form.Check.Input
                                        type="checkbox"
                                        name="terms"
                                        ref={register({ required: 'شرایط و قوانین را مطالعه کنید.' })}
                                        style={{ cursor: 'pointer' }}
                                        className={styles['checkbox-input']}
                                    />
                                    <Form.Check.Label className={styles.terms}>
                                        <Link href={'/terms'}>
                                            <a title="شرایط و قوانین">شرایط و قوانین </a>
                                        </Link>
                                        <span>را خوانده ام و آن را می پذیرم.</span>
                                    </Form.Check.Label>
                                </Form.Check>
                            </div>
                            {errors.terms && <div className={styles.error}>{errors.terms.message}</div>}
                        </Form.Group>
                    </>
                )}
                <div className={styles['form-control-container']}>
                    <Button variant="success" type="submit" className={styles['login-btn']} disabled={submitting}>
                        {submitting ? 'در حال ارسال...' : 'ورود'}
                    </Button>
                </div>
            </Form>
            {errorMessage.status ? (
                <div className={styles['error-message']}>
                    <ExclamationTriangleFill />
                    <span>{errorMessage.text}</span>
                </div>
            ) : null}
        </>
    );
}
