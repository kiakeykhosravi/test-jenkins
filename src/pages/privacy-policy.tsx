import React from 'react';
import { Container } from 'react-bootstrap';
import styles from 'styles/privacy-terms.module.scss';
import schemas from 'utils/schemas';
import Head from 'next/head';

export default function PrivacyPolicy() {
    return (
        <>
            <Head>
                <meta name="robots" content="index, follow" />
                <title>قوانین حفظ حریم خصوصی</title>
                <meta name="description" content="حفظ حریم شخصی کاربران، قوانین و شرایط استفاده از آسان سمینار" />
                <meta property="og:url" content="https://asanseminar.ir" />
                <meta property="og:title" content="قوانین حفظ حریم خصوصی" />
                <meta
                    property="og:description"
                    content="حفظ حریم شخصی کاربران، قوانین و شرایط استفاده از آسان سمینار"
                />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content="قوانین حفظ حریم خصوصی" />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={{
                        __html: schemas.privacySchema,
                    }}
                />
            </Head>
            <div className={styles.main}>
                <Container>
                    <div className={styles.card}>
                        <h1 className={styles.title}>قوانین حفظ حریم خصوصی</h1>
                        <p className={styles.paragraph}>
                            حفظ حریم شخصی کاربران برای آسان سمینار اهمیت بسیاری دارد. در همین راستا از شما تقاضا داریم
                            برای مطالعه مطلب زیر چند دقیقه وقت بگذارید. همه موارد زیر برای این وبسایت و وبسایت‌ های
                            وابسته به آن در مورد محصولات و خدماتشان صادق است.
                        </p>
                        <p className={styles.paragraph}>
                            در آسان سمینار، ما در تلاش برای ارائه بهترین کیفیت محصولات و خدمات به مشتریان خود هستیم. ما
                            به حریم خصوصی شما اهمیت می‌دهیم، هنگامی که به وبسایت ما دسترسی پیدا می کنید، فرض می‌کنیم که
                            شما سیاست حفظ حریم خصوصی سایت ما را خوانده و پذیرفته‌اید. توجه داشته باشید که آسان سمینار در
                            برابر هر گونه سوء استفاده از اطلاعاتی که شما به هر وبسایت یا برنامه ثالث ارائه می‌دهید،
                            مسئول نیست.
                        </p>
                        <ul>
                            <li>
                                <h4>جمع‌آوری اطلاعات</h4>
                                <p className={styles['inner-paragraph']}>
                                    اطلاعاتی که ما جمع‌آوری می‌کنیم شامل نام، نام خانوادگی و شماره تلفن همراه است. این
                                    اطلاعات به منظور ثبت سابقه و بهبود محصولات آتی ما جمع‌آوری می‌شوند.
                                </p>
                            </li>
                            <li>
                                <h4>امنیت اطلاعات</h4>
                                <p className={styles['inner-paragraph']}>
                                    آسان سمینار از زیرساخت های فندق برای رمزگذاری و حفاظت از داده ‌ها استفاده می‌کند.
                                    اطلاعات کاربران به عنوان اطلاعات محرمانه نزد آسان سمینار محافظت شده و دسترسی به آن
                                    توسط اشخاص ثالث ممنوع می باشد، مگر برابر قانون و تصمیم مقام ذیصلاح قضایی. بنابراین
                                    در موارد قانونی و هم‌چنین درخواست و صدور دستور مقتضی از سوی مراجع قضایی، آسان سمینار
                                    مکلف به ارائه اطلاعات کاربران به مراجع مذکور است. لازم به توضیح است به منظور حفظ
                                    امنیت اطلاعات کاربران و اصل محرمانگی داده ها، ارائه اطلاعات کاربری به خود کاربر و
                                    اشخاص ثالث به صورت تلفنی مقدور نیست.
                                </p>
                            </li>
                            <li>
                                <h4>کوکی‌ها</h4>
                                <p className={styles['inner-paragraph']}>
                                    کوکی‌ها مقدار محدودی از داده‌های ذخیره شده در رایانه کاربر هستند که به منظور ثبت
                                    برخی از عوامل خاص مانند تنظیمات کاربر، الگوی ترافیک، و غیره به کار می‌روند. ما برای
                                    امکان ارائه برخی از سرویس ها و همچنین بهبود تجربه کاربری، برخی اطلاعات را در کوکی ها
                                    ذخیره میکنیم. اطلاعات شخصی شما کاملا امن بوده و در کوکی ها ذخیره نمی شود.
                                </p>
                            </li>
                            <li>
                                <h4>تغییرات سیاست</h4>
                                <p className={styles['inner-paragraph']}>
                                    سیاست های ما به طور منظم در وبسایت به روز می‌شوند. در صورت داشتن هرگونه سوال، با ما
                                    تماس بگیرید.
                                </p>
                            </li>
                        </ul>
                    </div>
                </Container>
            </div>
        </>
    );
}
