import React, { useContext, useEffect, useState } from 'react';
import apiOrganizationsTop from 'modules/home/api/api-organizations-top';
import { IUserStory } from 'models/user-story';
import { IOrganizationCompact } from 'models/organization-compact';
import { IWebinarCompact } from 'models/webinar-compact';
import apiUserStories from 'modules/home/api/api-user-stories';
import apiHomepageCourses from 'modules/home/api/api-homepage-courses';
import apiLatestBlogs from 'modules/blog/api/api-latest-blogs';
import { ISlide } from 'models/slide';
import apiSliders from 'modules/home/api/api-sliders';
import Head from 'next/head';
import HomeOrganizationsSection from 'modules/home/components/organizations-section';
import HomeBlogSection from 'modules/home/components/blog-section';
import UserStorySection from 'modules/home/components/user-story-section';
import HomeOnelookSection from 'modules/home/components/onelook-section';
import StatisticSection from 'modules/home/components/statistic-section';
import HomePlansSection from 'modules/home/components/plans-section';
import CourseCardSection from 'modules/home/components/course-card-section';
import HomeBannerSection from 'modules/home/components/banner-section';
import apiHomepageMetadata from 'modules/home/api/api-homepage-metadata';
import apiGetRelatedOrganizations from 'modules/home/api/api-get-related-organizations';
import { AuthContext } from 'contexts/auth-context';
import schemas from 'utils/schemas';
import { IBlogCompact } from 'models/blog-compact';

export const getStaticProps = async () => {
    const slidesResult = await apiSliders();
    const organizationsResult = await apiOrganizationsTop();
    const userStoryResult = await apiUserStories();
    const coursesResult = await apiHomepageCourses();
    const blogsResult = await apiLatestBlogs();
    const metadataResult = await apiHomepageMetadata();

    const slides = slidesResult.status === 'data' ? slidesResult.data : [];
    const organizations = organizationsResult.status === 'data' ? organizationsResult.data : [];
    const userStories = userStoryResult.status === 'data' ? userStoryResult.data : [];
    const courses = coursesResult.status === 'data' ? coursesResult.data : [];
    const blogs = blogsResult.status === 'data' ? blogsResult.data : [];
    const metadata = metadataResult.status === 'data' ? metadataResult.data : null;

    return {
        props: {
            slides,
            organizations,
            userStories,
            courses,
            blogs,
            metadata,
        },
        revalidate: 60,
    };
};

export default function Index(props: {
    slides: ISlide[];
    organizations: IOrganizationCompact[];
    userStories: IUserStory[];
    courses: IWebinarCompact[];
    blogs: IBlogCompact[];
    metadata;
}) {
    const [relatedOrganizations, setRelatedOrganizations] = useState<IOrganizationCompact[]>([]);
    const authContext = useContext(AuthContext);

    useEffect(() => {
        if (authContext.user) {
            getRelatedOrganizations();
        }
    }, [authContext.user]);

    const getRelatedOrganizations = async () => {
        const result = await apiGetRelatedOrganizations(false);
        if (result.status === 'data') {
            setRelatedOrganizations(result.data.organizations);
        }
    };

    const statisticData = [
        {
            count: props.metadata ? props.metadata.users_count : 20000,
            icon: '/images/statistics-icon.png',
            title: 'تعداد مشتریان',
            duration: 2,
        },
        {
            count: props.metadata ? props.metadata.webinars_count : 1286,
            icon: '/images/statistics-icon.png',
            title: 'تعداد وبینارها',
            duration: 2,
        },
        {
            count: props.metadata ? props.metadata.meetings_count : 22593,
            icon: '/images/statistics-icon.png',
            title: 'تعداد جلسات',
            duration: 2,
        },
    ];

    const metaTags = {
        title: 'آسان سمینار - برگزاری وبینار | کلاس آنلاین',
        description:
            'برگزاری وبینار و پلتفرم آموزش آنلاین، ایجاد برند پیج اختصاصی و رسیدن به صفحه اول گوگل خود را به آسان سمینار بسپارید.',
    };

    return (
        <div style={{ paddingTop: '3rem' }}>
            <Head>
                <meta name="robots" content="index, follow" />
                <title>{metaTags.title}</title>
                <meta name="description" content={metaTags.description} />
                <link rel="canonical" href="https://www.asanseminar.ir" />
                <meta property="og:url" content="https://asanseminar.ir" />
                <meta property="og:title" content={metaTags.title} />
                <meta property="og:description" content={metaTags.description} />
                <meta property="og:type" content="website" />
                <meta property="og:image" content="/images/asanseminar_logo.png" />
                <meta property="og:image:type" content="image/png" />
                <meta property="og:image:alt" content="لوگو آسان سمینار" />
                <meta property="og:site_name" content="آسان سمینار" />
                <meta property="og:locale" content="fa_IR" />
                <meta name="twitter:card" content="summary_large_image" />
                <meta name="twitter:title" content={metaTags.title} />
                <meta name="twitter:image:alt" content="لوگو آسان سمینار" />
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={{
                        __html: schemas.contactusSchema,
                    }}
                />
            </Head>
            <HomeBannerSection slides={props.slides} organizations={relatedOrganizations} />
            <CourseCardSection data={props.courses} classes="text-white" />
            <HomePlansSection />
            <StatisticSection data={statisticData} />
            <HomeOnelookSection />
            <UserStorySection data={props.userStories} />
            <HomeBlogSection data={props.blogs} />
            <HomeOrganizationsSection data={props.organizations} />
        </div>
    );
}
