import React from 'react';
import Link from 'next/link';
import { Image } from 'react-bootstrap';
import styles from 'styles/error.module.scss';
import Head from 'next/head';

export default function Custom500() {
    return (
        <div className={styles.main}>
            <Head>
                <title>خطای داخلی سرور - آسان سمینار</title>
            </Head>
            <div className={styles.image}>
                <Image src="/images/error-img.png" alt="learn" className={styles.logo} />
            </div>
            <div className={styles.error}>
                <div className={styles['err-code']}>
                    <h2>خطای ۵۰۰</h2>
                </div>
                <div className={styles['err-txt']}>
                    <h4>خطای داخلی سرور</h4>
                </div>
            </div>
            <Image src="/images/asanseminar_full.png" alt="learn" className={styles['err-img']} />
            <div className={styles.return}>
                <Link href={'/'}>
                    <a title={'بازگشت به آسان سمینار'} className="btn">
                        بازگشت به آسان سمینار
                    </a>
                </Link>
            </div>
        </div>
    );
}
