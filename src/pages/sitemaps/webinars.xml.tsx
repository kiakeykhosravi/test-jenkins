import apiSitemapWebinars from 'modules/sitemap/api/api-sitemap-webinars';
import sitemapServerSideProps from 'utils/sitemap-server-side-props';

export async function getServerSideProps({ res }) {
    return await sitemapServerSideProps(apiSitemapWebinars, res);
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
export default function WebinarsSitemap() {}
