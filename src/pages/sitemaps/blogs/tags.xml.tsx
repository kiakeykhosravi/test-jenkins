import sitemapServerSideProps from 'utils/sitemap-server-side-props';
import apiSitemapBlogTags from 'modules/sitemap/api/api-sitemap-blog-tags';

export async function getServerSideProps({ res }) {
    return await sitemapServerSideProps(apiSitemapBlogTags, res);
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
export default function BlogSitemap() {}
