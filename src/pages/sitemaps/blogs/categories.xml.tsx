import sitemapServerSideProps from 'utils/sitemap-server-side-props';
import apiSitemapBlogCategories from 'modules/sitemap/api/api-sitemap-blog-categories';

export async function getServerSideProps({ res }) {
    return await sitemapServerSideProps(apiSitemapBlogCategories, res);
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
export default function BlogSitemap() {}
