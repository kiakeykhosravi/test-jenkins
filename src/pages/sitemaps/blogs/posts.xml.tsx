import sitemapServerSideProps from 'utils/sitemap-server-side-props';
import apiSitemapBlogPosts from 'modules/sitemap/api/api-sitemap-blog-posts';

export async function getServerSideProps({ res }) {
    return await sitemapServerSideProps(apiSitemapBlogPosts, res);
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
export default function BlogSitemap() {}
