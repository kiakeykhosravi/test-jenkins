import sitemapServerSideProps from 'utils/sitemap-server-side-props';
import apiSitemapOrganizationsList from 'modules/sitemap/api/api-sitemap-organizations-list';

export async function getServerSideProps({ res }) {
    return await sitemapServerSideProps(apiSitemapOrganizationsList, res);
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
export default function OrganizationsSitemap() {}
