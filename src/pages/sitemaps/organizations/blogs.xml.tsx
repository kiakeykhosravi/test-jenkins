import sitemapServerSideProps from 'utils/sitemap-server-side-props';
import apiSitemapOrganizationsBlogs from 'modules/sitemap/api/api-sitemap-organizations-blogs';

export async function getServerSideProps({ res }) {
    return await sitemapServerSideProps(apiSitemapOrganizationsBlogs, res);
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
export default function OrganizationsSitemap() {}
