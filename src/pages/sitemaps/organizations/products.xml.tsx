import sitemapServerSideProps from 'utils/sitemap-server-side-props';
import apiSitemapOrganizationsProducts from 'modules/sitemap/api/api-sitemap-organizations-products';

export async function getServerSideProps({ res }) {
    return await sitemapServerSideProps(apiSitemapOrganizationsProducts, res);
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
export default function OrganizationsSitemap() {}
