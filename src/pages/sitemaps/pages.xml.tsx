import apiSitemapPages from 'modules/sitemap/api/api-sitemap-pages';
import sitemapServerSideProps from 'utils/sitemap-server-side-props';

export async function getServerSideProps({ res }) {
    return await sitemapServerSideProps(apiSitemapPages, res);
}

// eslint-disable-next-line @typescript-eslint/no-empty-function
export default function PagesSitemap() {}
