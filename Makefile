build:
	docker-compose --env-file .docker-compose.env up -d --build

up:
	docker-compose --env-file .docker-compose.env up -d

logs:
	docker-compose logs -f

down:
	docker-compose down
