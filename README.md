# Readme

## Environment variables

This variables inlined into JavaScript sent to the browser because of the NEXT_PUBLIC_ prefix:
```
NEXT_PUBLIC_APP_URL -> Asanseminar host
NEXT_PUBLIC_BACKEND_BASE_ADDRESS -> Asanseminar backend/admin host
NEXT_PUBLIC_FRONTEND_API_ADDRESS -> Browser api calls host
```
This variables only available in the Node.js environment, meaning they won't be exposed to the browser:
```
NEXT_PRIVATE_BACKEND_API_ADDRESS -> Node server api calls address
```

## Run with docker on local environment

### Create .docker-compose.env file

```
cp docker-compose.env .docker-compose.env
```

#### Set environment variables

```
nano .docker-compose.env
```
```
BUILD_BACKEND_API_ADDRESS='https://example.com/api'
START_BACKEND_API_ADDRESS='https://example.com/api'
```
> ATTENTION! in production environment you must use NEXT_PRIVATE_BACKEND_API_ADDRESS variable for build args and deployment env variables. or you can create your own docker compose file in production!

### Run containers with docker-compose or makefile

#### Create and start containers
makefile:
```
make up
```
docker-compose:
```
docker-compose --env-file .docker-compose.env up -d
```

#### Stop and remove resources
makefile:
```
make down
```
docker-compose:
```
docker-compose down
```

#### Create and start containers and rebuild services
makefile:
```
make build
```
docker-compose:
```
docker-compose --env-file .docker-compose.env up -d --build
```

#### View output from containers
makefile:
```
make logs
```
docker-compose:
```
docker-compose logs -f
```